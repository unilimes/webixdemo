TEMPLATE = app

CONFIG(debug, debug|release) {
    BUILD = debug
}else {
    BUILD = release
    DEFINES += QT_NO_DEBUG_OUTPUT
}

QT += core gui qml quick webenginewidgets webengine
CONFIG += c++11

SOURCES += $$files(src/*.cpp, true)

HEADERS += $$files(incl/*.h, true)

# Static lib configuration
INCLUDEPATH += lib
LIBS += lib/utils/$$BUILD/utils.lib
PRE_TARGETDEPS += $$LIBS

RESOURCES += main.qrc \
             gui/runtime/runtime.qrc

# List gui files so it's visible in Qt Creator.
#OTHER_FILES += $$files(gui/*, true)

# Will build the final executable in the main project directory.
TARGET = W7designer
