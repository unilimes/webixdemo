WillowLynx7 Designer
====================

### Requirement

- Microsoft Visual Studio 2015 or later
- Qt Creator 4.4.1 with Qt 5.10.0 or later
- Node.js 8.9.1 or later

### How to develop in Web Browser

- Enter gui folder: `cd gui`
- Install modules: `npm install`
- Start web app: `npm start`
- In web browser, open: `localhost:8080`

### How to develop in Qt Creator

- In gui folder, run: `npm install` and `npm run build`
- Open W7designer.pro in Qt Creator
- Specify the build setting
- Build & Run

### License

Copyright © 2017-2018 Willowglen MSC Berhad