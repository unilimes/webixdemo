#include "incl/application.h"

/* Init class static member */
Application* Application::single = Q_NULLPTR;
QQmlApplicationEngine* Application::engine = Q_NULLPTR;
QQmlWebChannel* Application::qmlWebChannel = Q_NULLPTR;

Application::Application()
{
    QtWebEngine::initialize();

    engine = new QQmlApplicationEngine;

    // Load QML component in the engine
    engine->load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    QObject *rootObject = engine->rootObjects().first();
    QObject *qmlObject = rootObject->findChild<QObject*>("webChannelObject");
    qmlWebChannel = qobject_cast<QQmlWebChannel *>(qmlObject);
}

Application::~Application()
{
    delete engine;
    delete qmlWebChannel;
}

Application* Application::getInstance()
{
    static QMutex mutex;
    QMutexLocker locker(&mutex);

    if (single == Q_NULLPTR) {
        single = new Application();
    }

    return single;
}

void Application::registerObject(QString objName , QObject *obj)
{
    qmlWebChannel->registerObject(objName, obj);
}
