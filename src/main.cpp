#include <QApplication>
#include <QQmlApplicationEngine>

#include "incl/application.h"
#include "utils/incl/utils.h"

int main(int argc, char *argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    app.setApplicationName("Designer");
    app.setWindowIcon(QIcon(":/images/WillowLynx_Logo_128.png"));

    Utils ut;
    ut.logMessage("Starting module");

    Application *application;
    application = Application::getInstance();

    /* Register native objects for JS usage */
    application->registerObject(QString("Utils"), &ut);

    return app.exec();
}
