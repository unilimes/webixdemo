/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/runtime/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export plugins */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__JetApp__ = __webpack_require__(20);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__JetApp__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__JetView__ = __webpack_require__(1);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__JetView__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__routers_HashRouter__ = __webpack_require__(15);
/* unused harmony reexport HashRouter */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__routers_StoreRouter__ = __webpack_require__(33);
/* unused harmony reexport StoreRouter */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__routers_UrlRouter__ = __webpack_require__(34);
/* unused harmony reexport UrlRouter */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__routers_EmptyRouter__ = __webpack_require__(35);
/* unused harmony reexport EmptyRouter */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__plugins_Guard__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__plugins_Locale__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__plugins_Menu__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__plugins_Status__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__plugins_Theme__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__plugins_User__ = __webpack_require__(43);












var plugins = {
    UnloadGuard: __WEBPACK_IMPORTED_MODULE_6__plugins_Guard__["a" /* UnloadGuard */], Locale: __WEBPACK_IMPORTED_MODULE_7__plugins_Locale__["a" /* Locale */], Menu: __WEBPACK_IMPORTED_MODULE_8__plugins_Menu__["a" /* Menu */], Theme: __WEBPACK_IMPORTED_MODULE_10__plugins_Theme__["a" /* Theme */], User: __WEBPACK_IMPORTED_MODULE_11__plugins_User__["a" /* User */], Status: __WEBPACK_IMPORTED_MODULE_9__plugins_Status__["a" /* Status */]
};
if (!window.Promise) {
    window.Promise = webix.promise;
}

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JetView; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__JetBase__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers__ = __webpack_require__(14);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var JetView = function (_JetBase) {
    _inherits(JetView, _JetBase);

    function JetView(app, name) {
        _classCallCheck(this, JetView);

        var _this = _possibleConstructorReturn(this, _JetBase.call(this));

        _this.app = app;
        _this._name = name;
        _this._children = [];
        return _this;
    }

    JetView.prototype.ui = function ui(_ui, name) {
        if (typeof _ui === "function") {
            var jetview = new _ui(this.app, name);
            this._children.push(jetview);
            jetview.render();
            return jetview;
        } else {
            var view = this.app.webix.ui(_ui);
            this._children.push(view);
            return view;
        }
    };

    JetView.prototype.show = function show(path, config) {
        var _this2 = this;

        // var target = this.subs[0];
        // if (config && config.target)
        // 	target = this._subs[path];
        if (typeof path === "string") {
            // root path
            if (path.substr(0, 1) === "/") {
                return this.app.show(path);
            }
            // local path, do nothing
            if (path.indexOf("./") === 0) {
                path = path.substr(2);
            }
            // parent path, call parent view
            if (path.indexOf("../") === 0) {
                var parent = this.getParentView();
                if (parent) {
                    parent.show("./" + path.substr(3));
                } else {
                    this.app.show("/" + path.substr(3));
                }
                return;
            }
            var sub = this.getSubView();
            if (!sub) {
                return this.app.show("/" + path);
            }
            if (sub.parent !== this) {
                return sub.parent.show(path, config);
            }
            var newChunk = Object(__WEBPACK_IMPORTED_MODULE_1__helpers__["a" /* parse */])(path);
            var url = null;
            var currentUrl = this.app.getRouter().get();
            if (this._index) {
                url = Object(__WEBPACK_IMPORTED_MODULE_1__helpers__["a" /* parse */])(currentUrl).slice(0, this._index).concat(newChunk);
                for (var i = 0; i < url.length; i++) {
                    url[i].index = i + 1;
                }
                var urlstr = Object(__WEBPACK_IMPORTED_MODULE_1__helpers__["b" /* url2str */])(url);
                this.app.canNavigate(urlstr, this).then(function (redirect) {
                    if (urlstr !== redirect) {
                        // url was blocked and redirected
                        _this2.app.show(redirect);
                    } else {
                        _this2._finishShow(sub.subview, url, redirect);
                    }
                }).catch(function () {
                    return false;
                });
            } else {
                this._finishShow(sub.subview, newChunk, "");
            }
            // route to page
            // var parsed = parse(path);
            // var path this.path.slice(0, index).concat(parsed);
            // if (config)
            // 	webix.extend(scope.path[index].params, config, true);
        } else {}
            // set parameters
            // webix.extend(scope.path[index].params, path, true);

            // scope.show(url_string(scope.path), -1);
    };

    JetView.prototype.init = function init(_$view, _$url) {
        // stub
    };

    JetView.prototype.ready = function ready(_$view, _$url) {
        // stub
    };

    JetView.prototype.config = function config() {
        this.app.webix.message("View:Config is not implemented");
    };

    JetView.prototype.urlChange = function urlChange(_$view, _$url) {
        // stub
    };

    JetView.prototype.destroy = function destroy() {
        // stub
    };

    JetView.prototype.destructor = function destructor() {
        this.destroy();
        // destroy child views
        var uis = this._children;
        for (var i = uis.length - 1; i >= 0; i--) {
            if (uis[i] && uis[i].destructor) {
                uis[i].destructor();
            }
        }
        // reset vars for better GC processing
        this.app = this._children = null;
        // destroy actual UI
        this._root.destructor();
        _JetBase.prototype.destructor.call(this);
    };

    JetView.prototype.use = function use(plugin, config) {
        plugin(this.app, this, config);
    };

    JetView.prototype._render = function _render(url) {
        var _this3 = this;

        var response = void 0;
        // using wrapper object, so ui can be changed from app:render event
        var result = { ui: {} };
        this.app.copyConfig(this.config(), result.ui, this._subs);
        this.app.callEvent("app:render", [this, url, result]);
        result.ui.$scope = this;
        try {
            this._root = this.app.webix.ui(result.ui, this._container);
            if (this._root.getParentView()) {
                this._container = this._root;
            }
            this._init(this._root, url);
            response = this._urlChange(url).then(function () {
                return _this3.ready(_this3._root, url);
            });
        } catch (e) {
            response = Promise.reject(e);
        }
        return response.catch(function (err) {
            return _this3._initError(_this3, err);
        });
    };

    JetView.prototype._init = function _init(view, url) {
        return this.init(view, url);
    };

    JetView.prototype._urlChange = function _urlChange(url) {
        var _this4 = this;

        var waits = [];
        for (var key in this._subs) {
            var frame = this._subs[key];
            if (frame.url) {
                // we have fixed subview url
                if (typeof frame.url === "string") {
                    var parsed = Object(__WEBPACK_IMPORTED_MODULE_1__helpers__["a" /* parse */])(frame.url);
                    waits.push(this._createSubView(frame, parsed));
                } else {
                    var view = frame.view;
                    if (typeof frame.url === "function" && !(view instanceof frame.url)) {
                        view = new frame.url(this.app, "");
                    }
                    if (!view) {
                        view = frame.url;
                    }
                    waits.push(this._renderSubView(frame, view, url));
                }
            } else if (key === "default" && url && url.length > 1) {
                // we have an url and subview for it
                var suburl = url.slice(1);
                waits.push(this._createSubView(frame, suburl));
            }
        }
        return Promise.all(waits).then(function () {
            _this4.urlChange(_this4._root, url);
        });
    };

    JetView.prototype._initError = function _initError(view, err) {
        this.app.error("app:error:initview", [err, view]);
        return true;
    };

    JetView.prototype._createSubView = function _createSubView(sub, suburl) {
        var _this5 = this;

        return this.app.createFromURL(suburl, sub.view).then(function (view) {
            return _this5._renderSubView(sub, view, suburl);
        });
    };

    JetView.prototype._renderSubView = function _renderSubView(sub, view, suburl) {
        var cell = this.app.webix.$$(sub.id);
        return view.render(cell, suburl, this).then(function (ui) {
            // destroy old view
            if (sub.view && sub.view !== view) {
                sub.view.destructor();
            }
            // save info about a new view
            sub.view = view;
            sub.id = ui.config.id;
            return ui;
        });
    };

    JetView.prototype._finishShow = function _finishShow(sub, url, path) {
        if (this._index) {
            this._urlChange(url.slice(this._index - 1));
            this.app.getRouter().set(path, { silent: true });
            this.app.callEvent("app:route", [url]);
        } else {
            this._createSubView(sub, url);
        }
    };

    return JetView;
}(__WEBPACK_IMPORTED_MODULE_0__JetBase__["a" /* JetBase */]);



/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "native", function() { return native; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__styles_app_css__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__styles_app_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__styles_app_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_utils_qwebchannel__ = __webpack_require__(44);
/* Main Entry App JavaScript */





webix.ready(function () {
    var app = new __WEBPACK_IMPORTED_MODULE_1_webix_jet__["a" /* JetApp */]({
        id: "gui-app",
        version: "1.0.0",
        start: "/layout"
    });

    app.render().then(function () {
        return attachEventsToUI();
    });

    app.attachEvent("app:error:resolve", function () {
        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        window.console.error(args);
    });
});

var native = {};

var nativePromise = new Promise(function (resolve) {
    new __WEBPACK_IMPORTED_MODULE_2_utils_qwebchannel__["a" /* default */](qt.webChannelTransport, function (channel) {
        var info = "";
        native = channel.objects;

        native.Utils.version(function (version) {
            info += version;
            native.Utils.screenInfo(function (screenInfo) {
                info += "\n\nDesktop info " + JSON.stringify(screenInfo, null, 4);
                resolve(info);
            });
        });
    });
});

nativePromise.then(function (info) {
    native.Utils.showMessage("Designer", "Loaded Webchannel" + info, false);
    native.Utils.logMessage("Loaded WebChannel" + info);
}, function (info) {
    native.Utils.showMessage("Designer", "Failed to load Webchannel", false);
    native.Utils.logMessage("Fail to load WebChannel");
});

function attachEventsToUI() {}



/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_views_menu_dropDownMenus__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_views_menu_iconMenu__ = __webpack_require__(5);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Base Menu JavaScript */




var MenuView = function (_JetView) {
    _inherits(MenuView, _JetView);

    function MenuView() {
        _classCallCheck(this, MenuView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    MenuView.prototype.config = function config() {
        var menu = {
            id: "menu",
            rows: [__WEBPACK_IMPORTED_MODULE_1_views_menu_dropDownMenus__["default"], __WEBPACK_IMPORTED_MODULE_2_views_menu_iconMenu__["default"]]
        };

        return menu;
    };

    return MenuView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (MenuView);

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_models_menuData__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app__ = __webpack_require__(2);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Drop Down Menus JavaScript */




var DropDownMenuView = function (_JetView) {
    _inherits(DropDownMenuView, _JetView);

    function DropDownMenuView() {
        _classCallCheck(this, DropDownMenuView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    DropDownMenuView.prototype.config = function config() {
        var dropDownMenu = {
            id: "dropDownMenuContainer",
            cols: [{ width: 10 }, {
                id: "dropDownMenu",
                view: "menu",
                borderless: true,
                submenuConfig: {
                    width: 200
                },
                data: [{
                    id: "fileMenu",
                    value: "File",
                    submenu: __WEBPACK_IMPORTED_MODULE_1_models_menuData__["c" /* fileMenu */]
                }, {
                    id: "editMenu",
                    value: "Edit",
                    submenu: __WEBPACK_IMPORTED_MODULE_1_models_menuData__["b" /* editMenu */]
                }, {
                    id: "viewMenu",
                    value: "View",
                    submenu: __WEBPACK_IMPORTED_MODULE_1_models_menuData__["f" /* viewMenu */]
                }, {
                    id: "drawMenu",
                    value: "Draw",
                    submenu: __WEBPACK_IMPORTED_MODULE_1_models_menuData__["a" /* drawMenu */]
                }, {
                    id: "modifyMenu",
                    value: "Modify",
                    submenu: __WEBPACK_IMPORTED_MODULE_1_models_menuData__["e" /* modifyMenu */]
                }, {
                    id: "helpMenu",
                    value: "Help",
                    submenu: __WEBPACK_IMPORTED_MODULE_1_models_menuData__["d" /* helpMenu */]
                }],
                on: {
                    onMenuItemClick: function onMenuItemClick(id) {
                        webix.message("Click: " + id + this.getMenuItem(id).value);
                        console.log(id + this.getMenuItem(id).value);
                        if (id === "File->Exit") {
                            __WEBPACK_IMPORTED_MODULE_2_app__["native"].Utils.showMessage("Designer", "This will exit application", true);
                            __WEBPACK_IMPORTED_MODULE_2_app__["native"].Utils.exitApp();
                        } else if (id == "File->Open") {
                            __WEBPACK_IMPORTED_MODULE_2_app__["native"].Utils.showMessage("Designer", "Start Test Sum", false);
                            /* Test to call promise for native function call.
                              Still not working */
                            var testCallPromise = new Promise(function (resolve) {
                                __WEBPACK_IMPORTED_MODULE_2_app__["native"].Utils.testCall3(10000, function (sum) {
                                    resolve(sum);
                                });
                            });

                            testCallPromise.then(function (sum) {
                                __WEBPACK_IMPORTED_MODULE_2_app__["native"].Utils.showMessage("Designer", "Test Sum: " + sum, false);
                            }, function (ver) {
                                __WEBPACK_IMPORTED_MODULE_2_app__["native"].Utils.showMessage("Designer", "Failed to Test Sum", false);
                            });
                            /* Direct callback function */
                            /* Note the callback function call is blocking.
                               Thus if the function take long time, then the whole UI will block for long time */
                            //                                native.Utils.testCall3(10000, function(sum){
                            //                                    native.Utils.showMessage("Designer", "Test Sum Result:" + sum, false);
                            //                                });
                        }
                    }
                },
                type: {
                    subsign: true
                }
            }, {}]
        };

        return dropDownMenu;
    };

    return DropDownMenuView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (DropDownMenuView);

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app__ = __webpack_require__(2);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Icon Menu JavaScript */


/* NOTE: Buttons in here are fixed and are not dependant on the active module */

var IconMenuView = function (_JetView) {
    _inherits(IconMenuView, _JetView);

    function IconMenuView() {
        _classCallCheck(this, IconMenuView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    IconMenuView.prototype.config = function config() {
        var iconMenu = {
            id: "iconMenuContainer",
            minHeight: 40,
            maxHeight: 40,
            type: "clean",
            rows: [{
                id: "iconMenu",
                view: "toolbar",
                height: 40,
                type: "clean",
                elements: [{ width: 10 }, {
                    id: "openButton",
                    view: "button",
                    type: "icon",
                    icon: "folder-open",
                    width: 32,
                    css: "app_button",
                    click: function click(id) {
                        __WEBPACK_IMPORTED_MODULE_1_app__["native"].Utils.testcall2();
                        __WEBPACK_IMPORTED_MODULE_1_app__["native"].Utils.showMessage("Call Native", "Open clicked: " + id, false);
                    }
                }, {
                    id: "saveButton",
                    view: "button",
                    type: "icon",
                    icon: "floppy",
                    width: 32,
                    css: "app_button",
                    click: function click(id) {
                        webix.message(id);
                    }
                }, {
                    id: "importButton",
                    view: "button",
                    type: "icon",
                    icon: "file-import",
                    width: 32,
                    css: "app_button",
                    click: function click(id) {
                        webix.message(id);
                    }
                }, {
                    id: "exportButton",
                    view: "button",
                    type: "icon",
                    icon: "file-export",
                    width: 32,
                    css: "app_button",
                    click: function click(id) {
                        webix.message(id);
                    }
                }, {}, { width: 10 }]
            }]
        };

        return iconMenu;
    };

    return IconMenuView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (IconMenuView);

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_views_sidebar_projectExplorer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_views_sidebar_propertyList__ = __webpack_require__(8);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Base Sidebar JavaScript */




var SidebarView = function (_JetView) {
    _inherits(SidebarView, _JetView);

    function SidebarView() {
        _classCallCheck(this, SidebarView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    SidebarView.prototype.config = function config() {
        var sidebar = {
            id: "sidebarContainer",
            minWidth: 50,
            width: 300,
            rows: [__WEBPACK_IMPORTED_MODULE_1_views_sidebar_projectExplorer__["default"], {
                id: "sidebarHorResizer",
                view: "resizer"
            }, __WEBPACK_IMPORTED_MODULE_2_views_sidebar_propertyList__["default"]]
        };

        return sidebar;
    };

    return SidebarView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (SidebarView);

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_models_projectStore__ = __webpack_require__(27);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Project Explorer JavaScript */



var ProjectExplorerView = function (_JetView) {
    _inherits(ProjectExplorerView, _JetView);

    function ProjectExplorerView() {
        _classCallCheck(this, ProjectExplorerView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    ProjectExplorerView.prototype.config = function config() {
        var projectExplorer = {
            id: "projectExplorerContainer",
            rows: [{
                view: "template",
                template: "Project Explorer",
                type: "header",
                height: 40,
                borderless: true
            }, {
                cols: [{ width: 5 }, {
                    id: "projectExplorerSearch",
                    view: "text",
                    value: "Search",
                    type: "clean",
                    height: 30,
                    on: {
                        onFocus: function onFocus(id) {
                            if (id.getValue() == "Search") {
                                id.setValue("");
                            }
                        },
                        onBlur: function onBlur(id) {
                            if (id.getValue() == "") {
                                id.setValue("Search");
                            }
                        }
                    }
                }, { width: 5 }]
            }, { height: 10 }, {
                view: "scrollview",
                id: "projectExplorerScrollview",
                scroll: "y",
                body: {
                    rows: [{
                        id: "projectExplorer",
                        view: "tree",
                        type: "lineTree",
                        height: 2000,
                        drag: false,
                        data: [],
                        ready: function ready() {
                            $$("projectExplorer").data.sync(__WEBPACK_IMPORTED_MODULE_1_models_projectStore__["a" /* projectStore */]);
                        },
                        on: {
                            onItemDblClick: function onItemDblClick() {
                                if ($$("projectExplorer").getItem(arguments.length <= 0 ? undefined : arguments[0]).isFolder != true) {
                                    var state = $$("projectStore").getItem(arguments.length <= 0 ? undefined : arguments[0]);
                                    if (state.opened == false) {
                                        $$("projectStore").updateItem(arguments.length <= 0 ? undefined : arguments[0], { opened: true, hidden: false });
                                    }
                                }
                            }
                        }
                    }]
                }
            }]
        };

        return projectExplorer;
    };

    return ProjectExplorerView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (ProjectExplorerView);

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_models_propertyStore__ = __webpack_require__(28);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Property List JavaScript */



var PropertyListView = function (_JetView) {
    _inherits(PropertyListView, _JetView);

    function PropertyListView() {
        _classCallCheck(this, PropertyListView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    PropertyListView.prototype.config = function config() {
        var propertyList = {
            id: "propertyListContainter",
            rows: [{
                view: "template",
                template: "Property List",
                type: "header",
                height: 40,
                borderless: true
            }, {
                id: "propertyListFunctions",
                view: "toolbar",
                type: "clean",
                height: 30,
                elements: [{ width: 5 }, {
                    id: "sortButton",
                    view: "button",
                    type: "icon",
                    icon: "sort-amount-asc",
                    width: 32,
                    css: "app_button",
                    click: function click() {
                        webix.message("detail");
                    }
                }, {
                    id: "viewFilter",
                    view: "toggle",
                    type: "icon",
                    name: "viewFilter",
                    offIcon: "sort-amount-asc",
                    onIcon: "sort-amount-desc",
                    width: 32,
                    on: {
                        onChange: function onChange(newv, oldv) {
                            __WEBPACK_IMPORTED_MODULE_1_models_propertyStore__["a" /* propertyStore */].showAll(newv);
                        }
                    }
                }, {
                    id: "propertyFilter",
                    view: "text",
                    on: {
                        onTimedKeyPress: function onTimedKeyPress() {
                            __WEBPACK_IMPORTED_MODULE_1_models_propertyStore__["a" /* propertyStore */].filterLabel(this.getValue().toLowerCase());
                        }
                    }
                }, { width: 5 }]
            }, { height: 10 }, {
                view: "scrollview",
                id: "propertyListScrollview",
                scroll: "y",
                body: {
                    rows: [{
                        view: "property",
                        id: "sets",
                        height: 2000,
                        elements: [],
                        on: {
                            'onAfterEditStop': function onAfterEditStop(state, editor, ignoreUpdate) {
                                if (!ignoreUpdate && __WEBPACK_IMPORTED_MODULE_1_models_propertyStore__["a" /* propertyStore */].getIndexById(editor.id) != -1) {
                                    var _propertyStore$update;

                                    __WEBPACK_IMPORTED_MODULE_1_models_propertyStore__["a" /* propertyStore */].updateItem(editor.id, (_propertyStore$update = {}, _propertyStore$update[editor.id] = state.value, _propertyStore$update));
                                }
                            }
                        }
                    }]
                }
            }]
        };
        __WEBPACK_IMPORTED_MODULE_1_models_propertyStore__["a" /* propertyStore */].data.attachEvent("onStoreUpdated", function () {
            $$('sets').define("elements", __WEBPACK_IMPORTED_MODULE_1_models_propertyStore__["a" /* propertyStore */].serialize());
            $$('sets').refresh();
        });
        return propertyList;
    };

    return PropertyListView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (PropertyListView);

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_views_module_moduleTools__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_views_module_moduleTabbarCell__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_views_module_moduleComponents__ = __webpack_require__(12);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Module JavaScript */
/* Base Menu JavaScript */





var ModuleView = function (_JetView) {
    _inherits(ModuleView, _JetView);

    function ModuleView() {
        _classCallCheck(this, ModuleView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    ModuleView.prototype.config = function config() {
        var module = {
            id: "module",
            rows: [__WEBPACK_IMPORTED_MODULE_1_views_module_moduleTools__["default"], {
                cols: [__WEBPACK_IMPORTED_MODULE_2_views_module_moduleTabbarCell__["default"], {
                    rows: [__WEBPACK_IMPORTED_MODULE_3_views_module_moduleComponents__["default"], { height: 40 }]
                }]
            }]
        };

        return module;
    };

    return ModuleView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (ModuleView);

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Module Tools JavaScript */


/* NOTE: Buttons in here are specific to the module loaded and should change upon module load */

var ModuleToolsView = function (_JetView) {
    _inherits(ModuleToolsView, _JetView);

    function ModuleToolsView() {
        _classCallCheck(this, ModuleToolsView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    ModuleToolsView.prototype.config = function config() {
        var moduleTools = {
            id: "moduleToolsContainer",
            minHeight: 40,
            maxHeight: 40,
            type: "clean",
            rows: [{
                id: "moduleTools",
                view: "toolbar",
                height: 40,
                type: "clean",
                elements: [{ width: 10 }, {
                    id: "selectButton",
                    view: "button",
                    type: "icon",
                    icon: "cursor-default-outline",
                    width: 32,
                    css: "app_button",
                    click: function click(id) {
                        webix.message(id);
                    }
                }, {
                    id: "panButton",
                    view: "button",
                    type: "icon",
                    icon: "pan",
                    width: 32,
                    css: "app_button",
                    click: function click(id) {
                        webix.message(id);
                    }
                }, {
                    id: "rotateButton",
                    view: "button",
                    type: "icon",
                    icon: "rotate-world",
                    width: 32,
                    css: "app_button",
                    click: function click(id) {
                        webix.message(id);
                    }
                }, {
                    id: "zoomInButton",
                    view: "button",
                    type: "icon",
                    icon: "magnify-plus-outline",
                    width: 32,
                    css: "app_button",
                    click: function click(id) {
                        webix.message(id);
                    }
                }, {
                    id: "zoomOutButton",
                    view: "button",
                    type: "icon",
                    icon: "magnify-minus-outline",
                    width: 32,
                    css: "app_button",
                    click: function click(id) {
                        webix.message(id);
                    }
                }, {}, {
                    view: "button",
                    type: "icon",
                    icon: "menu",
                    width: 50,
                    css: "app_button",
                    click: function click() {
                        if ($$("componentsHeader").isVisible()) {
                            $$("componentsHeader").hide();
                        } else {
                            $$("componentsHeader").show();
                        }
                        $$("componentsBar").toggle();
                        $$("componentsScrollview").resize();
                    }
                }, { width: 10 }]
            }]
        };

        return moduleTools;
    };

    return ModuleToolsView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (ModuleToolsView);

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_modules_schematic_views_schematic__ = __webpack_require__(29);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Module Tabview JavaScript */



var ModuleTabbarCellView = function (_JetView) {
    _inherits(ModuleTabbarCellView, _JetView);

    function ModuleTabbarCellView() {
        _classCallCheck(this, ModuleTabbarCellView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    ModuleTabbarCellView.prototype.config = function config() {
        var _this2 = this;

        var tabview = {
            id: "moduleContainer",
            rows: [{
                id: "moduleCells",
                animate: false,
                cells: [{ id: "startscreen", view: "template", template: "Start Screen" }]

            }, {
                view: "tabbar",
                id: "moduleTabbar",
                labelAlign: "right",
                tabOffset: 5,
                height: 40,
                click: function click() {
                    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                        args[_key] = arguments[_key];
                    }

                    if (args[1].target.className === "webix_tab_close webix_icon fa-times") {
                        /* This is to check if the close tab button has been clicked */
                        return;
                    } else {
                        _this2.showModule();
                    }
                },
                on: {
                    onBeforeTabClose: function onBeforeTabClose(id) {
                        $$("moduleCells").removeView($$("moduleTabbar").getValue());
                        $$("projectStore").updateItem(id, { opened: false, hidden: true });
                    }
                },
                options: [{ id: "startscreen", value: "Start", width: 150 }],
                multiview: "true",
                type: "bottom"
            }]
        };
        $$("projectStore").attachEvent("onDataUpdate", function (id, data) {
            if (data.opened && data.module) {
                var maxLength = 8;
                if (data.module) maxLength = 4;
                if ($$("moduleTabbar").config.options.length > maxLength) {
                    webix.message("Max open tab reached");
                } else {
                    $$("moduleCells").addView({ id: id, view: data.module });
                    $$("moduleTabbar").addOption({ id: id, value: data.value, close: true, icon: "file", width: 200 }, true);
                }
            }
        });
        return tabview;
    };

    /* Method space for loading Schematic Editor or Dashboard designer or any other module */
    /* needed for when switching between tabs, should unload current module and load new module */
    ModuleTabbarCellView.prototype.showModule = function showModule() {
        var tabID = $$("moduleTabbar").getValue();
        console.log(tabID + " ~ showModule code goes here");
    };

    return ModuleTabbarCellView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (ModuleTabbarCellView);

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_models_componentData__ = __webpack_require__(32);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Module Components JavaScript */



var ModuleComponentsView = function (_JetView) {
    _inherits(ModuleComponentsView, _JetView);

    function ModuleComponentsView() {
        _classCallCheck(this, ModuleComponentsView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    ModuleComponentsView.prototype.config = function config() {
        var components = {
            view: "scrollview",
            id: "componentsScrollview",
            scroll: "y",
            body: {
                rows: [{
                    id: "componentsHeader",
                    view: "template",
                    template: "Components",
                    type: "header",
                    height: 40,
                    borderless: true,
                    hidden: true
                }, {
                    id: "componentsBar",
                    view: "sidebar",
                    data: __WEBPACK_IMPORTED_MODULE_1_models_componentData__["a" /* default */],
                    position: "right",
                    height: 2000,
                    collapsed: true,
                    on: {
                        onAfterSelect: function onAfterSelect(id) {
                            /* Here is where you implement the gadget selection call in the Three.js */
                            webix.message("Selected: " + this.getItem(id).value);
                        }
                    }
                }]
            }
        };

        return components;
    };

    return ModuleComponentsView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (ModuleComponentsView);

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JetBase; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function locate(ui, id) {
    var cells = ui.getChildViews();
    for (var _iterator = cells, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
        var _ref;

        if (_isArray) {
            if (_i >= _iterator.length) break;
            _ref = _iterator[_i++];
        } else {
            _i = _iterator.next();
            if (_i.done) break;
            _ref = _i.value;
        }

        var cell = _ref;

        var kid = cell;
        if (kid.config.localId === id) {
            return kid;
        } else {
            kid = locate(kid, id);
        }
        if (kid) {
            return kid;
        }
    }
    return null;
}
var JetBase = function () {
    function JetBase() {
        _classCallCheck(this, JetBase);

        this._id = webix.uid();
        this._events = [];
        this._subs = {};
    }

    JetBase.prototype.getRoot = function getRoot() {
        return this._root;
    };

    JetBase.prototype.destructor = function destructor() {
        var events = this._events;
        for (var i = events.length - 1; i >= 0; i--) {
            events[i].obj.detachEvent(events[i].id);
        }
        // destroy sub views
        for (var key in this._subs) {
            var subView = this._subs[key].view;
            // it possible that subview was not loaded with any content yet
            // so check on null
            if (subView) {
                subView.destructor();
            }
        }
        this._events = this._container = this._app = this._parent = null;
    };

    JetBase.prototype.render = function render(root, url, parent) {
        var _this = this;

        this._parent = parent;
        if (url) {
            this._index = url[0].index;
        }
        root = root || document.body;
        var _container = typeof root === "string" ? webix.toNode(root) : root;
        if (this._container !== _container) {
            this._container = _container;
            return this._render(url).then(function () {
                return _this.getRoot();
            });
        } else {
            return this._urlChange(url).then(function () {
                return _this.getRoot();
            });
        }
    };

    JetBase.prototype.getIndex = function getIndex() {
        return this._index;
    };

    JetBase.prototype.getId = function getId() {
        return this._id;
    };

    JetBase.prototype.getParentView = function getParentView() {
        return this._parent;
    };

    JetBase.prototype.$$ = function $$(id) {
        var view = webix.$$(id);
        if (!view) {
            view = locate(this.getRoot(), id);
        }
        return view;
    };

    JetBase.prototype.on = function on(obj, name, code) {
        var id = obj.attachEvent(name, code);
        this._events.push({ obj: obj, id: id });
        return id;
    };

    JetBase.prototype.contains = function contains(view) {
        for (var key in this._subs) {
            var kid = this._subs[key].view;
            if (kid === view || kid.contains(view)) {
                return true;
            }
        }
        return false;
    };

    JetBase.prototype.getSubView = function getSubView(name) {
        var sub = this._subs[name || "default"];
        if (sub) {
            return { subview: sub, parent: this };
        }
        // when called from a child view, searches for nearest parent with subview
        if (this._parent) {
            return this._parent.getSubView(name);
        }
        return null;
    };

    JetBase.prototype.getName = function getName() {
        return this._name;
    };

    return JetBase;
}();

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export diff */
/* harmony export (immutable) */ __webpack_exports__["a"] = parse;
/* harmony export (immutable) */ __webpack_exports__["b"] = url2str;
function diff(oUrl, nUrl) {
    var i = 0;
    for (i; i < nUrl.length; i++) {
        var left = oUrl[i];
        var right = nUrl[i];
        if (!left) {
            break;
        }
        if (left.page !== right.page) {
            break;
        }
        for (var key in left.params) {
            if (left.params[key] !== right.params[key]) {
                break;
            }
        }
    }
    return i;
}
function parse(url) {
    // remove starting /
    if (url[0] === "/") {
        url = url.substr(1);
    }
    // split url by "/"
    var parts = url.split("/");
    var chunks = [];
    // for each page in url
    for (var i = 0; i < parts.length; i++) {
        var test = parts[i];
        var result = {};
        // detect params
        // support old 			some:a=b:c=d
        // and new notation		some?a=b&c=d
        var pos = test.indexOf(":");
        if (pos === -1) {
            pos = test.indexOf("?");
        }
        if (pos !== -1) {
            var params = test.substr(pos + 1).split(/[\:\?\&]/g);
            // create hash of named params
            for (var _iterator = params, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
                var _ref;

                if (_isArray) {
                    if (_i >= _iterator.length) break;
                    _ref = _iterator[_i++];
                } else {
                    _i = _iterator.next();
                    if (_i.done) break;
                    _ref = _i.value;
                }

                var param = _ref;

                var dchunk = param.split("=");
                result[dchunk[0]] = dchunk[1];
            }
        }
        // store parsed values
        chunks[i] = {
            page: pos > -1 ? test.substr(0, pos) : test,
            params: result, index: i + 1
        };
    }
    // return array of page objects
    return chunks;
}
function url2str(stack) {
    var url = [];
    for (var _iterator2 = stack, _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
        var _ref2;

        if (_isArray2) {
            if (_i2 >= _iterator2.length) break;
            _ref2 = _iterator2[_i2++];
        } else {
            _i2 = _iterator2.next();
            if (_i2.done) break;
            _ref2 = _i2.value;
        }

        var chunk = _ref2;

        url.push("/" + chunk.page);
        var params = obj2str(chunk.params);
        if (params) {
            url.push(":" + params);
        }
    }
    return url.join("");
}
function obj2str(obj) {
    var str = [];
    for (var key in obj) {
        if (str.length) {
            str.push(":");
        }
        str.push(key + "=" + obj[key]);
    }
    return str.join("");
}

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HashRouter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_routie_lib_routie__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_routie_lib_routie___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_webix_routie_lib_routie__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }


var HashRouter = function () {
    function HashRouter(cb, config) {
        var _this = this;

        _classCallCheck(this, HashRouter);

        this.config = config || {};
        var rcb = function rcb(_$a) {};
        __WEBPACK_IMPORTED_MODULE_0_webix_routie_lib_routie___default()("!*", function (url) {
            _this._lastUrl = "";
            return rcb(_this.get());
        });
        rcb = cb;
    }

    HashRouter.prototype.set = function set(path, config) {
        if (this.config.routes) {
            var compare = path.split("?", 2);
            for (var key in this.config.routes) {
                if (this.config.routes[key] === compare[0]) {
                    path = key + (compare.length > 1 ? "?" + compare[1] : "");
                    break;
                }
            }
        }
        this._lastUrl = path;
        __WEBPACK_IMPORTED_MODULE_0_webix_routie_lib_routie___default.a.navigate("!" + path, config);
    };

    HashRouter.prototype.get = function get() {
        var path = this._lastUrl || (window.location.hash || "").replace("#!", "");
        if (this.config.routes) {
            var compare = path.split("?", 2);
            var key = this.config.routes[compare[0]];
            if (key) {
                path = key + (compare.length > 1 ? "?" + compare[1] : "");
            }
        }
        return path;
    };

    return HashRouter;
}();

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_views_menu_menu__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_views_sidebar_sidebar__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_views_module_module__ = __webpack_require__(9);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* Layout JavaScript */





var WillowLynxDesigner = function (_JetView) {
    _inherits(WillowLynxDesigner, _JetView);

    function WillowLynxDesigner() {
        _classCallCheck(this, WillowLynxDesigner);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    WillowLynxDesigner.prototype.config = function config() {
        var ui = {
            id: "layout",
            rows: [__WEBPACK_IMPORTED_MODULE_1_views_menu_menu__["default"], {
                cols: [__WEBPACK_IMPORTED_MODULE_2_views_sidebar_sidebar__["default"], {
                    id: "siderbarVerResizer",
                    view: "resizer"
                }, __WEBPACK_IMPORTED_MODULE_3_views_module_module__["default"]]
            }]
        };
        return ui;
    };

    return WillowLynxDesigner;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* harmony default export */ __webpack_exports__["default"] = (WillowLynxDesigner);

/***/ }),
/* 17 */
/***/ (function(module, exports) {

/* File Menu JavaScript */

/***/ }),
/* 18 */
/***/ (function(module, exports) {



/***/ }),
/* 19 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JetApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__JetBase__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__JetView__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__JetViewLegacy__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__JetViewRaw__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__routers_HashRouter__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__helpers__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__patch__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__patch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__patch__);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }








var JetApp = function (_JetBase) {
    _inherits(JetApp, _JetBase);

    function JetApp(config) {
        _classCallCheck(this, JetApp);

        var _this = _possibleConstructorReturn(this, _JetBase.call(this));

        _this.webix = config.webix || webix;
        // init config
        _this.config = _this.webix.extend({
            name: "App",
            version: "1.0",
            start: "/home"
        }, config, true);
        _this._name = _this.config.name;
        _this._services = {};
        webix.extend(_this, webix.EventSystem);
        webix.attachEvent("onClick", function (e) {
            return _this.clickHandler(e);
        });
        return _this;
    }

    JetApp.prototype.getService = function getService(name) {
        var obj = this._services[name];
        if (typeof obj === "function") {
            obj = this._services[name] = obj(this);
        }
        return obj;
    };

    JetApp.prototype.setService = function setService(name, handler) {
        this._services[name] = handler;
    };
    // copy object and collect extra handlers


    JetApp.prototype.copyConfig = function copyConfig(obj, target, config) {
        // raw ui config
        if (obj.$ui) {
            obj = { $subview: new __WEBPACK_IMPORTED_MODULE_2__JetViewLegacy__["a" /* JetViewLegacy */](this, "", obj) };
        } else if (obj instanceof JetApp) {
            obj = { $subview: obj };
        }
        // subview placeholder
        if (obj.$subview) {
            return this.addSubView(obj, target, config);
        }
        // process sub-properties
        target = target || (obj instanceof Array ? [] : {});
        for (var method in obj) {
            var point = obj[method];
            // view class
            if (typeof point === "function" && point.prototype && point.prototype.config) {
                point = { $subview: point };
            }
            if (point && (typeof point === "undefined" ? "undefined" : _typeof(point)) === "object" && !(point instanceof webix.DataCollection)) {
                if (point instanceof Date) {
                    target[method] = new Date(point);
                } else {
                    target[method] = this.copyConfig(point, point instanceof Array ? [] : {}, config);
                }
            } else {
                target[method] = point;
            }
        }
        return target;
    };

    JetApp.prototype.getRouter = function getRouter() {
        return this.$router;
    };

    JetApp.prototype.clickHandler = function clickHandler(e) {
        if (e) {
            var target = e.target || e.srcElement;
            if (target && target.getAttribute) {
                var trigger = target.getAttribute("trigger");
                if (trigger) {
                    this.trigger(trigger);
                }
                var route = target.getAttribute("route");
                if (route) {
                    this.show(route);
                }
            }
        }
    };

    JetApp.prototype.refresh = function refresh() {
        var temp = this._container;
        this._view.destructor();
        this._view = this._container = null;
        this.render(temp, Object(__WEBPACK_IMPORTED_MODULE_5__helpers__["a" /* parse */])(this.getRouter().get()), this._parent);
    };

    JetApp.prototype.loadView = function loadView(url) {
        var _this2 = this;

        var views = this.config.views;
        var result = null;
        try {
            if (views) {
                if (typeof views === "function") {
                    // custom loading strategy
                    result = views(url);
                } else {
                    // predefined hash
                    result = views[url];
                }
                if (typeof result === "string") {
                    url = result;
                    result = null;
                }
            }
            if (!result) {
                url = url.replace(/\./g, "/");
                var view = __webpack_require__(25)("./" + url);
                if (view.__esModule) {
                    view = view.default;
                }
                result = view;
            }
        } catch (e) {
            result = this._loadError(url, e);
        }
        // custom handler can return view or its promise
        if (!result.then) {
            result = Promise.resolve(result);
        }
        // set error handler
        result = result.catch(function (err) {
            return _this2._loadError(url, err);
        });
        return result;
    };

    JetApp.prototype.createFromURL = function createFromURL(url, now) {
        var _this3 = this;

        var chunk = url[0];
        var name = chunk.page;
        var view = void 0;
        if (now && now.getName() === name) {
            view = Promise.resolve(now);
        } else {
            view = this.loadView(chunk.page).then(function (ui) {
                var obj = void 0;
                if (typeof ui === "function") {
                    if (ui.prototype && ui.prototype.show) {
                        // UI class
                        return new ui(_this3, name);
                    } else {
                        // UI factory functions
                        ui = ui();
                    }
                }
                if (ui instanceof JetApp || ui instanceof __WEBPACK_IMPORTED_MODULE_1__JetView__["a" /* JetView */]) {
                    obj = ui;
                } else {
                    // UI object
                    if (ui.$ui) {
                        obj = new __WEBPACK_IMPORTED_MODULE_2__JetViewLegacy__["a" /* JetViewLegacy */](_this3, name, ui);
                    } else {
                        obj = new __WEBPACK_IMPORTED_MODULE_3__JetViewRaw__["a" /* JetViewRaw */](_this3, name, ui);
                    }
                }
                return obj;
            });
        }
        return view;
    };
    // show view path


    JetApp.prototype.show = function show(name) {
        var _this4 = this;

        if (this.$router.get() !== name) {
            this.canNavigate(name).then(function (url) {
                _this4.$router.set(url, { silent: true });
                _this4._render(url);
            }).catch(function () {
                return false;
            });
        }
    };

    JetApp.prototype.canNavigate = function canNavigate(url, view) {
        var obj = {
            url: Object(__WEBPACK_IMPORTED_MODULE_5__helpers__["a" /* parse */])(url),
            redirect: url,
            confirm: Promise.resolve(true)
        };
        var res = this.callEvent("app:guard", [url, view || this._view, obj]);
        if (!res) {
            return Promise.reject("");
        }
        return obj.confirm.then(function () {
            return obj.redirect;
        });
    };

    JetApp.prototype.destructor = function destructor() {
        this._view.destructor();
    };
    // event helpers


    JetApp.prototype.trigger = function trigger(name) {
        for (var _len = arguments.length, rest = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            rest[_key - 1] = arguments[_key];
        }

        this.apply(name, rest);
    };

    JetApp.prototype.apply = function apply(name, data) {
        this.callEvent(name, data);
    };

    JetApp.prototype.action = function action(name) {
        return this.webix.bind(function () {
            for (var _len2 = arguments.length, rest = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                rest[_key2] = arguments[_key2];
            }

            this.apply(name, rest);
        }, this);
    };

    JetApp.prototype.on = function on(name, handler) {
        this.attachEvent(name, handler);
    };

    JetApp.prototype.use = function use(plugin, config) {
        plugin(this, null, config);
    };

    JetApp.prototype.error = function error(name, er) {
        this.callEvent(name, er);
        this.callEvent("app:error", er);
        /* tslint:disable */
        if (this.config.debug) {
            if (er[0]) {
                console.error(er[0]);
            }
            debugger;
        }
        /* tslint:enable */
    };
    // renders top view


    JetApp.prototype._render = function _render(url) {
        var _this5 = this;

        var firstInit = !this.$router;
        if (firstInit) {
            url = this._first_start(url);
        }
        var strUrl = typeof url === "string" ? url : Object(__WEBPACK_IMPORTED_MODULE_5__helpers__["b" /* url2str */])(url);
        return this.canNavigate(strUrl).then(function (newurl) {
            _this5.$router.set(newurl, { silent: true });
            return _this5._render_stage(newurl);
        });
    };

    JetApp.prototype._render_stage = function _render_stage(url) {
        var _this6 = this;

        var parsed = typeof url === "string" ? Object(__WEBPACK_IMPORTED_MODULE_5__helpers__["a" /* parse */])(url) : url;
        // block resizing while rendering parts of UI
        return webix.ui.freeze(function () {
            return _this6.createFromURL(parsed, _this6._view).then(function (view) {
                // save reference for old and new views
                var oldview = _this6._view;
                _this6._view = view;
                // render url state for the root
                return view.render(_this6._container, parsed, _this6._parent).then(function (root) {
                    // destroy and detack old view
                    if (oldview && oldview !== _this6._view) {
                        oldview.destructor();
                    }
                    if (_this6._view.getRoot().getParentView()) {
                        _this6._container = root;
                    }
                    _this6._root = root;
                    _this6.callEvent("app:route", [parsed]);
                    return view;
                });
            }).catch(function (er) {
                _this6.error("app:error:render", [er]);
            });
        });
    };

    JetApp.prototype._urlChange = function _urlChange(_$url) {
        alert("Not implemented");
        return Promise.resolve(true);
    };

    JetApp.prototype._first_start = function _first_start(url) {
        var _this7 = this;

        var cb = function cb(a) {
            return setTimeout(function () {
                _this7._render(a);
            }, 1);
        };
        this.$router = new (this.config.router || __WEBPACK_IMPORTED_MODULE_4__routers_HashRouter__["a" /* HashRouter */])(cb, this.config);
        // start animation for top-level app
        if (this._container === document.body && this.config.animation !== false) {
            var node = this._container;
            webix.html.addCss(node, "webixappstart");
            setTimeout(function () {
                webix.html.removeCss(node, "webixappstart");
                webix.html.addCss(node, "webixapp");
            }, 10);
        }
        if (!url || url.length === 1) {
            url = this.$router.get() || this.config.start;
            this.$router.set(url, { silent: true });
        }
        return url;
    };
    // error during view resolving


    JetApp.prototype._loadError = function _loadError(url, err) {
        this.error("app:error:resolve", [err, url]);
        return { template: " " };
    };

    JetApp.prototype.addSubView = function addSubView(obj, target, config) {
        var url = obj.$subview !== true ? obj.$subview : null;
        var name = obj.name || (url ? this.webix.uid() : "default");
        target.id = obj.id || "s" + this.webix.uid();
        var view = config[name] = { id: target.id, url: url };
        if (view.url instanceof __WEBPACK_IMPORTED_MODULE_1__JetView__["a" /* JetView */]) {
            view.view = view.url;
        }
        return target;
    };

    return JetApp;
}(__WEBPACK_IMPORTED_MODULE_0__JetBase__["a" /* JetBase */]);

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JetViewLegacy; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__JetView__ = __webpack_require__(1);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }


// wrapper for raw objects and Jet 1.x structs
var JetViewLegacy = function (_JetView) {
    _inherits(JetViewLegacy, _JetView);

    function JetViewLegacy(app, name, ui) {
        _classCallCheck(this, JetViewLegacy);

        var _this = _possibleConstructorReturn(this, _JetView.call(this, app, name));

        _this._ui = ui;
        _this._windows = [];
        return _this;
    }

    JetViewLegacy.prototype.getRoot = function getRoot() {
        if (this.app.config.jet1xMode) {
            var parent = this.getParentView();
            if (parent) {
                return parent.getRoot();
            }
        }
        return this._root;
    };

    JetViewLegacy.prototype.config = function config() {
        return this._ui.$ui || this._ui;
    };

    JetViewLegacy.prototype.destructor = function destructor() {
        var destroy = this._ui.$ondestroy;
        if (destroy) {
            destroy();
        }
        for (var _iterator = this._windows, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
            var _ref;

            if (_isArray) {
                if (_i >= _iterator.length) break;
                _ref = _iterator[_i++];
            } else {
                _i = _iterator.next();
                if (_i.done) break;
                _ref = _i.value;
            }

            var window = _ref;

            window.destructor();
        }
        _JetView.prototype.destructor.call(this);
    };

    JetViewLegacy.prototype.show = function show(path, config) {
        if (path.indexOf("/") === 0 || path.indexOf("./") === 0) {
            return _JetView.prototype.show.call(this, path, config);
        }
        _JetView.prototype.show.call(this, "../" + path, config);
    };

    JetViewLegacy.prototype.init = function init(a, b) {
        if (this.app.config.legacyEarlyInit) {
            this._realInitHandler(a, b);
        }
    };

    JetViewLegacy.prototype.ready = function ready(a, b) {
        if (!this.app.config.legacyEarlyInit) {
            this._realInitHandler(a, b);
        }
    };

    JetViewLegacy.prototype._realInitHandler = function _realInitHandler(a, b) {
        var init = this._ui.$oninit;
        if (init) {
            var root = this.getRoot();
            init(root, root.$scope);
        }
        var events = this._ui.$onevent;
        if (events) {
            for (var key in events) {
                this.on(this.app, key, events[key]);
            }
        }
        var windows = this._ui.$windows;
        if (windows) {
            for (var _iterator2 = windows, _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
                var _ref2;

                if (_isArray2) {
                    if (_i2 >= _iterator2.length) break;
                    _ref2 = _iterator2[_i2++];
                } else {
                    _i2 = _iterator2.next();
                    if (_i2.done) break;
                    _ref2 = _i2.value;
                }

                var conf = _ref2;

                if (conf.$ui) {
                    var view = new JetViewLegacy(this.app, this.getName(), conf);
                    view.render(document.body);
                    this._windows.push(view);
                } else {
                    this.ui(conf);
                }
            }
        }
    };

    JetViewLegacy.prototype._urlChange = function _urlChange(url) {
        var _this2 = this;

        return _JetView.prototype._urlChange.call(this, url).then(function () {
            var onurlchange = _this2._ui.$onurlchange;
            if (onurlchange) {
                var root = _this2.getRoot();
                onurlchange(url[0].params, url.slice(1), root.$scope);
            }
        });
    };

    return JetViewLegacy;
}(__WEBPACK_IMPORTED_MODULE_0__JetView__["a" /* JetView */]);

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JetViewRaw; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__JetView__ = __webpack_require__(1);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }


// wrapper for raw objects and Jet 1.x structs
var JetViewRaw = function (_JetView) {
    _inherits(JetViewRaw, _JetView);

    function JetViewRaw(app, name, ui) {
        _classCallCheck(this, JetViewRaw);

        var _this = _possibleConstructorReturn(this, _JetView.call(this, app, name));

        _this._ui = ui;
        return _this;
    }

    JetViewRaw.prototype.config = function config() {
        return this._ui;
    };

    return JetViewRaw;
}(__WEBPACK_IMPORTED_MODULE_0__JetView__["a" /* JetView */]);

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * webix-routie - router for Webix-Jet
 * v0.4.0
 * MIT License
 *
 * based on routie - a tiny hash router
 * http://projects.jga.me/routie
 * copyright Greg Allen 2016
 * MIT License
*/

var Routie = function Routie(w, isModule) {

  var routes = [];
  var map = {};
  var reference = 'routie';
  var oldReference = w[reference];
  var oldUrl;

  var Route = function Route(path, name) {
    this.name = name;
    this.path = path;
    this.keys = [];
    this.fns = [];
    this.params = {};
    this.regex = pathToRegexp(this.path, this.keys, false, false);
  };

  Route.prototype.addHandler = function (fn) {
    this.fns.push(fn);
  };

  Route.prototype.removeHandler = function (fn) {
    for (var i = 0, c = this.fns.length; i < c; i++) {
      var f = this.fns[i];
      if (fn == f) {
        this.fns.splice(i, 1);
        return;
      }
    }
  };

  Route.prototype.run = function (params) {
    for (var i = 0, c = this.fns.length; i < c; i++) {
      if (this.fns[i].apply(this, params) === false) return false;
    }
    return true;
  };

  Route.prototype.match = function (path, params) {
    var m = this.regex.exec(path);

    if (!m) return false;

    for (var i = 1, len = m.length; i < len; ++i) {
      var key = this.keys[i - 1];

      var val = 'string' == typeof m[i] ? decodeURIComponent(m[i]) : m[i];

      if (key) {
        this.params[key.name] = val;
      }
      params.push(val);
    }

    return true;
  };

  Route.prototype.toURL = function (params) {
    var path = this.path;
    for (var param in params) {
      path = path.replace('/:' + param, '/' + params[param]);
    }
    path = path.replace(/\/:.*\?/g, '/').replace(/\?/g, '');
    if (path.indexOf(':') != -1) {
      throw new Error('missing parameters for url: ' + path);
    }
    return path;
  };

  var pathToRegexp = function pathToRegexp(path, keys, sensitive, strict) {
    if (path instanceof RegExp) return path;
    if (path instanceof Array) path = '(' + path.join('|') + ')';
    path = path.concat(strict ? '' : '/?').replace(/\/\(/g, '(?:/').replace(/\+/g, '__plus__').replace(/(\/)?(\.)?:(\w+)(?:(\(.*?\)))?(\?)?/g, function (_, slash, format, key, capture, optional) {
      keys.push({ name: key, optional: !!optional });
      slash = slash || '';
      return '' + (optional ? '' : slash) + '(?:' + (optional ? slash : '') + (format || '') + (capture || format && '([^/.]+?)' || '([^/]+?)') + ')' + (optional || '');
    }).replace(/([/.])/g, '\\$1').replace(/__plus__/g, '(.+)').replace(/\*/g, '(.*)');
    return new RegExp('^' + path + '$', sensitive ? '' : 'i');
  };

  var addHandler = function addHandler(path, fn) {
    var s = path.split(' ');
    var name = s.length == 2 ? s[0] : null;
    path = s.length == 2 ? s[1] : s[0];

    if (!map[path]) {
      map[path] = new Route(path, name);
      routes.push(map[path]);
    }
    map[path].addHandler(fn);
  };

  var routie = function routie(path, fn) {
    if (typeof fn == 'function') {
      addHandler(path, fn);
      routie.reload();
    } else if ((typeof path === 'undefined' ? 'undefined' : _typeof(path)) == 'object') {
      for (var p in path) {
        addHandler(p, path[p]);
      }
      routie.reload();
    } else if (typeof fn === 'undefined') {
      routie.navigate(path);
    }
  };

  routie.lookup = function (name, obj) {
    for (var i = 0, c = routes.length; i < c; i++) {
      var route = routes[i];
      if (route.name == name) {
        return route.toURL(obj);
      }
    }
  };

  routie.remove = function (path, fn) {
    var route = map[path];
    if (!route) return;
    route.removeHandler(fn);
  };

  routie.removeAll = function () {
    map = {};
    routes = [];
    oldUrl = '';
  };

  routie.navigate = function (path, options) {
    options = options || {};
    var silent = options.silent || false;

    if (silent) {
      removeListener();
    }
    setTimeout(function () {
      window.location.hash = path;

      if (silent) {
        setTimeout(function () {
          addListener();
        }, 1);
      }
    }, 1);
  };

  routie.noConflict = function () {
    w[reference] = oldReference;
    return routie;
  };

  var getHash = function getHash() {
    return window.location.hash.substring(1);
  };

  var checkRoute = function checkRoute(hash, route) {
    var params = [];
    if (route.match(hash, params)) {
      return route.run(params) !== false ? 1 : 0;
    }
    return -1;
  };

  var hashChanged = routie.reload = function () {
    var hash = getHash();
    for (var i = 0, c = routes.length; i < c; i++) {
      var route = routes[i];
      var state = checkRoute(hash, route);
      if (state === 1) {
        //route processed
        oldUrl = hash;
        break;
      } else if (state === 0) {
        //route rejected
        routie.navigate(oldUrl, { silent: true });
        break;
      }
    }
  };

  var addListener = function addListener() {
    if (w.addEventListener) {
      w.addEventListener('hashchange', hashChanged, false);
    } else {
      w.attachEvent('onhashchange', hashChanged);
    }
  };

  var removeListener = function removeListener() {
    if (w.removeEventListener) {
      w.removeEventListener('hashchange', hashChanged);
    } else {
      w.detachEvent('onhashchange', hashChanged);
    }
  };
  addListener();
  oldUrl = getHash();

  if (isModule) {
    return routie;
  } else {
    w[reference] = routie;
  }
};

if (false) {
  Routie(window);
} else {
  module.exports = Routie(window, true);
  module.exports.default = module.exports;
}

/***/ }),
/* 24 */
/***/ (function(module, exports) {

var w = webix;
// will be added in webix 5.1
if (!w.ui.freeze) {
    w.ui.freeze = function (handler) {
        // disabled because webix jet 5.0 can't handle resize of scrollview correctly
        // w.ui.$freeze = true;
        var res = handler();
        if (res && res.then) {
            res.then(function (some) {
                w.ui.$freeze = false;
                w.ui.resize();
                return some;
            });
        } else {
            w.ui.$freeze = false;
            w.ui.resize();
        }
        return res;
    };
}

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./layout": 16,
	"./layout.js": 16,
	"./menu/dropDownMenus": 4,
	"./menu/dropDownMenus.js": 4,
	"./menu/fileMenu": 17,
	"./menu/fileMenu.js": 17,
	"./menu/iconMenu": 5,
	"./menu/iconMenu.js": 5,
	"./menu/menu": 3,
	"./menu/menu.js": 3,
	"./module/module": 9,
	"./module/module.js": 9,
	"./module/moduleComponents": 12,
	"./module/moduleComponents.js": 12,
	"./module/moduleTabbarCell": 11,
	"./module/moduleTabbarCell.js": 11,
	"./module/moduleTools": 10,
	"./module/moduleTools.js": 10,
	"./sidebar/projectExplorer": 7,
	"./sidebar/projectExplorer.js": 7,
	"./sidebar/propertyList": 8,
	"./sidebar/propertyList.js": 8,
	"./sidebar/sidebar": 6,
	"./sidebar/sidebar.js": 6
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 25;

/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fileMenu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return editMenu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return viewMenu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return drawMenu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return modifyMenu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return helpMenu; });
/* Drop Down Menu Data */

var viewsSubmenu = [{ value: "Cross View", icon: "cross-view" }, { value: "3 x 1 View", icon: "three-one-view" }, { value: "Single", icon: "square" }, { value: "Horizontal Split", icon: "horizontal-split-view" }, { value: "Vertical Split", icon: "vertical-split-view" }];

var activeViewSubmenu = [{ value: "Orthographic", icon: "orthographic" }, { value: "Perspective", icon: "perspective" }, { value: "Top", icon: "view-top" }, { value: "Bottom", icon: "view-bottom" }, { value: "Front", icon: "view-front" }, { value: "Back", icon: "view-back" }, { value: "Left", icon: "view-left" }, { value: "Right", icon: "view-right" }];

var twoDSubmenu = [{ value: "Rectangle", icon: "rectangle" }, { value: "Triangle", icon: "triangle" }, { value: "Ellipse", icon: "ellipse" }, { value: "Polygon", icon: "hexagon" }, { value: "Disk", icon: "disk" }, { value: "Point", icon: "point" }, { value: "Line", icon: "line" }, { value: "Image", icon: "image" }, { value: "Label", icon: "label" }, { value: "Status Flag", icon: "flag-variant" }];

var threeDSubmenu = [{ value: "Box", icon: "box-3d" }, { value: "Sphere", icon: "sphere" }, { value: "Cylinder", icon: "cylinder" }, { value: "Cone", icon: "cone" }, { value: "Torus", icon: "torus" }, { value: "Tube", icon: "tube" }];

var alignmentSubmenu = [{ value: "Edge Align", icon: "code" }, { value: "Center Align", icon: "code" }];

var fileMenu = [{ id: "File->Open", value: "Open", icon: "folder-open" }, { id: "File->Save", value: "Save", icon: "floppy" }, { id: "File->Import", value: "Import", icon: "file-import" }, { id: "File->Export", value: "Export", icon: "file-export" }, { id: "File->Page Properties", value: "Page Properties", icon: "-" }, { id: "File->Print", value: "Print", icon: "printer" }, { id: "File->Options", value: "Options", icon: "-" }, { id: "File->Exit", value: "Exit", icon: "exit-to-app" }];

var editMenu = [{ value: "Cut", icon: "content-cut" }, { value: "Copy", icon: "content-copy" }, { value: "Paste", icon: "content-paste" }, { value: "Undo", icon: "undo" }, { value: "Redo", icon: "redo" }, { value: "Select All", icon: "select-all" }, { value: "Invert Selection", icon: "select-inverse" }, { value: "Find", icon: "file-find" }, { value: "Delete", icon: "delete" }];

var viewMenu = [{ value: "Select", icon: "cursor-default-outline" }, { value: "Pan", icon: "pan" }, { value: "Rotate View", icon: "rotate-world" }, { value: "Zoom In", icon: "magnify-plus-outline" }, { value: "Zoom Out", icon: "magnify-minus-outline" }, { value: "Fit to Page", icon: "fit-to-screen" }, { value: "Views", icon: "view-quilt", submenu: viewsSubmenu }, { value: "Active View", icon: "-", submenu: activeViewSubmenu }, { value: "Grid", icon: "grid" }, { value: "Test Mode", icon: "play-box-outline" }];

var drawMenu = [{ value: "2D Gadgets", icon: "rectangle", submenu: twoDSubmenu }, { value: "3D Gadgets", icon: "box-3d", submenu: threeDSubmenu }, { value: "State Gadget", icon: "traffic-light" }, { value: "Table Gadget", icon: "table-large" }, { value: "Camera", icon: "free-camera" }, { value: "Spot Light", icon: "spotlight" }, { value: "Point Light", icon: "lightbulb" }, { value: "Symbol Library", icon: "hexagon-multiple" }];

var modifyMenu = [{ value: "Scale", icon: "scale-object" }, { value: "Rotate", icon: "rotate-3d" }, { value: "Group", icon: "group" }, { value: "Ungroup", icon: "ungroup" }, { value: "Data Assign", icon: "database" }, { value: "Color", icon: "palette" }, { value: "Text", icon: "format-text" }, { value: "Duplicate", icon: "content-duplicate" }, { value: "Alignment", icon: "format-align-middle", submenu: alignmentSubmenu }, { value: "Wireframe", icon: "wireframe" }, { value: "Scripting", icon: "code-braces" }, { value: "Property Popup", icon: "wrench" }];

var helpMenu = [{ value: "Contents", icon: "book-open-page-variant" }, { value: "About", icon: "help-circle-outline" }];



/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return projectStore; });
/* Project store contain project data which are
   used by project explorer to populating the data display
   and controlling the state of the opened module */

var projectStoreClass = webix.proto({
    $init: function $init(config) {
        //functions executed on component initialization
    },
    loadStore: function loadStore() {
        //populate the store by parsing the config file
    }
}, webix.TreeCollection);

var projectStore = new projectStoreClass({
    id: "projectStore",
    data: [//test data
    {
        id: "project",
        value: "Willowlynx Project",
        open: true,
        isFolder: true,
        data: [{
            id: "dashboard",
            open: true,
            value: "Dashboards",
            isFolder: true,
            data: [{
                id: "dashboard1",
                value: "Main Page"
            }, {
                id: "dashboard2",
                value: "Reports"
            }, {
                id: "dashboard3",
                value: "Trend"
            }]
        }, {
            id: "schematics",
            open: true,
            value: "Schematics",
            isFolder: true,
            data: [{
                id: "schematic1",
                value: "P00.json",
                path: "",
                module: "schematic",
                opened: false,
                hidden: true
            }, {
                id: "schematic2",
                value: "P01_Map_Overview.json",
                path: "",
                module: "schematic",
                opened: false,
                hidden: true
            }]
        }]
    }]
});

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return propertyStore; });
/* The property store need to be dynamically updated
   according to the currently selected object. This
   can be done by passing data to updateElements() */

var propertyStoreClass = webix.proto({
    $init: function $init(config) {
        //functions executed on component initialization
        this.showAllProps = false;
        this.filterInput = '';
    },
    defaults: {
        on: { 'onBindRequest': function onBindRequest() {
                this.showAll();
            } }
    },
    /* Data format
       [{ id: unique id to be referred for data update,
          label: the property name,
          value: the propertie's value,
          type: Webix's input editor type,
          simple: set to true if this property is for
                  simple configuration
       }] */
    updateElements: function updateElements(data) {
        if (data.length) {
            this.parse(data);
            this.showAll();
        } else {
            this.clearAll();
        }
    },
    /* update visible data based on filter */
    showAll: function showAll() {
        var status = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.showAllProps;

        this.showAllProps = status;
        var value = this.filterInput;
        this.filter(function (obj) {
            return obj.label.toLowerCase().indexOf(value) !== -1 && (status || obj.simple != status);
        });
    },
    filterLabel: function filterLabel(value) {
        this.filterInput = value;
        var status = this.showAllProps;
        this.filter(function (obj) {
            return obj.label.toLowerCase().indexOf(value) !== -1 && (status || obj.simple != status);
        });
    }
}, webix.DataCollection);

var propertyStore = new propertyStoreClass({
    id: "propertyStore",
    data: []
});

/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_webix_jet__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_schematicStore__ = __webpack_require__(30);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




webix.protoUI({
    name: "schematic",
    $init: function $init(config) {
        //creating promise
        this.promise = webix.promise.defer().then(webix.bind(function () {
            var _this = this;

            //create the module store
            var storeid = config.id + "Store";
            var store = new __WEBPACK_IMPORTED_MODULE_1__models_schematicStore__["a" /* schematicStore */]({
                id: storeid
            });

            //init lib when it is ready
            var scene = new THREE.Scene();
            var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

            var raycaster = new THREE.Raycaster();
            var renderer = new THREE.WebGLRenderer();
            renderer.setSize(this.$width, this.$height);
            this.$view.appendChild(renderer.domElement);

            //Example on 1 object (cube) handling
            var cube1 = {
                uid: "cube1",
                name: "cube1",
                wireframe: true,
                width: 1,
                height: 1,
                depth: 1,
                fps: 30,
                color: '#00ff00'
            };

            var geometry = new THREE.BoxGeometry(cube1.width, cube1.height, cube1.depth);
            var material = new THREE.MeshBasicMaterial();
            material.color.setHex(cube1.color.replace(/#/g, "0x"));
            var cube = new THREE.Mesh(geometry, material);
            scene.add(cube);
            cube.name = cube1.name;
            store.addCubeProps(cube1);

            var outlineMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000, wireframe: cube1.wireframe });
            var outline = new THREE.Mesh(geometry, outlineMaterial);
            outline.name = 'wireframe';
            scene.add(outline);

            camera.position.z = 5;

            var stop = false;
            var fpsInterval = void 0,
                lastDrawTime = void 0;

            //handle store update
            store.data.attachEvent("onStoreUpdated", function (id, obj) {
                switch (id) {
                    case store.getDataId("fps"):
                        var new_fps = obj.value;
                        fpsInterval = 1000 / new_fps;
                        break;
                    case store.getDataId("color"):
                        cube.material.color.setHex(obj.value.replace(/#/g, "0x"));
                        break;
                    case store.getDataId("wireframe"):
                        if (!obj.value) {
                            scene.remove(outline);
                        } else {
                            scene.add(outline);
                        }
                        break;
                    case store.getDataId("width"):
                        cube.scale.setX(obj.value);
                        break;
                    case store.getDataId("height"):
                        cube.scale.setY(obj.value);
                        break;
                    case store.getDataId("depth"):
                        cube.scale.setZ(obj.value);
                        break;
                    default:
                }
            });

            var mouse = new THREE.Vector2(),
                INTERSECTED = void 0;

            //initialize the timer variables and start the animation
            var startAnimating = function startAnimating(fps) {
                fpsInterval = 1000 / fps;
                lastDrawTime = performance.now();
                animate();
            };

            //draw loop
            var animate = function animate(now) {
                requestAnimationFrame(animate);

                //calc elapsed time since last loop
                var elapsed = now - lastDrawTime;

                //if enough time has elapsed, draw the next frame
                if (elapsed > fpsInterval) {
                    lastDrawTime = now - elapsed % fpsInterval;

                    cube.rotation.x += 0.1;
                    cube.rotation.y += 0.1;
                    if (scene.getObjectByName('wireframe')) {
                        outline.rotation.copy(cube.rotation);
                        outline.scale.copy(cube.scale);
                    }

                    renderer.render(scene, camera);
                }
            };

            var onWindowResize = function onWindowResize() {
                camera.aspect = _this.$width / _this.$height;
                camera.updateProjectionMatrix();
                renderer.setSize(_this.$width, _this.$height);
            };
            window.addEventListener('resize', onWindowResize, false);

            var onMouseDown = function onMouseDown(event) {
                event.preventDefault();
                mouse.x = event.clientX / window.innerWidth * 2 - 1;
                mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
                // find intersections
                raycaster.setFromCamera(mouse, camera);
                var intersects = raycaster.intersectObjects(scene.children);
                if (intersects.length > 0) {
                    if (INTERSECTED != intersects[0].object) {
                        INTERSECTED = intersects[0].object;
                        store.selectObj(INTERSECTED.name);
                    } else {
                        store.deselectObj();
                        INTERSECTED = null;
                    }
                }
            };
            document.addEventListener('mousedown', onMouseDown, false);

            startAnimating(30);

            //call an optional callback code
            if (this.config.ready) this.config.ready.call(this, scene);
        }, this));

        //if file is already loaded, resolve promise
        if (window.THREE) this.promise.resolve();else
            //wait for data loading and resolve promise after it
            webix.require("three.min.js", function () {
                this.promise.resolve();
            }, this);
    }
}, webix.ui.view);

var SchematicView = function (_JetView) {
    _inherits(SchematicView, _JetView);

    function SchematicView() {
        _classCallCheck(this, SchematicView);

        return _possibleConstructorReturn(this, _JetView.apply(this, arguments));
    }

    SchematicView.prototype.config = function config() {
        return { id: "schematic", view: "schematic" };
    };

    return SchematicView;
}(__WEBPACK_IMPORTED_MODULE_0_webix_jet__["b" /* JetView */]);

/* unused harmony default export */ var _unused_webpack_default_export = (SchematicView);
;

/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return schematicStore; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__testJSON_P00_HomePage_json__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__testJSON_P00_HomePage_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__testJSON_P00_HomePage_json__);
/* Schematic store is an example of module based store.
   Currently it's designed to store data of the selected component*/

//temporary for testing. the json file will be loaded by native function


var schematicStore = webix.proto({
  $init: function $init(config) {
    //functions executed on component initialization
    this.gadgets = new Map();
  },
  getDataId: function getDataId(id) {
    return this.config.id + "-" + id;
  },
  getDataValue: function getDataValue(id) {
    var uid = this.config.id + "-" + id;
    return this.getItem(uid).value;
  },
  loadStore: function loadStore() {
    // populate the store. eg:
    __WEBPACK_IMPORTED_MODULE_0__testJSON_P00_HomePage_json___default.a.Layer.forEach(function (item, index, array) {
      console.log(item, index);
    });
  },
  addCubeProps: function addCubeProps(_ref) {
    var _ref$uid = _ref.uid,
        uid = _ref$uid === undefined ? 'unknown' : _ref$uid,
        _ref$name = _ref.name,
        name = _ref$name === undefined ? 'unknown' : _ref$name,
        _ref$wireframe = _ref.wireframe,
        wireframe = _ref$wireframe === undefined ? true : _ref$wireframe,
        _ref$width = _ref.width,
        width = _ref$width === undefined ? 1 : _ref$width,
        _ref$height = _ref.height,
        height = _ref$height === undefined ? 1 : _ref$height,
        _ref$depth = _ref.depth,
        depth = _ref$depth === undefined ? 1 : _ref$depth,
        _ref$fps = _ref.fps,
        fps = _ref$fps === undefined ? 30 : _ref$fps,
        _ref$color = _ref.color,
        color = _ref$color === undefined ? '#00ff00' : _ref$color;

    var props = [{ id: this.getDataId("name"), label: "Name", value: name, type: "text", simple: true }, { id: this.getDataId("width"), label: "Width", value: width, type: "text", simple: true }, { id: this.getDataId("height"), label: "Height", value: height, type: "text", simple: true }, { id: this.getDataId("depth"), label: "Depth", value: depth, type: "text", simple: true }, { id: this.getDataId("wireframe"), label: "Wireframe", value: wireframe, type: "checkbox", simple: false }, { id: this.getDataId("fps"), label: "FPS", value: fps, type: "text", simple: false }, { id: this.getDataId("color"), label: "Color", value: color, type: "color", simple: true }];
    this.gadgets.set(uid, props);
  },
  selectObj: function selectObj(uid) {
    if (this.gadgets.has(uid)) {
      var data = this.gadgets.get(uid);
      this.parse(data);
      $$('propertyStore').updateElements(data);
      $$('propertyStore').data.sync(this.data);
    } else {
      console.log(uid, 'not found!');
    }
  },
  deselectObj: function deselectObj() {
    $$('propertyStore').updateElements([]);
    this.data.unsync();
  }
}, webix.DataCollection);

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = {"decoder":{"version":1},"SchematicHeader":{"version":1,"type":"BSF","size":[2784,1320,500],"backcolor":[0,0,0]},"Layer":[{"version":1,"name":"","value":0,"gadget":[{"version":1,"type":"RECTANGLE","name":"RECTANGLE 0","gadgetNum":2003,"scale":[1,1,1],"rotation":[0,0,0],"position":[-725,225,0],"geometry":{"width":55,"height":35},"animation":{"coloration":0,"blinking":0},"color":[196,196,196],"darkness":100,"opacity":255,"wireframe":0,"data":{"rowid":[0,301991359]}},{"version":1,"type":"RECTANGLE","name":"RECTANGLE 0","gadgetNum":2004,"scale":[1,1,1],"rotation":[0,0,0],"position":[-725,170,0],"geometry":{"width":55,"height":35},"animation":{"coloration":0,"blinking":0},"color":[196,196,196],"darkness":100,"opacity":255,"wireframe":0,"data":{"rowid":[0,301991360]}},{"version":1,"type":"RECTANGLE","name":"RECTANGLE 0","gadgetNum":2005,"scale":[1,1,1],"rotation":[0,0,0],"position":[-725,110,0],"geometry":{"width":55,"height":35},"animation":{"coloration":0,"blinking":0},"color":[196,196,196],"darkness":100,"opacity":255,"wireframe":0,"data":{"rowid":[0,301991361]}},{"version":1,"type":"RECTANGLE","name":"RECTANGLE 0","gadgetNum":2006,"scale":[1,1,1],"rotation":[0,0,0],"position":[-725,50,0],"geometry":{"width":55,"height":35},"animation":{"coloration":0,"blinking":0},"color":[196,196,196],"darkness":100,"opacity":255,"wireframe":0,"data":{"rowid":[0,301991362]}},{"version":1,"type":"RECTANGLE","name":"RECTANGLE 0","gadgetNum":2007,"scale":[1,1,1],"rotation":[0,0,0],"position":[-725,-5,0],"geometry":{"width":55,"height":35},"animation":{"coloration":0,"blinking":0},"color":[196,196,196],"darkness":100,"opacity":255,"wireframe":0,"data":{"rowid":[0,301991363]}},{"version":1,"type":"RECTANGLE","name":"RECTANGLE 0","gadgetNum":2008,"scale":[1,1,1],"rotation":[0,0,0],"position":[-725,-60,0],"geometry":{"width":55,"height":35},"animation":{"coloration":0,"blinking":0},"color":[196,196,196],"darkness":100,"opacity":255,"wireframe":0,"data":{"rowid":[0,301991364]}},{"version":1,"type":"RECTANGLE","name":"RECTANGLE 0","gadgetNum":2009,"scale":[1,1,1],"rotation":[0,0,0],"position":[-725,-115,0],"geometry":{"width":55,"height":35},"animation":{"coloration":0,"blinking":0},"color":[196,196,196],"darkness":100,"opacity":255,"wireframe":0,"data":{"rowid":[0,301991386]}},{"version":1,"type":"RECTANGLE","name":"RECTANGLE 0","gadgetNum":2010,"scale":[1,1,1],"rotation":[0,0,0],"position":[-725,-175,0],"geometry":{"width":55,"height":35},"animation":{"coloration":0,"blinking":0},"color":[196,196,196],"darkness":100,"opacity":255,"wireframe":0,"data":{"rowid":[0,301991389]}},{"version":1,"type":"LABEL","name":"Gadget 0","gadgetNum":2011,"scale":[1,1,1],"rotation":[0,0,0],"position":[-675,255,0],"animation":{"coloration":0,"blinking":0},"color":[255,255,255],"colorbk":[0,0,0],"transparentbk":0,"darkness":100,"text":"Page 1 Map Overview","font":{"family":"Arial","size":18,"bold":0,"italic":0,"underline":0},"frame":{"fixedsize":0,"width":0,"height":0,"alignmenth":"CENTER","alignmentv":"CENTER"},"data":{"rowid":[0,0]}},{"version":1,"type":"LABEL","name":"Gadget 0","gadgetNum":2012,"scale":[1,1,1],"rotation":[0,0,0],"position":[-675,200,0],"animation":{"coloration":0,"blinking":0},"color":[255,255,255],"colorbk":[0,0,0],"transparentbk":0,"darkness":100,"text":"Page 2 Waterworks","font":{"family":"Arial","size":18,"bold":0,"italic":0,"underline":0},"frame":{"fixedsize":0,"width":0,"height":0,"alignmenth":"CENTER","alignmentv":"CENTER"},"data":{"rowid":[0,0]}},{"version":1,"type":"LABEL","name":"Gadget 0","gadgetNum":2013,"scale":[1,1,1],"rotation":[0,0,0],"position":[-675,140,0],"animation":{"coloration":0,"blinking":0},"color":[255,255,255],"colorbk":[0,0,0],"transparentbk":0,"darkness":100,"text":"Page 3 Process example 1","font":{"family":"Arial","size":18,"bold":0,"italic":0,"underline":0},"frame":{"fixedsize":0,"width":0,"height":0,"alignmenth":"CENTER","alignmentv":"CENTER"},"data":{"rowid":[0,0]}},{"version":1,"type":"LABEL","name":"Gadget 0","gadgetNum":2014,"scale":[1,1,1],"rotation":[0,0,0],"position":[-675,80,0],"animation":{"coloration":0,"blinking":0},"color":[255,255,255],"colorbk":[0,0,0],"transparentbk":0,"darkness":100,"text":"Page 4 Process example 2","font":{"family":"Arial","size":18,"bold":0,"italic":0,"underline":0},"frame":{"fixedsize":0,"width":0,"height":0,"alignmenth":"CENTER","alignmentv":"CENTER"},"data":{"rowid":[0,0]}},{"version":1,"type":"LABEL","name":"Gadget 0","gadgetNum":2015,"scale":[1,1,1],"rotation":[0,0,0],"position":[-675,25,0],"animation":{"coloration":0,"blinking":0},"color":[255,255,255],"colorbk":[0,0,0],"transparentbk":0,"darkness":100,"text":"Page 5 IO Status","font":{"family":"Arial","size":18,"bold":0,"italic":0,"underline":0},"frame":{"fixedsize":0,"width":0,"height":0,"alignmenth":"CENTER","alignmentv":"CENTER"},"data":{"rowid":[0,0]}},{"version":1,"type":"LABEL","name":"Gadget 0","gadgetNum":2016,"scale":[1,1,1],"rotation":[0,0,0],"position":[-675,-30,0],"animation":{"coloration":0,"blinking":0},"color":[255,255,255],"colorbk":[0,0,0],"transparentbk":0,"darkness":100,"text":"Page 6 SR Neutralising","font":{"family":"Arial","size":18,"bold":0,"italic":0,"underline":0},"frame":{"fixedsize":0,"width":0,"height":0,"alignmenth":"CENTER","alignmentv":"CENTER"},"data":{"rowid":[0,0]}},{"version":1,"type":"LABEL","name":"Gadget 0","gadgetNum":2017,"scale":[1,1,1],"rotation":[0,0,0],"position":[-675,-85,0],"animation":{"coloration":0,"blinking":0},"color":[255,255,255],"colorbk":[0,0,0],"transparentbk":0,"darkness":100,"text":"Page 7 Popup","font":{"family":"Arial","size":18,"bold":0,"italic":0,"underline":0},"frame":{"fixedsize":0,"width":0,"height":0,"alignmenth":"CENTER","alignmentv":"CENTER"},"data":{"rowid":[0,0]}},{"version":1,"type":"LABEL","name":"Gadget 0","gadgetNum":2018,"scale":[1,1,1],"rotation":[0,0,0],"position":[-675,-145,0],"animation":{"coloration":0,"blinking":0},"color":[255,255,255],"colorbk":[0,0,0],"transparentbk":0,"darkness":100,"text":"Page 8 Zoom Group","font":{"family":"Arial","size":18,"bold":0,"italic":0,"underline":0},"frame":{"fixedsize":0,"width":0,"height":0,"alignmenth":"CENTER","alignmentv":"CENTER"},"data":{"rowid":[0,0]}}]}]}

/***/ }),
/* 32 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* Module Components Data */

/* NOTE: THIS IS TEST DATA */

var data = [{ id: "Box", value: "Box", icon: "box-3d" }, { id: "Sphere", value: "Sphere", icon: "sphere" }, { id: "Cylinder", value: "Cylinder", icon: "cylinder" }, { id: "Cone", value: "Cone", icon: "cone" }, { id: "Torus", value: "Torus", icon: "torus" }, { id: "Tube", value: "Tube", icon: "tube" }, { id: "Rectangle", value: "Rectangle", icon: "rectangle" }, { id: "Triangle", value: "Triangle", icon: "triangle" }, { id: "Ellipse", value: "Ellipse", icon: "ellipse" }, { id: "Polygon", value: "Polygon", icon: "hexagon" }, { id: "Disk", value: "Disk", icon: "disk" }, { id: "Point", value: "Point", icon: "point" }, { id: "Line", value: "Line", icon: "line" }, { id: "Image", value: "Image", icon: "image" }, { id: "Label", value: "Label", icon: "label" }, { id: "Status Flag", value: "Status Flag", icon: "flag-variant" }, { id: "State Gadget", value: "State Gadget", icon: "traffic-light" }, { id: "Camera", value: "Camera", icon: "free-camera" }, { id: "Spot Light", value: "Spot Light", icon: "spotlight" }, { id: "Point Light", value: "Point Light", icon: "lightbulb" }];

/* harmony default export */ __webpack_exports__["a"] = (data);

/***/ }),
/* 33 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export StoreRouter */
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var StoreRouter = function () {
    function StoreRouter(cb, config) {
        _classCallCheck(this, StoreRouter);

        this.name = config.storeName || config.id + ":route";
        this.cb = cb;
    }

    StoreRouter.prototype.set = function set(path, config) {
        var _this = this;

        webix.storage.session.put(this.name, path);
        if (!config || !config.silent) {
            setTimeout(function () {
                return _this.cb(path);
            }, 1);
        }
    };

    StoreRouter.prototype.get = function get() {
        return webix.storage.session.get(this.name);
    };

    return StoreRouter;
}();

/***/ }),
/* 34 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export UrlRouter */
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UrlRouter = function () {
    function UrlRouter(cb, config) {
        _classCallCheck(this, UrlRouter);

        this.cb = cb;
        this.prefix = config.routerPrefix || "";
    }

    UrlRouter.prototype.set = function set(path, config) {
        var _this = this;

        window.history.pushState(null, null, this.prefix + path);
        if (!config || !config.silent) {
            setTimeout(function () {
                return _this.cb(path);
            }, 1);
        }
    };

    UrlRouter.prototype.get = function get() {
        var path = window.location.pathname.replace(this.prefix, "");
        return path !== "/" ? path : "";
    };

    return UrlRouter;
}();

/***/ }),
/* 35 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export EmptyRouter */
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EmptyRouter = function () {
    function EmptyRouter(cb, _$config) {
        _classCallCheck(this, EmptyRouter);

        this.path = "";
        this.cb = cb;
    }

    EmptyRouter.prototype.set = function set(path, config) {
        var _this = this;

        this.path = path;
        if (!config || !config.silent) {
            setTimeout(function () {
                return _this.cb(path);
            }, 1);
        }
    };

    EmptyRouter.prototype.get = function get() {
        return this.path;
    };

    return EmptyRouter;
}();

/***/ }),
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = UnloadGuard;
function UnloadGuard(app, view, config) {
    view.on(app, "app:guard", function (_$url, point, promise) {
        if (point === view || point.contains(view)) {
            var res = config();
            if (res === false) {
                promise.confirm = Promise.reject(res);
            } else {
                promise.confirm = promise.confirm.then(function () {
                    return res;
                });
            }
        }
    });
}

/***/ }),
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Locale;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_node_polyglot_build_polyglot__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_node_polyglot_build_polyglot___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_node_polyglot_build_polyglot__);

function Locale(app, view, config) {
    config = config || {};
    var storage = config.storage;
    var lang = storage ? storage.get("lang") : config.lang || "en";
    var service = {
        _: null,
        polyglot: null,
        getLang: function getLang() {
            return lang;
        },
        setLang: function setLang(name, silent) {
            var data = __webpack_require__(39)("./" + name);
            if (data.__esModule) {
                data = data.default;
            }
            var poly = service.polyglot = new __WEBPACK_IMPORTED_MODULE_0_node_polyglot_build_polyglot___default.a({ phrases: data });
            poly.locale(name);
            service._ = webix.bind(poly.t, poly);
            lang = name;
            if (storage) {
                storage.put("lang", lang);
            }
            if (!silent) {
                app.refresh();
            }
        }
    };
    app.setService("locale", service);
    service.setLang(lang, true);
}

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

//     (c) 2012 Airbnb, Inc.
//
//     polyglot.js may be freely distributed under the terms of the BSD
//     license. For all licensing information, details, and documention:
//     http://airbnb.github.com/polyglot.js
//
//
// Polyglot.js is an I18n helper library written in JavaScript, made to
// work both in the browser and in Node. It provides a simple solution for
// interpolation and pluralization, based off of Airbnb's
// experience adding I18n functionality to its Backbone.js and Node apps.
//
// Polylglot is agnostic to your translation backend. It doesn't perform any
// translation; it simply gives you a way to manage translated phrases from
// your client- or server-side JavaScript application.
//


(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
      return factory(root);
    }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object') {
    module.exports = factory(root);
  } else {
    root.Polyglot = factory(root);
  }
})(this, function (root) {
  'use strict';

  // ### Polyglot class constructor

  function Polyglot(options) {
    options = options || {};
    this.phrases = {};
    this.extend(options.phrases || {});
    this.currentLocale = options.locale || 'en';
    this.allowMissing = !!options.allowMissing;
    this.warn = options.warn || warn;
  }

  // ### Version
  Polyglot.VERSION = '0.4.3';

  // ### polyglot.locale([locale])
  //
  // Get or set locale. Internally, Polyglot only uses locale for pluralization.
  Polyglot.prototype.locale = function (newLocale) {
    if (newLocale) this.currentLocale = newLocale;
    return this.currentLocale;
  };

  // ### polyglot.extend(phrases)
  //
  // Use `extend` to tell Polyglot how to translate a given key.
  //
  //     polyglot.extend({
  //       "hello": "Hello",
  //       "hello_name": "Hello, %{name}"
  //     });
  //
  // The key can be any string.  Feel free to call `extend` multiple times;
  // it will override any phrases with the same key, but leave existing phrases
  // untouched.
  //
  // It is also possible to pass nested phrase objects, which get flattened
  // into an object with the nested keys concatenated using dot notation.
  //
  //     polyglot.extend({
  //       "nav": {
  //         "hello": "Hello",
  //         "hello_name": "Hello, %{name}",
  //         "sidebar": {
  //           "welcome": "Welcome"
  //         }
  //       }
  //     });
  //
  //     console.log(polyglot.phrases);
  //     // {
  //     //   'nav.hello': 'Hello',
  //     //   'nav.hello_name': 'Hello, %{name}',
  //     //   'nav.sidebar.welcome': 'Welcome'
  //     // }
  //
  // `extend` accepts an optional second argument, `prefix`, which can be used
  // to prefix every key in the phrases object with some string, using dot
  // notation.
  //
  //     polyglot.extend({
  //       "hello": "Hello",
  //       "hello_name": "Hello, %{name}"
  //     }, "nav");
  //
  //     console.log(polyglot.phrases);
  //     // {
  //     //   'nav.hello': 'Hello',
  //     //   'nav.hello_name': 'Hello, %{name}'
  //     // }
  //
  // This feature is used internally to support nested phrase objects.
  Polyglot.prototype.extend = function (morePhrases, prefix) {
    var phrase;

    for (var key in morePhrases) {
      if (morePhrases.hasOwnProperty(key)) {
        phrase = morePhrases[key];
        if (prefix) key = prefix + '.' + key;
        if ((typeof phrase === 'undefined' ? 'undefined' : _typeof(phrase)) === 'object') {
          this.extend(phrase, key);
        } else {
          this.phrases[key] = phrase;
        }
      }
    }
  };

  // ### polyglot.clear()
  //
  // Clears all phrases. Useful for special cases, such as freeing
  // up memory if you have lots of phrases but no longer need to
  // perform any translation. Also used internally by `replace`.
  Polyglot.prototype.clear = function () {
    this.phrases = {};
  };

  // ### polyglot.replace(phrases)
  //
  // Completely replace the existing phrases with a new set of phrases.
  // Normally, just use `extend` to add more phrases, but under certain
  // circumstances, you may want to make sure no old phrases are lying around.
  Polyglot.prototype.replace = function (newPhrases) {
    this.clear();
    this.extend(newPhrases);
  };

  // ### polyglot.t(key, options)
  //
  // The most-used method. Provide a key, and `t` will return the
  // phrase.
  //
  //     polyglot.t("hello");
  //     => "Hello"
  //
  // The phrase value is provided first by a call to `polyglot.extend()` or
  // `polyglot.replace()`.
  //
  // Pass in an object as the second argument to perform interpolation.
  //
  //     polyglot.t("hello_name", {name: "Spike"});
  //     => "Hello, Spike"
  //
  // If you like, you can provide a default value in case the phrase is missing.
  // Use the special option key "_" to specify a default.
  //
  //     polyglot.t("i_like_to_write_in_language", {
  //       _: "I like to write in %{language}.",
  //       language: "JavaScript"
  //     });
  //     => "I like to write in JavaScript."
  //
  Polyglot.prototype.t = function (key, options) {
    var phrase, result;
    options = options == null ? {} : options;
    // allow number as a pluralization shortcut
    if (typeof options === 'number') {
      options = { smart_count: options };
    }
    if (typeof this.phrases[key] === 'string') {
      phrase = this.phrases[key];
    } else if (typeof options._ === 'string') {
      phrase = options._;
    } else if (this.allowMissing) {
      phrase = key;
    } else {
      this.warn('Missing translation for key: "' + key + '"');
      result = key;
    }
    if (typeof phrase === 'string') {
      options = clone(options);
      result = choosePluralForm(phrase, this.currentLocale, options.smart_count);
      result = interpolate(result, options);
    }
    return result;
  };

  // ### polyglot.has(key)
  //
  // Check if polyglot has a translation for given key
  Polyglot.prototype.has = function (key) {
    return key in this.phrases;
  };

  // #### Pluralization methods
  // The string that separates the different phrase possibilities.
  var delimeter = '||||';

  // Mapping from pluralization group plural logic.
  var pluralTypes = {
    chinese: function chinese(n) {
      return 0;
    },
    german: function german(n) {
      return n !== 1 ? 1 : 0;
    },
    french: function french(n) {
      return n > 1 ? 1 : 0;
    },
    russian: function russian(n) {
      return n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2;
    },
    czech: function czech(n) {
      return n === 1 ? 0 : n >= 2 && n <= 4 ? 1 : 2;
    },
    polish: function polish(n) {
      return n === 1 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2;
    },
    icelandic: function icelandic(n) {
      return n % 10 !== 1 || n % 100 === 11 ? 1 : 0;
    }
  };

  // Mapping from pluralization group to individual locales.
  var pluralTypeToLanguages = {
    chinese: ['fa', 'id', 'ja', 'ko', 'lo', 'ms', 'th', 'tr', 'zh'],
    german: ['da', 'de', 'en', 'es', 'fi', 'el', 'he', 'hu', 'it', 'nl', 'no', 'pt', 'sv'],
    french: ['fr', 'tl', 'pt-br'],
    russian: ['hr', 'ru'],
    czech: ['cs'],
    polish: ['pl'],
    icelandic: ['is']
  };

  function langToTypeMap(mapping) {
    var type,
        langs,
        l,
        ret = {};
    for (type in mapping) {
      if (mapping.hasOwnProperty(type)) {
        langs = mapping[type];
        for (l in langs) {
          ret[langs[l]] = type;
        }
      }
    }
    return ret;
  }

  // Trim a string.
  function trim(str) {
    var trimRe = /^\s+|\s+$/g;
    return str.replace(trimRe, '');
  }

  // Based on a phrase text that contains `n` plural forms separated
  // by `delimeter`, a `locale`, and a `count`, choose the correct
  // plural form, or none if `count` is `null`.
  function choosePluralForm(text, locale, count) {
    var ret, texts, chosenText;
    if (count != null && text) {
      texts = text.split(delimeter);
      chosenText = texts[pluralTypeIndex(locale, count)] || texts[0];
      ret = trim(chosenText);
    } else {
      ret = text;
    }
    return ret;
  }

  function pluralTypeName(locale) {
    var langToPluralType = langToTypeMap(pluralTypeToLanguages);
    return langToPluralType[locale] || langToPluralType.en;
  }

  function pluralTypeIndex(locale, count) {
    return pluralTypes[pluralTypeName(locale)](count);
  }

  // ### interpolate
  //
  // Does the dirty work. Creates a `RegExp` object for each
  // interpolation placeholder.
  function interpolate(phrase, options) {
    for (var arg in options) {
      if (arg !== '_' && options.hasOwnProperty(arg)) {
        // We create a new `RegExp` each time instead of using a more-efficient
        // string replace so that the same argument can be replaced multiple times
        // in the same phrase.
        phrase = phrase.replace(new RegExp('%\\{' + arg + '\\}', 'g'), options[arg]);
      }
    }
    return phrase;
  }

  // ### warn
  //
  // Provides a warning in the console if a phrase key is missing.
  function warn(message) {
    root.console && root.console.warn && root.console.warn('WARNING: ' + message);
  }

  // ### clone
  //
  // Clone an object.
  function clone(source) {
    var ret = {};
    for (var prop in source) {
      ret[prop] = source[prop];
    }
    return ret;
  }

  return Polyglot;
});

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./en": 18,
	"./en.js": 18
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 39;

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Menu;
function show(view, config, value) {
    if (config.urls) {
        value = config.urls[value] || value;
    }
    view.show("./" + value);
}
function Menu(app, view, config) {
    var ui = view.$$(config.id || config);
    var silent = false;
    ui.attachEvent("onchange", function () {
        if (!silent) {
            show(view, config, this.getValue());
        }
    });
    ui.attachEvent("onafterselect", function () {
        if (!silent) {
            var id = null;
            if (ui.setValue) {
                id = this.getValue();
            } else if (ui.getSelectedId) {
                id = ui.getSelectedId();
            }
            show(view, config, id);
        }
    });
    view.on(app, "app:route", function (url) {
        var segment = url[view.getIndex()];
        if (segment) {
            silent = true;
            var page = segment.page;
            if (ui.setValue && ui.getValue() !== page) {
                ui.setValue(page);
            } else if (ui.select && ui.exists(page) && ui.getSelectedId() !== page) {
                ui.select(page);
            }
            silent = false;
        }
    });
}

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Status;
var baseicons = {
    "good": "check",
    "error": "warning",
    "saving": "refresh fa-spin"
};
var basetext = {
    "good": "Ok",
    "error": "Error",
    "saving": "Connecting..."
};
function Status(app, view, config) {
    var status = "good";
    var count = 0;
    var iserror = false;
    var expireDelay = config.expire;
    if (!expireDelay && expireDelay !== false) expireDelay = 2000;
    var texts = config.texts || basetext;
    var icons = config.icons || baseicons;
    if (typeof config === "string") config = { target: config };
    function refresh(content) {
        var area = view.$$(config.target);
        if (area) {
            if (!content) content = "<div class='status_" + status + "'><span class='webix_icon fa-" + icons[status] + "'></span> " + texts[status] + "</div>";
            area.setHTML(content);
        }
    }
    function success() {
        count--;
        setStatus("good");
    }
    function fail(err) {
        count--;
        setStatus("error", err);
    }
    function start(promise) {
        count++;
        setStatus("saving");
        if (promise && promise.then) {
            promise.then(success, fail);
        }
    }
    function getStatus() {
        return status;
    }
    function hideStatus() {
        if (count == 0) {
            refresh(" ");
        }
    }
    function setStatus(mode, err) {
        if (count < 0) {
            count = 0;
        }
        if (mode === "saving") {
            status = "saving";
            refresh();
        } else {
            iserror = mode === "error";
            if (count === 0) {
                status = iserror ? "error" : "good";
                if (iserror) {
                    app.error("app:error:server", [err.responseText || err]);
                } else {
                    if (expireDelay) setTimeout(hideStatus, expireDelay);
                }
                refresh();
            }
        }
    }
    function track(data) {
        var dp = webix.dp(data);
        if (dp) {
            view.on(dp, "onAfterDataSend", start);
            view.on(dp, "onAfterSaveError", function (id, obj, response) {
                return fail(response);
            });
            view.on(dp, "onAfterSave", success);
        }
    }
    app.setService("status", {
        getStatus: getStatus,
        setStatus: setStatus,
        track: track
    });
    if (config.remote) {
        view.on(webix, "onRemoteCall", start);
    }
    if (config.ajax) {
        view.on(webix, "onBeforeAjax", function (mode, url, data, request, headers, files, promise) {
            start(promise);
        });
    }
    if (config.data) {
        track(config.data);
    }
}

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Theme;
function Theme(app, view, config) {
    config = config || {};
    var storage = config.storage;
    var theme = storage ? storage.get("theme") : config.theme || "flat-default";
    var service = {
        getTheme: function getTheme() {
            return theme;
        },
        setTheme: function setTheme(name, silent) {
            var parts = name.split("-");
            var links = document.getElementsByTagName("link");
            for (var i = 0; i < links.length; i++) {
                var lname = links[i].getAttribute("title");
                if (lname) {
                    if (lname === name || lname === parts[0]) {
                        links[i].rel = "stylesheet";
                        links[i].disabled = false;
                    } else {
                        links[i].rel = "alternate stylesheet";
                        links[i].disabled = true;
                    }
                    webix.skin.set(parts[0]);
                }
                //remove old css
                webix.html.removeCss(document.body, "theme-" + theme);
                //add new css
                webix.html.addCss(document.body, "theme-" + name);
            }
            theme = name;
            if (storage) {
                storage.put("theme", name);
            }
            if (!silent) {
                app.refresh();
            }
        }
    };
    app.setService("theme", service);
    service.setTheme(theme, true);
}

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = User;
function User(app, view, config) {
    config = config || {};
    var login = config.login || "/login";
    var logout = config.logout || "/logout";
    var afterLogin = config.afterLogin || app.config.start;
    var afterLogout = config.afterLogout || "/login";
    var ping = config.ping || 5 * 60 * 1000;
    var model = config.model;
    var user = config.user;
    var service = {
        getUser: function getUser() {
            return user;
        },
        getStatus: function getStatus(server) {
            if (!server) {
                return user !== null;
            }
            return model.status().catch(function () {
                return null;
            }).then(function (data) {
                user = data;
            });
        },
        login: function login(name, pass) {
            return model.login(name, pass).then(function (data) {
                user = data;
                if (!data) {
                    throw "Access denied";
                }
                app.show(afterLogin);
            });
        },
        logout: function logout() {
            user = null;
            return model.logout();
        }
    };
    function canNavigate(url, obj) {
        if (url === logout) {
            service.logout();
            obj.redirect = afterLogout;
        } else if (url !== login && !service.getStatus()) {
            obj.redirect = login;
        }
    }
    app.setService("user", service);
    app.attachEvent("app:guard", function (url, _$root, obj) {
        if (typeof user === "undefined") {
            obj.confirm = service.getStatus(true).then(function (some) {
                return canNavigate(url, obj);
            });
        }
        return canNavigate(url, obj);
    });
    if (ping) {
        setInterval(function () {
            return service.getStatus(true);
        }, ping);
    }
}

/***/ }),
/* 44 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Copyright (C) 2016 Klar�lvdalens Datakonsult AB, a KDAB Group company, info@kdab.com, author Milian Wolff <milian.wolff@kdab.com>
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtWebChannel module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or (at your option) the GNU General
** Public license version 3 or any later version approved by the KDE Free
** Qt Foundation. The licenses are as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html and
** https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

var QWebChannelMessageTypes = {
    signal: 1,
    propertyUpdate: 2,
    init: 3,
    idle: 4,
    debug: 5,
    invokeMethod: 6,
    connectToSignal: 7,
    disconnectFromSignal: 8,
    setProperty: 9,
    response: 10
};

/* harmony default export */ __webpack_exports__["a"] = (function (transport, initCallback) {

    if ((typeof transport === 'undefined' ? 'undefined' : _typeof(transport)) !== 'object' || typeof transport.send !== 'function') {
        console.error('The QWebChannel expects a transport object with a send function and onmessage callback property.' + ' Given is: transport: ' + (typeof transport === 'undefined' ? 'undefined' : _typeof(transport)) + ', transport.send: ' + _typeof(transport.send));
        return;
    }

    var channel = this;
    this.transport = transport;

    this.send = function (data) {

        if (typeof data !== 'string') {
            data = JSON.stringify(data);
        }
        channel.transport.send(data);
    };

    this.transport.onmessage = function (message) {

        var data = message.data;
        if (typeof data === 'string') {
            data = JSON.parse(data);
        }
        switch (data.type) {
            case QWebChannelMessageTypes.signal:
                channel.handleSignal(data);
                break;
            case QWebChannelMessageTypes.response:
                channel.handleResponse(data);
                break;
            case QWebChannelMessageTypes.propertyUpdate:
                channel.handlePropertyUpdate(data);
                break;
            default:
                console.error('invalid message received:', message.data);
                break;
        }
    };

    this.execCallbacks = {};

    this.execId = 0;

    this.exec = function (data, callback) {

        if (!callback) {
            // if no callback is given, send directly
            channel.send(data);
            return;
        }
        if (channel.execId === Number.MAX_VALUE) {
            // wrap
            channel.execId = Number.MIN_VALUE;
        }
        if (data.hasOwnProperty('id')) {
            console.error('Cannot exec message with property id: ' + JSON.stringify(data));
            return;
        }
        data.id = channel.execId++;
        channel.execCallbacks[data.id] = callback;
        channel.send(data);
    };

    this.objects = {};

    this.handleSignal = function (message) {

        var object = channel.objects[message.object];
        if (object) {
            object.signalEmitted(message.signal, message.args);
        } else {
            console.warn('Unhandled signal: ' + message.object + '::' + message.signal);
        }
    };

    this.handleResponse = function (message) {

        if (!message.hasOwnProperty('id')) {
            console.error('Invalid response message received: ', JSON.stringify(message));
            return;
        }
        channel.execCallbacks[message.id](message.data);
        delete channel.execCallbacks[message.id];
    };

    this.handlePropertyUpdate = function (message) {

        for (var i in message.data) {
            var data = message.data[i];
            var object = channel.objects[data.object];
            if (object) {
                object.propertyUpdate(data.signals, data.properties);
            } else {
                console.warn('Unhandled property update: ' + data.object + '::' + data.signal);
            }
        }
        channel.exec({ type: QWebChannelMessageTypes.idle });
    };

    this.debug = function (message) {
        channel.send({ type: QWebChannelMessageTypes.debug, data: message });
    };

    channel.exec({ type: QWebChannelMessageTypes.init }, function (data) {

        for (var objectName in data) {
            /*let object = */new QObject(objectName, data[objectName], channel);
        }
        // now unwrap properties, which might reference other registered objects
        for (var _objectName in channel.objects) {
            channel.objects[_objectName].unwrapProperties();
        }
        if (initCallback) {
            initCallback(channel);
        }
        channel.exec({ type: QWebChannelMessageTypes.idle });
    });
});

function QObject(name, data, webChannel) {
    this.__id__ = name;
    webChannel.objects[name] = this;

    // List of callbacks that get invoked upon signal emission
    this.__objectSignals__ = {};

    // Cache of all properties, updated when a notify signal is emitted
    this.__propertyCache__ = {};

    var object = this;

    // ----------------------------------------------------------------------

    this.unwrapQObject = function (response) {
        if (response instanceof Array) {
            // support list of objects
            var ret = new Array(response.length);
            for (var i = 0; i < response.length; ++i) {
                ret[i] = object.unwrapQObject(response[i]);
            }
            return ret;
        }
        if (!response || !response['__QObject*__'] || response.id === undefined) {
            return response;
        }

        var objectId = response.id;
        if (webChannel.objects[objectId]) {
            return webChannel.objects[objectId];
        }

        if (!response.data) {
            console.error('Cannot unwrap unknown QObject ' + objectId + ' without data.');
            return;
        }

        var qObject = new QObject(objectId, response.data, webChannel);
        qObject.destroyed.connect(function () {
            if (webChannel.objects[objectId] === qObject) {
                delete webChannel.objects[objectId];
                // reset the now deleted QObject to an empty {} object
                // just assigning {} though would not have the desired effect, but the
                // below also ensures all external references will see the empty map
                // NOTE: this detour is necessary to workaround QTBUG-40021
                var propertyNames = [];
                for (var propertyName in qObject) {
                    propertyNames.push(propertyName);
                }
                for (var idx in propertyNames) {
                    delete qObject[propertyNames[idx]];
                }
            }
        });
        // here we are already initialized, and thus must directly unwrap the properties
        qObject.unwrapProperties();
        return qObject;
    };

    this.unwrapProperties = function () {

        for (var propertyIdx in object.__propertyCache__) {
            object.__propertyCache__[propertyIdx] = object.unwrapQObject(object.__propertyCache__[propertyIdx]);
        }
    };

    function addSignal(signalData, isPropertyNotifySignal) {
        var signalName = signalData[0];
        var signalIndex = signalData[1];
        object[signalName] = {
            connect: function connect(callback) {
                if (typeof callback !== 'function') {
                    console.error('Bad callback given to connect to signal ' + signalName);
                    return;
                }

                object.__objectSignals__[signalIndex] = object.__objectSignals__[signalIndex] || [];
                object.__objectSignals__[signalIndex].push(callback);

                if (!isPropertyNotifySignal && signalName !== 'destroyed') {
                    // only required for "pure" signals, handled separately for properties in propertyUpdate
                    // also note that we always get notified about the destroyed signal
                    webChannel.exec({
                        type: QWebChannelMessageTypes.connectToSignal,
                        object: object.__id__,
                        signal: signalIndex
                    });
                }
            },
            disconnect: function disconnect(callback) {
                if (typeof callback !== 'function') {
                    console.error('Bad callback given to disconnect from signal ' + signalName);
                    return;
                }
                object.__objectSignals__[signalIndex] = object.__objectSignals__[signalIndex] || [];
                var idx = object.__objectSignals__[signalIndex].indexOf(callback);
                if (idx === -1) {
                    console.error('Cannot find connection of signal ' + signalName + ' to ' + callback.name);
                    return;
                }
                object.__objectSignals__[signalIndex].splice(idx, 1);
                if (!isPropertyNotifySignal && object.__objectSignals__[signalIndex].length === 0) {
                    // only required for "pure" signals, handled separately for properties in propertyUpdate
                    webChannel.exec({
                        type: QWebChannelMessageTypes.disconnectFromSignal,
                        object: object.__id__,
                        signal: signalIndex
                    });
                }
            }
        };
    }

    /**
     * Invokes all callbacks for the given signalname. Also works for property notify callbacks.
     */
    function invokeSignalCallbacks(signalName, signalArgs) {

        var connections = object.__objectSignals__[signalName];

        if (connections) {
            connections.forEach(function (callback) {
                callback.apply(callback, signalArgs);
            });
        }
    }

    this.propertyUpdate = function (signals, propertyMap) {
        // update property cache
        for (var propertyIndex in propertyMap) {
            var propertyValue = propertyMap[propertyIndex];
            object.__propertyCache__[propertyIndex] = propertyValue;
        }

        for (var signalName in signals) {
            // Invoke all callbacks, as signalEmitted() does not. This ensures the
            // property cache is updated before the callbacks are invoked.
            invokeSignalCallbacks(signalName, signals[signalName]);
        }
    };

    this.signalEmitted = function (signalName, signalArgs) {
        invokeSignalCallbacks(signalName, this.unwrapQObject(signalArgs));
    };

    function addMethod(methodData) {
        var methodName = methodData[0];
        var methodIdx = methodData[1];
        object[methodName] = function () {
            var args = [];
            var callback = void 0;
            for (var i = 0; i < arguments.length; ++i) {
                var argument = arguments[i];
                if (typeof argument === 'function') {
                    callback = argument;
                } else if (argument instanceof QObject && webChannel.objects[argument.__id__] !== undefined) {
                    args.push({
                        'id': argument.__id__
                    });
                } else {
                    args.push(argument);
                }
            }

            webChannel.exec({
                type: QWebChannelMessageTypes.invokeMethod,
                object: object.__id__,
                method: methodIdx,
                args: args
            }, function (response) {
                if (response !== undefined) {
                    var result = object.unwrapQObject(response);
                    if (callback) {
                        callback(result);
                    }
                }
            });
        };
    }

    function bindGetterSetter(propertyInfo) {
        var propertyIndex = propertyInfo[0];
        var propertyName = propertyInfo[1];
        var notifySignalData = propertyInfo[2];
        // initialize property cache with current value
        // NOTE: if this is an object, it is not directly unwrapped as it might
        // reference other QObject that we do not know yet
        object.__propertyCache__[propertyIndex] = propertyInfo[3];

        if (notifySignalData) {
            if (notifySignalData[0] === 1) {
                // signal name is optimized away, reconstruct the actual name
                notifySignalData[0] = propertyName + 'Changed';
            }
            addSignal(notifySignalData, true);
        }

        Object.defineProperty(object, propertyName, {
            configurable: true,
            get: function get() {
                var propertyValue = object.__propertyCache__[propertyIndex];
                if (propertyValue === undefined) {
                    // This shouldn't happen
                    console.warn('Undefined value in property cache for property \'' + propertyName + '\' in object ' + object.__id__);
                }

                return propertyValue;
            },
            set: function set(value) {
                if (value === undefined) {
                    console.warn('Property setter for ' + propertyName + ' called with undefined value!');
                    return;
                }
                object.__propertyCache__[propertyIndex] = value;
                var valueToSend = value;
                if (valueToSend instanceof QObject && webChannel.objects[valueToSend.__id__] !== undefined) {
                    valueToSend = { 'id': valueToSend.__id__ };
                }
                webChannel.exec({
                    type: QWebChannelMessageTypes.setProperty,
                    object: object.__id__,
                    property: propertyIndex,
                    value: valueToSend
                });
            }
        });
    }

    // ----------------------------------------------------------------------

    data.methods.forEach(addMethod);

    data.properties.forEach(bindGetterSetter);

    data.signals.forEach(function (signal) {
        addSignal(signal, false);
    });

    for (var _name in data.enums) {
        object[_name] = data.enums[_name];
    }
}

//required for use with nodejs
// if (typeof module === 'object') {
//     module.exports = {
//         QWebChannel: QWebChannel
//     };
// }

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYTcyOTAwZTg1MjExNzMxNWI0ZmYiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L0pldFZpZXcuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy92aWV3cy9tZW51L21lbnUuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy92aWV3cy9tZW51L2Ryb3BEb3duTWVudXMuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy92aWV3cy9tZW51L2ljb25NZW51LmpzIiwid2VicGFjazovLy8uL3NvdXJjZXMvdmlld3Mvc2lkZWJhci9zaWRlYmFyLmpzIiwid2VicGFjazovLy8uL3NvdXJjZXMvdmlld3Mvc2lkZWJhci9wcm9qZWN0RXhwbG9yZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy92aWV3cy9zaWRlYmFyL3Byb3BlcnR5TGlzdC5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2VzL3ZpZXdzL21vZHVsZS9tb2R1bGUuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy92aWV3cy9tb2R1bGUvbW9kdWxlVG9vbHMuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy92aWV3cy9tb2R1bGUvbW9kdWxlVGFiYmFyQ2VsbC5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2VzL3ZpZXdzL21vZHVsZS9tb2R1bGVDb21wb25lbnRzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy93ZWJpeC1qZXQvSmV0QmFzZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L2hlbHBlcnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9yb3V0ZXJzL0hhc2hSb3V0ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy92aWV3cy9sYXlvdXQuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy92aWV3cy9tZW51L2ZpbGVNZW51LmpzIiwid2VicGFjazovLy8uL3NvdXJjZXMvc3R5bGVzL2FwcC5jc3MiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9KZXRBcHAuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9KZXRWaWV3TGVnYWN5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy93ZWJpeC1qZXQvSmV0Vmlld1Jhdy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvd2ViaXgtcm91dGllL2xpYi9yb3V0aWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9wYXRjaC5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2VzL3ZpZXdzIF5cXC5cXC8uKiQiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy9tb2RlbHMvbWVudURhdGEuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy9tb2RlbHMvcHJvamVjdFN0b3JlLmpzIiwid2VicGFjazovLy8uL3NvdXJjZXMvbW9kZWxzL3Byb3BlcnR5U3RvcmUuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy9tb2R1bGVzL3NjaGVtYXRpYy92aWV3cy9zY2hlbWF0aWMuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy9tb2R1bGVzL3NjaGVtYXRpYy9tb2RlbHMvc2NoZW1hdGljU3RvcmUuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlcy9tb2R1bGVzL3NjaGVtYXRpYy90ZXN0SlNPTi9QMDBfSG9tZVBhZ2UuanNvbiIsIndlYnBhY2s6Ly8vLi9zb3VyY2VzL21vZGVscy9jb21wb25lbnREYXRhLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy93ZWJpeC1qZXQvcm91dGVycy9TdG9yZVJvdXRlci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3JvdXRlcnMvVXJsUm91dGVyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy93ZWJpeC1qZXQvcm91dGVycy9FbXB0eVJvdXRlci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3BsdWdpbnMvR3VhcmQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9wbHVnaW5zL0xvY2FsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbm9kZS1wb2x5Z2xvdC9idWlsZC9wb2x5Z2xvdC5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2VzL2xvY2FsZXMgXlxcLlxcLy4qJCIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3BsdWdpbnMvTWVudS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3BsdWdpbnMvU3RhdHVzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy93ZWJpeC1qZXQvcGx1Z2lucy9UaGVtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3BsdWdpbnMvVXNlci5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2VzL3V0aWxzL3F3ZWJjaGFubmVsLmpzIl0sIm5hbWVzIjpbInBsdWdpbnMiLCJVbmxvYWRHdWFyZCIsIkxvY2FsZSIsIk1lbnUiLCJUaGVtZSIsIlVzZXIiLCJTdGF0dXMiLCJ3aW5kb3ciLCJQcm9taXNlIiwid2ViaXgiLCJwcm9taXNlIiwiSmV0VmlldyIsImFwcCIsIm5hbWUiLCJfbmFtZSIsIl9jaGlsZHJlbiIsInVpIiwiamV0dmlldyIsInB1c2giLCJyZW5kZXIiLCJ2aWV3Iiwic2hvdyIsInBhdGgiLCJjb25maWciLCJzdWJzdHIiLCJpbmRleE9mIiwicGFyZW50IiwiZ2V0UGFyZW50VmlldyIsInN1YiIsImdldFN1YlZpZXciLCJuZXdDaHVuayIsInBhcnNlIiwidXJsIiwiY3VycmVudFVybCIsImdldFJvdXRlciIsImdldCIsIl9pbmRleCIsInNsaWNlIiwiY29uY2F0IiwiaSIsImxlbmd0aCIsImluZGV4IiwidXJsc3RyIiwidXJsMnN0ciIsImNhbk5hdmlnYXRlIiwidGhlbiIsInJlZGlyZWN0IiwiX2ZpbmlzaFNob3ciLCJzdWJ2aWV3IiwiY2F0Y2giLCJpbml0IiwiXyR2aWV3IiwiXyR1cmwiLCJyZWFkeSIsIm1lc3NhZ2UiLCJ1cmxDaGFuZ2UiLCJkZXN0cm95IiwiZGVzdHJ1Y3RvciIsInVpcyIsIl9yb290IiwidXNlIiwicGx1Z2luIiwiX3JlbmRlciIsInJlc3BvbnNlIiwicmVzdWx0IiwiY29weUNvbmZpZyIsIl9zdWJzIiwiY2FsbEV2ZW50IiwiJHNjb3BlIiwiX2NvbnRhaW5lciIsIl9pbml0IiwiX3VybENoYW5nZSIsImUiLCJyZWplY3QiLCJfaW5pdEVycm9yIiwiZXJyIiwid2FpdHMiLCJrZXkiLCJmcmFtZSIsInBhcnNlZCIsIl9jcmVhdGVTdWJWaWV3IiwiX3JlbmRlclN1YlZpZXciLCJzdWJ1cmwiLCJhbGwiLCJlcnJvciIsImNyZWF0ZUZyb21VUkwiLCJjZWxsIiwiJCQiLCJpZCIsInNldCIsInNpbGVudCIsInZlcnNpb24iLCJzdGFydCIsImF0dGFjaEV2ZW50c1RvVUkiLCJhdHRhY2hFdmVudCIsImFyZ3MiLCJjb25zb2xlIiwibmF0aXZlIiwibmF0aXZlUHJvbWlzZSIsInJlc29sdmUiLCJxdCIsIndlYkNoYW5uZWxUcmFuc3BvcnQiLCJjaGFubmVsIiwiaW5mbyIsIm9iamVjdHMiLCJVdGlscyIsInNjcmVlbkluZm8iLCJKU09OIiwic3RyaW5naWZ5Iiwic2hvd01lc3NhZ2UiLCJsb2dNZXNzYWdlIiwiTWVudVZpZXciLCJtZW51Iiwicm93cyIsIkRyb3BEb3duTWVudVZpZXciLCJkcm9wRG93bk1lbnUiLCJjb2xzIiwid2lkdGgiLCJib3JkZXJsZXNzIiwic3VibWVudUNvbmZpZyIsImRhdGEiLCJ2YWx1ZSIsInN1Ym1lbnUiLCJmaWxlTWVudSIsImVkaXRNZW51Iiwidmlld01lbnUiLCJkcmF3TWVudSIsIm1vZGlmeU1lbnUiLCJoZWxwTWVudSIsIm9uIiwib25NZW51SXRlbUNsaWNrIiwiZ2V0TWVudUl0ZW0iLCJsb2ciLCJleGl0QXBwIiwidGVzdENhbGxQcm9taXNlIiwidGVzdENhbGwzIiwic3VtIiwidmVyIiwidHlwZSIsInN1YnNpZ24iLCJJY29uTWVudVZpZXciLCJpY29uTWVudSIsIm1pbkhlaWdodCIsIm1heEhlaWdodCIsImhlaWdodCIsImVsZW1lbnRzIiwiaWNvbiIsImNzcyIsImNsaWNrIiwidGVzdGNhbGwyIiwiU2lkZWJhclZpZXciLCJzaWRlYmFyIiwibWluV2lkdGgiLCJQcm9qZWN0RXhwbG9yZXJWaWV3IiwicHJvamVjdEV4cGxvcmVyIiwidGVtcGxhdGUiLCJvbkZvY3VzIiwiZ2V0VmFsdWUiLCJzZXRWYWx1ZSIsIm9uQmx1ciIsInNjcm9sbCIsImJvZHkiLCJkcmFnIiwic3luYyIsIm9uSXRlbURibENsaWNrIiwiZ2V0SXRlbSIsImlzRm9sZGVyIiwic3RhdGUiLCJvcGVuZWQiLCJ1cGRhdGVJdGVtIiwiaGlkZGVuIiwiUHJvcGVydHlMaXN0VmlldyIsInByb3BlcnR5TGlzdCIsIm9mZkljb24iLCJvbkljb24iLCJvbkNoYW5nZSIsIm5ld3YiLCJvbGR2IiwicHJvcGVydHlTdG9yZSIsInNob3dBbGwiLCJvblRpbWVkS2V5UHJlc3MiLCJmaWx0ZXJMYWJlbCIsInRvTG93ZXJDYXNlIiwiZWRpdG9yIiwiaWdub3JlVXBkYXRlIiwiZ2V0SW5kZXhCeUlkIiwiZGVmaW5lIiwic2VyaWFsaXplIiwicmVmcmVzaCIsIk1vZHVsZVZpZXciLCJtb2R1bGUiLCJNb2R1bGVUb29sc1ZpZXciLCJtb2R1bGVUb29scyIsImlzVmlzaWJsZSIsImhpZGUiLCJ0b2dnbGUiLCJyZXNpemUiLCJNb2R1bGVUYWJiYXJDZWxsVmlldyIsInRhYnZpZXciLCJhbmltYXRlIiwiY2VsbHMiLCJsYWJlbEFsaWduIiwidGFiT2Zmc2V0IiwidGFyZ2V0IiwiY2xhc3NOYW1lIiwic2hvd01vZHVsZSIsIm9uQmVmb3JlVGFiQ2xvc2UiLCJyZW1vdmVWaWV3Iiwib3B0aW9ucyIsIm11bHRpdmlldyIsIm1heExlbmd0aCIsImFkZFZpZXciLCJhZGRPcHRpb24iLCJjbG9zZSIsInRhYklEIiwiTW9kdWxlQ29tcG9uZW50c1ZpZXciLCJjb21wb25lbnRzIiwicG9zaXRpb24iLCJjb2xsYXBzZWQiLCJvbkFmdGVyU2VsZWN0IiwibG9jYXRlIiwiZ2V0Q2hpbGRWaWV3cyIsImtpZCIsImxvY2FsSWQiLCJKZXRCYXNlIiwiX2lkIiwidWlkIiwiX2V2ZW50cyIsImdldFJvb3QiLCJldmVudHMiLCJvYmoiLCJkZXRhY2hFdmVudCIsInN1YlZpZXciLCJfYXBwIiwiX3BhcmVudCIsInJvb3QiLCJkb2N1bWVudCIsInRvTm9kZSIsImdldEluZGV4IiwiZ2V0SWQiLCJjb2RlIiwiY29udGFpbnMiLCJnZXROYW1lIiwiZGlmZiIsIm9VcmwiLCJuVXJsIiwibGVmdCIsInJpZ2h0IiwicGFnZSIsInBhcmFtcyIsInBhcnRzIiwic3BsaXQiLCJjaHVua3MiLCJ0ZXN0IiwicG9zIiwicGFyYW0iLCJkY2h1bmsiLCJzdGFjayIsImNodW5rIiwib2JqMnN0ciIsImpvaW4iLCJzdHIiLCJIYXNoUm91dGVyIiwiY2IiLCJyY2IiLCJfJGEiLCJyb3V0aWUiLCJfbGFzdFVybCIsInJvdXRlcyIsImNvbXBhcmUiLCJuYXZpZ2F0ZSIsImxvY2F0aW9uIiwiaGFzaCIsInJlcGxhY2UiLCJXaWxsb3dMeW54RGVzaWduZXIiLCJKZXRBcHAiLCJleHRlbmQiLCJfc2VydmljZXMiLCJFdmVudFN5c3RlbSIsImNsaWNrSGFuZGxlciIsImdldFNlcnZpY2UiLCJzZXRTZXJ2aWNlIiwiaGFuZGxlciIsIiR1aSIsIiRzdWJ2aWV3IiwiYWRkU3ViVmlldyIsIkFycmF5IiwibWV0aG9kIiwicG9pbnQiLCJwcm90b3R5cGUiLCJEYXRhQ29sbGVjdGlvbiIsIkRhdGUiLCIkcm91dGVyIiwic3JjRWxlbWVudCIsImdldEF0dHJpYnV0ZSIsInRyaWdnZXIiLCJyb3V0ZSIsInRlbXAiLCJfdmlldyIsImxvYWRWaWV3Iiwidmlld3MiLCJfX2VzTW9kdWxlIiwiZGVmYXVsdCIsIl9sb2FkRXJyb3IiLCJub3ciLCJjb25maXJtIiwicmVzIiwicmVzdCIsImFwcGx5IiwiYWN0aW9uIiwiYmluZCIsImVyIiwiZGVidWciLCJmaXJzdEluaXQiLCJfZmlyc3Rfc3RhcnQiLCJzdHJVcmwiLCJuZXd1cmwiLCJfcmVuZGVyX3N0YWdlIiwiZnJlZXplIiwib2xkdmlldyIsImFsZXJ0Iiwic2V0VGltZW91dCIsImEiLCJyb3V0ZXIiLCJhbmltYXRpb24iLCJub2RlIiwiaHRtbCIsImFkZENzcyIsInJlbW92ZUNzcyIsIkpldFZpZXdMZWdhY3kiLCJfdWkiLCJfd2luZG93cyIsImpldDF4TW9kZSIsIiRvbmRlc3Ryb3kiLCJiIiwibGVnYWN5RWFybHlJbml0IiwiX3JlYWxJbml0SGFuZGxlciIsIiRvbmluaXQiLCIkb25ldmVudCIsIndpbmRvd3MiLCIkd2luZG93cyIsImNvbmYiLCJvbnVybGNoYW5nZSIsIiRvbnVybGNoYW5nZSIsIkpldFZpZXdSYXciLCJSb3V0aWUiLCJ3IiwiaXNNb2R1bGUiLCJtYXAiLCJyZWZlcmVuY2UiLCJvbGRSZWZlcmVuY2UiLCJvbGRVcmwiLCJSb3V0ZSIsImtleXMiLCJmbnMiLCJyZWdleCIsInBhdGhUb1JlZ2V4cCIsImFkZEhhbmRsZXIiLCJmbiIsInJlbW92ZUhhbmRsZXIiLCJjIiwiZiIsInNwbGljZSIsInJ1biIsIm1hdGNoIiwibSIsImV4ZWMiLCJsZW4iLCJ2YWwiLCJkZWNvZGVVUklDb21wb25lbnQiLCJ0b1VSTCIsIkVycm9yIiwic2Vuc2l0aXZlIiwic3RyaWN0IiwiUmVnRXhwIiwiXyIsInNsYXNoIiwiZm9ybWF0IiwiY2FwdHVyZSIsIm9wdGlvbmFsIiwicyIsInJlbG9hZCIsInAiLCJsb29rdXAiLCJyZW1vdmUiLCJyZW1vdmVBbGwiLCJyZW1vdmVMaXN0ZW5lciIsImFkZExpc3RlbmVyIiwibm9Db25mbGljdCIsImdldEhhc2giLCJzdWJzdHJpbmciLCJjaGVja1JvdXRlIiwiaGFzaENoYW5nZWQiLCJhZGRFdmVudExpc3RlbmVyIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImV4cG9ydHMiLCJzb21lIiwiJGZyZWV6ZSIsInZpZXdzU3VibWVudSIsImFjdGl2ZVZpZXdTdWJtZW51IiwidHdvRFN1Ym1lbnUiLCJ0aHJlZURTdWJtZW51IiwiYWxpZ25tZW50U3VibWVudSIsInByb2plY3RTdG9yZUNsYXNzIiwicHJvdG8iLCIkaW5pdCIsImxvYWRTdG9yZSIsIlRyZWVDb2xsZWN0aW9uIiwicHJvamVjdFN0b3JlIiwib3BlbiIsInByb3BlcnR5U3RvcmVDbGFzcyIsInNob3dBbGxQcm9wcyIsImZpbHRlcklucHV0IiwiZGVmYXVsdHMiLCJ1cGRhdGVFbGVtZW50cyIsImNsZWFyQWxsIiwic3RhdHVzIiwiZmlsdGVyIiwibGFiZWwiLCJzaW1wbGUiLCJwcm90b1VJIiwiZGVmZXIiLCJzdG9yZWlkIiwic3RvcmUiLCJzY2VuZSIsIlRIUkVFIiwiU2NlbmUiLCJjYW1lcmEiLCJQZXJzcGVjdGl2ZUNhbWVyYSIsImlubmVyV2lkdGgiLCJpbm5lckhlaWdodCIsInJheWNhc3RlciIsIlJheWNhc3RlciIsInJlbmRlcmVyIiwiV2ViR0xSZW5kZXJlciIsInNldFNpemUiLCIkd2lkdGgiLCIkaGVpZ2h0IiwiJHZpZXciLCJhcHBlbmRDaGlsZCIsImRvbUVsZW1lbnQiLCJjdWJlMSIsIndpcmVmcmFtZSIsImRlcHRoIiwiZnBzIiwiY29sb3IiLCJnZW9tZXRyeSIsIkJveEdlb21ldHJ5IiwibWF0ZXJpYWwiLCJNZXNoQmFzaWNNYXRlcmlhbCIsInNldEhleCIsImN1YmUiLCJNZXNoIiwiYWRkIiwiYWRkQ3ViZVByb3BzIiwib3V0bGluZU1hdGVyaWFsIiwib3V0bGluZSIsInoiLCJzdG9wIiwiZnBzSW50ZXJ2YWwiLCJsYXN0RHJhd1RpbWUiLCJnZXREYXRhSWQiLCJuZXdfZnBzIiwic2NhbGUiLCJzZXRYIiwic2V0WSIsInNldFoiLCJtb3VzZSIsIlZlY3RvcjIiLCJJTlRFUlNFQ1RFRCIsInN0YXJ0QW5pbWF0aW5nIiwicGVyZm9ybWFuY2UiLCJyZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJlbGFwc2VkIiwicm90YXRpb24iLCJ4IiwieSIsImdldE9iamVjdEJ5TmFtZSIsImNvcHkiLCJvbldpbmRvd1Jlc2l6ZSIsImFzcGVjdCIsInVwZGF0ZVByb2plY3Rpb25NYXRyaXgiLCJvbk1vdXNlRG93biIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJjbGllbnRYIiwiY2xpZW50WSIsInNldEZyb21DYW1lcmEiLCJpbnRlcnNlY3RzIiwiaW50ZXJzZWN0T2JqZWN0cyIsImNoaWxkcmVuIiwib2JqZWN0Iiwic2VsZWN0T2JqIiwiZGVzZWxlY3RPYmoiLCJjYWxsIiwicmVxdWlyZSIsIlNjaGVtYXRpY1ZpZXciLCJzY2hlbWF0aWNTdG9yZSIsImdhZGdldHMiLCJNYXAiLCJnZXREYXRhVmFsdWUiLCJ0ZXN0RmlsZSIsIkxheWVyIiwiZm9yRWFjaCIsIml0ZW0iLCJhcnJheSIsInByb3BzIiwiaGFzIiwidW5zeW5jIiwiU3RvcmVSb3V0ZXIiLCJzdG9yZU5hbWUiLCJzdG9yYWdlIiwic2Vzc2lvbiIsInB1dCIsIlVybFJvdXRlciIsInByZWZpeCIsInJvdXRlclByZWZpeCIsImhpc3RvcnkiLCJwdXNoU3RhdGUiLCJwYXRobmFtZSIsIkVtcHR5Um91dGVyIiwiXyRjb25maWciLCJsYW5nIiwic2VydmljZSIsInBvbHlnbG90IiwiZ2V0TGFuZyIsInNldExhbmciLCJwb2x5IiwicGhyYXNlcyIsImxvY2FsZSIsInQiLCJmYWN0b3J5IiwiUG9seWdsb3QiLCJjdXJyZW50TG9jYWxlIiwiYWxsb3dNaXNzaW5nIiwid2FybiIsIlZFUlNJT04iLCJuZXdMb2NhbGUiLCJtb3JlUGhyYXNlcyIsInBocmFzZSIsImhhc093blByb3BlcnR5IiwiY2xlYXIiLCJuZXdQaHJhc2VzIiwic21hcnRfY291bnQiLCJjbG9uZSIsImNob29zZVBsdXJhbEZvcm0iLCJpbnRlcnBvbGF0ZSIsImRlbGltZXRlciIsInBsdXJhbFR5cGVzIiwiY2hpbmVzZSIsIm4iLCJnZXJtYW4iLCJmcmVuY2giLCJydXNzaWFuIiwiY3plY2giLCJwb2xpc2giLCJpY2VsYW5kaWMiLCJwbHVyYWxUeXBlVG9MYW5ndWFnZXMiLCJsYW5nVG9UeXBlTWFwIiwibWFwcGluZyIsImxhbmdzIiwibCIsInJldCIsInRyaW0iLCJ0cmltUmUiLCJ0ZXh0IiwiY291bnQiLCJ0ZXh0cyIsImNob3NlblRleHQiLCJwbHVyYWxUeXBlSW5kZXgiLCJwbHVyYWxUeXBlTmFtZSIsImxhbmdUb1BsdXJhbFR5cGUiLCJlbiIsImFyZyIsInNvdXJjZSIsInByb3AiLCJ1cmxzIiwiZ2V0U2VsZWN0ZWRJZCIsInNlZ21lbnQiLCJzZWxlY3QiLCJleGlzdHMiLCJiYXNlaWNvbnMiLCJiYXNldGV4dCIsImlzZXJyb3IiLCJleHBpcmVEZWxheSIsImV4cGlyZSIsImljb25zIiwiY29udGVudCIsImFyZWEiLCJzZXRIVE1MIiwic3VjY2VzcyIsInNldFN0YXR1cyIsImZhaWwiLCJnZXRTdGF0dXMiLCJoaWRlU3RhdHVzIiwibW9kZSIsInJlc3BvbnNlVGV4dCIsInRyYWNrIiwiZHAiLCJyZW1vdGUiLCJhamF4IiwicmVxdWVzdCIsImhlYWRlcnMiLCJmaWxlcyIsInRoZW1lIiwiZ2V0VGhlbWUiLCJzZXRUaGVtZSIsImxpbmtzIiwiZ2V0RWxlbWVudHNCeVRhZ05hbWUiLCJsbmFtZSIsInJlbCIsImRpc2FibGVkIiwic2tpbiIsImxvZ2luIiwibG9nb3V0IiwiYWZ0ZXJMb2dpbiIsImFmdGVyTG9nb3V0IiwicGluZyIsIm1vZGVsIiwidXNlciIsImdldFVzZXIiLCJzZXJ2ZXIiLCJwYXNzIiwiXyRyb290Iiwic2V0SW50ZXJ2YWwiLCJRV2ViQ2hhbm5lbE1lc3NhZ2VUeXBlcyIsInNpZ25hbCIsInByb3BlcnR5VXBkYXRlIiwiaWRsZSIsImludm9rZU1ldGhvZCIsImNvbm5lY3RUb1NpZ25hbCIsImRpc2Nvbm5lY3RGcm9tU2lnbmFsIiwic2V0UHJvcGVydHkiLCJ0cmFuc3BvcnQiLCJpbml0Q2FsbGJhY2siLCJzZW5kIiwib25tZXNzYWdlIiwiaGFuZGxlU2lnbmFsIiwiaGFuZGxlUmVzcG9uc2UiLCJoYW5kbGVQcm9wZXJ0eVVwZGF0ZSIsImV4ZWNDYWxsYmFja3MiLCJleGVjSWQiLCJjYWxsYmFjayIsIk51bWJlciIsIk1BWF9WQUxVRSIsIk1JTl9WQUxVRSIsInNpZ25hbEVtaXR0ZWQiLCJzaWduYWxzIiwicHJvcGVydGllcyIsIm9iamVjdE5hbWUiLCJRT2JqZWN0IiwidW53cmFwUHJvcGVydGllcyIsIndlYkNoYW5uZWwiLCJfX2lkX18iLCJfX29iamVjdFNpZ25hbHNfXyIsIl9fcHJvcGVydHlDYWNoZV9fIiwidW53cmFwUU9iamVjdCIsInVuZGVmaW5lZCIsIm9iamVjdElkIiwicU9iamVjdCIsImRlc3Ryb3llZCIsImNvbm5lY3QiLCJwcm9wZXJ0eU5hbWVzIiwicHJvcGVydHlOYW1lIiwiaWR4IiwicHJvcGVydHlJZHgiLCJhZGRTaWduYWwiLCJzaWduYWxEYXRhIiwiaXNQcm9wZXJ0eU5vdGlmeVNpZ25hbCIsInNpZ25hbE5hbWUiLCJzaWduYWxJbmRleCIsImRpc2Nvbm5lY3QiLCJpbnZva2VTaWduYWxDYWxsYmFja3MiLCJzaWduYWxBcmdzIiwiY29ubmVjdGlvbnMiLCJwcm9wZXJ0eU1hcCIsInByb3BlcnR5SW5kZXgiLCJwcm9wZXJ0eVZhbHVlIiwiYWRkTWV0aG9kIiwibWV0aG9kRGF0YSIsIm1ldGhvZE5hbWUiLCJtZXRob2RJZHgiLCJhcmd1bWVudHMiLCJhcmd1bWVudCIsImJpbmRHZXR0ZXJTZXR0ZXIiLCJwcm9wZXJ0eUluZm8iLCJub3RpZnlTaWduYWxEYXRhIiwiT2JqZWN0IiwiZGVmaW5lUHJvcGVydHkiLCJjb25maWd1cmFibGUiLCJ2YWx1ZVRvU2VuZCIsInByb3BlcnR5IiwibWV0aG9kcyIsImVudW1zIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxJQUFNQSxVQUFVO0FBQ25CQyxpQkFBQSxtRUFEbUIsRUFDTkMsUUFBQSwrREFETSxFQUNFQyxNQUFBLDJEQURGLEVBQ1FDLE9BQUEsOERBRFIsRUFDZUMsTUFBQSw0REFEZixFQUNxQkMsUUFBQSwrREFBQUE7QUFEckIsQ0FBaEI7QUFHUCxJQUFJLENBQUNDLE9BQU9DLE9BQVosRUFBcUI7QUFDakJELFdBQU9DLE9BQVAsR0FBaUJDLE1BQU1DLE9BQXZCO0FBQ0gsQzs7Ozs7Ozs7Ozs7Ozs7OztBQ2pCRDtBQUNBOztJQUNhQyxPOzs7QUFDVCxxQkFBWUMsR0FBWixFQUFpQkMsSUFBakIsRUFBdUI7QUFBQTs7QUFBQSxxREFDbkIsbUJBRG1COztBQUVuQixjQUFLRCxHQUFMLEdBQVdBLEdBQVg7QUFDQSxjQUFLRSxLQUFMLEdBQWFELElBQWI7QUFDQSxjQUFLRSxTQUFMLEdBQWlCLEVBQWpCO0FBSm1CO0FBS3RCOztzQkFDREMsRSxlQUFHQSxHLEVBQUlILEksRUFBTTtBQUNULFlBQUksT0FBT0csR0FBUCxLQUFjLFVBQWxCLEVBQThCO0FBQzFCLGdCQUFNQyxVQUFVLElBQUlELEdBQUosQ0FBTyxLQUFLSixHQUFaLEVBQWlCQyxJQUFqQixDQUFoQjtBQUNBLGlCQUFLRSxTQUFMLENBQWVHLElBQWYsQ0FBb0JELE9BQXBCO0FBQ0FBLG9CQUFRRSxNQUFSO0FBQ0EsbUJBQU9GLE9BQVA7QUFDSCxTQUxELE1BTUs7QUFDRCxnQkFBTUcsT0FBTyxLQUFLUixHQUFMLENBQVNILEtBQVQsQ0FBZU8sRUFBZixDQUFrQkEsR0FBbEIsQ0FBYjtBQUNBLGlCQUFLRCxTQUFMLENBQWVHLElBQWYsQ0FBb0JFLElBQXBCO0FBQ0EsbUJBQU9BLElBQVA7QUFDSDtBQUNKLEs7O3NCQUNEQyxJLGlCQUFLQyxJLEVBQU1DLE0sRUFBUTtBQUFBOztBQUNmO0FBQ0E7QUFDQTtBQUNBLFlBQUksT0FBT0QsSUFBUCxLQUFnQixRQUFwQixFQUE4QjtBQUMxQjtBQUNBLGdCQUFJQSxLQUFLRSxNQUFMLENBQVksQ0FBWixFQUFlLENBQWYsTUFBc0IsR0FBMUIsRUFBK0I7QUFDM0IsdUJBQU8sS0FBS1osR0FBTCxDQUFTUyxJQUFULENBQWNDLElBQWQsQ0FBUDtBQUNIO0FBQ0Q7QUFDQSxnQkFBSUEsS0FBS0csT0FBTCxDQUFhLElBQWIsTUFBdUIsQ0FBM0IsRUFBOEI7QUFDMUJILHVCQUFPQSxLQUFLRSxNQUFMLENBQVksQ0FBWixDQUFQO0FBQ0g7QUFDRDtBQUNBLGdCQUFJRixLQUFLRyxPQUFMLENBQWEsS0FBYixNQUF3QixDQUE1QixFQUErQjtBQUMzQixvQkFBTUMsU0FBUyxLQUFLQyxhQUFMLEVBQWY7QUFDQSxvQkFBSUQsTUFBSixFQUFZO0FBQ1JBLDJCQUFPTCxJQUFQLENBQVksT0FBT0MsS0FBS0UsTUFBTCxDQUFZLENBQVosQ0FBbkI7QUFDSCxpQkFGRCxNQUdLO0FBQ0QseUJBQUtaLEdBQUwsQ0FBU1MsSUFBVCxDQUFjLE1BQU1DLEtBQUtFLE1BQUwsQ0FBWSxDQUFaLENBQXBCO0FBQ0g7QUFDRDtBQUNIO0FBQ0QsZ0JBQU1JLE1BQU0sS0FBS0MsVUFBTCxFQUFaO0FBQ0EsZ0JBQUksQ0FBQ0QsR0FBTCxFQUFVO0FBQ04sdUJBQU8sS0FBS2hCLEdBQUwsQ0FBU1MsSUFBVCxDQUFjLE1BQU1DLElBQXBCLENBQVA7QUFDSDtBQUNELGdCQUFJTSxJQUFJRixNQUFKLEtBQWUsSUFBbkIsRUFBeUI7QUFDckIsdUJBQU9FLElBQUlGLE1BQUosQ0FBV0wsSUFBWCxDQUFnQkMsSUFBaEIsRUFBc0JDLE1BQXRCLENBQVA7QUFDSDtBQUNELGdCQUFNTyxXQUFXLCtEQUFBQyxDQUFNVCxJQUFOLENBQWpCO0FBQ0EsZ0JBQUlVLE1BQU0sSUFBVjtBQUNBLGdCQUFNQyxhQUFhLEtBQUtyQixHQUFMLENBQVNzQixTQUFULEdBQXFCQyxHQUFyQixFQUFuQjtBQUNBLGdCQUFJLEtBQUtDLE1BQVQsRUFBaUI7QUFDYkosc0JBQU0sK0RBQUFELENBQU1FLFVBQU4sRUFBa0JJLEtBQWxCLENBQXdCLENBQXhCLEVBQTJCLEtBQUtELE1BQWhDLEVBQXdDRSxNQUF4QyxDQUErQ1IsUUFBL0MsQ0FBTjtBQUNBLHFCQUFLLElBQUlTLElBQUksQ0FBYixFQUFnQkEsSUFBSVAsSUFBSVEsTUFBeEIsRUFBZ0NELEdBQWhDLEVBQXFDO0FBQ2pDUCx3QkFBSU8sQ0FBSixFQUFPRSxLQUFQLEdBQWVGLElBQUksQ0FBbkI7QUFDSDtBQUNELG9CQUFNRyxTQUFTLGlFQUFBQyxDQUFRWCxHQUFSLENBQWY7QUFDQSxxQkFBS3BCLEdBQUwsQ0FBU2dDLFdBQVQsQ0FBcUJGLE1BQXJCLEVBQTZCLElBQTdCLEVBQW1DRyxJQUFuQyxDQUF3QyxvQkFBWTtBQUNoRCx3QkFBSUgsV0FBV0ksUUFBZixFQUF5QjtBQUNyQjtBQUNBLCtCQUFLbEMsR0FBTCxDQUFTUyxJQUFULENBQWN5QixRQUFkO0FBQ0gscUJBSEQsTUFJSztBQUNELCtCQUFLQyxXQUFMLENBQWlCbkIsSUFBSW9CLE9BQXJCLEVBQThCaEIsR0FBOUIsRUFBbUNjLFFBQW5DO0FBQ0g7QUFDSixpQkFSRCxFQVFHRyxLQVJILENBUVM7QUFBQSwyQkFBTSxLQUFOO0FBQUEsaUJBUlQ7QUFTSCxhQWZELE1BZ0JLO0FBQ0QscUJBQUtGLFdBQUwsQ0FBaUJuQixJQUFJb0IsT0FBckIsRUFBOEJsQixRQUE5QixFQUF3QyxFQUF4QztBQUNIO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNILFNBdERELE1BdURLLENBR0o7QUFGRztBQUNBOztBQUVKO0FBQ0gsSzs7c0JBQ0RvQixJLGlCQUFLQyxNLEVBQVFDLEssRUFBTztBQUNoQjtBQUNILEs7O3NCQUNEQyxLLGtCQUFNRixNLEVBQVFDLEssRUFBTztBQUNqQjtBQUNILEs7O3NCQUNEN0IsTSxxQkFBUztBQUNMLGFBQUtYLEdBQUwsQ0FBU0gsS0FBVCxDQUFlNkMsT0FBZixDQUF1QixnQ0FBdkI7QUFDSCxLOztzQkFDREMsUyxzQkFBVUosTSxFQUFRQyxLLEVBQU87QUFDckI7QUFDSCxLOztzQkFDREksTyxzQkFBVTtBQUNOO0FBQ0gsSzs7c0JBQ0RDLFUseUJBQWE7QUFDVCxhQUFLRCxPQUFMO0FBQ0E7QUFDQSxZQUFNRSxNQUFNLEtBQUszQyxTQUFqQjtBQUNBLGFBQUssSUFBSXdCLElBQUltQixJQUFJbEIsTUFBSixHQUFhLENBQTFCLEVBQTZCRCxLQUFLLENBQWxDLEVBQXFDQSxHQUFyQyxFQUEwQztBQUN0QyxnQkFBSW1CLElBQUluQixDQUFKLEtBQVVtQixJQUFJbkIsQ0FBSixFQUFPa0IsVUFBckIsRUFBaUM7QUFDN0JDLG9CQUFJbkIsQ0FBSixFQUFPa0IsVUFBUDtBQUNIO0FBQ0o7QUFDRDtBQUNBLGFBQUs3QyxHQUFMLEdBQVcsS0FBS0csU0FBTCxHQUFpQixJQUE1QjtBQUNBO0FBQ0EsYUFBSzRDLEtBQUwsQ0FBV0YsVUFBWDtBQUNBLDJCQUFNQSxVQUFOO0FBQ0gsSzs7c0JBQ0RHLEcsZ0JBQUlDLE0sRUFBUXRDLE0sRUFBUTtBQUNoQnNDLGVBQU8sS0FBS2pELEdBQVosRUFBaUIsSUFBakIsRUFBdUJXLE1BQXZCO0FBQ0gsSzs7c0JBQ0R1QyxPLG9CQUFROUIsRyxFQUFLO0FBQUE7O0FBQ1QsWUFBSStCLGlCQUFKO0FBQ0E7QUFDQSxZQUFNQyxTQUFTLEVBQUVoRCxJQUFJLEVBQU4sRUFBZjtBQUNBLGFBQUtKLEdBQUwsQ0FBU3FELFVBQVQsQ0FBb0IsS0FBSzFDLE1BQUwsRUFBcEIsRUFBbUN5QyxPQUFPaEQsRUFBMUMsRUFBOEMsS0FBS2tELEtBQW5EO0FBQ0EsYUFBS3RELEdBQUwsQ0FBU3VELFNBQVQsQ0FBbUIsWUFBbkIsRUFBaUMsQ0FBQyxJQUFELEVBQU9uQyxHQUFQLEVBQVlnQyxNQUFaLENBQWpDO0FBQ0FBLGVBQU9oRCxFQUFQLENBQVVvRCxNQUFWLEdBQW1CLElBQW5CO0FBQ0EsWUFBSTtBQUNBLGlCQUFLVCxLQUFMLEdBQWEsS0FBSy9DLEdBQUwsQ0FBU0gsS0FBVCxDQUFlTyxFQUFmLENBQWtCZ0QsT0FBT2hELEVBQXpCLEVBQTZCLEtBQUtxRCxVQUFsQyxDQUFiO0FBQ0EsZ0JBQUksS0FBS1YsS0FBTCxDQUFXaEMsYUFBWCxFQUFKLEVBQWdDO0FBQzVCLHFCQUFLMEMsVUFBTCxHQUFrQixLQUFLVixLQUF2QjtBQUNIO0FBQ0QsaUJBQUtXLEtBQUwsQ0FBVyxLQUFLWCxLQUFoQixFQUF1QjNCLEdBQXZCO0FBQ0ErQix1QkFBVyxLQUFLUSxVQUFMLENBQWdCdkMsR0FBaEIsRUFBcUJhLElBQXJCLENBQTBCLFlBQU07QUFDdkMsdUJBQU8sT0FBS1EsS0FBTCxDQUFXLE9BQUtNLEtBQWhCLEVBQXVCM0IsR0FBdkIsQ0FBUDtBQUNILGFBRlUsQ0FBWDtBQUdILFNBVEQsQ0FVQSxPQUFPd0MsQ0FBUCxFQUFVO0FBQ05ULHVCQUFXdkQsUUFBUWlFLE1BQVIsQ0FBZUQsQ0FBZixDQUFYO0FBQ0g7QUFDRCxlQUFPVCxTQUFTZCxLQUFULENBQWU7QUFBQSxtQkFBTyxPQUFLeUIsVUFBTCxTQUFzQkMsR0FBdEIsQ0FBUDtBQUFBLFNBQWYsQ0FBUDtBQUNILEs7O3NCQUNETCxLLGtCQUFNbEQsSSxFQUFNWSxHLEVBQUs7QUFDYixlQUFPLEtBQUtrQixJQUFMLENBQVU5QixJQUFWLEVBQWdCWSxHQUFoQixDQUFQO0FBQ0gsSzs7c0JBQ0R1QyxVLHVCQUFXdkMsRyxFQUFLO0FBQUE7O0FBQ1osWUFBTTRDLFFBQVEsRUFBZDtBQUNBLGFBQUssSUFBTUMsR0FBWCxJQUFrQixLQUFLWCxLQUF2QixFQUE4QjtBQUMxQixnQkFBTVksUUFBUSxLQUFLWixLQUFMLENBQVdXLEdBQVgsQ0FBZDtBQUNBLGdCQUFJQyxNQUFNOUMsR0FBVixFQUFlO0FBQ1g7QUFDQSxvQkFBSSxPQUFPOEMsTUFBTTlDLEdBQWIsS0FBcUIsUUFBekIsRUFBbUM7QUFDL0Isd0JBQU0rQyxTQUFTLCtEQUFBaEQsQ0FBTStDLE1BQU05QyxHQUFaLENBQWY7QUFDQTRDLDBCQUFNMUQsSUFBTixDQUFXLEtBQUs4RCxjQUFMLENBQW9CRixLQUFwQixFQUEyQkMsTUFBM0IsQ0FBWDtBQUNILGlCQUhELE1BSUs7QUFDRCx3QkFBSTNELE9BQU8wRCxNQUFNMUQsSUFBakI7QUFDQSx3QkFBSSxPQUFPMEQsTUFBTTlDLEdBQWIsS0FBcUIsVUFBckIsSUFBbUMsRUFBRVosZ0JBQWdCMEQsTUFBTTlDLEdBQXhCLENBQXZDLEVBQXFFO0FBQ2pFWiwrQkFBTyxJQUFJMEQsTUFBTTlDLEdBQVYsQ0FBYyxLQUFLcEIsR0FBbkIsRUFBd0IsRUFBeEIsQ0FBUDtBQUNIO0FBQ0Qsd0JBQUksQ0FBQ1EsSUFBTCxFQUFXO0FBQ1BBLCtCQUFPMEQsTUFBTTlDLEdBQWI7QUFDSDtBQUNENEMsMEJBQU0xRCxJQUFOLENBQVcsS0FBSytELGNBQUwsQ0FBb0JILEtBQXBCLEVBQTJCMUQsSUFBM0IsRUFBaUNZLEdBQWpDLENBQVg7QUFDSDtBQUNKLGFBaEJELE1BaUJLLElBQUk2QyxRQUFRLFNBQVIsSUFBcUI3QyxHQUFyQixJQUE0QkEsSUFBSVEsTUFBSixHQUFhLENBQTdDLEVBQWdEO0FBQ2pEO0FBQ0Esb0JBQU0wQyxTQUFTbEQsSUFBSUssS0FBSixDQUFVLENBQVYsQ0FBZjtBQUNBdUMsc0JBQU0xRCxJQUFOLENBQVcsS0FBSzhELGNBQUwsQ0FBb0JGLEtBQXBCLEVBQTJCSSxNQUEzQixDQUFYO0FBQ0g7QUFDSjtBQUNELGVBQU8xRSxRQUFRMkUsR0FBUixDQUFZUCxLQUFaLEVBQW1CL0IsSUFBbkIsQ0FBd0IsWUFBTTtBQUNqQyxtQkFBS1UsU0FBTCxDQUFlLE9BQUtJLEtBQXBCLEVBQTJCM0IsR0FBM0I7QUFDSCxTQUZNLENBQVA7QUFHSCxLOztzQkFDRDBDLFUsdUJBQVd0RCxJLEVBQU11RCxHLEVBQUs7QUFDbEIsYUFBSy9ELEdBQUwsQ0FBU3dFLEtBQVQsQ0FBZSxvQkFBZixFQUFxQyxDQUFDVCxHQUFELEVBQU12RCxJQUFOLENBQXJDO0FBQ0EsZUFBTyxJQUFQO0FBQ0gsSzs7c0JBQ0Q0RCxjLDJCQUFlcEQsRyxFQUFLc0QsTSxFQUFRO0FBQUE7O0FBQ3hCLGVBQU8sS0FBS3RFLEdBQUwsQ0FBU3lFLGFBQVQsQ0FBdUJILE1BQXZCLEVBQStCdEQsSUFBSVIsSUFBbkMsRUFBeUN5QixJQUF6QyxDQUE4QyxnQkFBUTtBQUN6RCxtQkFBTyxPQUFLb0MsY0FBTCxDQUFvQnJELEdBQXBCLEVBQXlCUixJQUF6QixFQUErQjhELE1BQS9CLENBQVA7QUFDSCxTQUZNLENBQVA7QUFHSCxLOztzQkFDREQsYywyQkFBZXJELEcsRUFBS1IsSSxFQUFNOEQsTSxFQUFRO0FBQzlCLFlBQU1JLE9BQU8sS0FBSzFFLEdBQUwsQ0FBU0gsS0FBVCxDQUFlOEUsRUFBZixDQUFrQjNELElBQUk0RCxFQUF0QixDQUFiO0FBQ0EsZUFBT3BFLEtBQUtELE1BQUwsQ0FBWW1FLElBQVosRUFBa0JKLE1BQWxCLEVBQTBCLElBQTFCLEVBQWdDckMsSUFBaEMsQ0FBcUMsY0FBTTtBQUM5QztBQUNBLGdCQUFJakIsSUFBSVIsSUFBSixJQUFZUSxJQUFJUixJQUFKLEtBQWFBLElBQTdCLEVBQW1DO0FBQy9CUSxvQkFBSVIsSUFBSixDQUFTcUMsVUFBVDtBQUNIO0FBQ0Q7QUFDQTdCLGdCQUFJUixJQUFKLEdBQVdBLElBQVg7QUFDQVEsZ0JBQUk0RCxFQUFKLEdBQVN4RSxHQUFHTyxNQUFILENBQVVpRSxFQUFuQjtBQUNBLG1CQUFPeEUsRUFBUDtBQUNILFNBVE0sQ0FBUDtBQVVILEs7O3NCQUNEK0IsVyx3QkFBWW5CLEcsRUFBS0ksRyxFQUFLVixJLEVBQU07QUFDeEIsWUFBSSxLQUFLYyxNQUFULEVBQWlCO0FBQ2IsaUJBQUttQyxVQUFMLENBQWdCdkMsSUFBSUssS0FBSixDQUFVLEtBQUtELE1BQUwsR0FBYyxDQUF4QixDQUFoQjtBQUNBLGlCQUFLeEIsR0FBTCxDQUFTc0IsU0FBVCxHQUFxQnVELEdBQXJCLENBQXlCbkUsSUFBekIsRUFBK0IsRUFBRW9FLFFBQVEsSUFBVixFQUEvQjtBQUNBLGlCQUFLOUUsR0FBTCxDQUFTdUQsU0FBVCxDQUFtQixXQUFuQixFQUFnQyxDQUFDbkMsR0FBRCxDQUFoQztBQUNILFNBSkQsTUFLSztBQUNELGlCQUFLZ0QsY0FBTCxDQUFvQnBELEdBQXBCLEVBQXlCSSxHQUF6QjtBQUNIO0FBQ0osSzs7O0VBN013Qix5RDs7Ozs7Ozs7Ozs7Ozs7QUNGN0I7QUFBQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUF2QixNQUFNNEMsS0FBTixDQUFZLFlBQU07QUFDZCxRQUFNekMsTUFBTSxJQUFJLHlEQUFKLENBQVc7QUFDbkI0RSxZQUFZLFNBRE87QUFFbkJHLGlCQUFZLE9BRk87QUFHbkJDLGVBQVk7QUFITyxLQUFYLENBQVo7O0FBTUFoRixRQUFJTyxNQUFKLEdBQWEwQixJQUFiLENBQWtCO0FBQUEsZUFBTWdELGtCQUFOO0FBQUEsS0FBbEI7O0FBRUFqRixRQUFJa0YsV0FBSixDQUFnQixtQkFBaEIsRUFBcUMsWUFBaUI7QUFBQSwwQ0FBTEMsSUFBSztBQUFMQSxnQkFBSztBQUFBOztBQUNsRHhGLGVBQU95RixPQUFQLENBQWVaLEtBQWYsQ0FBcUJXLElBQXJCO0FBQ0gsS0FGRDtBQUdILENBWkQ7O0FBY0EsSUFBSUUsU0FBUyxFQUFiOztBQUVBLElBQUlDLGdCQUFnQixJQUFJMUYsT0FBSixDQUFZLFVBQUMyRixPQUFELEVBQWE7QUFDekMsUUFBSSxrRUFBSixDQUFnQkMsR0FBR0MsbUJBQW5CLEVBQXdDLFVBQVNDLE9BQVQsRUFBa0I7QUFDdEQsWUFBSUMsT0FBTyxFQUFYO0FBQ0FOLGlCQUFTSyxRQUFRRSxPQUFqQjs7QUFFQVAsZUFBT1EsS0FBUCxDQUFhZCxPQUFiLENBQXFCLFVBQVNBLE9BQVQsRUFBa0I7QUFDbkNZLG9CQUFRWixPQUFSO0FBQ0FNLG1CQUFPUSxLQUFQLENBQWFDLFVBQWIsQ0FBd0IsVUFBU0EsVUFBVCxFQUFxQjtBQUN6Q0gsd0JBQVEsc0JBQXNCSSxLQUFLQyxTQUFMLENBQWVGLFVBQWYsRUFBMkIsSUFBM0IsRUFBaUMsQ0FBakMsQ0FBOUI7QUFDQVAsd0JBQVFJLElBQVI7QUFDSCxhQUhEO0FBSUgsU0FORDtBQU9ILEtBWEQ7QUFZSCxDQWJtQixDQUFwQjs7QUFnQkFMLGNBQWNyRCxJQUFkLENBQW1CLFVBQVMwRCxJQUFULEVBQWU7QUFDOUJOLFdBQU9RLEtBQVAsQ0FBYUksV0FBYixDQUF5QixVQUF6QixFQUFxQyxzQkFBc0JOLElBQTNELEVBQWlFLEtBQWpFO0FBQ0FOLFdBQU9RLEtBQVAsQ0FBYUssVUFBYixDQUF3QixzQkFBc0JQLElBQTlDO0FBQ0gsQ0FIRCxFQUdHLFVBQVNBLElBQVQsRUFBZTtBQUNkTixXQUFPUSxLQUFQLENBQWFJLFdBQWIsQ0FBeUIsVUFBekIsRUFBcUMsMkJBQXJDLEVBQWtFLEtBQWxFO0FBQ0FaLFdBQU9RLEtBQVAsQ0FBYUssVUFBYixDQUF3Qix5QkFBeEI7QUFDSCxDQU5EOztBQVFBLFNBQVNqQixnQkFBVCxHQUE0QixDQUUzQjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hERDtBQUNBO0FBQ0E7QUFDQTs7SUFFcUJrQixROzs7Ozs7Ozs7dUJBQ2pCeEYsTSxxQkFBUztBQUNMLFlBQUl5RixPQUFPO0FBQ1B4QixnQkFBRyxNQURJO0FBRVB5QixrQkFBSyxDQUFFLGlFQUFGLEVBQWlCLDREQUFqQjtBQUZFLFNBQVg7O0FBS0EsZUFBT0QsSUFBUDtBQUNILEs7OztFQVJpQywwRDs7K0RBQWpCRCxROzs7Ozs7Ozs7Ozs7Ozs7OztBQ0xyQjtBQUNBO0FBQ0E7QUFDQTs7SUFFcUJHLGdCOzs7Ozs7Ozs7K0JBQ2pCM0YsTSxxQkFBUztBQUNMLFlBQUk0RixlQUFlO0FBQ2YzQixnQkFBRyx1QkFEWTtBQUVmNEIsa0JBQUssQ0FDRCxFQUFFQyxPQUFPLEVBQVQsRUFEQyxFQUVEO0FBQ0k3QixvQkFBRyxjQURQO0FBRUlwRSxzQkFBSyxNQUZUO0FBR0lrRyw0QkFBVyxJQUhmO0FBSUlDLCtCQUFjO0FBQ1ZGLDJCQUFNO0FBREksaUJBSmxCO0FBT0lHLHNCQUFLLENBQ0Q7QUFDSWhDLHdCQUFHLFVBRFA7QUFFSWlDLDJCQUFNLE1BRlY7QUFHSUMsNkJBQVEsaUVBQUFDO0FBSFosaUJBREMsRUFNRDtBQUNJbkMsd0JBQUcsVUFEUDtBQUVJaUMsMkJBQU0sTUFGVjtBQUdJQyw2QkFBUSxpRUFBQUU7QUFIWixpQkFOQyxFQVdEO0FBQ0lwQyx3QkFBRyxVQURQO0FBRUlpQywyQkFBTSxNQUZWO0FBR0lDLDZCQUFRLGlFQUFBRztBQUhaLGlCQVhDLEVBZ0JEO0FBQ0lyQyx3QkFBRyxVQURQO0FBRUlpQywyQkFBTSxNQUZWO0FBR0lDLDZCQUFRLGlFQUFBSTtBQUhaLGlCQWhCQyxFQXFCRDtBQUNJdEMsd0JBQUcsWUFEUDtBQUVJaUMsMkJBQU0sUUFGVjtBQUdJQyw2QkFBUSxtRUFBQUs7QUFIWixpQkFyQkMsRUEwQkQ7QUFDSXZDLHdCQUFHLFVBRFA7QUFFSWlDLDJCQUFNLE1BRlY7QUFHSUMsNkJBQVEsaUVBQUFNO0FBSFosaUJBMUJDLENBUFQ7QUF1Q0lDLG9CQUFHO0FBQ0NDLHFDQUFnQix5QkFBUzFDLEVBQVQsRUFBWTtBQUN4Qi9FLDhCQUFNNkMsT0FBTixDQUFjLFlBQVlrQyxFQUFaLEdBQWlCLEtBQUsyQyxXQUFMLENBQWlCM0MsRUFBakIsRUFBcUJpQyxLQUFwRDtBQUNBekIsZ0NBQVFvQyxHQUFSLENBQVk1QyxLQUFLLEtBQUsyQyxXQUFMLENBQWlCM0MsRUFBakIsRUFBcUJpQyxLQUF0QztBQUNBLDRCQUFJakMsT0FBTyxZQUFYLEVBQXlCO0FBQ3JCUyw0QkFBQSwyQ0FBQUEsQ0FBT1EsS0FBUCxDQUFhSSxXQUFiLENBQXlCLFVBQXpCLEVBQXFDLDRCQUFyQyxFQUFtRSxJQUFuRTtBQUNBWiw0QkFBQSwyQ0FBQUEsQ0FBT1EsS0FBUCxDQUFhNEIsT0FBYjtBQUNILHlCQUhELE1BR08sSUFBSTdDLE1BQU0sWUFBVixFQUF3QjtBQUMzQlMsNEJBQUEsMkNBQUFBLENBQU9RLEtBQVAsQ0FBYUksV0FBYixDQUF5QixVQUF6QixFQUFxQyxnQkFBckMsRUFBdUQsS0FBdkQ7QUFDaEM7O0FBRUEsZ0NBQUl5QixrQkFBa0IsSUFBSTlILE9BQUosQ0FBWSxVQUFDMkYsT0FBRCxFQUFhO0FBQ3pDRixnQ0FBQSwyQ0FBQUEsQ0FBT1EsS0FBUCxDQUFhOEIsU0FBYixDQUF1QixLQUF2QixFQUE4QixVQUFTQyxHQUFULEVBQWE7QUFDdkNyQyw0Q0FBUXFDLEdBQVI7QUFDSCxpQ0FGRDtBQUdMLDZCQUpxQixDQUF0Qjs7QUFPQUYsNENBQWdCekYsSUFBaEIsQ0FBcUIsVUFBUzJGLEdBQVQsRUFBYztBQUMvQnZDLGdDQUFBLDJDQUFBQSxDQUFPUSxLQUFQLENBQWFJLFdBQWIsQ0FBeUIsVUFBekIsRUFBcUMsZUFBZTJCLEdBQXBELEVBQXlELEtBQXpEO0FBQ0gsNkJBRkQsRUFFRyxVQUFTQyxHQUFULEVBQWM7QUFDYnhDLGdDQUFBLDJDQUFBQSxDQUFPUSxLQUFQLENBQWFJLFdBQWIsQ0FBeUIsVUFBekIsRUFBcUMsb0JBQXJDLEVBQTJELEtBQTNEO0FBQ0gsNkJBSkQ7QUFLZ0M7QUFDQTs7QUFFaEM7QUFDQTtBQUNBO0FBQzZCO0FBQ0o7QUE5QkYsaUJBdkNQO0FBdUVJNkIsc0JBQUs7QUFDREMsNkJBQVE7QUFEUDtBQXZFVCxhQUZDLEVBNkVELEVBN0VDO0FBRlUsU0FBbkI7O0FBcUZBLGVBQU94QixZQUFQO0FBQ0gsSzs7O0VBeEZ5QywwRDs7K0RBQXpCRCxnQjs7Ozs7Ozs7Ozs7Ozs7OztBQ0xyQjtBQUNBO0FBQ0E7QUFDQTs7SUFFcUIwQixZOzs7Ozs7Ozs7MkJBQ2pCckgsTSxxQkFBUztBQUNMLFlBQUlzSCxXQUFXO0FBQ1hyRCxnQkFBRyxtQkFEUTtBQUVYc0QsdUJBQVUsRUFGQztBQUdYQyx1QkFBVSxFQUhDO0FBSVhMLGtCQUFLLE9BSk07QUFLWHpCLGtCQUFLLENBQUM7QUFDRnpCLG9CQUFHLFVBREQ7QUFFRnBFLHNCQUFLLFNBRkg7QUFHRjRILHdCQUFPLEVBSEw7QUFJRk4sc0JBQUssT0FKSDtBQUtGTywwQkFBVSxDQUNOLEVBQUU1QixPQUFPLEVBQVQsRUFETSxFQUVOO0FBQ0k3Qix3QkFBRyxZQURQO0FBRUlwRSwwQkFBTSxRQUZWO0FBR0lzSCwwQkFBTSxNQUhWO0FBSUlRLDBCQUFNLGFBSlY7QUFLSTdCLDJCQUFPLEVBTFg7QUFNSThCLHlCQUFLLFlBTlQ7QUFPSUMsMkJBQU0sZUFBQzVELEVBQUQsRUFBUTtBQUNWUyx3QkFBQSwyQ0FBQUEsQ0FBT1EsS0FBUCxDQUFhNEMsU0FBYjtBQUNBcEQsd0JBQUEsMkNBQUFBLENBQU9RLEtBQVAsQ0FBYUksV0FBYixDQUF5QixhQUF6QixFQUF3QyxtQkFBbUJyQixFQUEzRCxFQUErRCxLQUEvRDtBQUNIO0FBVkwsaUJBRk0sRUFjTjtBQUNJQSx3QkFBRyxZQURQO0FBRUlwRSwwQkFBTSxRQUZWO0FBR0lzSCwwQkFBTSxNQUhWO0FBSUlRLDBCQUFNLFFBSlY7QUFLSTdCLDJCQUFPLEVBTFg7QUFNSThCLHlCQUFLLFlBTlQ7QUFPSUMsMkJBQU0sZUFBQzVELEVBQUQsRUFBUTtBQUFFL0UsOEJBQU02QyxPQUFOLENBQWNrQyxFQUFkO0FBQW9CO0FBUHhDLGlCQWRNLEVBdUJOO0FBQ0lBLHdCQUFHLGNBRFA7QUFFSXBFLDBCQUFNLFFBRlY7QUFHSXNILDBCQUFNLE1BSFY7QUFJSVEsMEJBQU0sYUFKVjtBQUtJN0IsMkJBQU8sRUFMWDtBQU1JOEIseUJBQUssWUFOVDtBQU9JQywyQkFBTSxlQUFDNUQsRUFBRCxFQUFRO0FBQUUvRSw4QkFBTTZDLE9BQU4sQ0FBY2tDLEVBQWQ7QUFBb0I7QUFQeEMsaUJBdkJNLEVBZ0NOO0FBQ0lBLHdCQUFHLGNBRFA7QUFFSXBFLDBCQUFNLFFBRlY7QUFHSXNILDBCQUFNLE1BSFY7QUFJSVEsMEJBQU0sYUFKVjtBQUtJN0IsMkJBQU8sRUFMWDtBQU1JOEIseUJBQUssWUFOVDtBQU9JQywyQkFBTSxlQUFDNUQsRUFBRCxFQUFRO0FBQUUvRSw4QkFBTTZDLE9BQU4sQ0FBY2tDLEVBQWQ7QUFBb0I7QUFQeEMsaUJBaENNLEVBeUNOLEVBekNNLEVBMENOLEVBQUU2QixPQUFPLEVBQVQsRUExQ007QUFMUixhQUFEO0FBTE0sU0FBZjs7QUF5REEsZUFBT3dCLFFBQVA7QUFDSCxLOzs7RUE1RHFDLDBEOzsrREFBckJELFk7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTHJCO0FBQ0E7QUFDQTtBQUNBOztJQUVxQlUsVzs7Ozs7Ozs7OzBCQUNqQi9ILE0scUJBQVM7QUFDTCxZQUFJZ0ksVUFBVTtBQUNWL0QsZ0JBQUksa0JBRE07QUFFVmdFLHNCQUFTLEVBRkM7QUFHVm5DLG1CQUFNLEdBSEk7QUFJVkosa0JBQUssQ0FDRCxzRUFEQyxFQUVEO0FBQ0l6QixvQkFBRyxtQkFEUDtBQUVJcEUsc0JBQUs7QUFGVCxhQUZDLEVBTUQsbUVBTkM7QUFKSyxTQUFkOztBQWNBLGVBQU9tSSxPQUFQO0FBQ0gsSzs7O0VBakJvQywwRDs7K0RBQXBCRCxXOzs7Ozs7Ozs7Ozs7Ozs7O0FDTHJCO0FBQ0E7QUFDQTs7SUFFcUJHLG1COzs7Ozs7Ozs7a0NBQ2pCbEksTSxxQkFBUztBQUNMLFlBQUltSSxrQkFBa0I7QUFDbEJsRSxnQkFBRywwQkFEZTtBQUVsQnlCLGtCQUFLLENBQ0Q7QUFDSTdGLHNCQUFLLFVBRFQ7QUFFSXVJLDBCQUFTLGtCQUZiO0FBR0lqQixzQkFBSyxRQUhUO0FBSUlNLHdCQUFPLEVBSlg7QUFLSTFCLDRCQUFXO0FBTGYsYUFEQyxFQVFEO0FBQ0lGLHNCQUFLLENBQ0QsRUFBRUMsT0FBTSxDQUFSLEVBREMsRUFFRDtBQUNJN0Isd0JBQUcsdUJBRFA7QUFFSXBFLDBCQUFLLE1BRlQ7QUFHSXFHLDJCQUFNLFFBSFY7QUFJSWlCLDBCQUFLLE9BSlQ7QUFLSU0sNEJBQU8sRUFMWDtBQU1JZix3QkFBRztBQUNDMkIsaUNBQVEsaUJBQUNwRSxFQUFELEVBQVE7QUFDWixnQ0FBR0EsR0FBR3FFLFFBQUgsTUFBaUIsUUFBcEIsRUFBOEI7QUFDMUJyRSxtQ0FBR3NFLFFBQUgsQ0FBWSxFQUFaO0FBQ0g7QUFDSix5QkFMRjtBQU1DQyxnQ0FBTyxnQkFBQ3ZFLEVBQUQsRUFBUTtBQUNYLGdDQUFHQSxHQUFHcUUsUUFBSCxNQUFpQixFQUFwQixFQUF3QjtBQUNwQnJFLG1DQUFHc0UsUUFBSCxDQUFZLFFBQVo7QUFDSDtBQUNKO0FBVkY7QUFOUCxpQkFGQyxFQXFCRCxFQUFFekMsT0FBTSxDQUFSLEVBckJDO0FBRFQsYUFSQyxFQWlDRCxFQUFFMkIsUUFBUSxFQUFWLEVBakNDLEVBa0NEO0FBQ0k1SCxzQkFBSyxZQURUO0FBRUlvRSxvQkFBRywyQkFGUDtBQUdJd0Usd0JBQU8sR0FIWDtBQUlJQyxzQkFBSztBQUNEaEQsMEJBQUssQ0FDRDtBQUNJekIsNEJBQUcsaUJBRFA7QUFFSXBFLDhCQUFLLE1BRlQ7QUFHSXNILDhCQUFLLFVBSFQ7QUFJSU0sZ0NBQU8sSUFKWDtBQUtJa0IsOEJBQUssS0FMVDtBQU1JMUMsOEJBQUssRUFOVDtBQU9JbkUsK0JBQU0saUJBQVc7QUFDYmtDLCtCQUFHLGlCQUFILEVBQXNCaUMsSUFBdEIsQ0FBMkIyQyxJQUEzQixDQUFnQyx5RUFBaEM7QUFDSCx5QkFUTDtBQVVJbEMsNEJBQUc7QUFDQ21DLDRDQUFlLDBCQUFhO0FBQ3hCLG9DQUFHN0UsR0FBRyxpQkFBSCxFQUFzQjhFLE9BQXRCLG1EQUF1Q0MsUUFBdkMsSUFBbUQsSUFBdEQsRUFBNEQ7QUFDeEQsd0NBQUlDLFFBQVFoRixHQUFHLGNBQUgsRUFBbUI4RSxPQUFuQixrREFBWjtBQUNBLHdDQUFHRSxNQUFNQyxNQUFOLElBQWdCLEtBQW5CLEVBQXlCO0FBQ3JCakYsMkNBQUcsY0FBSCxFQUFtQmtGLFVBQW5CLG1EQUFzQyxFQUFDRCxRQUFPLElBQVIsRUFBY0UsUUFBTyxLQUFyQixFQUF0QztBQUNIO0FBQ0o7QUFDSjtBQVJGO0FBVlAscUJBREM7QUFESjtBQUpULGFBbENDO0FBRmEsU0FBdEI7O0FBcUVBLGVBQU9oQixlQUFQO0FBQ0gsSzs7O0VBeEU0QywwRDs7K0RBQTVCRCxtQjs7Ozs7Ozs7Ozs7Ozs7OztBQ0pyQjtBQUNBO0FBQ0E7O0lBRXFCa0IsZ0I7Ozs7Ozs7OzsrQkFDakJwSixNLHFCQUFTO0FBQ0wsWUFBSXFKLGVBQWU7QUFDZnBGLGdCQUFHLHdCQURZO0FBRWZ5QixrQkFBSyxDQUNEO0FBQ0k3RixzQkFBSyxVQURUO0FBRUl1SSwwQkFBUyxlQUZiO0FBR0lqQixzQkFBSyxRQUhUO0FBSUlNLHdCQUFPLEVBSlg7QUFLSTFCLDRCQUFXO0FBTGYsYUFEQyxFQVFEO0FBQ0k5QixvQkFBRyx1QkFEUDtBQUVJcEUsc0JBQUssU0FGVDtBQUdJc0gsc0JBQUssT0FIVDtBQUlJTSx3QkFBTyxFQUpYO0FBS0lDLDBCQUFVLENBQ04sRUFBRTVCLE9BQU8sQ0FBVCxFQURNLEVBRU47QUFDSTdCLHdCQUFHLFlBRFA7QUFFSXBFLDBCQUFNLFFBRlY7QUFHSXNILDBCQUFNLE1BSFY7QUFJSVEsMEJBQU0saUJBSlY7QUFLSTdCLDJCQUFPLEVBTFg7QUFNSThCLHlCQUFLLFlBTlQ7QUFPSUMsMkJBQU0saUJBQU07QUFBRTNJLDhCQUFNNkMsT0FBTixDQUFjLFFBQWQ7QUFBMEI7QUFQNUMsaUJBRk0sRUFXTjtBQUNJa0Msd0JBQUcsWUFEUDtBQUVJcEUsMEJBQU0sUUFGVjtBQUdJc0gsMEJBQU0sTUFIVjtBQUlJN0gsMEJBQUssWUFKVDtBQUtJZ0ssNkJBQVMsaUJBTGI7QUFNSUMsNEJBQVEsa0JBTlo7QUFPSXpELDJCQUFPLEVBUFg7QUFRSVksd0JBQUc7QUFDQzhDLGtDQUFTLGtCQUFDQyxJQUFELEVBQU9DLElBQVAsRUFBZ0I7QUFBQ0MsNEJBQUEsMkVBQUFBLENBQWNDLE9BQWQsQ0FBc0JILElBQXRCO0FBQTZCO0FBRHhEO0FBUlAsaUJBWE0sRUF1Qk47QUFDSXhGLHdCQUFJLGdCQURSO0FBRUlwRSwwQkFBSyxNQUZUO0FBR0k2Ryx3QkFBRztBQUNDbUQseUNBQWdCLDJCQUFVO0FBQ3RCRiw0QkFBQSwyRUFBQUEsQ0FBY0csV0FBZCxDQUEwQixLQUFLeEIsUUFBTCxHQUFnQnlCLFdBQWhCLEVBQTFCO0FBQ0g7QUFIRjtBQUhQLGlCQXZCTSxFQWdDTixFQUFFakUsT0FBTyxDQUFULEVBaENNO0FBTGQsYUFSQyxFQWdERCxFQUFFMkIsUUFBTyxFQUFULEVBaERDLEVBaUREO0FBQ0k1SCxzQkFBSyxZQURUO0FBRUlvRSxvQkFBRyx3QkFGUDtBQUdJd0Usd0JBQU8sR0FIWDtBQUlJQyxzQkFBSztBQUNEaEQsMEJBQUssQ0FDRDtBQUNJN0YsOEJBQUssVUFEVDtBQUVJb0UsNEJBQUcsTUFGUDtBQUdJd0QsZ0NBQVEsSUFIWjtBQUlJQyxrQ0FBUyxFQUpiO0FBS0loQiw0QkFBRztBQUNDLCtDQUFtQix5QkFBU3NDLEtBQVQsRUFBZ0JnQixNQUFoQixFQUF3QkMsWUFBeEIsRUFBcUM7QUFDcEQsb0NBQUcsQ0FBQ0EsWUFBRCxJQUFpQiwyRUFBQU4sQ0FBY08sWUFBZCxDQUEyQkYsT0FBTy9GLEVBQWxDLEtBQXlDLENBQUMsQ0FBOUQsRUFBZ0U7QUFBQTs7QUFDNUQwRixvQ0FBQSwyRUFBQUEsQ0FBY1QsVUFBZCxDQUF5QmMsT0FBTy9GLEVBQWhDLHFEQUFxQytGLE9BQU8vRixFQUE1QyxJQUFnRCtFLE1BQU05QyxLQUF0RDtBQUNIO0FBQ0o7QUFMRjtBQUxQLHFCQURDO0FBREo7QUFKVCxhQWpEQztBQUZVLFNBQW5CO0FBMkVBeUQsUUFBQSwyRUFBQUEsQ0FBYzFELElBQWQsQ0FBbUIxQixXQUFuQixDQUErQixnQkFBL0IsRUFBaUQsWUFBVTtBQUN2RFAsZUFBRyxNQUFILEVBQVdtRyxNQUFYLENBQWtCLFVBQWxCLEVBQThCLDJFQUFBUixDQUFjUyxTQUFkLEVBQTlCO0FBQ0FwRyxlQUFHLE1BQUgsRUFBV3FHLE9BQVg7QUFDSCxTQUhEO0FBSUEsZUFBT2hCLFlBQVA7QUFDSCxLOzs7RUFsRnlDLDBEOzsrREFBekJELGdCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztJQUVxQmtCLFU7Ozs7Ozs7Ozt5QkFDakJ0SyxNLHFCQUFTO0FBQ0wsWUFBSXVLLFNBQVM7QUFDVHRHLGdCQUFHLFFBRE07QUFFVHlCLGtCQUFLLENBQ0QsaUVBREMsRUFFRDtBQUNJRyxzQkFBSyxDQUNELHNFQURDLEVBRUQ7QUFDSUgsMEJBQUssQ0FDRCxzRUFEQyxFQUVELEVBQUUrQixRQUFPLEVBQVQsRUFGQztBQURULGlCQUZDO0FBRFQsYUFGQztBQUZJLFNBQWI7O0FBa0JBLGVBQU84QyxNQUFQO0FBQ0gsSzs7O0VBckJtQywwRDs7K0RBQW5CRCxVOzs7Ozs7Ozs7Ozs7Ozs7QUNQckI7QUFDQTs7QUFFQTs7SUFFcUJFLGU7Ozs7Ozs7Ozs4QkFDakJ4SyxNLHFCQUFTO0FBQ0wsWUFBSXlLLGNBQWM7QUFDZHhHLGdCQUFHLHNCQURXO0FBRWRzRCx1QkFBVSxFQUZJO0FBR2RDLHVCQUFVLEVBSEk7QUFJZEwsa0JBQUssT0FKUztBQUtkekIsa0JBQUssQ0FBQztBQUNGekIsb0JBQUcsYUFERDtBQUVGcEUsc0JBQUssU0FGSDtBQUdGNEgsd0JBQU8sRUFITDtBQUlGTixzQkFBSyxPQUpIO0FBS0ZPLDBCQUFVLENBQ04sRUFBRTVCLE9BQU8sRUFBVCxFQURNLEVBRU47QUFDSTdCLHdCQUFHLGNBRFA7QUFFSXBFLDBCQUFNLFFBRlY7QUFHSXNILDBCQUFNLE1BSFY7QUFJSVEsMEJBQU0sd0JBSlY7QUFLSTdCLDJCQUFPLEVBTFg7QUFNSThCLHlCQUFLLFlBTlQ7QUFPSUMsMkJBQU0sZUFBQzVELEVBQUQsRUFBUTtBQUFFL0UsOEJBQU02QyxPQUFOLENBQWNrQyxFQUFkO0FBQW9CO0FBUHhDLGlCQUZNLEVBV047QUFDSUEsd0JBQUcsV0FEUDtBQUVJcEUsMEJBQU0sUUFGVjtBQUdJc0gsMEJBQU0sTUFIVjtBQUlJUSwwQkFBTSxLQUpWO0FBS0k3QiwyQkFBTyxFQUxYO0FBTUk4Qix5QkFBSyxZQU5UO0FBT0lDLDJCQUFNLGVBQUM1RCxFQUFELEVBQVE7QUFBRS9FLDhCQUFNNkMsT0FBTixDQUFja0MsRUFBZDtBQUFvQjtBQVB4QyxpQkFYTSxFQW9CTjtBQUNJQSx3QkFBRyxjQURQO0FBRUlwRSwwQkFBTSxRQUZWO0FBR0lzSCwwQkFBTSxNQUhWO0FBSUlRLDBCQUFNLGNBSlY7QUFLSTdCLDJCQUFPLEVBTFg7QUFNSThCLHlCQUFLLFlBTlQ7QUFPSUMsMkJBQU0sZUFBQzVELEVBQUQsRUFBUTtBQUFFL0UsOEJBQU02QyxPQUFOLENBQWNrQyxFQUFkO0FBQW9CO0FBUHhDLGlCQXBCTSxFQTZCTjtBQUNJQSx3QkFBRyxjQURQO0FBRUlwRSwwQkFBTSxRQUZWO0FBR0lzSCwwQkFBTSxNQUhWO0FBSUlRLDBCQUFNLHNCQUpWO0FBS0k3QiwyQkFBTyxFQUxYO0FBTUk4Qix5QkFBSyxZQU5UO0FBT0lDLDJCQUFNLGVBQUM1RCxFQUFELEVBQVE7QUFBRS9FLDhCQUFNNkMsT0FBTixDQUFja0MsRUFBZDtBQUFvQjtBQVB4QyxpQkE3Qk0sRUFzQ047QUFDSUEsd0JBQUcsZUFEUDtBQUVJcEUsMEJBQU0sUUFGVjtBQUdJc0gsMEJBQU0sTUFIVjtBQUlJUSwwQkFBTSx1QkFKVjtBQUtJN0IsMkJBQU8sRUFMWDtBQU1JOEIseUJBQUssWUFOVDtBQU9JQywyQkFBTSxlQUFDNUQsRUFBRCxFQUFRO0FBQUUvRSw4QkFBTTZDLE9BQU4sQ0FBY2tDLEVBQWQ7QUFBb0I7QUFQeEMsaUJBdENNLEVBK0NOLEVBL0NNLEVBZ0ROO0FBQ0lwRSwwQkFBTSxRQURWO0FBRUlzSCwwQkFBTSxNQUZWO0FBR0lRLDBCQUFNLE1BSFY7QUFJSTdCLDJCQUFPLEVBSlg7QUFLSThCLHlCQUFLLFlBTFQ7QUFNSUMsMkJBQU0saUJBQU07QUFDUiw0QkFBSTdELEdBQUcsa0JBQUgsRUFBdUIwRyxTQUF2QixFQUFKLEVBQXdDO0FBQ3BDMUcsK0JBQUcsa0JBQUgsRUFBdUIyRyxJQUF2QjtBQUNILHlCQUZELE1BRU87QUFDSDNHLCtCQUFHLGtCQUFILEVBQXVCbEUsSUFBdkI7QUFDSDtBQUNEa0UsMkJBQUcsZUFBSCxFQUFvQjRHLE1BQXBCO0FBQ0E1RywyQkFBRyxzQkFBSCxFQUEyQjZHLE1BQTNCO0FBQ0g7QUFkTCxpQkFoRE0sRUFnRU4sRUFBRS9FLE9BQU8sRUFBVCxFQWhFTTtBQUxSLGFBQUQ7QUFMUyxTQUFsQjs7QUErRUEsZUFBTzJFLFdBQVA7QUFDSCxLOzs7RUFsRndDLDBEOzsrREFBeEJELGU7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMckI7QUFDQTtBQUNBOztJQUVxQk0sb0I7Ozs7Ozs7OzttQ0FDakI5SyxNLHFCQUFTO0FBQUE7O0FBQ0wsWUFBSStLLFVBQVU7QUFDVjlHLGdCQUFHLGlCQURPO0FBRVZ5QixrQkFBSyxDQUNEO0FBQ0l6QixvQkFBRyxhQURQO0FBRUkrRyx5QkFBUSxLQUZaO0FBR0lDLHVCQUFNLENBQ0YsRUFBRWhILElBQUcsYUFBTCxFQUFvQnBFLE1BQUssVUFBekIsRUFBcUN1SSxVQUFTLGNBQTlDLEVBREU7O0FBSFYsYUFEQyxFQVNEO0FBQ0l2SSxzQkFBSyxRQURUO0FBRUlvRSxvQkFBRyxjQUZQO0FBR0lpSCw0QkFBVyxPQUhmO0FBSUlDLDJCQUFVLENBSmQ7QUFLSTFELHdCQUFPLEVBTFg7QUFNSUksdUJBQU0saUJBQWE7QUFBQSxzREFBVHJELElBQVM7QUFBVEEsNEJBQVM7QUFBQTs7QUFDZix3QkFBS0EsS0FBSyxDQUFMLEVBQVE0RyxNQUFSLENBQWVDLFNBQWYsS0FBNkIscUNBQWxDLEVBQTBFO0FBQ3RFO0FBQ0E7QUFDSCxxQkFIRCxNQUdPO0FBQ0gsK0JBQUtDLFVBQUw7QUFDSDtBQUNKLGlCQWJMO0FBY0k1RSxvQkFBRztBQUNDNkUsc0NBQWlCLDBCQUFDdEgsRUFBRCxFQUFRO0FBQ3JCRCwyQkFBRyxhQUFILEVBQWtCd0gsVUFBbEIsQ0FBNkJ4SCxHQUFHLGNBQUgsRUFBbUJzRSxRQUFuQixFQUE3QjtBQUNBdEUsMkJBQUcsY0FBSCxFQUFtQmtGLFVBQW5CLENBQThCakYsRUFBOUIsRUFBaUMsRUFBQ2dGLFFBQU8sS0FBUixFQUFlRSxRQUFPLElBQXRCLEVBQWpDO0FBQ0g7QUFKRixpQkFkUDtBQW9CSXNDLHlCQUFRLENBQUUsRUFBRXhILElBQUcsYUFBTCxFQUFvQmlDLE9BQU0sT0FBMUIsRUFBbUNKLE9BQU0sR0FBekMsRUFBRixDQXBCWjtBQXFCSTRGLDJCQUFVLE1BckJkO0FBc0JJdkUsc0JBQUs7QUF0QlQsYUFUQztBQUZLLFNBQWQ7QUFxQ0FuRCxXQUFHLGNBQUgsRUFBbUJPLFdBQW5CLENBQStCLGNBQS9CLEVBQStDLFVBQVNOLEVBQVQsRUFBYWdDLElBQWIsRUFBa0I7QUFDN0QsZ0JBQUdBLEtBQUtnRCxNQUFMLElBQWVoRCxLQUFLc0UsTUFBdkIsRUFBOEI7QUFDMUIsb0JBQUlvQixZQUFZLENBQWhCO0FBQ0Esb0JBQUcxRixLQUFLc0UsTUFBUixFQUFnQm9CLFlBQVksQ0FBWjtBQUNoQixvQkFBRzNILEdBQUcsY0FBSCxFQUFtQmhFLE1BQW5CLENBQTBCeUwsT0FBMUIsQ0FBa0N4SyxNQUFsQyxHQUEyQzBLLFNBQTlDLEVBQXdEO0FBQ3BEek0sMEJBQU02QyxPQUFOLENBQWMsc0JBQWQ7QUFDSCxpQkFGRCxNQUVLO0FBQ0RpQyx1QkFBRyxhQUFILEVBQWtCNEgsT0FBbEIsQ0FBMEIsRUFBQzNILElBQUdBLEVBQUosRUFBUXBFLE1BQUtvRyxLQUFLc0UsTUFBbEIsRUFBMUI7QUFDQXZHLHVCQUFHLGNBQUgsRUFBbUI2SCxTQUFuQixDQUE2QixFQUFDNUgsSUFBR0EsRUFBSixFQUFRaUMsT0FBTUQsS0FBS0MsS0FBbkIsRUFBMEI0RixPQUFNLElBQWhDLEVBQXNDbkUsTUFBSyxNQUEzQyxFQUFtRDdCLE9BQU8sR0FBMUQsRUFBN0IsRUFBNkYsSUFBN0Y7QUFDSDtBQUNKO0FBQ0osU0FYRDtBQVlBLGVBQU9pRixPQUFQO0FBQ0gsSzs7QUFFRDtBQUNBO21DQUNBTyxVLHlCQUFhO0FBQ1QsWUFBSVMsUUFBUS9ILEdBQUcsY0FBSCxFQUFtQnNFLFFBQW5CLEVBQVo7QUFDQTdELGdCQUFRb0MsR0FBUixDQUFZa0YsUUFBUSw4QkFBcEI7QUFDSCxLOzs7RUEzRDZDLDBEOzsrREFBN0JqQixvQjs7Ozs7Ozs7Ozs7Ozs7OztBQ0pyQjtBQUNBO0FBQ0E7O0lBRXFCa0Isb0I7Ozs7Ozs7OzttQ0FDakJoTSxNLHFCQUFTO0FBQ0wsWUFBSWlNLGFBQWE7QUFDYnBNLGtCQUFLLFlBRFE7QUFFYm9FLGdCQUFHLHNCQUZVO0FBR2J3RSxvQkFBTyxHQUhNO0FBSWJDLGtCQUFLO0FBQ0RoRCxzQkFBSyxDQUNEO0FBQ0l6Qix3QkFBRyxrQkFEUDtBQUVJcEUsMEJBQUssVUFGVDtBQUdJdUksOEJBQVMsWUFIYjtBQUlJakIsMEJBQUssUUFKVDtBQUtJTSw0QkFBTyxFQUxYO0FBTUkxQixnQ0FBVyxJQU5mO0FBT0lvRCw0QkFBTztBQVBYLGlCQURDLEVBVUQ7QUFDSWxGLHdCQUFHLGVBRFA7QUFFSXBFLDBCQUFNLFNBRlY7QUFHSW9HLDBCQUFNLHFFQUhWO0FBSUlpRyw4QkFBUyxPQUpiO0FBS0l6RSw0QkFBUSxJQUxaO0FBTUkwRSwrQkFBVSxJQU5kO0FBT0l6Rix3QkFBRztBQUNDMEYsdUNBQWUsdUJBQVNuSSxFQUFULEVBQVk7QUFDdkI7QUFDQS9FLGtDQUFNNkMsT0FBTixDQUFjLGVBQWUsS0FBSytHLE9BQUwsQ0FBYTdFLEVBQWIsRUFBaUJpQyxLQUE5QztBQUNIO0FBSkY7QUFQUCxpQkFWQztBQURKO0FBSlEsU0FBakI7O0FBaUNBLGVBQU8rRixVQUFQO0FBQ0gsSzs7O0VBcEM2QywwRDs7K0RBQTdCRCxvQjs7Ozs7Ozs7OztBQ0pyQixTQUFTSyxNQUFULENBQWdCNU0sRUFBaEIsRUFBb0J3RSxFQUFwQixFQUF3QjtBQUNwQixRQUFNZ0gsUUFBUXhMLEdBQUc2TSxhQUFILEVBQWQ7QUFDQSx5QkFBbUJyQixLQUFuQixrSEFBMEI7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBLFlBQWZsSCxJQUFlOztBQUN0QixZQUFJd0ksTUFBTXhJLElBQVY7QUFDQSxZQUFJd0ksSUFBSXZNLE1BQUosQ0FBV3dNLE9BQVgsS0FBdUJ2SSxFQUEzQixFQUErQjtBQUMzQixtQkFBT3NJLEdBQVA7QUFDSCxTQUZELE1BR0s7QUFDREEsa0JBQU1GLE9BQU9FLEdBQVAsRUFBWXRJLEVBQVosQ0FBTjtBQUNIO0FBQ0QsWUFBSXNJLEdBQUosRUFBUztBQUNMLG1CQUFPQSxHQUFQO0FBQ0g7QUFDSjtBQUNELFdBQU8sSUFBUDtBQUNIO0FBQ0QsSUFBYUUsT0FBYjtBQUNJLHVCQUFjO0FBQUE7O0FBQ1YsYUFBS0MsR0FBTCxHQUFXeE4sTUFBTXlOLEdBQU4sRUFBWDtBQUNBLGFBQUtDLE9BQUwsR0FBZSxFQUFmO0FBQ0EsYUFBS2pLLEtBQUwsR0FBYSxFQUFiO0FBQ0g7O0FBTEwsc0JBTUlrSyxPQU5KLHNCQU1jO0FBQ04sZUFBTyxLQUFLekssS0FBWjtBQUNILEtBUkw7O0FBQUEsc0JBU0lGLFVBVEoseUJBU2lCO0FBQ1QsWUFBTTRLLFNBQVMsS0FBS0YsT0FBcEI7QUFDQSxhQUFLLElBQUk1TCxJQUFJOEwsT0FBTzdMLE1BQVAsR0FBZ0IsQ0FBN0IsRUFBZ0NELEtBQUssQ0FBckMsRUFBd0NBLEdBQXhDLEVBQTZDO0FBQ3pDOEwsbUJBQU85TCxDQUFQLEVBQVUrTCxHQUFWLENBQWNDLFdBQWQsQ0FBMEJGLE9BQU85TCxDQUFQLEVBQVVpRCxFQUFwQztBQUNIO0FBQ0Q7QUFDQSxhQUFLLElBQU1YLEdBQVgsSUFBa0IsS0FBS1gsS0FBdkIsRUFBOEI7QUFDMUIsZ0JBQU1zSyxVQUFVLEtBQUt0SyxLQUFMLENBQVdXLEdBQVgsRUFBZ0J6RCxJQUFoQztBQUNBO0FBQ0E7QUFDQSxnQkFBSW9OLE9BQUosRUFBYTtBQUNUQSx3QkFBUS9LLFVBQVI7QUFDSDtBQUNKO0FBQ0QsYUFBSzBLLE9BQUwsR0FBZSxLQUFLOUosVUFBTCxHQUFrQixLQUFLb0ssSUFBTCxHQUFZLEtBQUtDLE9BQUwsR0FBZSxJQUE1RDtBQUNILEtBeEJMOztBQUFBLHNCQXlCSXZOLE1BekJKLG1CQXlCV3dOLElBekJYLEVBeUJpQjNNLEdBekJqQixFQXlCc0JOLE1BekJ0QixFQXlCOEI7QUFBQTs7QUFDdEIsYUFBS2dOLE9BQUwsR0FBZWhOLE1BQWY7QUFDQSxZQUFJTSxHQUFKLEVBQVM7QUFDTCxpQkFBS0ksTUFBTCxHQUFjSixJQUFJLENBQUosRUFBT1MsS0FBckI7QUFDSDtBQUNEa00sZUFBT0EsUUFBUUMsU0FBUzNFLElBQXhCO0FBQ0EsWUFBTTVGLGFBQWMsT0FBT3NLLElBQVAsS0FBZ0IsUUFBakIsR0FBNkJsTyxNQUFNb08sTUFBTixDQUFhRixJQUFiLENBQTdCLEdBQWtEQSxJQUFyRTtBQUNBLFlBQUksS0FBS3RLLFVBQUwsS0FBb0JBLFVBQXhCLEVBQW9DO0FBQ2hDLGlCQUFLQSxVQUFMLEdBQWtCQSxVQUFsQjtBQUNBLG1CQUFPLEtBQUtQLE9BQUwsQ0FBYTlCLEdBQWIsRUFBa0JhLElBQWxCLENBQXVCO0FBQUEsdUJBQU0sTUFBS3VMLE9BQUwsRUFBTjtBQUFBLGFBQXZCLENBQVA7QUFDSCxTQUhELE1BSUs7QUFDRCxtQkFBTyxLQUFLN0osVUFBTCxDQUFnQnZDLEdBQWhCLEVBQXFCYSxJQUFyQixDQUEwQjtBQUFBLHVCQUFNLE1BQUt1TCxPQUFMLEVBQU47QUFBQSxhQUExQixDQUFQO0FBQ0g7QUFDSixLQXZDTDs7QUFBQSxzQkF3Q0lVLFFBeENKLHVCQXdDZTtBQUNQLGVBQU8sS0FBSzFNLE1BQVo7QUFDSCxLQTFDTDs7QUFBQSxzQkEyQ0kyTSxLQTNDSixvQkEyQ1k7QUFDSixlQUFPLEtBQUtkLEdBQVo7QUFDSCxLQTdDTDs7QUFBQSxzQkE4Q0l0TSxhQTlDSiw0QkE4Q29CO0FBQ1osZUFBTyxLQUFLK00sT0FBWjtBQUNILEtBaERMOztBQUFBLHNCQWlESW5KLEVBakRKLGVBaURPQyxFQWpEUCxFQWlEVztBQUNILFlBQUlwRSxPQUFPWCxNQUFNOEUsRUFBTixDQUFTQyxFQUFULENBQVg7QUFDQSxZQUFJLENBQUNwRSxJQUFMLEVBQVc7QUFDUEEsbUJBQU93TSxPQUFPLEtBQUtRLE9BQUwsRUFBUCxFQUF1QjVJLEVBQXZCLENBQVA7QUFDSDtBQUNELGVBQU9wRSxJQUFQO0FBQ0gsS0F2REw7O0FBQUEsc0JBd0RJNkcsRUF4REosZUF3RE9xRyxHQXhEUCxFQXdEWXpOLElBeERaLEVBd0RrQm1PLElBeERsQixFQXdEd0I7QUFDaEIsWUFBTXhKLEtBQUs4SSxJQUFJeEksV0FBSixDQUFnQmpGLElBQWhCLEVBQXNCbU8sSUFBdEIsQ0FBWDtBQUNBLGFBQUtiLE9BQUwsQ0FBYWpOLElBQWIsQ0FBa0IsRUFBRW9OLFFBQUYsRUFBTzlJLE1BQVAsRUFBbEI7QUFDQSxlQUFPQSxFQUFQO0FBQ0gsS0E1REw7O0FBQUEsc0JBNkRJeUosUUE3REoscUJBNkRhN04sSUE3RGIsRUE2RG1CO0FBQ1gsYUFBSyxJQUFNeUQsR0FBWCxJQUFrQixLQUFLWCxLQUF2QixFQUE4QjtBQUMxQixnQkFBTTRKLE1BQU0sS0FBSzVKLEtBQUwsQ0FBV1csR0FBWCxFQUFnQnpELElBQTVCO0FBQ0EsZ0JBQUkwTSxRQUFRMU0sSUFBUixJQUFnQjBNLElBQUltQixRQUFKLENBQWE3TixJQUFiLENBQXBCLEVBQXdDO0FBQ3BDLHVCQUFPLElBQVA7QUFDSDtBQUNKO0FBQ0QsZUFBTyxLQUFQO0FBQ0gsS0FyRUw7O0FBQUEsc0JBc0VJUyxVQXRFSix1QkFzRWVoQixJQXRFZixFQXNFcUI7QUFDYixZQUFNZSxNQUFNLEtBQUtzQyxLQUFMLENBQVdyRCxRQUFRLFNBQW5CLENBQVo7QUFDQSxZQUFJZSxHQUFKLEVBQVM7QUFDTCxtQkFBTyxFQUFFb0IsU0FBU3BCLEdBQVgsRUFBZ0JGLFFBQVEsSUFBeEIsRUFBUDtBQUNIO0FBQ0Q7QUFDQSxZQUFJLEtBQUtnTixPQUFULEVBQWtCO0FBQ2QsbUJBQU8sS0FBS0EsT0FBTCxDQUFhN00sVUFBYixDQUF3QmhCLElBQXhCLENBQVA7QUFDSDtBQUNELGVBQU8sSUFBUDtBQUNILEtBaEZMOztBQUFBLHNCQWlGSXFPLE9BakZKLHNCQWlGYztBQUNOLGVBQU8sS0FBS3BPLEtBQVo7QUFDSCxLQW5GTDs7QUFBQTtBQUFBLEk7Ozs7Ozs7Ozs7QUNoQk8sU0FBU3FPLElBQVQsQ0FBY0MsSUFBZCxFQUFvQkMsSUFBcEIsRUFBMEI7QUFDN0IsUUFBSTlNLElBQUksQ0FBUjtBQUNBLFNBQUtBLENBQUwsRUFBUUEsSUFBSThNLEtBQUs3TSxNQUFqQixFQUF5QkQsR0FBekIsRUFBOEI7QUFDMUIsWUFBTStNLE9BQU9GLEtBQUs3TSxDQUFMLENBQWI7QUFDQSxZQUFNZ04sUUFBUUYsS0FBSzlNLENBQUwsQ0FBZDtBQUNBLFlBQUksQ0FBQytNLElBQUwsRUFBVztBQUNQO0FBQ0g7QUFDRCxZQUFJQSxLQUFLRSxJQUFMLEtBQWNELE1BQU1DLElBQXhCLEVBQThCO0FBQzFCO0FBQ0g7QUFDRCxhQUFLLElBQU0zSyxHQUFYLElBQWtCeUssS0FBS0csTUFBdkIsRUFBK0I7QUFDM0IsZ0JBQUlILEtBQUtHLE1BQUwsQ0FBWTVLLEdBQVosTUFBcUIwSyxNQUFNRSxNQUFOLENBQWE1SyxHQUFiLENBQXpCLEVBQTRDO0FBQ3hDO0FBQ0g7QUFDSjtBQUNKO0FBQ0QsV0FBT3RDLENBQVA7QUFDSDtBQUNNLFNBQVNSLEtBQVQsQ0FBZUMsR0FBZixFQUFvQjtBQUN2QjtBQUNBLFFBQUlBLElBQUksQ0FBSixNQUFXLEdBQWYsRUFBb0I7QUFDaEJBLGNBQU1BLElBQUlSLE1BQUosQ0FBVyxDQUFYLENBQU47QUFDSDtBQUNEO0FBQ0EsUUFBTWtPLFFBQVExTixJQUFJMk4sS0FBSixDQUFVLEdBQVYsQ0FBZDtBQUNBLFFBQU1DLFNBQVMsRUFBZjtBQUNBO0FBQ0EsU0FBSyxJQUFJck4sSUFBSSxDQUFiLEVBQWdCQSxJQUFJbU4sTUFBTWxOLE1BQTFCLEVBQWtDRCxHQUFsQyxFQUF1QztBQUNuQyxZQUFNc04sT0FBT0gsTUFBTW5OLENBQU4sQ0FBYjtBQUNBLFlBQU15QixTQUFTLEVBQWY7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFJOEwsTUFBTUQsS0FBS3BPLE9BQUwsQ0FBYSxHQUFiLENBQVY7QUFDQSxZQUFJcU8sUUFBUSxDQUFDLENBQWIsRUFBZ0I7QUFDWkEsa0JBQU1ELEtBQUtwTyxPQUFMLENBQWEsR0FBYixDQUFOO0FBQ0g7QUFDRCxZQUFJcU8sUUFBUSxDQUFDLENBQWIsRUFBZ0I7QUFDWixnQkFBTUwsU0FBU0ksS0FBS3JPLE1BQUwsQ0FBWXNPLE1BQU0sQ0FBbEIsRUFBcUJILEtBQXJCLENBQTJCLFdBQTNCLENBQWY7QUFDQTtBQUNBLGlDQUFvQkYsTUFBcEIsa0hBQTRCO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQSxvQkFBakJNLEtBQWlCOztBQUN4QixvQkFBTUMsU0FBU0QsTUFBTUosS0FBTixDQUFZLEdBQVosQ0FBZjtBQUNBM0wsdUJBQU9nTSxPQUFPLENBQVAsQ0FBUCxJQUFvQkEsT0FBTyxDQUFQLENBQXBCO0FBQ0g7QUFDSjtBQUNEO0FBQ0FKLGVBQU9yTixDQUFQLElBQVk7QUFDUmlOLGtCQUFPTSxNQUFNLENBQUMsQ0FBUCxHQUFXRCxLQUFLck8sTUFBTCxDQUFZLENBQVosRUFBZXNPLEdBQWYsQ0FBWCxHQUFpQ0QsSUFEaEM7QUFFUkosb0JBQVF6TCxNQUZBLEVBRVF2QixPQUFPRixJQUFJO0FBRm5CLFNBQVo7QUFJSDtBQUNEO0FBQ0EsV0FBT3FOLE1BQVA7QUFDSDtBQUNNLFNBQVNqTixPQUFULENBQWlCc04sS0FBakIsRUFBd0I7QUFDM0IsUUFBTWpPLE1BQU0sRUFBWjtBQUNBLDBCQUFvQmlPLEtBQXBCLHlIQUEyQjtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBQUEsWUFBaEJDLEtBQWdCOztBQUN2QmxPLFlBQUlkLElBQUosQ0FBUyxNQUFNZ1AsTUFBTVYsSUFBckI7QUFDQSxZQUFNQyxTQUFTVSxRQUFRRCxNQUFNVCxNQUFkLENBQWY7QUFDQSxZQUFJQSxNQUFKLEVBQVk7QUFDUnpOLGdCQUFJZCxJQUFKLENBQVMsTUFBTXVPLE1BQWY7QUFDSDtBQUNKO0FBQ0QsV0FBT3pOLElBQUlvTyxJQUFKLENBQVMsRUFBVCxDQUFQO0FBQ0g7QUFDRCxTQUFTRCxPQUFULENBQWlCN0IsR0FBakIsRUFBc0I7QUFDbEIsUUFBTStCLE1BQU0sRUFBWjtBQUNBLFNBQUssSUFBTXhMLEdBQVgsSUFBa0J5SixHQUFsQixFQUF1QjtBQUNuQixZQUFJK0IsSUFBSTdOLE1BQVIsRUFBZ0I7QUFDWjZOLGdCQUFJblAsSUFBSixDQUFTLEdBQVQ7QUFDSDtBQUNEbVAsWUFBSW5QLElBQUosQ0FBUzJELE1BQU0sR0FBTixHQUFZeUosSUFBSXpKLEdBQUosQ0FBckI7QUFDSDtBQUNELFdBQU93TCxJQUFJRCxJQUFKLENBQVMsRUFBVCxDQUFQO0FBQ0gsQzs7Ozs7Ozs7Ozs7O0FDM0VEO0FBQ0EsSUFBYUUsVUFBYjtBQUNJLHdCQUFZQyxFQUFaLEVBQWdCaFAsTUFBaEIsRUFBd0I7QUFBQTs7QUFBQTs7QUFDcEIsYUFBS0EsTUFBTCxHQUFjQSxVQUFVLEVBQXhCO0FBQ0EsWUFBSWlQLE1BQU0sYUFBVUMsR0FBVixFQUFlLENBQUcsQ0FBNUI7QUFDQUMsUUFBQSwrREFBQUEsQ0FBTyxJQUFQLEVBQWEsZUFBTztBQUNoQixrQkFBS0MsUUFBTCxHQUFnQixFQUFoQjtBQUNBLG1CQUFPSCxJQUFJLE1BQUtyTyxHQUFMLEVBQUosQ0FBUDtBQUNILFNBSEQ7QUFJQXFPLGNBQU1ELEVBQU47QUFDSDs7QUFUTCx5QkFVSTlLLEdBVkosZ0JBVVFuRSxJQVZSLEVBVWNDLE1BVmQsRUFVc0I7QUFDZCxZQUFJLEtBQUtBLE1BQUwsQ0FBWXFQLE1BQWhCLEVBQXdCO0FBQ3BCLGdCQUFNQyxVQUFVdlAsS0FBS3FPLEtBQUwsQ0FBVyxHQUFYLEVBQWdCLENBQWhCLENBQWhCO0FBQ0EsaUJBQUssSUFBTTlLLEdBQVgsSUFBa0IsS0FBS3RELE1BQUwsQ0FBWXFQLE1BQTlCLEVBQXNDO0FBQ2xDLG9CQUFJLEtBQUtyUCxNQUFMLENBQVlxUCxNQUFaLENBQW1CL0wsR0FBbkIsTUFBNEJnTSxRQUFRLENBQVIsQ0FBaEMsRUFBNEM7QUFDeEN2UCwyQkFBT3VELE9BQU9nTSxRQUFRck8sTUFBUixHQUFpQixDQUFqQixHQUFxQixNQUFNcU8sUUFBUSxDQUFSLENBQTNCLEdBQXdDLEVBQS9DLENBQVA7QUFDQTtBQUNIO0FBQ0o7QUFDSjtBQUNELGFBQUtGLFFBQUwsR0FBZ0JyUCxJQUFoQjtBQUNBb1AsUUFBQSwrREFBQUEsQ0FBT0ksUUFBUCxDQUFnQixNQUFNeFAsSUFBdEIsRUFBNEJDLE1BQTVCO0FBQ0gsS0F0Qkw7O0FBQUEseUJBdUJJWSxHQXZCSixrQkF1QlU7QUFDRixZQUFJYixPQUFPLEtBQUtxUCxRQUFMLElBQWlCLENBQUNwUSxPQUFPd1EsUUFBUCxDQUFnQkMsSUFBaEIsSUFBd0IsRUFBekIsRUFBNkJDLE9BQTdCLENBQXFDLElBQXJDLEVBQTJDLEVBQTNDLENBQTVCO0FBQ0EsWUFBSSxLQUFLMVAsTUFBTCxDQUFZcVAsTUFBaEIsRUFBd0I7QUFDcEIsZ0JBQU1DLFVBQVV2UCxLQUFLcU8sS0FBTCxDQUFXLEdBQVgsRUFBZ0IsQ0FBaEIsQ0FBaEI7QUFDQSxnQkFBTTlLLE1BQU0sS0FBS3RELE1BQUwsQ0FBWXFQLE1BQVosQ0FBbUJDLFFBQVEsQ0FBUixDQUFuQixDQUFaO0FBQ0EsZ0JBQUloTSxHQUFKLEVBQVM7QUFDTHZELHVCQUFPdUQsT0FBT2dNLFFBQVFyTyxNQUFSLEdBQWlCLENBQWpCLEdBQXFCLE1BQU1xTyxRQUFRLENBQVIsQ0FBM0IsR0FBd0MsRUFBL0MsQ0FBUDtBQUNIO0FBQ0o7QUFDRCxlQUFPdlAsSUFBUDtBQUNILEtBakNMOztBQUFBO0FBQUEsSTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7SUFFcUI0UCxrQjs7Ozs7Ozs7O2lDQUNqQjNQLE0scUJBQVM7QUFDTCxZQUFJUCxLQUFLO0FBQ0x3RSxnQkFBRyxRQURFO0FBRUx5QixrQkFBSyxDQUNELHdEQURDLEVBRUQ7QUFDSUcsc0JBQUssQ0FDRCw4REFEQyxFQUVEO0FBQ0k1Qix3QkFBRyxvQkFEUDtBQUVJcEUsMEJBQU07QUFGVixpQkFGQyxFQU1ELDREQU5DO0FBRFQsYUFGQztBQUZBLFNBQVQ7QUFnQkEsZUFBT0osRUFBUDtBQUNILEs7OztFQW5CMkMsMEQ7OytEQUEzQmtRLGtCOzs7Ozs7QUNOckIsMEI7Ozs7Ozs7Ozs7OztBQ0FBLHlDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQWFDLE1BQWI7QUFBQTs7QUFDSSxvQkFBWTVQLE1BQVosRUFBb0I7QUFBQTs7QUFBQSxxREFDaEIsbUJBRGdCOztBQUVoQixjQUFLZCxLQUFMLEdBQWFjLE9BQU9kLEtBQVAsSUFBZ0JBLEtBQTdCO0FBQ0E7QUFDQSxjQUFLYyxNQUFMLEdBQWMsTUFBS2QsS0FBTCxDQUFXMlEsTUFBWCxDQUFrQjtBQUM1QnZRLGtCQUFNLEtBRHNCO0FBRTVCOEUscUJBQVMsS0FGbUI7QUFHNUJDLG1CQUFPO0FBSHFCLFNBQWxCLEVBSVhyRSxNQUpXLEVBSUgsSUFKRyxDQUFkO0FBS0EsY0FBS1QsS0FBTCxHQUFhLE1BQUtTLE1BQUwsQ0FBWVYsSUFBekI7QUFDQSxjQUFLd1EsU0FBTCxHQUFpQixFQUFqQjtBQUNBNVEsY0FBTTJRLE1BQU4sUUFBbUIzUSxNQUFNNlEsV0FBekI7QUFDQTdRLGNBQU1xRixXQUFOLENBQWtCLFNBQWxCLEVBQTZCO0FBQUEsbUJBQUssTUFBS3lMLFlBQUwsQ0FBa0IvTSxDQUFsQixDQUFMO0FBQUEsU0FBN0I7QUFaZ0I7QUFhbkI7O0FBZEwscUJBZUlnTixVQWZKLHVCQWVlM1EsSUFmZixFQWVxQjtBQUNiLFlBQUl5TixNQUFNLEtBQUsrQyxTQUFMLENBQWV4USxJQUFmLENBQVY7QUFDQSxZQUFJLE9BQU95TixHQUFQLEtBQWUsVUFBbkIsRUFBK0I7QUFDM0JBLGtCQUFNLEtBQUsrQyxTQUFMLENBQWV4USxJQUFmLElBQXVCeU4sSUFBSSxJQUFKLENBQTdCO0FBQ0g7QUFDRCxlQUFPQSxHQUFQO0FBQ0gsS0FyQkw7O0FBQUEscUJBc0JJbUQsVUF0QkosdUJBc0JlNVEsSUF0QmYsRUFzQnFCNlEsT0F0QnJCLEVBc0I4QjtBQUN0QixhQUFLTCxTQUFMLENBQWV4USxJQUFmLElBQXVCNlEsT0FBdkI7QUFDSCxLQXhCTDtBQXlCSTs7O0FBekJKLHFCQTBCSXpOLFVBMUJKLHVCQTBCZXFLLEdBMUJmLEVBMEJvQjNCLE1BMUJwQixFQTBCNEJwTCxNQTFCNUIsRUEwQm9DO0FBQzVCO0FBQ0EsWUFBSStNLElBQUlxRCxHQUFSLEVBQWE7QUFDVHJELGtCQUFNLEVBQUVzRCxVQUFVLElBQUkscUVBQUosQ0FBa0IsSUFBbEIsRUFBd0IsRUFBeEIsRUFBNEJ0RCxHQUE1QixDQUFaLEVBQU47QUFDSCxTQUZELE1BR0ssSUFBSUEsZUFBZTZDLE1BQW5CLEVBQTJCO0FBQzVCN0Msa0JBQU0sRUFBRXNELFVBQVV0RCxHQUFaLEVBQU47QUFDSDtBQUNEO0FBQ0EsWUFBSUEsSUFBSXNELFFBQVIsRUFBa0I7QUFDZCxtQkFBTyxLQUFLQyxVQUFMLENBQWdCdkQsR0FBaEIsRUFBcUIzQixNQUFyQixFQUE2QnBMLE1BQTdCLENBQVA7QUFDSDtBQUNEO0FBQ0FvTCxpQkFBU0EsV0FBVzJCLGVBQWV3RCxLQUFmLEdBQXVCLEVBQXZCLEdBQTRCLEVBQXZDLENBQVQ7QUFDQSxhQUFLLElBQU1DLE1BQVgsSUFBcUJ6RCxHQUFyQixFQUEwQjtBQUN0QixnQkFBSTBELFFBQVExRCxJQUFJeUQsTUFBSixDQUFaO0FBQ0E7QUFDQSxnQkFBSSxPQUFPQyxLQUFQLEtBQWlCLFVBQWpCLElBQ0FBLE1BQU1DLFNBRE4sSUFDbUJELE1BQU1DLFNBQU4sQ0FBZ0IxUSxNQUR2QyxFQUMrQztBQUMzQ3lRLHdCQUFRLEVBQUVKLFVBQVVJLEtBQVosRUFBUjtBQUNIO0FBQ0QsZ0JBQUlBLFNBQVMsUUFBT0EsS0FBUCx5Q0FBT0EsS0FBUCxPQUFpQixRQUExQixJQUNBLEVBQUVBLGlCQUFpQnZSLE1BQU15UixjQUF6QixDQURKLEVBQzhDO0FBQzFDLG9CQUFJRixpQkFBaUJHLElBQXJCLEVBQTJCO0FBQ3ZCeEYsMkJBQU9vRixNQUFQLElBQWlCLElBQUlJLElBQUosQ0FBU0gsS0FBVCxDQUFqQjtBQUNILGlCQUZELE1BR0s7QUFDRHJGLDJCQUFPb0YsTUFBUCxJQUFpQixLQUFLOU4sVUFBTCxDQUFnQitOLEtBQWhCLEVBQXdCQSxpQkFBaUJGLEtBQWpCLEdBQXlCLEVBQXpCLEdBQThCLEVBQXRELEVBQTJEdlEsTUFBM0QsQ0FBakI7QUFDSDtBQUNKLGFBUkQsTUFTSztBQUNEb0wsdUJBQU9vRixNQUFQLElBQWlCQyxLQUFqQjtBQUNIO0FBQ0o7QUFDRCxlQUFPckYsTUFBUDtBQUNILEtBN0RMOztBQUFBLHFCQThESXpLLFNBOURKLHdCQThEZ0I7QUFDUixlQUFPLEtBQUtrUSxPQUFaO0FBQ0gsS0FoRUw7O0FBQUEscUJBaUVJYixZQWpFSix5QkFpRWlCL00sQ0FqRWpCLEVBaUVvQjtBQUNaLFlBQUlBLENBQUosRUFBTztBQUNILGdCQUFNbUksU0FBVW5JLEVBQUVtSSxNQUFGLElBQVluSSxFQUFFNk4sVUFBOUI7QUFDQSxnQkFBSTFGLFVBQVVBLE9BQU8yRixZQUFyQixFQUFtQztBQUMvQixvQkFBTUMsVUFBVTVGLE9BQU8yRixZQUFQLENBQW9CLFNBQXBCLENBQWhCO0FBQ0Esb0JBQUlDLE9BQUosRUFBYTtBQUNULHlCQUFLQSxPQUFMLENBQWFBLE9BQWI7QUFDSDtBQUNELG9CQUFNQyxRQUFRN0YsT0FBTzJGLFlBQVAsQ0FBb0IsT0FBcEIsQ0FBZDtBQUNBLG9CQUFJRSxLQUFKLEVBQVc7QUFDUCx5QkFBS25SLElBQUwsQ0FBVW1SLEtBQVY7QUFDSDtBQUNKO0FBQ0o7QUFDSixLQS9FTDs7QUFBQSxxQkFnRkk1RyxPQWhGSixzQkFnRmM7QUFDTixZQUFNNkcsT0FBTyxLQUFLcE8sVUFBbEI7QUFDQSxhQUFLcU8sS0FBTCxDQUFXalAsVUFBWDtBQUNBLGFBQUtpUCxLQUFMLEdBQWEsS0FBS3JPLFVBQUwsR0FBa0IsSUFBL0I7QUFDQSxhQUFLbEQsTUFBTCxDQUFZc1IsSUFBWixFQUFrQiwrREFBQTFRLENBQU0sS0FBS0csU0FBTCxHQUFpQkMsR0FBakIsRUFBTixDQUFsQixFQUFpRCxLQUFLdU0sT0FBdEQ7QUFDSCxLQXJGTDs7QUFBQSxxQkFzRklpRSxRQXRGSixxQkFzRmEzUSxHQXRGYixFQXNGa0I7QUFBQTs7QUFDVixZQUFNNFEsUUFBUSxLQUFLclIsTUFBTCxDQUFZcVIsS0FBMUI7QUFDQSxZQUFJNU8sU0FBUyxJQUFiO0FBQ0EsWUFBSTtBQUNBLGdCQUFJNE8sS0FBSixFQUFXO0FBQ1Asb0JBQUksT0FBT0EsS0FBUCxLQUFpQixVQUFyQixFQUFpQztBQUM3QjtBQUNBNU8sNkJBQVM0TyxNQUFNNVEsR0FBTixDQUFUO0FBQ0gsaUJBSEQsTUFJSztBQUNEO0FBQ0FnQyw2QkFBUzRPLE1BQU01USxHQUFOLENBQVQ7QUFDSDtBQUNELG9CQUFJLE9BQU9nQyxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzVCaEMsMEJBQU1nQyxNQUFOO0FBQ0FBLDZCQUFTLElBQVQ7QUFDSDtBQUNKO0FBQ0QsZ0JBQUksQ0FBQ0EsTUFBTCxFQUFhO0FBQ1RoQyxzQkFBTUEsSUFBSWlQLE9BQUosQ0FBWSxLQUFaLEVBQW1CLEdBQW5CLENBQU47QUFDQSxvQkFBSTdQLE9BQU8sNEJBQVEsR0FBZVksR0FBdkIsQ0FBWDtBQUNBLG9CQUFJWixLQUFLeVIsVUFBVCxFQUFxQjtBQUNqQnpSLDJCQUFPQSxLQUFLMFIsT0FBWjtBQUNIO0FBQ0Q5Tyx5QkFBUzVDLElBQVQ7QUFDSDtBQUNKLFNBdkJELENBd0JBLE9BQU9vRCxDQUFQLEVBQVU7QUFDTlIscUJBQVMsS0FBSytPLFVBQUwsQ0FBZ0IvUSxHQUFoQixFQUFxQndDLENBQXJCLENBQVQ7QUFDSDtBQUNEO0FBQ0EsWUFBSSxDQUFDUixPQUFPbkIsSUFBWixFQUFrQjtBQUNkbUIscUJBQVN4RCxRQUFRMkYsT0FBUixDQUFnQm5DLE1BQWhCLENBQVQ7QUFDSDtBQUNEO0FBQ0FBLGlCQUFTQSxPQUFPZixLQUFQLENBQWE7QUFBQSxtQkFBTyxPQUFLOFAsVUFBTCxDQUFnQi9RLEdBQWhCLEVBQXFCMkMsR0FBckIsQ0FBUDtBQUFBLFNBQWIsQ0FBVDtBQUNBLGVBQU9YLE1BQVA7QUFDSCxLQTNITDs7QUFBQSxxQkE0SElxQixhQTVISiwwQkE0SGtCckQsR0E1SGxCLEVBNEh1QmdSLEdBNUh2QixFQTRINEI7QUFBQTs7QUFDcEIsWUFBTTlDLFFBQVFsTyxJQUFJLENBQUosQ0FBZDtBQUNBLFlBQU1uQixPQUFPcVAsTUFBTVYsSUFBbkI7QUFDQSxZQUFJcE8sYUFBSjtBQUNBLFlBQUk0UixPQUFPQSxJQUFJOUQsT0FBSixPQUFrQnJPLElBQTdCLEVBQW1DO0FBQy9CTyxtQkFBT1osUUFBUTJGLE9BQVIsQ0FBZ0I2TSxHQUFoQixDQUFQO0FBQ0gsU0FGRCxNQUdLO0FBQ0Q1UixtQkFBTyxLQUFLdVIsUUFBTCxDQUFjekMsTUFBTVYsSUFBcEIsRUFDRjNNLElBREUsQ0FDRyxjQUFNO0FBQ1osb0JBQUl5TCxZQUFKO0FBQ0Esb0JBQUksT0FBT3ROLEVBQVAsS0FBYyxVQUFsQixFQUE4QjtBQUMxQix3QkFBSUEsR0FBR2lSLFNBQUgsSUFBZ0JqUixHQUFHaVIsU0FBSCxDQUFhNVEsSUFBakMsRUFBdUM7QUFDbkM7QUFDQSwrQkFBTyxJQUFJTCxFQUFKLFNBQWFILElBQWIsQ0FBUDtBQUNILHFCQUhELE1BSUs7QUFDRDtBQUNBRyw2QkFBS0EsSUFBTDtBQUNIO0FBQ0o7QUFDRCxvQkFBSUEsY0FBY21RLE1BQWQsSUFBd0JuUSxjQUFjLHlEQUExQyxFQUFtRDtBQUMvQ3NOLDBCQUFNdE4sRUFBTjtBQUNILGlCQUZELE1BR0s7QUFDRDtBQUNBLHdCQUFJQSxHQUFHMlEsR0FBUCxFQUFZO0FBQ1JyRCw4QkFBTSxJQUFJLHFFQUFKLFNBQXdCek4sSUFBeEIsRUFBOEJHLEVBQTlCLENBQU47QUFDSCxxQkFGRCxNQUdLO0FBQ0RzTiw4QkFBTSxJQUFJLCtEQUFKLFNBQXFCek4sSUFBckIsRUFBMkJHLEVBQTNCLENBQU47QUFDSDtBQUNKO0FBQ0QsdUJBQU9zTixHQUFQO0FBQ0gsYUExQk0sQ0FBUDtBQTJCSDtBQUNELGVBQU9sTixJQUFQO0FBQ0gsS0FqS0w7QUFrS0k7OztBQWxLSixxQkFtS0lDLElBbktKLGlCQW1LU1IsSUFuS1QsRUFtS2U7QUFBQTs7QUFDUCxZQUFJLEtBQUt1UixPQUFMLENBQWFqUSxHQUFiLE9BQXVCdEIsSUFBM0IsRUFBaUM7QUFDN0IsaUJBQUsrQixXQUFMLENBQWlCL0IsSUFBakIsRUFBdUJnQyxJQUF2QixDQUE0QixlQUFPO0FBQy9CLHVCQUFLdVAsT0FBTCxDQUFhM00sR0FBYixDQUFpQnpELEdBQWpCLEVBQXNCLEVBQUUwRCxRQUFRLElBQVYsRUFBdEI7QUFDQSx1QkFBSzVCLE9BQUwsQ0FBYTlCLEdBQWI7QUFDSCxhQUhELEVBR0dpQixLQUhILENBR1M7QUFBQSx1QkFBTSxLQUFOO0FBQUEsYUFIVDtBQUlIO0FBQ0osS0ExS0w7O0FBQUEscUJBMktJTCxXQTNLSix3QkEyS2dCWixHQTNLaEIsRUEyS3FCWixJQTNLckIsRUEySzJCO0FBQ25CLFlBQU1rTixNQUFNO0FBQ1J0TSxpQkFBSywrREFBQUQsQ0FBTUMsR0FBTixDQURHO0FBRVJjLHNCQUFVZCxHQUZGO0FBR1JpUixxQkFBU3pTLFFBQVEyRixPQUFSLENBQWdCLElBQWhCO0FBSEQsU0FBWjtBQUtBLFlBQU0rTSxNQUFNLEtBQUsvTyxTQUFMLENBQWUsV0FBZixFQUE0QixDQUFDbkMsR0FBRCxFQUFPWixRQUFRLEtBQUtzUixLQUFwQixFQUE0QnBFLEdBQTVCLENBQTVCLENBQVo7QUFDQSxZQUFJLENBQUM0RSxHQUFMLEVBQVU7QUFDTixtQkFBTzFTLFFBQVFpRSxNQUFSLENBQWUsRUFBZixDQUFQO0FBQ0g7QUFDRCxlQUFPNkosSUFBSTJFLE9BQUosQ0FBWXBRLElBQVosQ0FBaUI7QUFBQSxtQkFBTXlMLElBQUl4TCxRQUFWO0FBQUEsU0FBakIsQ0FBUDtBQUNILEtBdExMOztBQUFBLHFCQXVMSVcsVUF2TEoseUJBdUxpQjtBQUNULGFBQUtpUCxLQUFMLENBQVdqUCxVQUFYO0FBQ0gsS0F6TEw7QUEwTEk7OztBQTFMSixxQkEyTEk4TyxPQTNMSixvQkEyTFkxUixJQTNMWixFQTJMMkI7QUFBQSwwQ0FBTnNTLElBQU07QUFBTkEsZ0JBQU07QUFBQTs7QUFDbkIsYUFBS0MsS0FBTCxDQUFXdlMsSUFBWCxFQUFpQnNTLElBQWpCO0FBQ0gsS0E3TEw7O0FBQUEscUJBOExJQyxLQTlMSixrQkE4TFV2UyxJQTlMVixFQThMZ0IyRyxJQTlMaEIsRUE4THNCO0FBQ2QsYUFBS3JELFNBQUwsQ0FBZXRELElBQWYsRUFBcUIyRyxJQUFyQjtBQUNILEtBaE1MOztBQUFBLHFCQWlNSTZMLE1Bak1KLG1CQWlNV3hTLElBak1YLEVBaU1pQjtBQUNULGVBQU8sS0FBS0osS0FBTCxDQUFXNlMsSUFBWCxDQUFnQixZQUFtQjtBQUFBLCtDQUFOSCxJQUFNO0FBQU5BLG9CQUFNO0FBQUE7O0FBQ3RDLGlCQUFLQyxLQUFMLENBQVd2UyxJQUFYLEVBQWlCc1MsSUFBakI7QUFDSCxTQUZNLEVBRUosSUFGSSxDQUFQO0FBR0gsS0FyTUw7O0FBQUEscUJBc01JbEwsRUF0TUosZUFzTU9wSCxJQXRNUCxFQXNNYTZRLE9BdE1iLEVBc01zQjtBQUNkLGFBQUs1TCxXQUFMLENBQWlCakYsSUFBakIsRUFBdUI2USxPQUF2QjtBQUNILEtBeE1MOztBQUFBLHFCQXlNSTlOLEdBek1KLGdCQXlNUUMsTUF6TVIsRUF5TWdCdEMsTUF6TWhCLEVBeU13QjtBQUNoQnNDLGVBQU8sSUFBUCxFQUFhLElBQWIsRUFBbUJ0QyxNQUFuQjtBQUNILEtBM01MOztBQUFBLHFCQTRNSTZELEtBNU1KLGtCQTRNVXZFLElBNU1WLEVBNE1nQjBTLEVBNU1oQixFQTRNb0I7QUFDWixhQUFLcFAsU0FBTCxDQUFldEQsSUFBZixFQUFxQjBTLEVBQXJCO0FBQ0EsYUFBS3BQLFNBQUwsQ0FBZSxXQUFmLEVBQTRCb1AsRUFBNUI7QUFDQTtBQUNBLFlBQUksS0FBS2hTLE1BQUwsQ0FBWWlTLEtBQWhCLEVBQXVCO0FBQ25CLGdCQUFJRCxHQUFHLENBQUgsQ0FBSixFQUFXO0FBQ1B2Tix3QkFBUVosS0FBUixDQUFjbU8sR0FBRyxDQUFILENBQWQ7QUFDSDtBQUNEO0FBQ0g7QUFDRDtBQUNILEtBdk5MO0FBd05JOzs7QUF4TkoscUJBeU5JelAsT0F6Tkosb0JBeU5ZOUIsR0F6TlosRUF5TmlCO0FBQUE7O0FBQ1QsWUFBTXlSLFlBQVksQ0FBQyxLQUFLckIsT0FBeEI7QUFDQSxZQUFJcUIsU0FBSixFQUFlO0FBQ1h6UixrQkFBTSxLQUFLMFIsWUFBTCxDQUFrQjFSLEdBQWxCLENBQU47QUFDSDtBQUNELFlBQU0yUixTQUFTLE9BQU8zUixHQUFQLEtBQWUsUUFBZixHQUEwQkEsR0FBMUIsR0FBZ0MsaUVBQUFXLENBQVFYLEdBQVIsQ0FBL0M7QUFDQSxlQUFPLEtBQUtZLFdBQUwsQ0FBaUIrUSxNQUFqQixFQUF5QjlRLElBQXpCLENBQThCLGtCQUFVO0FBQzNDLG1CQUFLdVAsT0FBTCxDQUFhM00sR0FBYixDQUFpQm1PLE1BQWpCLEVBQXlCLEVBQUVsTyxRQUFRLElBQVYsRUFBekI7QUFDQSxtQkFBTyxPQUFLbU8sYUFBTCxDQUFtQkQsTUFBbkIsQ0FBUDtBQUNILFNBSE0sQ0FBUDtBQUlILEtBbk9MOztBQUFBLHFCQW9PSUMsYUFwT0osMEJBb09rQjdSLEdBcE9sQixFQW9PdUI7QUFBQTs7QUFDZixZQUFNK0MsU0FBVSxPQUFPL0MsR0FBUCxLQUFlLFFBQWhCLEdBQTRCLCtEQUFBRCxDQUFNQyxHQUFOLENBQTVCLEdBQXlDQSxHQUF4RDtBQUNBO0FBQ0EsZUFBT3ZCLE1BQU1PLEVBQU4sQ0FBUzhTLE1BQVQsQ0FBZ0I7QUFBQSxtQkFBTSxPQUFLek8sYUFBTCxDQUFtQk4sTUFBbkIsRUFBMkIsT0FBSzJOLEtBQWhDLEVBQXVDN1AsSUFBdkMsQ0FBNEMsZ0JBQVE7QUFDN0U7QUFDQSxvQkFBTWtSLFVBQVUsT0FBS3JCLEtBQXJCO0FBQ0EsdUJBQUtBLEtBQUwsR0FBYXRSLElBQWI7QUFDQTtBQUNBLHVCQUFPQSxLQUFLRCxNQUFMLENBQVksT0FBS2tELFVBQWpCLEVBQTZCVSxNQUE3QixFQUFxQyxPQUFLMkosT0FBMUMsRUFBbUQ3TCxJQUFuRCxDQUF3RCxnQkFBUTtBQUNuRTtBQUNBLHdCQUFJa1IsV0FBV0EsWUFBWSxPQUFLckIsS0FBaEMsRUFBdUM7QUFDbkNxQixnQ0FBUXRRLFVBQVI7QUFDSDtBQUNELHdCQUFJLE9BQUtpUCxLQUFMLENBQVd0RSxPQUFYLEdBQXFCek0sYUFBckIsRUFBSixFQUEwQztBQUN0QywrQkFBSzBDLFVBQUwsR0FBa0JzSyxJQUFsQjtBQUNIO0FBQ0QsMkJBQUtoTCxLQUFMLEdBQWFnTCxJQUFiO0FBQ0EsMkJBQUt4SyxTQUFMLENBQWUsV0FBZixFQUE0QixDQUFDWSxNQUFELENBQTVCO0FBQ0EsMkJBQU8zRCxJQUFQO0FBQ0gsaUJBWE0sQ0FBUDtBQVlILGFBakI0QixFQWlCMUI2QixLQWpCMEIsQ0FpQnBCLGNBQU07QUFDWCx1QkFBS21DLEtBQUwsQ0FBVyxrQkFBWCxFQUErQixDQUFDbU8sRUFBRCxDQUEvQjtBQUNILGFBbkI0QixDQUFOO0FBQUEsU0FBaEIsQ0FBUDtBQW9CSCxLQTNQTDs7QUFBQSxxQkE0UEloUCxVQTVQSix1QkE0UGVuQixLQTVQZixFQTRQc0I7QUFDZDRRLGNBQU0saUJBQU47QUFDQSxlQUFPeFQsUUFBUTJGLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBUDtBQUNILEtBL1BMOztBQUFBLHFCQWdRSXVOLFlBaFFKLHlCQWdRaUIxUixHQWhRakIsRUFnUXNCO0FBQUE7O0FBQ2QsWUFBTXVPLEtBQUssU0FBTEEsRUFBSztBQUFBLG1CQUFLMEQsV0FBVyxZQUFNO0FBQzdCLHVCQUFLblEsT0FBTCxDQUFhb1EsQ0FBYjtBQUNILGFBRmUsRUFFYixDQUZhLENBQUw7QUFBQSxTQUFYO0FBR0EsYUFBSzlCLE9BQUwsR0FBZSxLQUFLLEtBQUs3USxNQUFMLENBQVk0UyxNQUFaLElBQXNCLHVFQUEzQixFQUF1QzVELEVBQXZDLEVBQTJDLEtBQUtoUCxNQUFoRCxDQUFmO0FBQ0E7QUFDQSxZQUFJLEtBQUs4QyxVQUFMLEtBQW9CdUssU0FBUzNFLElBQTdCLElBQXFDLEtBQUsxSSxNQUFMLENBQVk2UyxTQUFaLEtBQTBCLEtBQW5FLEVBQTBFO0FBQ3RFLGdCQUFNQyxPQUFPLEtBQUtoUSxVQUFsQjtBQUNBNUQsa0JBQU02VCxJQUFOLENBQVdDLE1BQVgsQ0FBa0JGLElBQWxCLEVBQXdCLGVBQXhCO0FBQ0FKLHVCQUFXLFlBQU07QUFDYnhULHNCQUFNNlQsSUFBTixDQUFXRSxTQUFYLENBQXFCSCxJQUFyQixFQUEyQixlQUEzQjtBQUNBNVQsc0JBQU02VCxJQUFOLENBQVdDLE1BQVgsQ0FBa0JGLElBQWxCLEVBQXdCLFVBQXhCO0FBQ0gsYUFIRCxFQUdHLEVBSEg7QUFJSDtBQUNELFlBQUksQ0FBQ3JTLEdBQUQsSUFBUUEsSUFBSVEsTUFBSixLQUFlLENBQTNCLEVBQThCO0FBQzFCUixrQkFBTSxLQUFLb1EsT0FBTCxDQUFhalEsR0FBYixNQUFzQixLQUFLWixNQUFMLENBQVlxRSxLQUF4QztBQUNBLGlCQUFLd00sT0FBTCxDQUFhM00sR0FBYixDQUFpQnpELEdBQWpCLEVBQXNCLEVBQUUwRCxRQUFRLElBQVYsRUFBdEI7QUFDSDtBQUNELGVBQU8xRCxHQUFQO0FBQ0gsS0FuUkw7QUFvUkk7OztBQXBSSixxQkFxUkkrUSxVQXJSSix1QkFxUmUvUSxHQXJSZixFQXFSb0IyQyxHQXJScEIsRUFxUnlCO0FBQ2pCLGFBQUtTLEtBQUwsQ0FBVyxtQkFBWCxFQUFnQyxDQUFDVCxHQUFELEVBQU0zQyxHQUFOLENBQWhDO0FBQ0EsZUFBTyxFQUFFMkgsVUFBVSxHQUFaLEVBQVA7QUFDSCxLQXhSTDs7QUFBQSxxQkF5UklrSSxVQXpSSix1QkF5UmV2RCxHQXpSZixFQXlSb0IzQixNQXpScEIsRUF5UjRCcEwsTUF6UjVCLEVBeVJvQztBQUM1QixZQUFNUyxNQUFNc00sSUFBSXNELFFBQUosS0FBaUIsSUFBakIsR0FBd0J0RCxJQUFJc0QsUUFBNUIsR0FBdUMsSUFBbkQ7QUFDQSxZQUFNL1EsT0FBT3lOLElBQUl6TixJQUFKLEtBQWFtQixNQUFNLEtBQUt2QixLQUFMLENBQVd5TixHQUFYLEVBQU4sR0FBeUIsU0FBdEMsQ0FBYjtBQUNBdkIsZUFBT25ILEVBQVAsR0FBWThJLElBQUk5SSxFQUFKLElBQVUsTUFBTSxLQUFLL0UsS0FBTCxDQUFXeU4sR0FBWCxFQUE1QjtBQUNBLFlBQU05TSxPQUFPRyxPQUFPVixJQUFQLElBQWUsRUFBRTJFLElBQUltSCxPQUFPbkgsRUFBYixFQUFpQnhELFFBQWpCLEVBQTVCO0FBQ0EsWUFBSVosS0FBS1ksR0FBTCxZQUFvQix5REFBeEIsRUFBaUM7QUFDN0JaLGlCQUFLQSxJQUFMLEdBQVlBLEtBQUtZLEdBQWpCO0FBQ0g7QUFDRCxlQUFPMkssTUFBUDtBQUNILEtBbFNMOztBQUFBO0FBQUEsRUFBNEIseURBQTVCLEU7Ozs7Ozs7Ozs7Ozs7OztBQ1BBO0FBQ0E7QUFDQSxJQUFhOEgsYUFBYjtBQUFBOztBQUNJLDJCQUFZN1QsR0FBWixFQUFpQkMsSUFBakIsRUFBdUJHLEVBQXZCLEVBQTJCO0FBQUE7O0FBQUEscURBQ3ZCLG9CQUFNSixHQUFOLEVBQVdDLElBQVgsQ0FEdUI7O0FBRXZCLGNBQUs2VCxHQUFMLEdBQVcxVCxFQUFYO0FBQ0EsY0FBSzJULFFBQUwsR0FBZ0IsRUFBaEI7QUFIdUI7QUFJMUI7O0FBTEwsNEJBTUl2RyxPQU5KLHNCQU1jO0FBQ04sWUFBSSxLQUFLeE4sR0FBTCxDQUFTVyxNQUFULENBQWdCcVQsU0FBcEIsRUFBK0I7QUFDM0IsZ0JBQU1sVCxTQUFTLEtBQUtDLGFBQUwsRUFBZjtBQUNBLGdCQUFJRCxNQUFKLEVBQVk7QUFDUix1QkFBT0EsT0FBTzBNLE9BQVAsRUFBUDtBQUNIO0FBQ0o7QUFDRCxlQUFPLEtBQUt6SyxLQUFaO0FBQ0gsS0FkTDs7QUFBQSw0QkFlSXBDLE1BZkoscUJBZWE7QUFDTCxlQUFPLEtBQUttVCxHQUFMLENBQVMvQyxHQUFULElBQWdCLEtBQUsrQyxHQUE1QjtBQUNILEtBakJMOztBQUFBLDRCQWtCSWpSLFVBbEJKLHlCQWtCaUI7QUFDVCxZQUFNRCxVQUFVLEtBQUtrUixHQUFMLENBQVNHLFVBQXpCO0FBQ0EsWUFBSXJSLE9BQUosRUFBYTtBQUNUQTtBQUNIO0FBQ0QsNkJBQXFCLEtBQUttUixRQUExQixrSEFBb0M7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBLGdCQUF6QnBVLE1BQXlCOztBQUNoQ0EsbUJBQU9rRCxVQUFQO0FBQ0g7QUFDRCwyQkFBTUEsVUFBTjtBQUNILEtBM0JMOztBQUFBLDRCQTRCSXBDLElBNUJKLGlCQTRCU0MsSUE1QlQsRUE0QmVDLE1BNUJmLEVBNEJ1QjtBQUNmLFlBQUlELEtBQUtHLE9BQUwsQ0FBYSxHQUFiLE1BQXNCLENBQXRCLElBQTJCSCxLQUFLRyxPQUFMLENBQWEsSUFBYixNQUF1QixDQUF0RCxFQUF5RDtBQUNyRCxtQkFBTyxtQkFBTUosSUFBTixZQUFXQyxJQUFYLEVBQWlCQyxNQUFqQixDQUFQO0FBQ0g7QUFDRCwyQkFBTUYsSUFBTixZQUFXLFFBQVFDLElBQW5CLEVBQXlCQyxNQUF6QjtBQUNILEtBakNMOztBQUFBLDRCQWtDSTJCLElBbENKLGlCQWtDU2dSLENBbENULEVBa0NZWSxDQWxDWixFQWtDZTtBQUNQLFlBQUksS0FBS2xVLEdBQUwsQ0FBU1csTUFBVCxDQUFnQndULGVBQXBCLEVBQXFDO0FBQ2pDLGlCQUFLQyxnQkFBTCxDQUFzQmQsQ0FBdEIsRUFBeUJZLENBQXpCO0FBQ0g7QUFDSixLQXRDTDs7QUFBQSw0QkF1Q0l6UixLQXZDSixrQkF1Q1U2USxDQXZDVixFQXVDYVksQ0F2Q2IsRUF1Q2dCO0FBQ1IsWUFBSSxDQUFDLEtBQUtsVSxHQUFMLENBQVNXLE1BQVQsQ0FBZ0J3VCxlQUFyQixFQUFzQztBQUNsQyxpQkFBS0MsZ0JBQUwsQ0FBc0JkLENBQXRCLEVBQXlCWSxDQUF6QjtBQUNIO0FBQ0osS0EzQ0w7O0FBQUEsNEJBNENJRSxnQkE1Q0osNkJBNENxQmQsQ0E1Q3JCLEVBNEN3QlksQ0E1Q3hCLEVBNEMyQjtBQUNuQixZQUFNNVIsT0FBTyxLQUFLd1IsR0FBTCxDQUFTTyxPQUF0QjtBQUNBLFlBQUkvUixJQUFKLEVBQVU7QUFDTixnQkFBTXlMLE9BQU8sS0FBS1AsT0FBTCxFQUFiO0FBQ0FsTCxpQkFBS3lMLElBQUwsRUFBV0EsS0FBS3ZLLE1BQWhCO0FBQ0g7QUFDRCxZQUFNaUssU0FBUyxLQUFLcUcsR0FBTCxDQUFTUSxRQUF4QjtBQUNBLFlBQUk3RyxNQUFKLEVBQVk7QUFDUixpQkFBSyxJQUFNeEosR0FBWCxJQUFrQndKLE1BQWxCLEVBQTBCO0FBQ3RCLHFCQUFLcEcsRUFBTCxDQUFRLEtBQUtySCxHQUFiLEVBQWtCaUUsR0FBbEIsRUFBdUJ3SixPQUFPeEosR0FBUCxDQUF2QjtBQUNIO0FBQ0o7QUFDRCxZQUFNc1EsVUFBVSxLQUFLVCxHQUFMLENBQVNVLFFBQXpCO0FBQ0EsWUFBSUQsT0FBSixFQUFhO0FBQ1Qsa0NBQW1CQSxPQUFuQix5SEFBNEI7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBLG9CQUFqQkUsSUFBaUI7O0FBQ3hCLG9CQUFJQSxLQUFLMUQsR0FBVCxFQUFjO0FBQ1Ysd0JBQU12USxPQUFPLElBQUlxVCxhQUFKLENBQWtCLEtBQUs3VCxHQUF2QixFQUE0QixLQUFLc08sT0FBTCxFQUE1QixFQUE0Q21HLElBQTVDLENBQWI7QUFDQWpVLHlCQUFLRCxNQUFMLENBQVl5TixTQUFTM0UsSUFBckI7QUFDQSx5QkFBSzBLLFFBQUwsQ0FBY3pULElBQWQsQ0FBbUJFLElBQW5CO0FBQ0gsaUJBSkQsTUFLSztBQUNELHlCQUFLSixFQUFMLENBQVFxVSxJQUFSO0FBQ0g7QUFDSjtBQUNKO0FBQ0osS0FyRUw7O0FBQUEsNEJBc0VJOVEsVUF0RUosdUJBc0VldkMsR0F0RWYsRUFzRW9CO0FBQUE7O0FBQ1osZUFBTyxtQkFBTXVDLFVBQU4sWUFBaUJ2QyxHQUFqQixFQUFzQmEsSUFBdEIsQ0FBMkIsWUFBTTtBQUNwQyxnQkFBTXlTLGNBQWMsT0FBS1osR0FBTCxDQUFTYSxZQUE3QjtBQUNBLGdCQUFJRCxXQUFKLEVBQWlCO0FBQ2Isb0JBQU0zRyxPQUFPLE9BQUtQLE9BQUwsRUFBYjtBQUNBa0gsNEJBQVl0VCxJQUFJLENBQUosRUFBT3lOLE1BQW5CLEVBQTJCek4sSUFBSUssS0FBSixDQUFVLENBQVYsQ0FBM0IsRUFBeUNzTSxLQUFLdkssTUFBOUM7QUFDSDtBQUNKLFNBTk0sQ0FBUDtBQU9ILEtBOUVMOztBQUFBO0FBQUEsRUFBbUMseURBQW5DLEU7Ozs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQ0E7QUFDQSxJQUFhb1IsVUFBYjtBQUFBOztBQUNJLHdCQUFZNVUsR0FBWixFQUFpQkMsSUFBakIsRUFBdUJHLEVBQXZCLEVBQTJCO0FBQUE7O0FBQUEscURBQ3ZCLG9CQUFNSixHQUFOLEVBQVdDLElBQVgsQ0FEdUI7O0FBRXZCLGNBQUs2VCxHQUFMLEdBQVcxVCxFQUFYO0FBRnVCO0FBRzFCOztBQUpMLHlCQUtJTyxNQUxKLHFCQUthO0FBQ0wsZUFBTyxLQUFLbVQsR0FBWjtBQUNILEtBUEw7O0FBQUE7QUFBQSxFQUFnQyx5REFBaEMsRTs7Ozs7Ozs7QUNGQTs7Ozs7Ozs7Ozs7QUFXQSxJQUFJZSxTQUFTLFNBQVRBLE1BQVMsQ0FBU0MsQ0FBVCxFQUFZQyxRQUFaLEVBQXNCOztBQUVqQyxNQUFJL0UsU0FBUyxFQUFiO0FBQ0EsTUFBSWdGLE1BQU0sRUFBVjtBQUNBLE1BQUlDLFlBQVksUUFBaEI7QUFDQSxNQUFJQyxlQUFlSixFQUFFRyxTQUFGLENBQW5CO0FBQ0EsTUFBSUUsTUFBSjs7QUFFQSxNQUFJQyxRQUFRLFNBQVJBLEtBQVEsQ0FBUzFVLElBQVQsRUFBZVQsSUFBZixFQUFxQjtBQUMvQixTQUFLQSxJQUFMLEdBQVlBLElBQVo7QUFDQSxTQUFLUyxJQUFMLEdBQVlBLElBQVo7QUFDQSxTQUFLMlUsSUFBTCxHQUFZLEVBQVo7QUFDQSxTQUFLQyxHQUFMLEdBQVcsRUFBWDtBQUNBLFNBQUt6RyxNQUFMLEdBQWMsRUFBZDtBQUNBLFNBQUswRyxLQUFMLEdBQWFDLGFBQWEsS0FBSzlVLElBQWxCLEVBQXdCLEtBQUsyVSxJQUE3QixFQUFtQyxLQUFuQyxFQUEwQyxLQUExQyxDQUFiO0FBRUQsR0FSRDs7QUFVQUQsUUFBTS9ELFNBQU4sQ0FBZ0JvRSxVQUFoQixHQUE2QixVQUFTQyxFQUFULEVBQWE7QUFDeEMsU0FBS0osR0FBTCxDQUFTaFYsSUFBVCxDQUFjb1YsRUFBZDtBQUNELEdBRkQ7O0FBSUFOLFFBQU0vRCxTQUFOLENBQWdCc0UsYUFBaEIsR0FBZ0MsVUFBU0QsRUFBVCxFQUFhO0FBQzNDLFNBQUssSUFBSS9ULElBQUksQ0FBUixFQUFXaVUsSUFBSSxLQUFLTixHQUFMLENBQVMxVCxNQUE3QixFQUFxQ0QsSUFBSWlVLENBQXpDLEVBQTRDalUsR0FBNUMsRUFBaUQ7QUFDL0MsVUFBSWtVLElBQUksS0FBS1AsR0FBTCxDQUFTM1QsQ0FBVCxDQUFSO0FBQ0EsVUFBSStULE1BQU1HLENBQVYsRUFBYTtBQUNYLGFBQUtQLEdBQUwsQ0FBU1EsTUFBVCxDQUFnQm5VLENBQWhCLEVBQW1CLENBQW5CO0FBQ0E7QUFDRDtBQUNGO0FBQ0YsR0FSRDs7QUFVQXlULFFBQU0vRCxTQUFOLENBQWdCMEUsR0FBaEIsR0FBc0IsVUFBU2xILE1BQVQsRUFBaUI7QUFDckMsU0FBSyxJQUFJbE4sSUFBSSxDQUFSLEVBQVdpVSxJQUFJLEtBQUtOLEdBQUwsQ0FBUzFULE1BQTdCLEVBQXFDRCxJQUFJaVUsQ0FBekMsRUFBNENqVSxHQUE1QyxFQUFpRDtBQUMvQyxVQUFJLEtBQUsyVCxHQUFMLENBQVMzVCxDQUFULEVBQVk2USxLQUFaLENBQWtCLElBQWxCLEVBQXdCM0QsTUFBeEIsTUFBb0MsS0FBeEMsRUFDRSxPQUFPLEtBQVA7QUFDSDtBQUNELFdBQU8sSUFBUDtBQUNELEdBTkQ7O0FBUUF1RyxRQUFNL0QsU0FBTixDQUFnQjJFLEtBQWhCLEdBQXdCLFVBQVN0VixJQUFULEVBQWVtTyxNQUFmLEVBQXNCO0FBQzVDLFFBQUlvSCxJQUFJLEtBQUtWLEtBQUwsQ0FBV1csSUFBWCxDQUFnQnhWLElBQWhCLENBQVI7O0FBRUEsUUFBSSxDQUFDdVYsQ0FBTCxFQUFRLE9BQU8sS0FBUDs7QUFHUixTQUFLLElBQUl0VSxJQUFJLENBQVIsRUFBV3dVLE1BQU1GLEVBQUVyVSxNQUF4QixFQUFnQ0QsSUFBSXdVLEdBQXBDLEVBQXlDLEVBQUV4VSxDQUEzQyxFQUE4QztBQUM1QyxVQUFJc0MsTUFBTSxLQUFLb1IsSUFBTCxDQUFVMVQsSUFBSSxDQUFkLENBQVY7O0FBRUEsVUFBSXlVLE1BQU8sWUFBWSxPQUFPSCxFQUFFdFUsQ0FBRixDQUFwQixHQUE0QjBVLG1CQUFtQkosRUFBRXRVLENBQUYsQ0FBbkIsQ0FBNUIsR0FBdURzVSxFQUFFdFUsQ0FBRixDQUFqRTs7QUFFQSxVQUFJc0MsR0FBSixFQUFTO0FBQ1AsYUFBSzRLLE1BQUwsQ0FBWTVLLElBQUloRSxJQUFoQixJQUF3Qm1XLEdBQXhCO0FBQ0Q7QUFDRHZILGFBQU92TyxJQUFQLENBQVk4VixHQUFaO0FBQ0Q7O0FBRUQsV0FBTyxJQUFQO0FBQ0QsR0FsQkQ7O0FBb0JBaEIsUUFBTS9ELFNBQU4sQ0FBZ0JpRixLQUFoQixHQUF3QixVQUFTekgsTUFBVCxFQUFpQjtBQUN2QyxRQUFJbk8sT0FBTyxLQUFLQSxJQUFoQjtBQUNBLFNBQUssSUFBSXlPLEtBQVQsSUFBa0JOLE1BQWxCLEVBQTBCO0FBQ3hCbk8sYUFBT0EsS0FBSzJQLE9BQUwsQ0FBYSxPQUFLbEIsS0FBbEIsRUFBeUIsTUFBSU4sT0FBT00sS0FBUCxDQUE3QixDQUFQO0FBQ0Q7QUFDRHpPLFdBQU9BLEtBQUsyUCxPQUFMLENBQWEsVUFBYixFQUF5QixHQUF6QixFQUE4QkEsT0FBOUIsQ0FBc0MsS0FBdEMsRUFBNkMsRUFBN0MsQ0FBUDtBQUNBLFFBQUkzUCxLQUFLRyxPQUFMLENBQWEsR0FBYixLQUFxQixDQUFDLENBQTFCLEVBQTZCO0FBQzNCLFlBQU0sSUFBSTBWLEtBQUosQ0FBVSxpQ0FBK0I3VixJQUF6QyxDQUFOO0FBQ0Q7QUFDRCxXQUFPQSxJQUFQO0FBQ0QsR0FWRDs7QUFZQSxNQUFJOFUsZUFBZSxTQUFmQSxZQUFlLENBQVM5VSxJQUFULEVBQWUyVSxJQUFmLEVBQXFCbUIsU0FBckIsRUFBZ0NDLE1BQWhDLEVBQXdDO0FBQ3pELFFBQUkvVixnQkFBZ0JnVyxNQUFwQixFQUE0QixPQUFPaFcsSUFBUDtBQUM1QixRQUFJQSxnQkFBZ0J3USxLQUFwQixFQUEyQnhRLE9BQU8sTUFBTUEsS0FBSzhPLElBQUwsQ0FBVSxHQUFWLENBQU4sR0FBdUIsR0FBOUI7QUFDM0I5TyxXQUFPQSxLQUNKZ0IsTUFESSxDQUNHK1UsU0FBUyxFQUFULEdBQWMsSUFEakIsRUFFSnBHLE9BRkksQ0FFSSxPQUZKLEVBRWEsTUFGYixFQUdKQSxPQUhJLENBR0ksS0FISixFQUdXLFVBSFgsRUFJSkEsT0FKSSxDQUlJLHNDQUpKLEVBSTRDLFVBQVNzRyxDQUFULEVBQVlDLEtBQVosRUFBbUJDLE1BQW5CLEVBQTJCNVMsR0FBM0IsRUFBZ0M2UyxPQUFoQyxFQUF5Q0MsUUFBekMsRUFBa0Q7QUFDakcxQixXQUFLL1UsSUFBTCxDQUFVLEVBQUVMLE1BQU1nRSxHQUFSLEVBQWE4UyxVQUFVLENBQUMsQ0FBRUEsUUFBMUIsRUFBVjtBQUNBSCxjQUFRQSxTQUFTLEVBQWpCO0FBQ0EsYUFBTyxNQUFNRyxXQUFXLEVBQVgsR0FBZ0JILEtBQXRCLElBQStCLEtBQS9CLElBQXdDRyxXQUFXSCxLQUFYLEdBQW1CLEVBQTNELEtBQWtFQyxVQUFVLEVBQTVFLEtBQW1GQyxXQUFZRCxVQUFVLFdBQVYsSUFBeUIsVUFBeEgsSUFBdUksR0FBdkksSUFBOElFLFlBQVksRUFBMUosQ0FBUDtBQUNELEtBUkksRUFTSjFHLE9BVEksQ0FTSSxTQVRKLEVBU2UsTUFUZixFQVVKQSxPQVZJLENBVUksV0FWSixFQVVpQixNQVZqQixFQVdKQSxPQVhJLENBV0ksS0FYSixFQVdXLE1BWFgsQ0FBUDtBQVlBLFdBQU8sSUFBSXFHLE1BQUosQ0FBVyxNQUFNaFcsSUFBTixHQUFhLEdBQXhCLEVBQTZCOFYsWUFBWSxFQUFaLEdBQWlCLEdBQTlDLENBQVA7QUFDRCxHQWhCRDs7QUFrQkEsTUFBSWYsYUFBYSxTQUFiQSxVQUFhLENBQVMvVSxJQUFULEVBQWVnVixFQUFmLEVBQW1CO0FBQ2xDLFFBQUlzQixJQUFJdFcsS0FBS3FPLEtBQUwsQ0FBVyxHQUFYLENBQVI7QUFDQSxRQUFJOU8sT0FBUStXLEVBQUVwVixNQUFGLElBQVksQ0FBYixHQUFrQm9WLEVBQUUsQ0FBRixDQUFsQixHQUF5QixJQUFwQztBQUNBdFcsV0FBUXNXLEVBQUVwVixNQUFGLElBQVksQ0FBYixHQUFrQm9WLEVBQUUsQ0FBRixDQUFsQixHQUF5QkEsRUFBRSxDQUFGLENBQWhDOztBQUVBLFFBQUksQ0FBQ2hDLElBQUl0VSxJQUFKLENBQUwsRUFBZ0I7QUFDZHNVLFVBQUl0VSxJQUFKLElBQVksSUFBSTBVLEtBQUosQ0FBVTFVLElBQVYsRUFBZ0JULElBQWhCLENBQVo7QUFDQStQLGFBQU8xUCxJQUFQLENBQVkwVSxJQUFJdFUsSUFBSixDQUFaO0FBQ0Q7QUFDRHNVLFFBQUl0VSxJQUFKLEVBQVUrVSxVQUFWLENBQXFCQyxFQUFyQjtBQUNELEdBVkQ7O0FBWUEsTUFBSTVGLFNBQVMsU0FBVEEsTUFBUyxDQUFTcFAsSUFBVCxFQUFlZ1YsRUFBZixFQUFtQjtBQUM5QixRQUFJLE9BQU9BLEVBQVAsSUFBYSxVQUFqQixFQUE2QjtBQUMzQkQsaUJBQVcvVSxJQUFYLEVBQWlCZ1YsRUFBakI7QUFDQTVGLGFBQU9tSCxNQUFQO0FBQ0QsS0FIRCxNQUdPLElBQUksUUFBT3ZXLElBQVAseUNBQU9BLElBQVAsTUFBZSxRQUFuQixFQUE2QjtBQUNsQyxXQUFLLElBQUl3VyxDQUFULElBQWN4VyxJQUFkLEVBQW9CO0FBQ2xCK1UsbUJBQVd5QixDQUFYLEVBQWN4VyxLQUFLd1csQ0FBTCxDQUFkO0FBQ0Q7QUFDRHBILGFBQU9tSCxNQUFQO0FBQ0QsS0FMTSxNQUtBLElBQUksT0FBT3ZCLEVBQVAsS0FBYyxXQUFsQixFQUErQjtBQUNwQzVGLGFBQU9JLFFBQVAsQ0FBZ0J4UCxJQUFoQjtBQUNEO0FBQ0YsR0FaRDs7QUFjQW9QLFNBQU9xSCxNQUFQLEdBQWdCLFVBQVNsWCxJQUFULEVBQWV5TixHQUFmLEVBQW9CO0FBQ2xDLFNBQUssSUFBSS9MLElBQUksQ0FBUixFQUFXaVUsSUFBSTVGLE9BQU9wTyxNQUEzQixFQUFtQ0QsSUFBSWlVLENBQXZDLEVBQTBDalUsR0FBMUMsRUFBK0M7QUFDN0MsVUFBSWlRLFFBQVE1QixPQUFPck8sQ0FBUCxDQUFaO0FBQ0EsVUFBSWlRLE1BQU0zUixJQUFOLElBQWNBLElBQWxCLEVBQXdCO0FBQ3RCLGVBQU8yUixNQUFNMEUsS0FBTixDQUFZNUksR0FBWixDQUFQO0FBQ0Q7QUFDRjtBQUNGLEdBUEQ7O0FBU0FvQyxTQUFPc0gsTUFBUCxHQUFnQixVQUFTMVcsSUFBVCxFQUFlZ1YsRUFBZixFQUFtQjtBQUNqQyxRQUFJOUQsUUFBUW9ELElBQUl0VSxJQUFKLENBQVo7QUFDQSxRQUFJLENBQUNrUixLQUFMLEVBQ0U7QUFDRkEsVUFBTStELGFBQU4sQ0FBb0JELEVBQXBCO0FBQ0QsR0FMRDs7QUFPQTVGLFNBQU91SCxTQUFQLEdBQW1CLFlBQVc7QUFDNUJyQyxVQUFNLEVBQU47QUFDQWhGLGFBQVMsRUFBVDtBQUNBbUYsYUFBUyxFQUFUO0FBQ0QsR0FKRDs7QUFNQXJGLFNBQU9JLFFBQVAsR0FBa0IsVUFBU3hQLElBQVQsRUFBZTBMLE9BQWYsRUFBd0I7QUFDeENBLGNBQVVBLFdBQVcsRUFBckI7QUFDQSxRQUFJdEgsU0FBU3NILFFBQVF0SCxNQUFSLElBQWtCLEtBQS9COztBQUVBLFFBQUlBLE1BQUosRUFBWTtBQUNWd1M7QUFDRDtBQUNEakUsZUFBVyxZQUFXO0FBQ3BCMVQsYUFBT3dRLFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCMVAsSUFBdkI7O0FBRUEsVUFBSW9FLE1BQUosRUFBWTtBQUNWdU8sbUJBQVcsWUFBVztBQUNwQmtFO0FBQ0QsU0FGRCxFQUVHLENBRkg7QUFHRDtBQUVGLEtBVEQsRUFTRyxDQVRIO0FBVUQsR0FqQkQ7O0FBbUJBekgsU0FBTzBILFVBQVAsR0FBb0IsWUFBVztBQUM3QjFDLE1BQUVHLFNBQUYsSUFBZUMsWUFBZjtBQUNBLFdBQU9wRixNQUFQO0FBQ0QsR0FIRDs7QUFLQSxNQUFJMkgsVUFBVSxTQUFWQSxPQUFVLEdBQVc7QUFDdkIsV0FBTzlYLE9BQU93USxRQUFQLENBQWdCQyxJQUFoQixDQUFxQnNILFNBQXJCLENBQStCLENBQS9CLENBQVA7QUFDRCxHQUZEOztBQUlBLE1BQUlDLGFBQWEsU0FBYkEsVUFBYSxDQUFTdkgsSUFBVCxFQUFld0IsS0FBZixFQUFzQjtBQUNyQyxRQUFJL0MsU0FBUyxFQUFiO0FBQ0EsUUFBSStDLE1BQU1vRSxLQUFOLENBQVk1RixJQUFaLEVBQWtCdkIsTUFBbEIsQ0FBSixFQUErQjtBQUM3QixhQUFRK0MsTUFBTW1FLEdBQU4sQ0FBVWxILE1BQVYsTUFBc0IsS0FBdEIsR0FBOEIsQ0FBOUIsR0FBa0MsQ0FBMUM7QUFDRDtBQUNELFdBQU8sQ0FBQyxDQUFSO0FBQ0QsR0FORDs7QUFRQSxNQUFJK0ksY0FBYzlILE9BQU9tSCxNQUFQLEdBQWdCLFlBQVc7QUFDM0MsUUFBSTdHLE9BQU9xSCxTQUFYO0FBQ0EsU0FBSyxJQUFJOVYsSUFBSSxDQUFSLEVBQVdpVSxJQUFJNUYsT0FBT3BPLE1BQTNCLEVBQW1DRCxJQUFJaVUsQ0FBdkMsRUFBMENqVSxHQUExQyxFQUErQztBQUM3QyxVQUFJaVEsUUFBUTVCLE9BQU9yTyxDQUFQLENBQVo7QUFDQSxVQUFJZ0ksUUFBUWdPLFdBQVd2SCxJQUFYLEVBQWlCd0IsS0FBakIsQ0FBWjtBQUNBLFVBQUlqSSxVQUFVLENBQWQsRUFBaUI7QUFDZjtBQUNBd0wsaUJBQVMvRSxJQUFUO0FBQ0E7QUFDRCxPQUpELE1BSU8sSUFBSXpHLFVBQVUsQ0FBZCxFQUFnQjtBQUNyQjtBQUNBbUcsZUFBT0ksUUFBUCxDQUFnQmlGLE1BQWhCLEVBQXdCLEVBQUVyUSxRQUFPLElBQVQsRUFBeEI7QUFDQTtBQUNEO0FBQ0Y7QUFDRixHQWZEOztBQWlCQSxNQUFJeVMsY0FBYyxTQUFkQSxXQUFjLEdBQVc7QUFDM0IsUUFBSXpDLEVBQUUrQyxnQkFBTixFQUF3QjtBQUN0Qi9DLFFBQUUrQyxnQkFBRixDQUFtQixZQUFuQixFQUFpQ0QsV0FBakMsRUFBOEMsS0FBOUM7QUFDRCxLQUZELE1BRU87QUFDTDlDLFFBQUU1UCxXQUFGLENBQWMsY0FBZCxFQUE4QjBTLFdBQTlCO0FBQ0Q7QUFDRixHQU5EOztBQVFBLE1BQUlOLGlCQUFpQixTQUFqQkEsY0FBaUIsR0FBVztBQUM5QixRQUFJeEMsRUFBRWdELG1CQUFOLEVBQTJCO0FBQ3pCaEQsUUFBRWdELG1CQUFGLENBQXNCLFlBQXRCLEVBQW9DRixXQUFwQztBQUNELEtBRkQsTUFFTztBQUNMOUMsUUFBRW5ILFdBQUYsQ0FBYyxjQUFkLEVBQThCaUssV0FBOUI7QUFDRDtBQUNGLEdBTkQ7QUFPQUw7QUFDQXBDLFdBQVNzQyxTQUFUOztBQUVBLE1BQUkxQyxRQUFKLEVBQWE7QUFDWCxXQUFPakYsTUFBUDtBQUNELEdBRkQsTUFFTztBQUNMZ0YsTUFBRUcsU0FBRixJQUFlbkYsTUFBZjtBQUNEO0FBRUYsQ0F2TkQ7O0FBeU5BLElBQUksS0FBSixFQUFpQztBQUMvQitFLFNBQU9sVixNQUFQO0FBQ0QsQ0FGRCxNQUVPO0FBQ0x1TCxTQUFPNk0sT0FBUCxHQUFpQmxELE9BQU9sVixNQUFQLEVBQWMsSUFBZCxDQUFqQjtBQUNBdUwsU0FBTzZNLE9BQVAsQ0FBZTdGLE9BQWYsR0FBeUJoSCxPQUFPNk0sT0FBaEM7QUFDRCxDOzs7Ozs7QUN6T0QsSUFBTWpELElBQUlqVixLQUFWO0FBQ0E7QUFDQSxJQUFJLENBQUNpVixFQUFFMVUsRUFBRixDQUFLOFMsTUFBVixFQUFrQjtBQUNkNEIsTUFBRTFVLEVBQUYsQ0FBSzhTLE1BQUwsR0FBYyxVQUFVcEMsT0FBVixFQUFtQjtBQUM3QjtBQUNBO0FBQ0EsWUFBTXdCLE1BQU14QixTQUFaO0FBQ0EsWUFBSXdCLE9BQU9BLElBQUlyUSxJQUFmLEVBQXFCO0FBQ2pCcVEsZ0JBQUlyUSxJQUFKLENBQVMsVUFBVStWLElBQVYsRUFBZ0I7QUFDckJsRCxrQkFBRTFVLEVBQUYsQ0FBSzZYLE9BQUwsR0FBZSxLQUFmO0FBQ0FuRCxrQkFBRTFVLEVBQUYsQ0FBS29MLE1BQUw7QUFDQSx1QkFBT3dNLElBQVA7QUFDSCxhQUpEO0FBS0gsU0FORCxNQU9LO0FBQ0RsRCxjQUFFMVUsRUFBRixDQUFLNlgsT0FBTCxHQUFlLEtBQWY7QUFDQW5ELGNBQUUxVSxFQUFGLENBQUtvTCxNQUFMO0FBQ0g7QUFDRCxlQUFPOEcsR0FBUDtBQUNILEtBaEJEO0FBaUJILEM7Ozs7OztBQ3BCRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCOzs7Ozs7Ozs7Ozs7QUN4Q0E7QUFBQTs7QUFFQSxJQUFJNEYsZUFBZSxDQUNmLEVBQUVyUixPQUFNLFlBQVIsRUFBc0J5QixNQUFLLFlBQTNCLEVBRGUsRUFFZixFQUFFekIsT0FBTSxZQUFSLEVBQXNCeUIsTUFBSyxnQkFBM0IsRUFGZSxFQUdmLEVBQUV6QixPQUFNLFFBQVIsRUFBa0J5QixNQUFLLFFBQXZCLEVBSGUsRUFJZixFQUFFekIsT0FBTSxrQkFBUixFQUE0QnlCLE1BQUssdUJBQWpDLEVBSmUsRUFLZixFQUFFekIsT0FBTSxnQkFBUixFQUEwQnlCLE1BQUsscUJBQS9CLEVBTGUsQ0FBbkI7O0FBUUEsSUFBSTZQLG9CQUFvQixDQUNwQixFQUFFdFIsT0FBTSxjQUFSLEVBQXdCeUIsTUFBSyxjQUE3QixFQURvQixFQUVwQixFQUFFekIsT0FBTSxhQUFSLEVBQXVCeUIsTUFBSyxhQUE1QixFQUZvQixFQUdwQixFQUFFekIsT0FBTSxLQUFSLEVBQWV5QixNQUFLLFVBQXBCLEVBSG9CLEVBSXBCLEVBQUV6QixPQUFNLFFBQVIsRUFBa0J5QixNQUFLLGFBQXZCLEVBSm9CLEVBS3BCLEVBQUV6QixPQUFNLE9BQVIsRUFBaUJ5QixNQUFLLFlBQXRCLEVBTG9CLEVBTXBCLEVBQUV6QixPQUFNLE1BQVIsRUFBZ0J5QixNQUFLLFdBQXJCLEVBTm9CLEVBT3BCLEVBQUV6QixPQUFNLE1BQVIsRUFBZ0J5QixNQUFLLFdBQXJCLEVBUG9CLEVBUXBCLEVBQUV6QixPQUFNLE9BQVIsRUFBaUJ5QixNQUFLLFlBQXRCLEVBUm9CLENBQXhCOztBQVdBLElBQUk4UCxjQUFjLENBQ2QsRUFBRXZSLE9BQU0sV0FBUixFQUFxQnlCLE1BQUssV0FBMUIsRUFEYyxFQUVkLEVBQUV6QixPQUFNLFVBQVIsRUFBb0J5QixNQUFLLFVBQXpCLEVBRmMsRUFHZCxFQUFFekIsT0FBTSxTQUFSLEVBQW1CeUIsTUFBSyxTQUF4QixFQUhjLEVBSWQsRUFBRXpCLE9BQU0sU0FBUixFQUFtQnlCLE1BQUssU0FBeEIsRUFKYyxFQUtkLEVBQUV6QixPQUFNLE1BQVIsRUFBZ0J5QixNQUFLLE1BQXJCLEVBTGMsRUFNZCxFQUFFekIsT0FBTSxPQUFSLEVBQWlCeUIsTUFBSyxPQUF0QixFQU5jLEVBT2QsRUFBRXpCLE9BQU0sTUFBUixFQUFnQnlCLE1BQUssTUFBckIsRUFQYyxFQVFkLEVBQUV6QixPQUFNLE9BQVIsRUFBaUJ5QixNQUFLLE9BQXRCLEVBUmMsRUFTZCxFQUFFekIsT0FBTSxPQUFSLEVBQWlCeUIsTUFBSyxPQUF0QixFQVRjLEVBVWQsRUFBRXpCLE9BQU0sYUFBUixFQUF1QnlCLE1BQUssY0FBNUIsRUFWYyxDQUFsQjs7QUFhQSxJQUFJK1AsZ0JBQWdCLENBQ2hCLEVBQUV4UixPQUFNLEtBQVIsRUFBZXlCLE1BQUssUUFBcEIsRUFEZ0IsRUFFaEIsRUFBRXpCLE9BQU0sUUFBUixFQUFrQnlCLE1BQUssUUFBdkIsRUFGZ0IsRUFHaEIsRUFBRXpCLE9BQU0sVUFBUixFQUFvQnlCLE1BQUssVUFBekIsRUFIZ0IsRUFJaEIsRUFBRXpCLE9BQU0sTUFBUixFQUFnQnlCLE1BQUssTUFBckIsRUFKZ0IsRUFLaEIsRUFBRXpCLE9BQU0sT0FBUixFQUFpQnlCLE1BQUssT0FBdEIsRUFMZ0IsRUFNaEIsRUFBRXpCLE9BQU0sTUFBUixFQUFnQnlCLE1BQUssTUFBckIsRUFOZ0IsQ0FBcEI7O0FBU0EsSUFBSWdRLG1CQUFtQixDQUNuQixFQUFFelIsT0FBTSxZQUFSLEVBQXNCeUIsTUFBSyxNQUEzQixFQURtQixFQUVuQixFQUFFekIsT0FBTSxjQUFSLEVBQXdCeUIsTUFBSyxNQUE3QixFQUZtQixDQUF2Qjs7QUFLQSxJQUFJdkIsV0FBVyxDQUNYLEVBQUVuQyxJQUFJLFlBQU4sRUFBb0JpQyxPQUFNLE1BQTFCLEVBQWtDeUIsTUFBSyxhQUF2QyxFQURXLEVBRVgsRUFBRTFELElBQUksWUFBTixFQUFvQmlDLE9BQU0sTUFBMUIsRUFBa0N5QixNQUFLLFFBQXZDLEVBRlcsRUFHWCxFQUFFMUQsSUFBSSxjQUFOLEVBQXNCaUMsT0FBTSxRQUE1QixFQUFzQ3lCLE1BQUssYUFBM0MsRUFIVyxFQUlYLEVBQUUxRCxJQUFJLGNBQU4sRUFBc0JpQyxPQUFNLFFBQTVCLEVBQXNDeUIsTUFBSyxhQUEzQyxFQUpXLEVBS1gsRUFBRTFELElBQUksdUJBQU4sRUFBK0JpQyxPQUFNLGlCQUFyQyxFQUF3RHlCLE1BQUssR0FBN0QsRUFMVyxFQU1YLEVBQUUxRCxJQUFJLGFBQU4sRUFBcUJpQyxPQUFNLE9BQTNCLEVBQW9DeUIsTUFBSyxTQUF6QyxFQU5XLEVBT1gsRUFBRTFELElBQUksZUFBTixFQUF1QmlDLE9BQU0sU0FBN0IsRUFBd0N5QixNQUFLLEdBQTdDLEVBUFcsRUFRWCxFQUFFMUQsSUFBSSxZQUFOLEVBQW9CaUMsT0FBTSxNQUExQixFQUFrQ3lCLE1BQUssYUFBdkMsRUFSVyxDQUFmOztBQVdBLElBQUl0QixXQUFXLENBQ1gsRUFBRUgsT0FBTSxLQUFSLEVBQWV5QixNQUFLLGFBQXBCLEVBRFcsRUFFWCxFQUFFekIsT0FBTSxNQUFSLEVBQWdCeUIsTUFBSyxjQUFyQixFQUZXLEVBR1gsRUFBRXpCLE9BQU0sT0FBUixFQUFpQnlCLE1BQUssZUFBdEIsRUFIVyxFQUlYLEVBQUV6QixPQUFNLE1BQVIsRUFBZ0J5QixNQUFLLE1BQXJCLEVBSlcsRUFLWCxFQUFFekIsT0FBTSxNQUFSLEVBQWdCeUIsTUFBSyxNQUFyQixFQUxXLEVBTVgsRUFBRXpCLE9BQU0sWUFBUixFQUFzQnlCLE1BQUssWUFBM0IsRUFOVyxFQU9YLEVBQUV6QixPQUFNLGtCQUFSLEVBQTRCeUIsTUFBSyxnQkFBakMsRUFQVyxFQVFYLEVBQUV6QixPQUFNLE1BQVIsRUFBZ0J5QixNQUFLLFdBQXJCLEVBUlcsRUFTWCxFQUFFekIsT0FBTSxRQUFSLEVBQWtCeUIsTUFBSyxRQUF2QixFQVRXLENBQWY7O0FBWUEsSUFBSXJCLFdBQVcsQ0FDWCxFQUFFSixPQUFNLFFBQVIsRUFBa0J5QixNQUFLLHdCQUF2QixFQURXLEVBRVgsRUFBRXpCLE9BQU0sS0FBUixFQUFleUIsTUFBSyxLQUFwQixFQUZXLEVBR1gsRUFBRXpCLE9BQU0sYUFBUixFQUF1QnlCLE1BQUssY0FBNUIsRUFIVyxFQUlYLEVBQUV6QixPQUFNLFNBQVIsRUFBbUJ5QixNQUFLLHNCQUF4QixFQUpXLEVBS1gsRUFBRXpCLE9BQU0sVUFBUixFQUFvQnlCLE1BQUssdUJBQXpCLEVBTFcsRUFNWCxFQUFFekIsT0FBTSxhQUFSLEVBQXVCeUIsTUFBSyxlQUE1QixFQU5XLEVBT1gsRUFBRXpCLE9BQU0sT0FBUixFQUFpQnlCLE1BQUssWUFBdEIsRUFBb0N4QixTQUFRb1IsWUFBNUMsRUFQVyxFQVFYLEVBQUVyUixPQUFNLGFBQVIsRUFBdUJ5QixNQUFLLEdBQTVCLEVBQWlDeEIsU0FBUXFSLGlCQUF6QyxFQVJXLEVBU1gsRUFBRXRSLE9BQU0sTUFBUixFQUFnQnlCLE1BQUssTUFBckIsRUFUVyxFQVVYLEVBQUV6QixPQUFNLFdBQVIsRUFBcUJ5QixNQUFLLGtCQUExQixFQVZXLENBQWY7O0FBYUEsSUFBSXBCLFdBQVcsQ0FDWCxFQUFFTCxPQUFNLFlBQVIsRUFBc0J5QixNQUFLLFdBQTNCLEVBQXdDeEIsU0FBUXNSLFdBQWhELEVBRFcsRUFFWCxFQUFFdlIsT0FBTSxZQUFSLEVBQXNCeUIsTUFBSyxRQUEzQixFQUFxQ3hCLFNBQVF1UixhQUE3QyxFQUZXLEVBR1gsRUFBRXhSLE9BQU0sY0FBUixFQUF3QnlCLE1BQUssZUFBN0IsRUFIVyxFQUlYLEVBQUV6QixPQUFNLGNBQVIsRUFBd0J5QixNQUFLLGFBQTdCLEVBSlcsRUFLWCxFQUFFekIsT0FBTSxRQUFSLEVBQWtCeUIsTUFBSyxhQUF2QixFQUxXLEVBTVgsRUFBRXpCLE9BQU0sWUFBUixFQUFzQnlCLE1BQUssV0FBM0IsRUFOVyxFQU9YLEVBQUV6QixPQUFNLGFBQVIsRUFBdUJ5QixNQUFLLFdBQTVCLEVBUFcsRUFRWCxFQUFFekIsT0FBTSxnQkFBUixFQUEwQnlCLE1BQUssa0JBQS9CLEVBUlcsQ0FBZjs7QUFXQSxJQUFJbkIsYUFBYSxDQUNiLEVBQUVOLE9BQU0sT0FBUixFQUFpQnlCLE1BQUssY0FBdEIsRUFEYSxFQUViLEVBQUV6QixPQUFNLFFBQVIsRUFBa0J5QixNQUFLLFdBQXZCLEVBRmEsRUFHYixFQUFFekIsT0FBTSxPQUFSLEVBQWlCeUIsTUFBSyxPQUF0QixFQUhhLEVBSWIsRUFBRXpCLE9BQU0sU0FBUixFQUFtQnlCLE1BQUssU0FBeEIsRUFKYSxFQUtiLEVBQUV6QixPQUFNLGFBQVIsRUFBdUJ5QixNQUFLLFVBQTVCLEVBTGEsRUFNYixFQUFFekIsT0FBTSxPQUFSLEVBQWlCeUIsTUFBSyxTQUF0QixFQU5hLEVBT2IsRUFBRXpCLE9BQU0sTUFBUixFQUFnQnlCLE1BQUssYUFBckIsRUFQYSxFQVFiLEVBQUV6QixPQUFNLFdBQVIsRUFBcUJ5QixNQUFLLG1CQUExQixFQVJhLEVBU2IsRUFBRXpCLE9BQU0sV0FBUixFQUFxQnlCLE1BQUsscUJBQTFCLEVBQWlEeEIsU0FBUXdSLGdCQUF6RCxFQVRhLEVBVWIsRUFBRXpSLE9BQU0sV0FBUixFQUFxQnlCLE1BQUssV0FBMUIsRUFWYSxFQVdiLEVBQUV6QixPQUFNLFdBQVIsRUFBcUJ5QixNQUFLLGFBQTFCLEVBWGEsRUFZYixFQUFFekIsT0FBTSxnQkFBUixFQUEwQnlCLE1BQUssUUFBL0IsRUFaYSxDQUFqQjs7QUFlQSxJQUFJbEIsV0FBVyxDQUNYLEVBQUVQLE9BQU0sVUFBUixFQUFvQnlCLE1BQUssd0JBQXpCLEVBRFcsRUFFWCxFQUFFekIsT0FBTSxPQUFSLEVBQWlCeUIsTUFBSyxxQkFBdEIsRUFGVyxDQUFmOzs7Ozs7Ozs7QUM5R0E7QUFBQTs7OztBQUlBLElBQUlpUSxvQkFBb0IxWSxNQUFNMlksS0FBTixDQUFZO0FBQ2xDQyxXQUFPLGVBQVM5WCxNQUFULEVBQWdCO0FBQ3JCO0FBQ0QsS0FIaUM7QUFJbEMrWCxlQUFXLHFCQUFVO0FBQ25CO0FBQ0Q7QUFOaUMsQ0FBWixFQU9yQjdZLE1BQU04WSxjQVBlLENBQXhCOztBQVNPLElBQU1DLGVBQWUsSUFBSUwsaUJBQUosQ0FBc0I7QUFDOUMzVCxRQUFHLGNBRDJDO0FBRTlDZ0MsVUFBSyxDQUFFO0FBQ0g7QUFDSWhDLFlBQUcsU0FEUDtBQUVJaUMsZUFBTSxvQkFGVjtBQUdJZ1MsY0FBSyxJQUhUO0FBSUluUCxrQkFBUyxJQUpiO0FBS0k5QyxjQUFLLENBQ0Q7QUFDSWhDLGdCQUFHLFdBRFA7QUFFSWlVLGtCQUFLLElBRlQ7QUFHSWhTLG1CQUFNLFlBSFY7QUFJSTZDLHNCQUFTLElBSmI7QUFLSTlDLGtCQUFLLENBQ0Q7QUFDSWhDLG9CQUFHLFlBRFA7QUFFSWlDLHVCQUFNO0FBRlYsYUFEQyxFQUtEO0FBQ0lqQyxvQkFBRyxZQURQO0FBRUlpQyx1QkFBTTtBQUZWLGFBTEMsRUFTRDtBQUNJakMsb0JBQUcsWUFEUDtBQUVJaUMsdUJBQU07QUFGVixhQVRDO0FBTFQsU0FEQyxFQXFCRDtBQUNJakMsZ0JBQUcsWUFEUDtBQUVJaVUsa0JBQUssSUFGVDtBQUdJaFMsbUJBQU0sWUFIVjtBQUlJNkMsc0JBQVMsSUFKYjtBQUtJOUMsa0JBQUssQ0FDRDtBQUNJaEMsb0JBQUcsWUFEUDtBQUVJaUMsdUJBQU0sVUFGVjtBQUdJbkcsc0JBQUssRUFIVDtBQUlJd0ssd0JBQU8sV0FKWDtBQUtJdEIsd0JBQVEsS0FMWjtBQU1JRSx3QkFBUTtBQU5aLGFBREMsRUFTRDtBQUNJbEYsb0JBQUcsWUFEUDtBQUVJaUMsdUJBQU0sdUJBRlY7QUFHSW5HLHNCQUFLLEVBSFQ7QUFJSXdLLHdCQUFPLFdBSlg7QUFLSXRCLHdCQUFRLEtBTFo7QUFNSUUsd0JBQVE7QUFOWixhQVRDO0FBTFQsU0FyQkM7QUFMVCxLQURDO0FBRnlDLENBQXRCLENBQXJCLEM7Ozs7Ozs7QUNiUDtBQUFBOzs7O0FBSUEsSUFBSWdQLHFCQUFxQmpaLE1BQU0yWSxLQUFOLENBQVk7QUFDbkNDLFdBQU8sZUFBUzlYLE1BQVQsRUFBZ0I7QUFDckI7QUFDQSxhQUFLb1ksWUFBTCxHQUFvQixLQUFwQjtBQUNBLGFBQUtDLFdBQUwsR0FBbUIsRUFBbkI7QUFDRCxLQUxrQztBQU1uQ0MsY0FBUztBQUNQNVIsWUFBRyxFQUFDLGlCQUFrQix5QkFBVTtBQUFDLHFCQUFLa0QsT0FBTDtBQUFlLGFBQTdDO0FBREksS0FOMEI7QUFTbkM7Ozs7Ozs7O0FBUUEyTyxvQkFBZ0Isd0JBQVN0UyxJQUFULEVBQWM7QUFDMUIsWUFBR0EsS0FBS2hGLE1BQVIsRUFBZTtBQUNiLGlCQUFLVCxLQUFMLENBQVd5RixJQUFYO0FBQ0EsaUJBQUsyRCxPQUFMO0FBQ0QsU0FIRCxNQUdLO0FBQ0gsaUJBQUs0TyxRQUFMO0FBQ0Q7QUFDSixLQXhCa0M7QUF5Qm5DO0FBQ0E1TyxhQUFTLG1CQUFvQztBQUFBLFlBQTNCNk8sTUFBMkIsdUVBQWxCLEtBQUtMLFlBQWE7O0FBQ3pDLGFBQUtBLFlBQUwsR0FBb0JLLE1BQXBCO0FBQ0EsWUFBSXZTLFFBQVEsS0FBS21TLFdBQWpCO0FBQ0EsYUFBS0ssTUFBTCxDQUFZLFVBQVMzTCxHQUFULEVBQWE7QUFDckIsbUJBQU9BLElBQUk0TCxLQUFKLENBQVU1TyxXQUFWLEdBQXdCN0osT0FBeEIsQ0FBZ0NnRyxLQUFoQyxNQUEyQyxDQUFDLENBQTVDLEtBQ0t1UyxVQUFVMUwsSUFBSTZMLE1BQUosSUFBY0gsTUFEN0IsQ0FBUDtBQUVILFNBSEQ7QUFJSCxLQWpDa0M7QUFrQ25DM08saUJBQWEscUJBQVM1RCxLQUFULEVBQWU7QUFDeEIsYUFBS21TLFdBQUwsR0FBbUJuUyxLQUFuQjtBQUNBLFlBQUl1UyxTQUFTLEtBQUtMLFlBQWxCO0FBQ0EsYUFBS00sTUFBTCxDQUFZLFVBQVMzTCxHQUFULEVBQWE7QUFDckIsbUJBQU9BLElBQUk0TCxLQUFKLENBQVU1TyxXQUFWLEdBQXdCN0osT0FBeEIsQ0FBZ0NnRyxLQUFoQyxNQUEyQyxDQUFDLENBQTVDLEtBQ0t1UyxVQUFVMUwsSUFBSTZMLE1BQUosSUFBY0gsTUFEN0IsQ0FBUDtBQUVILFNBSEQ7QUFJSDtBQXpDa0MsQ0FBWixFQTBDdEJ2WixNQUFNeVIsY0ExQ2dCLENBQXpCOztBQTRDTyxJQUFNaEgsZ0JBQWdCLElBQUl3TyxrQkFBSixDQUF1QjtBQUNoRGxVLFFBQUcsZUFENkM7QUFFaERnQyxVQUFNO0FBRjBDLENBQXZCLENBQXRCLEM7Ozs7Ozs7Ozs7Ozs7OztBQ2hEUDtBQUNBOztBQUVBL0csTUFBTTJaLE9BQU4sQ0FBYztBQUNWdlosVUFBSyxXQURLO0FBRVZ3WSxXQUFNLGVBQVM5WCxNQUFULEVBQWdCO0FBQ2xCO0FBQ0EsYUFBS2IsT0FBTCxHQUFlRCxNQUFNQyxPQUFOLENBQWMyWixLQUFkLEdBQXNCeFgsSUFBdEIsQ0FBMkJwQyxNQUFNNlMsSUFBTixDQUFXLFlBQVU7QUFBQTs7QUFDM0Q7QUFDQSxnQkFBSWdILFVBQVUvWSxPQUFPaUUsRUFBUCxHQUFZLE9BQTFCO0FBQ0EsZ0JBQUkrVSxRQUFRLElBQUksOEVBQUosQ0FBbUI7QUFDM0IvVSxvQkFBRzhVO0FBRHdCLGFBQW5CLENBQVo7O0FBSUE7QUFDQSxnQkFBSUUsUUFBUSxJQUFJQyxNQUFNQyxLQUFWLEVBQVo7QUFDQSxnQkFBSUMsU0FBUyxJQUFJRixNQUFNRyxpQkFBVixDQUE2QixFQUE3QixFQUFpQ3JhLE9BQU9zYSxVQUFQLEdBQWtCdGEsT0FBT3VhLFdBQTFELEVBQXVFLEdBQXZFLEVBQTRFLElBQTVFLENBQWI7O0FBRUEsZ0JBQUlDLFlBQVksSUFBSU4sTUFBTU8sU0FBVixFQUFoQjtBQUNBLGdCQUFJQyxXQUFXLElBQUlSLE1BQU1TLGFBQVYsRUFBZjtBQUNBRCxxQkFBU0UsT0FBVCxDQUFrQixLQUFLQyxNQUF2QixFQUErQixLQUFLQyxPQUFwQztBQUNBLGlCQUFLQyxLQUFMLENBQVdDLFdBQVgsQ0FBd0JOLFNBQVNPLFVBQWpDOztBQUVBO0FBQ0EsZ0JBQUlDLFFBQVE7QUFDSnZOLHFCQUFLLE9BREQ7QUFFSnJOLHNCQUFNLE9BRkY7QUFHSjZhLDJCQUFXLElBSFA7QUFJSnJVLHVCQUFPLENBSkg7QUFLSjJCLHdCQUFRLENBTEo7QUFNSjJTLHVCQUFPLENBTkg7QUFPSkMscUJBQUssRUFQRDtBQVFKQyx1QkFBTztBQVJILGFBQVo7O0FBV0EsZ0JBQUlDLFdBQVcsSUFBSXJCLE1BQU1zQixXQUFWLENBQXVCTixNQUFNcFUsS0FBN0IsRUFBb0NvVSxNQUFNelMsTUFBMUMsRUFBa0R5UyxNQUFNRSxLQUF4RCxDQUFmO0FBQ0EsZ0JBQUlLLFdBQVcsSUFBSXZCLE1BQU13QixpQkFBVixFQUFmO0FBQ0FELHFCQUFTSCxLQUFULENBQWVLLE1BQWYsQ0FBc0JULE1BQU1JLEtBQU4sQ0FBWTVLLE9BQVosQ0FBb0IsSUFBcEIsRUFBMkIsSUFBM0IsQ0FBdEI7QUFDQSxnQkFBSWtMLE9BQU8sSUFBSTFCLE1BQU0yQixJQUFWLENBQWdCTixRQUFoQixFQUEwQkUsUUFBMUIsQ0FBWDtBQUNBeEIsa0JBQU02QixHQUFOLENBQVdGLElBQVg7QUFDQUEsaUJBQUt0YixJQUFMLEdBQVk0YSxNQUFNNWEsSUFBbEI7QUFDQTBaLGtCQUFNK0IsWUFBTixDQUFtQmIsS0FBbkI7O0FBRUEsZ0JBQUljLGtCQUFrQixJQUFJOUIsTUFBTXdCLGlCQUFWLENBQTZCLEVBQUVKLE9BQU8sUUFBVCxFQUFtQkgsV0FBV0QsTUFBTUMsU0FBcEMsRUFBN0IsQ0FBdEI7QUFDQSxnQkFBSWMsVUFBVSxJQUFJL0IsTUFBTTJCLElBQVYsQ0FBZ0JOLFFBQWhCLEVBQTBCUyxlQUExQixDQUFkO0FBQ0FDLG9CQUFRM2IsSUFBUixHQUFlLFdBQWY7QUFDQTJaLGtCQUFNNkIsR0FBTixDQUFXRyxPQUFYOztBQUVBN0IsbUJBQU9sTixRQUFQLENBQWdCZ1AsQ0FBaEIsR0FBb0IsQ0FBcEI7O0FBRUEsZ0JBQUlDLE9BQU8sS0FBWDtBQUNBLGdCQUFJQyxvQkFBSjtBQUFBLGdCQUFpQkMscUJBQWpCOztBQUVBO0FBQ0FyQyxrQkFBTS9TLElBQU4sQ0FBVzFCLFdBQVgsQ0FBdUIsZ0JBQXZCLEVBQXlDLFVBQVNOLEVBQVQsRUFBYThJLEdBQWIsRUFBaUI7QUFDdEQsd0JBQU85SSxFQUFQO0FBQ0EseUJBQUsrVSxNQUFNc0MsU0FBTixDQUFnQixLQUFoQixDQUFMO0FBQ0ksNEJBQUlDLFVBQVV4TyxJQUFJN0csS0FBbEI7QUFDQWtWLHNDQUFjLE9BQU9HLE9BQXJCO0FBQ0o7QUFDQSx5QkFBS3ZDLE1BQU1zQyxTQUFOLENBQWdCLE9BQWhCLENBQUw7QUFDSVYsNkJBQUtILFFBQUwsQ0FBY0gsS0FBZCxDQUFvQkssTUFBcEIsQ0FBMkI1TixJQUFJN0csS0FBSixDQUFVd0osT0FBVixDQUFrQixJQUFsQixFQUF5QixJQUF6QixDQUEzQjtBQUNKO0FBQ0EseUJBQUtzSixNQUFNc0MsU0FBTixDQUFnQixXQUFoQixDQUFMO0FBQ0ksNEJBQUcsQ0FBQ3ZPLElBQUk3RyxLQUFSLEVBQWM7QUFDVitTLGtDQUFNeEMsTUFBTixDQUFjd0UsT0FBZDtBQUNILHlCQUZELE1BRUs7QUFDRGhDLGtDQUFNNkIsR0FBTixDQUFXRyxPQUFYO0FBQ0g7QUFDTDtBQUNBLHlCQUFLakMsTUFBTXNDLFNBQU4sQ0FBZ0IsT0FBaEIsQ0FBTDtBQUNJViw2QkFBS1ksS0FBTCxDQUFXQyxJQUFYLENBQWdCMU8sSUFBSTdHLEtBQXBCO0FBQ0o7QUFDQSx5QkFBSzhTLE1BQU1zQyxTQUFOLENBQWdCLFFBQWhCLENBQUw7QUFDSVYsNkJBQUtZLEtBQUwsQ0FBV0UsSUFBWCxDQUFnQjNPLElBQUk3RyxLQUFwQjtBQUNKO0FBQ0EseUJBQUs4UyxNQUFNc0MsU0FBTixDQUFnQixPQUFoQixDQUFMO0FBQ0lWLDZCQUFLWSxLQUFMLENBQVdHLElBQVgsQ0FBZ0I1TyxJQUFJN0csS0FBcEI7QUFDSjtBQUNBO0FBeEJBO0FBMEJILGFBM0JEOztBQTZCQSxnQkFBSTBWLFFBQVEsSUFBSTFDLE1BQU0yQyxPQUFWLEVBQVo7QUFBQSxnQkFBaUNDLG9CQUFqQzs7QUFFQTtBQUNBLGdCQUFJQyxpQkFBaUIsU0FBakJBLGNBQWlCLE1BQU87QUFDeEJYLDhCQUFjLE9BQU9mLEdBQXJCO0FBQ0FnQiwrQkFBZVcsWUFBWXZLLEdBQVosRUFBZjtBQUNBekc7QUFDSCxhQUpEOztBQU1BO0FBQ0EsZ0JBQUlBLFVBQVUsU0FBVkEsT0FBVSxNQUFPO0FBQ2pCaVIsc0NBQXVCalIsT0FBdkI7O0FBRUE7QUFDQSxvQkFBSWtSLFVBQVV6SyxNQUFNNEosWUFBcEI7O0FBRUE7QUFDQSxvQkFBSWEsVUFBVWQsV0FBZCxFQUEyQjtBQUN2QkMsbUNBQWU1SixNQUFPeUssVUFBVWQsV0FBaEM7O0FBRUFSLHlCQUFLdUIsUUFBTCxDQUFjQyxDQUFkLElBQW1CLEdBQW5CO0FBQ0F4Qix5QkFBS3VCLFFBQUwsQ0FBY0UsQ0FBZCxJQUFtQixHQUFuQjtBQUNBLHdCQUFHcEQsTUFBTXFELGVBQU4sQ0FBc0IsV0FBdEIsQ0FBSCxFQUFzQztBQUNsQ3JCLGdDQUFRa0IsUUFBUixDQUFpQkksSUFBakIsQ0FBc0IzQixLQUFLdUIsUUFBM0I7QUFDQWxCLGdDQUFRTyxLQUFSLENBQWNlLElBQWQsQ0FBbUIzQixLQUFLWSxLQUF4QjtBQUNIOztBQUVEOUIsNkJBQVM5WixNQUFULENBQWdCcVosS0FBaEIsRUFBdUJHLE1BQXZCO0FBQ0g7QUFDSixhQW5CRDs7QUFxQkEsZ0JBQUlvRCxpQkFBaUIsU0FBakJBLGNBQWlCLEdBQU07QUFDdkJwRCx1QkFBT3FELE1BQVAsR0FBZ0IsTUFBSzVDLE1BQUwsR0FBYyxNQUFLQyxPQUFuQztBQUNBVix1QkFBT3NELHNCQUFQO0FBQ0FoRCx5QkFBU0UsT0FBVCxDQUFrQixNQUFLQyxNQUF2QixFQUErQixNQUFLQyxPQUFwQztBQUNILGFBSkQ7QUFLQTlhLG1CQUFPa1ksZ0JBQVAsQ0FBeUIsUUFBekIsRUFBbUNzRixjQUFuQyxFQUFtRCxLQUFuRDs7QUFFQSxnQkFBSUcsY0FBYyxTQUFkQSxXQUFjLFFBQVM7QUFDdkJDLHNCQUFNQyxjQUFOO0FBQ0FqQixzQkFBTVEsQ0FBTixHQUFZUSxNQUFNRSxPQUFOLEdBQWdCOWQsT0FBT3NhLFVBQXpCLEdBQXdDLENBQXhDLEdBQTRDLENBQXREO0FBQ0FzQyxzQkFBTVMsQ0FBTixHQUFVLEVBQUlPLE1BQU1HLE9BQU4sR0FBZ0IvZCxPQUFPdWEsV0FBM0IsSUFBMkMsQ0FBM0MsR0FBK0MsQ0FBekQ7QUFDQTtBQUNBQywwQkFBVXdELGFBQVYsQ0FBeUJwQixLQUF6QixFQUFnQ3hDLE1BQWhDO0FBQ0Esb0JBQUk2RCxhQUFhekQsVUFBVTBELGdCQUFWLENBQTRCakUsTUFBTWtFLFFBQWxDLENBQWpCO0FBQ0Esb0JBQUtGLFdBQVdoYyxNQUFYLEdBQW9CLENBQXpCLEVBQTZCO0FBQzNCLHdCQUFLNmEsZUFBZW1CLFdBQVksQ0FBWixFQUFnQkcsTUFBcEMsRUFBNkM7QUFDM0N0QixzQ0FBY21CLFdBQVksQ0FBWixFQUFnQkcsTUFBOUI7QUFDQXBFLDhCQUFNcUUsU0FBTixDQUFnQnZCLFlBQVl4YyxJQUE1QjtBQUNELHFCQUhELE1BR087QUFDSDBaLDhCQUFNc0UsV0FBTjtBQUNBeEIsc0NBQWMsSUFBZDtBQUNEO0FBQ0o7QUFDSixhQWhCRDtBQWlCQXpPLHFCQUFTNkosZ0JBQVQsQ0FBMkIsV0FBM0IsRUFBd0N5RixXQUF4QyxFQUFxRCxLQUFyRDs7QUFFQVosMkJBQWUsRUFBZjs7QUFFQTtBQUNBLGdCQUFJLEtBQUsvYixNQUFMLENBQVk4QixLQUFoQixFQUNJLEtBQUs5QixNQUFMLENBQVk4QixLQUFaLENBQWtCeWIsSUFBbEIsQ0FBdUIsSUFBdkIsRUFBNkJ0RSxLQUE3QjtBQUVQLFNBM0l5QyxFQTJJdkMsSUEzSXVDLENBQTNCLENBQWY7O0FBNklBO0FBQ0EsWUFBSWphLE9BQU9rYSxLQUFYLEVBQ0ksS0FBSy9aLE9BQUwsQ0FBYXlGLE9BQWIsR0FESjtBQUdJO0FBQ0ExRixrQkFBTXNlLE9BQU4sQ0FBYyxjQUFkLEVBQThCLFlBQVU7QUFBRSxxQkFBS3JlLE9BQUwsQ0FBYXlGLE9BQWI7QUFBeUIsYUFBbkUsRUFBcUUsSUFBckU7QUFDUDtBQXZKUyxDQUFkLEVBd0pHMUYsTUFBTU8sRUFBTixDQUFTSSxJQXhKWjs7SUEwSnFCNGQsYTs7Ozs7Ozs7OzRCQUNqQnpkLE0scUJBQVE7QUFDSixlQUFPLEVBQUVpRSxJQUFHLFdBQUwsRUFBa0JwRSxNQUFLLFdBQXZCLEVBQVA7QUFDSCxLOzs7RUFIc0MsMEQ7OzBFQUF0QjRkLGE7QUFJcEIsQzs7Ozs7Ozs7QUNqS0Q7QUFBQTtBQUFBOzs7QUFHQTtBQUNBOztBQUVPLElBQU1DLGlCQUFpQnhlLE1BQU0yWSxLQUFOLENBQVk7QUFDeENDLFNBQU8sZUFBUzlYLE1BQVQsRUFBZ0I7QUFDckI7QUFDQSxTQUFLMmQsT0FBTCxHQUFlLElBQUlDLEdBQUosRUFBZjtBQUNELEdBSnVDO0FBS3hDdEMsYUFBVyxtQkFBU3JYLEVBQVQsRUFBWTtBQUNyQixXQUFPLEtBQUtqRSxNQUFMLENBQVlpRSxFQUFaLEdBQWUsR0FBZixHQUFtQkEsRUFBMUI7QUFDRCxHQVB1QztBQVF4QzRaLGdCQUFjLHNCQUFTNVosRUFBVCxFQUFZO0FBQ3hCLFFBQUkwSSxNQUFNLEtBQUszTSxNQUFMLENBQVlpRSxFQUFaLEdBQWUsR0FBZixHQUFtQkEsRUFBN0I7QUFDQSxXQUFPLEtBQUs2RSxPQUFMLENBQWE2RCxHQUFiLEVBQWtCekcsS0FBekI7QUFDRCxHQVh1QztBQVl4QzZSLGFBQVcscUJBQVU7QUFDbkI7QUFDQStGLElBQUEsbUVBQUFBLENBQVNDLEtBQVQsQ0FBZUMsT0FBZixDQUF1QixVQUFTQyxJQUFULEVBQWUvYyxLQUFmLEVBQXNCZ2QsS0FBdEIsRUFBNkI7QUFDbER6WixjQUFRb0MsR0FBUixDQUFZb1gsSUFBWixFQUFrQi9jLEtBQWxCO0FBQ0QsS0FGRDtBQUdELEdBakJ1QztBQWtCeEM2WixnQkFBYyw0QkFTUTtBQUFBLHdCQVJBcE8sR0FRQTtBQUFBLFFBUkFBLEdBUUEsNEJBUk0sU0FRTjtBQUFBLHlCQVBBck4sSUFPQTtBQUFBLFFBUEFBLElBT0EsNkJBUE8sU0FPUDtBQUFBLDhCQU5BNmEsU0FNQTtBQUFBLFFBTkFBLFNBTUEsa0NBTlksSUFNWjtBQUFBLDBCQUxBclUsS0FLQTtBQUFBLFFBTEFBLEtBS0EsOEJBTFEsQ0FLUjtBQUFBLDJCQUpBMkIsTUFJQTtBQUFBLFFBSkFBLE1BSUEsK0JBSlMsQ0FJVDtBQUFBLDBCQUhBMlMsS0FHQTtBQUFBLFFBSEFBLEtBR0EsOEJBSFEsQ0FHUjtBQUFBLHdCQUZBQyxHQUVBO0FBQUEsUUFGQUEsR0FFQSw0QkFGTSxFQUVOO0FBQUEsMEJBREFDLEtBQ0E7QUFBQSxRQURBQSxLQUNBLDhCQURRLFNBQ1I7O0FBQ3BCLFFBQUk2RCxRQUFRLENBQ0osRUFBRWxhLElBQUcsS0FBS3FYLFNBQUwsQ0FBZSxNQUFmLENBQUwsRUFBNkIzQyxPQUFNLE1BQW5DLEVBQTJDelMsT0FBTTVHLElBQWpELEVBQXVENkgsTUFBSyxNQUE1RCxFQUFvRXlSLFFBQU8sSUFBM0UsRUFESSxFQUVKLEVBQUUzVSxJQUFHLEtBQUtxWCxTQUFMLENBQWUsT0FBZixDQUFMLEVBQThCM0MsT0FBTSxPQUFwQyxFQUE2Q3pTLE9BQU1KLEtBQW5ELEVBQTBEcUIsTUFBSyxNQUEvRCxFQUF1RXlSLFFBQU8sSUFBOUUsRUFGSSxFQUdKLEVBQUUzVSxJQUFHLEtBQUtxWCxTQUFMLENBQWUsUUFBZixDQUFMLEVBQStCM0MsT0FBTSxRQUFyQyxFQUErQ3pTLE9BQU11QixNQUFyRCxFQUE2RE4sTUFBSyxNQUFsRSxFQUEwRXlSLFFBQU8sSUFBakYsRUFISSxFQUlKLEVBQUUzVSxJQUFHLEtBQUtxWCxTQUFMLENBQWUsT0FBZixDQUFMLEVBQThCM0MsT0FBTSxPQUFwQyxFQUE2Q3pTLE9BQU1rVSxLQUFuRCxFQUEwRGpULE1BQUssTUFBL0QsRUFBdUV5UixRQUFPLElBQTlFLEVBSkksRUFLSixFQUFFM1UsSUFBRyxLQUFLcVgsU0FBTCxDQUFlLFdBQWYsQ0FBTCxFQUFrQzNDLE9BQU0sV0FBeEMsRUFBcUR6UyxPQUFNaVUsU0FBM0QsRUFBc0VoVCxNQUFLLFVBQTNFLEVBQXVGeVIsUUFBTyxLQUE5RixFQUxJLEVBTUosRUFBRTNVLElBQUcsS0FBS3FYLFNBQUwsQ0FBZSxLQUFmLENBQUwsRUFBNEIzQyxPQUFNLEtBQWxDLEVBQXlDelMsT0FBTW1VLEdBQS9DLEVBQW9EbFQsTUFBSyxNQUF6RCxFQUFpRXlSLFFBQU8sS0FBeEUsRUFOSSxFQU9KLEVBQUUzVSxJQUFHLEtBQUtxWCxTQUFMLENBQWUsT0FBZixDQUFMLEVBQThCM0MsT0FBTSxPQUFwQyxFQUE2Q3pTLE9BQU1vVSxLQUFuRCxFQUEwRG5ULE1BQUssT0FBL0QsRUFBd0V5UixRQUFPLElBQS9FLEVBUEksQ0FBWjtBQVNBLFNBQUsrRSxPQUFMLENBQWF6WixHQUFiLENBQWlCeUksR0FBakIsRUFBc0J3UixLQUF0QjtBQUNELEdBdEN1QztBQXVDeENkLGFBQVcsbUJBQVMxUSxHQUFULEVBQWE7QUFDdEIsUUFBRyxLQUFLZ1IsT0FBTCxDQUFhUyxHQUFiLENBQWlCelIsR0FBakIsQ0FBSCxFQUF5QjtBQUN2QixVQUFJMUcsT0FBTyxLQUFLMFgsT0FBTCxDQUFhL2MsR0FBYixDQUFpQitMLEdBQWpCLENBQVg7QUFDQSxXQUFLbk0sS0FBTCxDQUFXeUYsSUFBWDtBQUNBakMsU0FBRyxlQUFILEVBQW9CdVUsY0FBcEIsQ0FBbUN0UyxJQUFuQztBQUNBakMsU0FBRyxlQUFILEVBQW9CaUMsSUFBcEIsQ0FBeUIyQyxJQUF6QixDQUE4QixLQUFLM0MsSUFBbkM7QUFDRCxLQUxELE1BS0s7QUFDSHhCLGNBQVFvQyxHQUFSLENBQVk4RixHQUFaLEVBQWdCLFlBQWhCO0FBQ0Q7QUFDRixHQWhEdUM7QUFpRHhDMlEsZUFBYSx1QkFBVTtBQUNyQnRaLE9BQUcsZUFBSCxFQUFvQnVVLGNBQXBCLENBQW1DLEVBQW5DO0FBQ0EsU0FBS3RTLElBQUwsQ0FBVW9ZLE1BQVY7QUFDRDtBQXBEdUMsQ0FBWixFQXFEM0JuZixNQUFNeVIsY0FyRHFCLENBQXZCLEM7Ozs7OztBQ05QLGtCQUFrQixXQUFXLFlBQVksb0JBQW9CLG9FQUFvRSxXQUFXLDJDQUEyQyw0SUFBNEksdUJBQXVCLGNBQWMsNEJBQTRCLDBFQUEwRSx1QkFBdUIsRUFBRSw0SUFBNEksdUJBQXVCLGNBQWMsNEJBQTRCLDBFQUEwRSx1QkFBdUIsRUFBRSw0SUFBNEksdUJBQXVCLGNBQWMsNEJBQTRCLDBFQUEwRSx1QkFBdUIsRUFBRSwySUFBMkksdUJBQXVCLGNBQWMsNEJBQTRCLDBFQUEwRSx1QkFBdUIsRUFBRSwySUFBMkksdUJBQXVCLGNBQWMsNEJBQTRCLDBFQUEwRSx1QkFBdUIsRUFBRSw0SUFBNEksdUJBQXVCLGNBQWMsNEJBQTRCLDBFQUEwRSx1QkFBdUIsRUFBRSw2SUFBNkksdUJBQXVCLGNBQWMsNEJBQTRCLDBFQUEwRSx1QkFBdUIsRUFBRSw2SUFBNkksdUJBQXVCLGNBQWMsNEJBQTRCLDBFQUEwRSx1QkFBdUIsRUFBRSxzSUFBc0ksNEJBQTRCLCtHQUErRyw2REFBNkQsVUFBVSwrRUFBK0UsU0FBUyxlQUFlLEVBQUUsc0lBQXNJLDRCQUE0Qiw2R0FBNkcsNkRBQTZELFVBQVUsK0VBQStFLFNBQVMsZUFBZSxFQUFFLHNJQUFzSSw0QkFBNEIsb0hBQW9ILDZEQUE2RCxVQUFVLCtFQUErRSxTQUFTLGVBQWUsRUFBRSxxSUFBcUksNEJBQTRCLG9IQUFvSCw2REFBNkQsVUFBVSwrRUFBK0UsU0FBUyxlQUFlLEVBQUUscUlBQXFJLDRCQUE0Qiw0R0FBNEcsNkRBQTZELFVBQVUsK0VBQStFLFNBQVMsZUFBZSxFQUFFLHNJQUFzSSw0QkFBNEIsa0hBQWtILDZEQUE2RCxVQUFVLCtFQUErRSxTQUFTLGVBQWUsRUFBRSxzSUFBc0ksNEJBQTRCLHdHQUF3Ryw2REFBNkQsVUFBVSwrRUFBK0UsU0FBUyxlQUFlLEVBQUUsdUlBQXVJLDRCQUE0Qiw2R0FBNkcsNkRBQTZELFVBQVUsK0VBQStFLFNBQVMsZUFBZSxFQUFFLEU7Ozs7Ozs7QUNBN2pNOztBQUVBOztBQUVBLElBQUkxSyxPQUFPLENBQ1AsRUFBRWhDLElBQUcsS0FBTCxFQUFZaUMsT0FBTSxLQUFsQixFQUF5QnlCLE1BQUssUUFBOUIsRUFETyxFQUVQLEVBQUUxRCxJQUFHLFFBQUwsRUFBZWlDLE9BQU0sUUFBckIsRUFBK0J5QixNQUFLLFFBQXBDLEVBRk8sRUFHUCxFQUFFMUQsSUFBRyxVQUFMLEVBQWlCaUMsT0FBTSxVQUF2QixFQUFtQ3lCLE1BQUssVUFBeEMsRUFITyxFQUlQLEVBQUUxRCxJQUFHLE1BQUwsRUFBYWlDLE9BQU0sTUFBbkIsRUFBMkJ5QixNQUFLLE1BQWhDLEVBSk8sRUFLUCxFQUFFMUQsSUFBRyxPQUFMLEVBQWNpQyxPQUFNLE9BQXBCLEVBQTZCeUIsTUFBSyxPQUFsQyxFQUxPLEVBTVAsRUFBRTFELElBQUcsTUFBTCxFQUFhaUMsT0FBTSxNQUFuQixFQUEyQnlCLE1BQUssTUFBaEMsRUFOTyxFQU9QLEVBQUUxRCxJQUFHLFdBQUwsRUFBa0JpQyxPQUFNLFdBQXhCLEVBQXFDeUIsTUFBSyxXQUExQyxFQVBPLEVBUVAsRUFBRTFELElBQUcsVUFBTCxFQUFpQmlDLE9BQU0sVUFBdkIsRUFBbUN5QixNQUFLLFVBQXhDLEVBUk8sRUFTUCxFQUFFMUQsSUFBRyxTQUFMLEVBQWdCaUMsT0FBTSxTQUF0QixFQUFpQ3lCLE1BQUssU0FBdEMsRUFUTyxFQVVQLEVBQUUxRCxJQUFHLFNBQUwsRUFBZ0JpQyxPQUFNLFNBQXRCLEVBQWlDeUIsTUFBSyxTQUF0QyxFQVZPLEVBV1AsRUFBRTFELElBQUcsTUFBTCxFQUFhaUMsT0FBTSxNQUFuQixFQUEyQnlCLE1BQUssTUFBaEMsRUFYTyxFQVlQLEVBQUUxRCxJQUFHLE9BQUwsRUFBY2lDLE9BQU0sT0FBcEIsRUFBNkJ5QixNQUFLLE9BQWxDLEVBWk8sRUFhUCxFQUFFMUQsSUFBRyxNQUFMLEVBQWFpQyxPQUFNLE1BQW5CLEVBQTJCeUIsTUFBSyxNQUFoQyxFQWJPLEVBY1AsRUFBRTFELElBQUcsT0FBTCxFQUFjaUMsT0FBTSxPQUFwQixFQUE2QnlCLE1BQUssT0FBbEMsRUFkTyxFQWVQLEVBQUUxRCxJQUFHLE9BQUwsRUFBY2lDLE9BQU0sT0FBcEIsRUFBNkJ5QixNQUFLLE9BQWxDLEVBZk8sRUFnQlAsRUFBRTFELElBQUcsYUFBTCxFQUFvQmlDLE9BQU0sYUFBMUIsRUFBeUN5QixNQUFLLGNBQTlDLEVBaEJPLEVBaUJQLEVBQUUxRCxJQUFHLGNBQUwsRUFBcUJpQyxPQUFNLGNBQTNCLEVBQTJDeUIsTUFBSyxlQUFoRCxFQWpCTyxFQWtCUCxFQUFFMUQsSUFBRyxRQUFMLEVBQWVpQyxPQUFNLFFBQXJCLEVBQStCeUIsTUFBSyxhQUFwQyxFQWxCTyxFQW1CUCxFQUFFMUQsSUFBRyxZQUFMLEVBQW1CaUMsT0FBTSxZQUF6QixFQUF1Q3lCLE1BQUssV0FBNUMsRUFuQk8sRUFvQlAsRUFBRTFELElBQUcsYUFBTCxFQUFvQmlDLE9BQU0sYUFBMUIsRUFBeUN5QixNQUFLLFdBQTlDLEVBcEJPLENBQVg7O0FBdUJBLHlEQUFlMUIsSUFBZixFOzs7Ozs7Ozs7O0FDM0JBLElBQWFxWSxXQUFiO0FBQ0kseUJBQVl0UCxFQUFaLEVBQWdCaFAsTUFBaEIsRUFBd0I7QUFBQTs7QUFDcEIsYUFBS1YsSUFBTCxHQUFhVSxPQUFPdWUsU0FBUCxJQUFvQnZlLE9BQU9pRSxFQUFQLEdBQVksUUFBN0M7QUFDQSxhQUFLK0ssRUFBTCxHQUFVQSxFQUFWO0FBQ0g7O0FBSkwsMEJBS0k5SyxHQUxKLGdCQUtRbkUsSUFMUixFQUtjQyxNQUxkLEVBS3NCO0FBQUE7O0FBQ2RkLGNBQU1zZixPQUFOLENBQWNDLE9BQWQsQ0FBc0JDLEdBQXRCLENBQTBCLEtBQUtwZixJQUEvQixFQUFxQ1MsSUFBckM7QUFDQSxZQUFJLENBQUNDLE1BQUQsSUFBVyxDQUFDQSxPQUFPbUUsTUFBdkIsRUFBK0I7QUFDM0J1Tyx1QkFBVztBQUFBLHVCQUFNLE1BQUsxRCxFQUFMLENBQVFqUCxJQUFSLENBQU47QUFBQSxhQUFYLEVBQWdDLENBQWhDO0FBQ0g7QUFDSixLQVZMOztBQUFBLDBCQVdJYSxHQVhKLGtCQVdVO0FBQ0YsZUFBTzFCLE1BQU1zZixPQUFOLENBQWNDLE9BQWQsQ0FBc0I3ZCxHQUF0QixDQUEwQixLQUFLdEIsSUFBL0IsQ0FBUDtBQUNILEtBYkw7O0FBQUE7QUFBQSxJOzs7Ozs7Ozs7O0FDQUEsSUFBYXFmLFNBQWI7QUFDSSx1QkFBWTNQLEVBQVosRUFBZ0JoUCxNQUFoQixFQUF3QjtBQUFBOztBQUNwQixhQUFLZ1AsRUFBTCxHQUFVQSxFQUFWO0FBQ0EsYUFBSzRQLE1BQUwsR0FBYzVlLE9BQU82ZSxZQUFQLElBQXVCLEVBQXJDO0FBQ0g7O0FBSkwsd0JBS0kzYSxHQUxKLGdCQUtRbkUsSUFMUixFQUtjQyxNQUxkLEVBS3NCO0FBQUE7O0FBQ2RoQixlQUFPOGYsT0FBUCxDQUFlQyxTQUFmLENBQXlCLElBQXpCLEVBQStCLElBQS9CLEVBQXFDLEtBQUtILE1BQUwsR0FBYzdlLElBQW5EO0FBQ0EsWUFBSSxDQUFDQyxNQUFELElBQVcsQ0FBQ0EsT0FBT21FLE1BQXZCLEVBQStCO0FBQzNCdU8sdUJBQVc7QUFBQSx1QkFBTSxNQUFLMUQsRUFBTCxDQUFRalAsSUFBUixDQUFOO0FBQUEsYUFBWCxFQUFnQyxDQUFoQztBQUNIO0FBQ0osS0FWTDs7QUFBQSx3QkFXSWEsR0FYSixrQkFXVTtBQUNGLFlBQU1iLE9BQU9mLE9BQU93USxRQUFQLENBQWdCd1AsUUFBaEIsQ0FBeUJ0UCxPQUF6QixDQUFpQyxLQUFLa1AsTUFBdEMsRUFBOEMsRUFBOUMsQ0FBYjtBQUNBLGVBQU83ZSxTQUFTLEdBQVQsR0FBZUEsSUFBZixHQUFzQixFQUE3QjtBQUNILEtBZEw7O0FBQUE7QUFBQSxJOzs7Ozs7Ozs7O0FDQUEsSUFBYWtmLFdBQWI7QUFDSSx5QkFBWWpRLEVBQVosRUFBZ0JrUSxRQUFoQixFQUEwQjtBQUFBOztBQUN0QixhQUFLbmYsSUFBTCxHQUFZLEVBQVo7QUFDQSxhQUFLaVAsRUFBTCxHQUFVQSxFQUFWO0FBQ0g7O0FBSkwsMEJBS0k5SyxHQUxKLGdCQUtRbkUsSUFMUixFQUtjQyxNQUxkLEVBS3NCO0FBQUE7O0FBQ2QsYUFBS0QsSUFBTCxHQUFZQSxJQUFaO0FBQ0EsWUFBSSxDQUFDQyxNQUFELElBQVcsQ0FBQ0EsT0FBT21FLE1BQXZCLEVBQStCO0FBQzNCdU8sdUJBQVc7QUFBQSx1QkFBTSxNQUFLMUQsRUFBTCxDQUFRalAsSUFBUixDQUFOO0FBQUEsYUFBWCxFQUFnQyxDQUFoQztBQUNIO0FBQ0osS0FWTDs7QUFBQSwwQkFXSWEsR0FYSixrQkFXVTtBQUNGLGVBQU8sS0FBS2IsSUFBWjtBQUNILEtBYkw7O0FBQUE7QUFBQSxJOzs7Ozs7OztBQ0FPLFNBQVNyQixXQUFULENBQXFCVyxHQUFyQixFQUEwQlEsSUFBMUIsRUFBZ0NHLE1BQWhDLEVBQXdDO0FBQzNDSCxTQUFLNkcsRUFBTCxDQUFRckgsR0FBUixlQUEwQixVQUFVd0MsS0FBVixFQUFpQjRPLEtBQWpCLEVBQXdCdFIsT0FBeEIsRUFBaUM7QUFDdkQsWUFBSXNSLFVBQVU1USxJQUFWLElBQWtCNFEsTUFBTS9DLFFBQU4sQ0FBZTdOLElBQWYsQ0FBdEIsRUFBNEM7QUFDeEMsZ0JBQU04UixNQUFNM1IsUUFBWjtBQUNBLGdCQUFJMlIsUUFBUSxLQUFaLEVBQW1CO0FBQ2Z4Uyx3QkFBUXVTLE9BQVIsR0FBa0J6UyxRQUFRaUUsTUFBUixDQUFleU8sR0FBZixDQUFsQjtBQUNILGFBRkQsTUFHSztBQUNEeFMsd0JBQVF1UyxPQUFSLEdBQWtCdlMsUUFBUXVTLE9BQVIsQ0FBZ0JwUSxJQUFoQixDQUFxQjtBQUFBLDJCQUFNcVEsR0FBTjtBQUFBLGlCQUFyQixDQUFsQjtBQUNIO0FBQ0o7QUFDSixLQVZEO0FBV0gsQzs7Ozs7Ozs7OztBQ1pEO0FBQ08sU0FBU2hULE1BQVQsQ0FBZ0JVLEdBQWhCLEVBQXFCUSxJQUFyQixFQUEyQkcsTUFBM0IsRUFBbUM7QUFDdENBLGFBQVNBLFVBQVUsRUFBbkI7QUFDQSxRQUFNd2UsVUFBVXhlLE9BQU93ZSxPQUF2QjtBQUNBLFFBQUlXLE9BQU9YLFVBQVVBLFFBQVE1ZCxHQUFSLENBQVksTUFBWixDQUFWLEdBQWlDWixPQUFPbWYsSUFBUCxJQUFlLElBQTNEO0FBQ0EsUUFBTUMsVUFBVTtBQUNacEosV0FBRyxJQURTO0FBRVpxSixrQkFBVSxJQUZFO0FBR1pDLGVBSFkscUJBR0Y7QUFBRSxtQkFBT0gsSUFBUDtBQUFjLFNBSGQ7QUFJWkksZUFKWSxtQkFJSmpnQixJQUpJLEVBSUU2RSxNQUpGLEVBSVU7QUFDbEIsZ0JBQUk4QixPQUFPLDRCQUFRLEdBQWlCM0csSUFBekIsQ0FBWDtBQUNBLGdCQUFJMkcsS0FBS3FMLFVBQVQsRUFBcUI7QUFDakJyTCx1QkFBT0EsS0FBS3NMLE9BQVo7QUFDSDtBQUNELGdCQUFNaU8sT0FBT0osUUFBUUMsUUFBUixHQUFtQixJQUFJLG9FQUFKLENBQWEsRUFBRUksU0FBU3haLElBQVgsRUFBYixDQUFoQztBQUNBdVosaUJBQUtFLE1BQUwsQ0FBWXBnQixJQUFaO0FBQ0E4ZixvQkFBUXBKLENBQVIsR0FBWTlXLE1BQU02UyxJQUFOLENBQVd5TixLQUFLRyxDQUFoQixFQUFtQkgsSUFBbkIsQ0FBWjtBQUNBTCxtQkFBTzdmLElBQVA7QUFDQSxnQkFBSWtmLE9BQUosRUFBYTtBQUNUQSx3QkFBUUUsR0FBUixDQUFZLE1BQVosRUFBb0JTLElBQXBCO0FBQ0g7QUFDRCxnQkFBSSxDQUFDaGIsTUFBTCxFQUFhO0FBQ1Q5RSxvQkFBSWdMLE9BQUo7QUFDSDtBQUNKO0FBbkJXLEtBQWhCO0FBcUJBaEwsUUFBSTZRLFVBQUosQ0FBZSxRQUFmLEVBQXlCa1AsT0FBekI7QUFDQUEsWUFBUUcsT0FBUixDQUFnQkosSUFBaEIsRUFBc0IsSUFBdEI7QUFDSCxDOzs7Ozs7OztBQzVCRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0MsV0FBUy9SLElBQVQsRUFBZXdTLE9BQWYsRUFBd0I7QUFDdkIsTUFBSSxJQUFKLEVBQWdEO0FBQzlDelYsSUFBQSxpQ0FBTyxFQUFQLG1DQUFXLFlBQVc7QUFDcEIsYUFBT3lWLFFBQVF4UyxJQUFSLENBQVA7QUFDRCxLQUZEO0FBQUE7QUFHRCxHQUpELE1BSU8sSUFBSSxRQUFPZ0ssT0FBUCx5Q0FBT0EsT0FBUCxPQUFtQixRQUF2QixFQUFpQztBQUN0QzdNLFdBQU82TSxPQUFQLEdBQWlCd0ksUUFBUXhTLElBQVIsQ0FBakI7QUFDRCxHQUZNLE1BRUE7QUFDTEEsU0FBS3lTLFFBQUwsR0FBZ0JELFFBQVF4UyxJQUFSLENBQWhCO0FBQ0Q7QUFDRixDQVZBLEVBVUMsSUFWRCxFQVVPLFVBQVNBLElBQVQsRUFBZTtBQUNyQjs7QUFFQTs7QUFDQSxXQUFTeVMsUUFBVCxDQUFrQnBVLE9BQWxCLEVBQTJCO0FBQ3pCQSxjQUFVQSxXQUFXLEVBQXJCO0FBQ0EsU0FBS2dVLE9BQUwsR0FBZSxFQUFmO0FBQ0EsU0FBSzVQLE1BQUwsQ0FBWXBFLFFBQVFnVSxPQUFSLElBQW1CLEVBQS9CO0FBQ0EsU0FBS0ssYUFBTCxHQUFxQnJVLFFBQVFpVSxNQUFSLElBQWtCLElBQXZDO0FBQ0EsU0FBS0ssWUFBTCxHQUFvQixDQUFDLENBQUN0VSxRQUFRc1UsWUFBOUI7QUFDQSxTQUFLQyxJQUFMLEdBQVl2VSxRQUFRdVUsSUFBUixJQUFnQkEsSUFBNUI7QUFDRDs7QUFFRDtBQUNBSCxXQUFTSSxPQUFULEdBQW1CLE9BQW5COztBQUVBO0FBQ0E7QUFDQTtBQUNBSixXQUFTblAsU0FBVCxDQUFtQmdQLE1BQW5CLEdBQTRCLFVBQVNRLFNBQVQsRUFBb0I7QUFDOUMsUUFBSUEsU0FBSixFQUFlLEtBQUtKLGFBQUwsR0FBcUJJLFNBQXJCO0FBQ2YsV0FBTyxLQUFLSixhQUFaO0FBQ0QsR0FIRDs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBRCxXQUFTblAsU0FBVCxDQUFtQmIsTUFBbkIsR0FBNEIsVUFBU3NRLFdBQVQsRUFBc0J2QixNQUF0QixFQUE4QjtBQUN4RCxRQUFJd0IsTUFBSjs7QUFFQSxTQUFLLElBQUk5YyxHQUFULElBQWdCNmMsV0FBaEIsRUFBNkI7QUFDM0IsVUFBSUEsWUFBWUUsY0FBWixDQUEyQi9jLEdBQTNCLENBQUosRUFBcUM7QUFDbkM4YyxpQkFBU0QsWUFBWTdjLEdBQVosQ0FBVDtBQUNBLFlBQUlzYixNQUFKLEVBQVl0YixNQUFNc2IsU0FBUyxHQUFULEdBQWV0YixHQUFyQjtBQUNaLFlBQUksUUFBTzhjLE1BQVAseUNBQU9BLE1BQVAsT0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIsZUFBS3ZRLE1BQUwsQ0FBWXVRLE1BQVosRUFBb0I5YyxHQUFwQjtBQUNELFNBRkQsTUFFTztBQUNMLGVBQUttYyxPQUFMLENBQWFuYyxHQUFiLElBQW9COGMsTUFBcEI7QUFDRDtBQUNGO0FBQ0Y7QUFDRixHQWREOztBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FQLFdBQVNuUCxTQUFULENBQW1CNFAsS0FBbkIsR0FBMkIsWUFBVztBQUNwQyxTQUFLYixPQUFMLEdBQWUsRUFBZjtBQUNELEdBRkQ7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBSSxXQUFTblAsU0FBVCxDQUFtQmhCLE9BQW5CLEdBQTZCLFVBQVM2USxVQUFULEVBQXFCO0FBQ2hELFNBQUtELEtBQUw7QUFDQSxTQUFLelEsTUFBTCxDQUFZMFEsVUFBWjtBQUNELEdBSEQ7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQVYsV0FBU25QLFNBQVQsQ0FBbUJpUCxDQUFuQixHQUF1QixVQUFTcmMsR0FBVCxFQUFjbUksT0FBZCxFQUF1QjtBQUM1QyxRQUFJMlUsTUFBSixFQUFZM2QsTUFBWjtBQUNBZ0osY0FBVUEsV0FBVyxJQUFYLEdBQWtCLEVBQWxCLEdBQXVCQSxPQUFqQztBQUNBO0FBQ0EsUUFBSSxPQUFPQSxPQUFQLEtBQW1CLFFBQXZCLEVBQWlDO0FBQy9CQSxnQkFBVSxFQUFDK1UsYUFBYS9VLE9BQWQsRUFBVjtBQUNEO0FBQ0QsUUFBSSxPQUFPLEtBQUtnVSxPQUFMLENBQWFuYyxHQUFiLENBQVAsS0FBNkIsUUFBakMsRUFBMkM7QUFDekM4YyxlQUFTLEtBQUtYLE9BQUwsQ0FBYW5jLEdBQWIsQ0FBVDtBQUNELEtBRkQsTUFFTyxJQUFJLE9BQU9tSSxRQUFRdUssQ0FBZixLQUFxQixRQUF6QixFQUFtQztBQUN4Q29LLGVBQVMzVSxRQUFRdUssQ0FBakI7QUFDRCxLQUZNLE1BRUEsSUFBSSxLQUFLK0osWUFBVCxFQUF1QjtBQUM1QkssZUFBUzljLEdBQVQ7QUFDRCxLQUZNLE1BRUE7QUFDTCxXQUFLMGMsSUFBTCxDQUFVLG1DQUFpQzFjLEdBQWpDLEdBQXFDLEdBQS9DO0FBQ0FiLGVBQVNhLEdBQVQ7QUFDRDtBQUNELFFBQUksT0FBTzhjLE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIzVSxnQkFBVWdWLE1BQU1oVixPQUFOLENBQVY7QUFDQWhKLGVBQVNpZSxpQkFBaUJOLE1BQWpCLEVBQXlCLEtBQUtOLGFBQTlCLEVBQTZDclUsUUFBUStVLFdBQXJELENBQVQ7QUFDQS9kLGVBQVNrZSxZQUFZbGUsTUFBWixFQUFvQmdKLE9BQXBCLENBQVQ7QUFDRDtBQUNELFdBQU9oSixNQUFQO0FBQ0QsR0F2QkQ7O0FBMEJBO0FBQ0E7QUFDQTtBQUNBb2QsV0FBU25QLFNBQVQsQ0FBbUIwTixHQUFuQixHQUF5QixVQUFTOWEsR0FBVCxFQUFjO0FBQ3JDLFdBQU9BLE9BQU8sS0FBS21jLE9BQW5CO0FBQ0QsR0FGRDs7QUFLQTtBQUNBO0FBQ0EsTUFBSW1CLFlBQVksTUFBaEI7O0FBRUE7QUFDQSxNQUFJQyxjQUFjO0FBQ2hCQyxhQUFXLGlCQUFTQyxDQUFULEVBQVk7QUFBRSxhQUFPLENBQVA7QUFBVyxLQURwQjtBQUVoQkMsWUFBVyxnQkFBU0QsQ0FBVCxFQUFZO0FBQUUsYUFBT0EsTUFBTSxDQUFOLEdBQVUsQ0FBVixHQUFjLENBQXJCO0FBQXlCLEtBRmxDO0FBR2hCRSxZQUFXLGdCQUFTRixDQUFULEVBQVk7QUFBRSxhQUFPQSxJQUFJLENBQUosR0FBUSxDQUFSLEdBQVksQ0FBbkI7QUFBdUIsS0FIaEM7QUFJaEJHLGFBQVcsaUJBQVNILENBQVQsRUFBWTtBQUFFLGFBQU9BLElBQUksRUFBSixLQUFXLENBQVgsSUFBZ0JBLElBQUksR0FBSixLQUFZLEVBQTVCLEdBQWlDLENBQWpDLEdBQXFDQSxJQUFJLEVBQUosSUFBVSxDQUFWLElBQWVBLElBQUksRUFBSixJQUFVLENBQXpCLEtBQStCQSxJQUFJLEdBQUosR0FBVSxFQUFWLElBQWdCQSxJQUFJLEdBQUosSUFBVyxFQUExRCxJQUFnRSxDQUFoRSxHQUFvRSxDQUFoSDtBQUFvSCxLQUo3SDtBQUtoQkksV0FBVyxlQUFTSixDQUFULEVBQVk7QUFBRSxhQUFRQSxNQUFNLENBQVAsR0FBWSxDQUFaLEdBQWlCQSxLQUFLLENBQUwsSUFBVUEsS0FBSyxDQUFoQixHQUFxQixDQUFyQixHQUF5QixDQUFoRDtBQUFvRCxLQUw3RDtBQU1oQkssWUFBVyxnQkFBU0wsQ0FBVCxFQUFZO0FBQUUsYUFBUUEsTUFBTSxDQUFOLEdBQVUsQ0FBVixHQUFjQSxJQUFJLEVBQUosSUFBVSxDQUFWLElBQWVBLElBQUksRUFBSixJQUFVLENBQXpCLEtBQStCQSxJQUFJLEdBQUosR0FBVSxFQUFWLElBQWdCQSxJQUFJLEdBQUosSUFBVyxFQUExRCxJQUFnRSxDQUFoRSxHQUFvRSxDQUExRjtBQUErRixLQU54RztBQU9oQk0sZUFBVyxtQkFBU04sQ0FBVCxFQUFZO0FBQUUsYUFBUUEsSUFBSSxFQUFKLEtBQVcsQ0FBWCxJQUFnQkEsSUFBSSxHQUFKLEtBQVksRUFBN0IsR0FBbUMsQ0FBbkMsR0FBdUMsQ0FBOUM7QUFBa0Q7QUFQM0QsR0FBbEI7O0FBVUE7QUFDQSxNQUFJTyx3QkFBd0I7QUFDMUJSLGFBQVcsQ0FBQyxJQUFELEVBQU8sSUFBUCxFQUFhLElBQWIsRUFBbUIsSUFBbkIsRUFBeUIsSUFBekIsRUFBK0IsSUFBL0IsRUFBcUMsSUFBckMsRUFBMkMsSUFBM0MsRUFBaUQsSUFBakQsQ0FEZTtBQUUxQkUsWUFBVyxDQUFDLElBQUQsRUFBTyxJQUFQLEVBQWEsSUFBYixFQUFtQixJQUFuQixFQUF5QixJQUF6QixFQUErQixJQUEvQixFQUFxQyxJQUFyQyxFQUEyQyxJQUEzQyxFQUFpRCxJQUFqRCxFQUF1RCxJQUF2RCxFQUE2RCxJQUE3RCxFQUFtRSxJQUFuRSxFQUF5RSxJQUF6RSxDQUZlO0FBRzFCQyxZQUFXLENBQUMsSUFBRCxFQUFPLElBQVAsRUFBYSxPQUFiLENBSGU7QUFJMUJDLGFBQVcsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQUplO0FBSzFCQyxXQUFXLENBQUMsSUFBRCxDQUxlO0FBTTFCQyxZQUFXLENBQUMsSUFBRCxDQU5lO0FBTzFCQyxlQUFXLENBQUMsSUFBRDtBQVBlLEdBQTVCOztBQVVBLFdBQVNFLGFBQVQsQ0FBdUJDLE9BQXZCLEVBQWdDO0FBQzlCLFFBQUlyYSxJQUFKO0FBQUEsUUFBVXNhLEtBQVY7QUFBQSxRQUFpQkMsQ0FBakI7QUFBQSxRQUFvQkMsTUFBTSxFQUExQjtBQUNBLFNBQUt4YSxJQUFMLElBQWFxYSxPQUFiLEVBQXNCO0FBQ3BCLFVBQUlBLFFBQVFuQixjQUFSLENBQXVCbFosSUFBdkIsQ0FBSixFQUFrQztBQUNoQ3NhLGdCQUFRRCxRQUFRcmEsSUFBUixDQUFSO0FBQ0EsYUFBS3VhLENBQUwsSUFBVUQsS0FBVixFQUFpQjtBQUNmRSxjQUFJRixNQUFNQyxDQUFOLENBQUosSUFBZ0J2YSxJQUFoQjtBQUNEO0FBQ0Y7QUFDRjtBQUNELFdBQU93YSxHQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxXQUFTQyxJQUFULENBQWM5UyxHQUFkLEVBQWtCO0FBQ2hCLFFBQUkrUyxTQUFTLFlBQWI7QUFDQSxXQUFPL1MsSUFBSVksT0FBSixDQUFZbVMsTUFBWixFQUFvQixFQUFwQixDQUFQO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EsV0FBU25CLGdCQUFULENBQTBCb0IsSUFBMUIsRUFBZ0NwQyxNQUFoQyxFQUF3Q3FDLEtBQXhDLEVBQThDO0FBQzVDLFFBQUlKLEdBQUosRUFBU0ssS0FBVCxFQUFnQkMsVUFBaEI7QUFDQSxRQUFJRixTQUFTLElBQVQsSUFBaUJELElBQXJCLEVBQTJCO0FBQ3pCRSxjQUFRRixLQUFLMVQsS0FBTCxDQUFXd1MsU0FBWCxDQUFSO0FBQ0FxQixtQkFBYUQsTUFBTUUsZ0JBQWdCeEMsTUFBaEIsRUFBd0JxQyxLQUF4QixDQUFOLEtBQXlDQyxNQUFNLENBQU4sQ0FBdEQ7QUFDQUwsWUFBTUMsS0FBS0ssVUFBTCxDQUFOO0FBQ0QsS0FKRCxNQUlPO0FBQ0xOLFlBQU1HLElBQU47QUFDRDtBQUNELFdBQU9ILEdBQVA7QUFDRDs7QUFFRCxXQUFTUSxjQUFULENBQXdCekMsTUFBeEIsRUFBZ0M7QUFDOUIsUUFBSTBDLG1CQUFtQmIsY0FBY0QscUJBQWQsQ0FBdkI7QUFDQSxXQUFPYyxpQkFBaUIxQyxNQUFqQixLQUE0QjBDLGlCQUFpQkMsRUFBcEQ7QUFDRDs7QUFFRCxXQUFTSCxlQUFULENBQXlCeEMsTUFBekIsRUFBaUNxQyxLQUFqQyxFQUF3QztBQUN0QyxXQUFPbEIsWUFBWXNCLGVBQWV6QyxNQUFmLENBQVosRUFBb0NxQyxLQUFwQyxDQUFQO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFTcEIsV0FBVCxDQUFxQlAsTUFBckIsRUFBNkIzVSxPQUE3QixFQUFzQztBQUNwQyxTQUFLLElBQUk2VyxHQUFULElBQWdCN1csT0FBaEIsRUFBeUI7QUFDdkIsVUFBSTZXLFFBQVEsR0FBUixJQUFlN1csUUFBUTRVLGNBQVIsQ0FBdUJpQyxHQUF2QixDQUFuQixFQUFnRDtBQUM5QztBQUNBO0FBQ0E7QUFDQWxDLGlCQUFTQSxPQUFPMVEsT0FBUCxDQUFlLElBQUlxRyxNQUFKLENBQVcsU0FBT3VNLEdBQVAsR0FBVyxLQUF0QixFQUE2QixHQUE3QixDQUFmLEVBQWtEN1csUUFBUTZXLEdBQVIsQ0FBbEQsQ0FBVDtBQUNEO0FBQ0Y7QUFDRCxXQUFPbEMsTUFBUDtBQUNEOztBQUVEO0FBQ0E7QUFDQTtBQUNBLFdBQVNKLElBQVQsQ0FBY2plLE9BQWQsRUFBdUI7QUFDckJxTCxTQUFLM0ksT0FBTCxJQUFnQjJJLEtBQUszSSxPQUFMLENBQWF1YixJQUE3QixJQUFxQzVTLEtBQUszSSxPQUFMLENBQWF1YixJQUFiLENBQWtCLGNBQWNqZSxPQUFoQyxDQUFyQztBQUNEOztBQUVEO0FBQ0E7QUFDQTtBQUNBLFdBQVMwZSxLQUFULENBQWU4QixNQUFmLEVBQXVCO0FBQ3JCLFFBQUlaLE1BQU0sRUFBVjtBQUNBLFNBQUssSUFBSWEsSUFBVCxJQUFpQkQsTUFBakIsRUFBeUI7QUFDdkJaLFVBQUlhLElBQUosSUFBWUQsT0FBT0MsSUFBUCxDQUFaO0FBQ0Q7QUFDRCxXQUFPYixHQUFQO0FBQ0Q7O0FBRUQsU0FBTzlCLFFBQVA7QUFDRCxDQTFSQSxDQUFELEM7Ozs7OztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1Qjs7Ozs7OztBQ2xCQTtBQUFBLFNBQVMvZixJQUFULENBQWNELElBQWQsRUFBb0JHLE1BQXBCLEVBQTRCa0csS0FBNUIsRUFBbUM7QUFDL0IsUUFBSWxHLE9BQU95aUIsSUFBWCxFQUFpQjtBQUNidmMsZ0JBQVFsRyxPQUFPeWlCLElBQVAsQ0FBWXZjLEtBQVosS0FBc0JBLEtBQTlCO0FBQ0g7QUFDRHJHLFNBQUtDLElBQUwsQ0FBVSxPQUFPb0csS0FBakI7QUFDSDtBQUNNLFNBQVN0SCxJQUFULENBQWNTLEdBQWQsRUFBbUJRLElBQW5CLEVBQXlCRyxNQUF6QixFQUFpQztBQUNwQyxRQUFNUCxLQUFLSSxLQUFLbUUsRUFBTCxDQUFRaEUsT0FBT2lFLEVBQVAsSUFBYWpFLE1BQXJCLENBQVg7QUFDQSxRQUFJbUUsU0FBUyxLQUFiO0FBQ0ExRSxPQUFHOEUsV0FBSCxDQUFlLFVBQWYsRUFBMkIsWUFBWTtBQUNuQyxZQUFJLENBQUNKLE1BQUwsRUFBYTtBQUNUckUsaUJBQUtELElBQUwsRUFBV0csTUFBWCxFQUFtQixLQUFLc0ksUUFBTCxFQUFuQjtBQUNIO0FBQ0osS0FKRDtBQUtBN0ksT0FBRzhFLFdBQUgsQ0FBZSxlQUFmLEVBQWdDLFlBQVk7QUFDeEMsWUFBSSxDQUFDSixNQUFMLEVBQWE7QUFDVCxnQkFBSUYsS0FBSyxJQUFUO0FBQ0EsZ0JBQUl4RSxHQUFHOEksUUFBUCxFQUFpQjtBQUNidEUscUJBQUssS0FBS3FFLFFBQUwsRUFBTDtBQUNILGFBRkQsTUFHSyxJQUFJN0ksR0FBR2lqQixhQUFQLEVBQXNCO0FBQ3ZCemUscUJBQUt4RSxHQUFHaWpCLGFBQUgsRUFBTDtBQUNIO0FBQ0Q1aUIsaUJBQUtELElBQUwsRUFBV0csTUFBWCxFQUFtQmlFLEVBQW5CO0FBQ0g7QUFDSixLQVhEO0FBWUFwRSxTQUFLNkcsRUFBTCxDQUFRckgsR0FBUixlQUEwQixVQUFVb0IsR0FBVixFQUFlO0FBQ3JDLFlBQU1raUIsVUFBVWxpQixJQUFJWixLQUFLME4sUUFBTCxFQUFKLENBQWhCO0FBQ0EsWUFBSW9WLE9BQUosRUFBYTtBQUNUeGUscUJBQVMsSUFBVDtBQUNBLGdCQUFNOEosT0FBTzBVLFFBQVExVSxJQUFyQjtBQUNBLGdCQUFJeE8sR0FBRzhJLFFBQUgsSUFBZTlJLEdBQUc2SSxRQUFILE9BQWtCMkYsSUFBckMsRUFBMkM7QUFDdkN4TyxtQkFBRzhJLFFBQUgsQ0FBWTBGLElBQVo7QUFDSCxhQUZELE1BR0ssSUFBSXhPLEdBQUdtakIsTUFBSCxJQUFhbmpCLEdBQUdvakIsTUFBSCxDQUFVNVUsSUFBVixDQUFiLElBQWdDeE8sR0FBR2lqQixhQUFILE9BQXVCelUsSUFBM0QsRUFBaUU7QUFDbEV4TyxtQkFBR21qQixNQUFILENBQVUzVSxJQUFWO0FBQ0g7QUFDRDlKLHFCQUFTLEtBQVQ7QUFDSDtBQUNKLEtBYkQ7QUFjSCxDOzs7Ozs7O0FDeENEO0FBQUEsSUFBTTJlLFlBQVk7QUFDZCxZQUFRLE9BRE07QUFFZCxhQUFTLFNBRks7QUFHZCxjQUFVO0FBSEksQ0FBbEI7QUFLQSxJQUFNQyxXQUFXO0FBQ2IsWUFBUSxJQURLO0FBRWIsYUFBUyxPQUZJO0FBR2IsY0FBVTtBQUhHLENBQWpCO0FBS08sU0FBU2hrQixNQUFULENBQWdCTSxHQUFoQixFQUFxQlEsSUFBckIsRUFBMkJHLE1BQTNCLEVBQW1DO0FBQ3RDLFFBQUl5WSxTQUFTLE1BQWI7QUFDQSxRQUFJc0osUUFBUSxDQUFaO0FBQ0EsUUFBSWlCLFVBQVUsS0FBZDtBQUNBLFFBQUlDLGNBQWNqakIsT0FBT2tqQixNQUF6QjtBQUNBLFFBQUksQ0FBQ0QsV0FBRCxJQUFnQkEsZ0JBQWdCLEtBQXBDLEVBQ0lBLGNBQWMsSUFBZDtBQUNKLFFBQUlqQixRQUFRaGlCLE9BQU9naUIsS0FBUCxJQUFnQmUsUUFBNUI7QUFDQSxRQUFJSSxRQUFRbmpCLE9BQU9takIsS0FBUCxJQUFnQkwsU0FBNUI7QUFDQSxRQUFJLE9BQU85aUIsTUFBUCxLQUFrQixRQUF0QixFQUNJQSxTQUFTLEVBQUVvTCxRQUFRcEwsTUFBVixFQUFUO0FBQ0osYUFBU3FLLE9BQVQsQ0FBaUIrWSxPQUFqQixFQUEwQjtBQUN0QixZQUFNQyxPQUFPeGpCLEtBQUttRSxFQUFMLENBQVFoRSxPQUFPb0wsTUFBZixDQUFiO0FBQ0EsWUFBSWlZLElBQUosRUFBVTtBQUNOLGdCQUFJLENBQUNELE9BQUwsRUFDSUEsVUFBVSx3QkFBd0IzSyxNQUF4QixHQUFpQywrQkFBakMsR0FBbUUwSyxNQUFNMUssTUFBTixDQUFuRSxHQUFtRixZQUFuRixHQUFrR3VKLE1BQU12SixNQUFOLENBQWxHLEdBQWtILFFBQTVIO0FBQ0o0SyxpQkFBS0MsT0FBTCxDQUFhRixPQUFiO0FBQ0g7QUFDSjtBQUNELGFBQVNHLE9BQVQsR0FBbUI7QUFDZnhCO0FBQ0F5QixrQkFBVSxNQUFWO0FBQ0g7QUFDRCxhQUFTQyxJQUFULENBQWNyZ0IsR0FBZCxFQUFtQjtBQUNmMmU7QUFDQXlCLGtCQUFVLE9BQVYsRUFBbUJwZ0IsR0FBbkI7QUFDSDtBQUNELGFBQVNpQixLQUFULENBQWVsRixPQUFmLEVBQXdCO0FBQ3BCNGlCO0FBQ0F5QixrQkFBVSxRQUFWO0FBQ0EsWUFBSXJrQixXQUFXQSxRQUFRbUMsSUFBdkIsRUFBNkI7QUFDekJuQyxvQkFBUW1DLElBQVIsQ0FBYWlpQixPQUFiLEVBQXNCRSxJQUF0QjtBQUNIO0FBQ0o7QUFDRCxhQUFTQyxTQUFULEdBQXFCO0FBQ2pCLGVBQU9qTCxNQUFQO0FBQ0g7QUFDRCxhQUFTa0wsVUFBVCxHQUFzQjtBQUNsQixZQUFJNUIsU0FBUyxDQUFiLEVBQWdCO0FBQ1oxWCxvQkFBUSxHQUFSO0FBQ0g7QUFDSjtBQUNELGFBQVNtWixTQUFULENBQW1CSSxJQUFuQixFQUF5QnhnQixHQUF6QixFQUE4QjtBQUMxQixZQUFJMmUsUUFBUSxDQUFaLEVBQWU7QUFDWEEsb0JBQVEsQ0FBUjtBQUNIO0FBQ0QsWUFBSTZCLFNBQVMsUUFBYixFQUF1QjtBQUNuQm5MLHFCQUFTLFFBQVQ7QUFDQXBPO0FBQ0gsU0FIRCxNQUlLO0FBQ0QyWSxzQkFBV1ksU0FBUyxPQUFwQjtBQUNBLGdCQUFJN0IsVUFBVSxDQUFkLEVBQWlCO0FBQ2J0Six5QkFBU3VLLFVBQVUsT0FBVixHQUFvQixNQUE3QjtBQUNBLG9CQUFJQSxPQUFKLEVBQWE7QUFDVDNqQix3QkFBSXdFLEtBQUosQ0FBVSxrQkFBVixFQUE4QixDQUFDVCxJQUFJeWdCLFlBQUosSUFBb0J6Z0IsR0FBckIsQ0FBOUI7QUFDSCxpQkFGRCxNQUdLO0FBQ0Qsd0JBQUk2ZixXQUFKLEVBQ0l2USxXQUFXaVIsVUFBWCxFQUF1QlYsV0FBdkI7QUFDUDtBQUNENVk7QUFDSDtBQUNKO0FBQ0o7QUFDRCxhQUFTeVosS0FBVCxDQUFlN2QsSUFBZixFQUFxQjtBQUNqQixZQUFNOGQsS0FBSzdrQixNQUFNNmtCLEVBQU4sQ0FBUzlkLElBQVQsQ0FBWDtBQUNBLFlBQUk4ZCxFQUFKLEVBQVE7QUFDSmxrQixpQkFBSzZHLEVBQUwsQ0FBUXFkLEVBQVIsRUFBWSxpQkFBWixFQUErQjFmLEtBQS9CO0FBQ0F4RSxpQkFBSzZHLEVBQUwsQ0FBUXFkLEVBQVIsRUFBWSxrQkFBWixFQUFnQyxVQUFDOWYsRUFBRCxFQUFLOEksR0FBTCxFQUFVdkssUUFBVjtBQUFBLHVCQUF1QmloQixLQUFLamhCLFFBQUwsQ0FBdkI7QUFBQSxhQUFoQztBQUNBM0MsaUJBQUs2RyxFQUFMLENBQVFxZCxFQUFSLEVBQVksYUFBWixFQUEyQlIsT0FBM0I7QUFDSDtBQUNKO0FBQ0Rsa0IsUUFBSTZRLFVBQUosQ0FBZSxRQUFmLEVBQXlCO0FBQ3JCd1QsNEJBRHFCO0FBRXJCRiw0QkFGcUI7QUFHckJNO0FBSHFCLEtBQXpCO0FBS0EsUUFBSTlqQixPQUFPZ2tCLE1BQVgsRUFBbUI7QUFDZm5rQixhQUFLNkcsRUFBTCxDQUFReEgsS0FBUixFQUFlLGNBQWYsRUFBK0JtRixLQUEvQjtBQUNIO0FBQ0QsUUFBSXJFLE9BQU9pa0IsSUFBWCxFQUFpQjtBQUNicGtCLGFBQUs2RyxFQUFMLENBQVF4SCxLQUFSLEVBQWUsY0FBZixFQUErQixVQUFDMGtCLElBQUQsRUFBT25qQixHQUFQLEVBQVl3RixJQUFaLEVBQWtCaWUsT0FBbEIsRUFBMkJDLE9BQTNCLEVBQW9DQyxLQUFwQyxFQUEyQ2psQixPQUEzQyxFQUF1RDtBQUNsRmtGLGtCQUFNbEYsT0FBTjtBQUNILFNBRkQ7QUFHSDtBQUNELFFBQUlhLE9BQU9pRyxJQUFYLEVBQWlCO0FBQ2I2ZCxjQUFNOWpCLE9BQU9pRyxJQUFiO0FBQ0g7QUFDSixDOzs7Ozs7OztBQ25HTSxTQUFTcEgsS0FBVCxDQUFlUSxHQUFmLEVBQW9CUSxJQUFwQixFQUEwQkcsTUFBMUIsRUFBa0M7QUFDckNBLGFBQVNBLFVBQVUsRUFBbkI7QUFDQSxRQUFNd2UsVUFBVXhlLE9BQU93ZSxPQUF2QjtBQUNBLFFBQUk2RixRQUFRN0YsVUFBVUEsUUFBUTVkLEdBQVIsQ0FBWSxPQUFaLENBQVYsR0FBa0NaLE9BQU9xa0IsS0FBUCxJQUFnQixjQUE5RDtBQUNBLFFBQU1qRixVQUFVO0FBQ1prRixnQkFEWSxzQkFDRDtBQUFFLG1CQUFPRCxLQUFQO0FBQWUsU0FEaEI7QUFFWkUsZ0JBRlksb0JBRUhqbEIsSUFGRyxFQUVHNkUsTUFGSCxFQUVXO0FBQ25CLGdCQUFJZ0ssUUFBUTdPLEtBQUs4TyxLQUFMLENBQVcsR0FBWCxDQUFaO0FBQ0EsZ0JBQUlvVyxRQUFRblgsU0FBU29YLG9CQUFULENBQThCLE1BQTlCLENBQVo7QUFDQSxpQkFBSyxJQUFJempCLElBQUksQ0FBYixFQUFnQkEsSUFBSXdqQixNQUFNdmpCLE1BQTFCLEVBQWtDRCxHQUFsQyxFQUF1QztBQUNuQyxvQkFBSTBqQixRQUFRRixNQUFNeGpCLENBQU4sRUFBUytQLFlBQVQsQ0FBc0IsT0FBdEIsQ0FBWjtBQUNBLG9CQUFJMlQsS0FBSixFQUFXO0FBQ1Asd0JBQUlBLFVBQVVwbEIsSUFBVixJQUFrQm9sQixVQUFVdlcsTUFBTSxDQUFOLENBQWhDLEVBQTBDO0FBQ3RDcVcsOEJBQU14akIsQ0FBTixFQUFTMmpCLEdBQVQsR0FBZSxZQUFmO0FBQ0FILDhCQUFNeGpCLENBQU4sRUFBUzRqQixRQUFULEdBQW9CLEtBQXBCO0FBQ0gscUJBSEQsTUFJSztBQUNESiw4QkFBTXhqQixDQUFOLEVBQVMyakIsR0FBVCxHQUFlLHNCQUFmO0FBQ0FILDhCQUFNeGpCLENBQU4sRUFBUzRqQixRQUFULEdBQW9CLElBQXBCO0FBQ0g7QUFDRDFsQiwwQkFBTTJsQixJQUFOLENBQVczZ0IsR0FBWCxDQUFlaUssTUFBTSxDQUFOLENBQWY7QUFDSDtBQUNEO0FBQ0FqUCxzQkFBTTZULElBQU4sQ0FBV0UsU0FBWCxDQUFxQjVGLFNBQVMzRSxJQUE5QixFQUFvQyxXQUFXMmIsS0FBL0M7QUFDQTtBQUNBbmxCLHNCQUFNNlQsSUFBTixDQUFXQyxNQUFYLENBQWtCM0YsU0FBUzNFLElBQTNCLEVBQWlDLFdBQVdwSixJQUE1QztBQUNIO0FBQ0Qra0Isb0JBQVEva0IsSUFBUjtBQUNBLGdCQUFJa2YsT0FBSixFQUFhO0FBQ1RBLHdCQUFRRSxHQUFSLENBQVksT0FBWixFQUFxQnBmLElBQXJCO0FBQ0g7QUFDRCxnQkFBSSxDQUFDNkUsTUFBTCxFQUFhO0FBQ1Q5RSxvQkFBSWdMLE9BQUo7QUFDSDtBQUNKO0FBOUJXLEtBQWhCO0FBZ0NBaEwsUUFBSTZRLFVBQUosQ0FBZSxPQUFmLEVBQXdCa1AsT0FBeEI7QUFDQUEsWUFBUW1GLFFBQVIsQ0FBaUJGLEtBQWpCLEVBQXdCLElBQXhCO0FBQ0gsQzs7Ozs7Ozs7QUN0Q00sU0FBU3ZsQixJQUFULENBQWNPLEdBQWQsRUFBbUJRLElBQW5CLEVBQXlCRyxNQUF6QixFQUFpQztBQUNwQ0EsYUFBU0EsVUFBVSxFQUFuQjtBQUNBLFFBQU04a0IsUUFBUTlrQixPQUFPOGtCLEtBQVAsSUFBZ0IsUUFBOUI7QUFDQSxRQUFNQyxTQUFTL2tCLE9BQU8ra0IsTUFBUCxJQUFpQixTQUFoQztBQUNBLFFBQU1DLGFBQWFobEIsT0FBT2dsQixVQUFQLElBQXFCM2xCLElBQUlXLE1BQUosQ0FBV3FFLEtBQW5EO0FBQ0EsUUFBTTRnQixjQUFjamxCLE9BQU9pbEIsV0FBUCxJQUFzQixRQUExQztBQUNBLFFBQU1DLE9BQU9sbEIsT0FBT2tsQixJQUFQLElBQWUsSUFBSSxFQUFKLEdBQVMsSUFBckM7QUFDQSxRQUFNQyxRQUFRbmxCLE9BQU9tbEIsS0FBckI7QUFDQSxRQUFJQyxPQUFPcGxCLE9BQU9vbEIsSUFBbEI7QUFDQSxRQUFNaEcsVUFBVTtBQUNaaUcsZUFEWSxxQkFDRjtBQUNOLG1CQUFPRCxJQUFQO0FBQ0gsU0FIVztBQUlaMUIsaUJBSlkscUJBSUY0QixNQUpFLEVBSU07QUFDZCxnQkFBSSxDQUFDQSxNQUFMLEVBQWE7QUFDVCx1QkFBT0YsU0FBUyxJQUFoQjtBQUNIO0FBQ0QsbUJBQU9ELE1BQU0xTSxNQUFOLEdBQWUvVyxLQUFmLENBQXFCO0FBQUEsdUJBQU0sSUFBTjtBQUFBLGFBQXJCLEVBQWlDSixJQUFqQyxDQUFzQyxnQkFBUTtBQUNqRDhqQix1QkFBT25mLElBQVA7QUFDSCxhQUZNLENBQVA7QUFHSCxTQVhXO0FBWVo2ZSxhQVpZLGlCQVlOeGxCLElBWk0sRUFZQWltQixJQVpBLEVBWU07QUFDZCxtQkFBT0osTUFBTUwsS0FBTixDQUFZeGxCLElBQVosRUFBa0JpbUIsSUFBbEIsRUFBd0Jqa0IsSUFBeEIsQ0FBNkIsZ0JBQVE7QUFDeEM4akIsdUJBQU9uZixJQUFQO0FBQ0Esb0JBQUksQ0FBQ0EsSUFBTCxFQUFXO0FBQ1AsMEJBQU8sZUFBUDtBQUNIO0FBQ0Q1RyxvQkFBSVMsSUFBSixDQUFTa2xCLFVBQVQ7QUFDSCxhQU5NLENBQVA7QUFPSCxTQXBCVztBQXFCWkQsY0FyQlksb0JBcUJIO0FBQ0xLLG1CQUFPLElBQVA7QUFDQSxtQkFBT0QsTUFBTUosTUFBTixFQUFQO0FBQ0g7QUF4QlcsS0FBaEI7QUEwQkEsYUFBUzFqQixXQUFULENBQXFCWixHQUFyQixFQUEwQnNNLEdBQTFCLEVBQStCO0FBQzNCLFlBQUl0TSxRQUFRc2tCLE1BQVosRUFBb0I7QUFDaEIzRixvQkFBUTJGLE1BQVI7QUFDQWhZLGdCQUFJeEwsUUFBSixHQUFlMGpCLFdBQWY7QUFDSCxTQUhELE1BSUssSUFBSXhrQixRQUFRcWtCLEtBQVIsSUFBaUIsQ0FBQzFGLFFBQVFzRSxTQUFSLEVBQXRCLEVBQTJDO0FBQzVDM1csZ0JBQUl4TCxRQUFKLEdBQWV1akIsS0FBZjtBQUNIO0FBQ0o7QUFDRHpsQixRQUFJNlEsVUFBSixDQUFlLE1BQWYsRUFBdUJrUCxPQUF2QjtBQUNBL2YsUUFBSWtGLFdBQUosY0FBNkIsVUFBVTlELEdBQVYsRUFBZStrQixNQUFmLEVBQXVCelksR0FBdkIsRUFBNEI7QUFDckQsWUFBSSxPQUFPcVksSUFBUCxLQUFnQixXQUFwQixFQUFpQztBQUM3QnJZLGdCQUFJMkUsT0FBSixHQUFjME4sUUFBUXNFLFNBQVIsQ0FBa0IsSUFBbEIsRUFBd0JwaUIsSUFBeEIsQ0FBNkI7QUFBQSx1QkFBUUQsWUFBWVosR0FBWixFQUFpQnNNLEdBQWpCLENBQVI7QUFBQSxhQUE3QixDQUFkO0FBQ0g7QUFDRCxlQUFPMUwsWUFBWVosR0FBWixFQUFpQnNNLEdBQWpCLENBQVA7QUFDSCxLQUxEO0FBTUEsUUFBSW1ZLElBQUosRUFBVTtBQUNOTyxvQkFBWTtBQUFBLG1CQUFNckcsUUFBUXNFLFNBQVIsQ0FBa0IsSUFBbEIsQ0FBTjtBQUFBLFNBQVosRUFBMkN3QixJQUEzQztBQUNIO0FBQ0osQzs7Ozs7Ozs7O0FDdEREOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0NBLElBQU1RLDBCQUEwQjtBQUM1QkMsWUFBUSxDQURvQjtBQUU1QkMsb0JBQWdCLENBRlk7QUFHNUJqa0IsVUFBTSxDQUhzQjtBQUk1QmtrQixVQUFNLENBSnNCO0FBSzVCNVQsV0FBTyxDQUxxQjtBQU01QjZULGtCQUFjLENBTmM7QUFPNUJDLHFCQUFpQixDQVBXO0FBUTVCQywwQkFBc0IsQ0FSTTtBQVM1QkMsaUJBQWEsQ0FUZTtBQVU1QnpqQixjQUFVO0FBVmtCLENBQWhDOztBQWFBLHlEQUFlLFVBQVMwakIsU0FBVCxFQUFvQkMsWUFBcEIsRUFBa0M7O0FBRTdDLFFBQUksUUFBT0QsU0FBUCx5Q0FBT0EsU0FBUCxPQUFxQixRQUFyQixJQUFpQyxPQUFPQSxVQUFVRSxJQUFqQixLQUEwQixVQUEvRCxFQUEyRTtBQUN2RTNoQixnQkFBUVosS0FBUixDQUFjLHFHQUNBLHdCQURBLFdBQ2tDcWlCLFNBRGxDLHlDQUNrQ0EsU0FEbEMsS0FDK0Msb0JBRC9DLFdBQzZFQSxVQUFVRSxJQUR2RixDQUFkO0FBRUE7QUFDSDs7QUFFRCxRQUFNcmhCLFVBQVUsSUFBaEI7QUFDQSxTQUFLbWhCLFNBQUwsR0FBaUJBLFNBQWpCOztBQUVBLFNBQUtFLElBQUwsR0FBWSxVQUFTbmdCLElBQVQsRUFBZTs7QUFFdkIsWUFBSSxPQUFPQSxJQUFQLEtBQWlCLFFBQXJCLEVBQStCO0FBQzNCQSxtQkFBT2IsS0FBS0MsU0FBTCxDQUFlWSxJQUFmLENBQVA7QUFDSDtBQUNEbEIsZ0JBQVFtaEIsU0FBUixDQUFrQkUsSUFBbEIsQ0FBdUJuZ0IsSUFBdkI7QUFDSCxLQU5EOztBQVFBLFNBQUtpZ0IsU0FBTCxDQUFlRyxTQUFmLEdBQTJCLFVBQVN0a0IsT0FBVCxFQUFrQjs7QUFFekMsWUFBSWtFLE9BQU9sRSxRQUFRa0UsSUFBbkI7QUFDQSxZQUFJLE9BQU9BLElBQVAsS0FBZ0IsUUFBcEIsRUFBOEI7QUFDMUJBLG1CQUFPYixLQUFLNUUsS0FBTCxDQUFXeUYsSUFBWCxDQUFQO0FBQ0g7QUFDRCxnQkFBUUEsS0FBS2tCLElBQWI7QUFDQSxpQkFBS3VlLHdCQUF3QkMsTUFBN0I7QUFDSTVnQix3QkFBUXVoQixZQUFSLENBQXFCcmdCLElBQXJCO0FBQ0E7QUFDSixpQkFBS3lmLHdCQUF3QmxqQixRQUE3QjtBQUNJdUMsd0JBQVF3aEIsY0FBUixDQUF1QnRnQixJQUF2QjtBQUNBO0FBQ0osaUJBQUt5Zix3QkFBd0JFLGNBQTdCO0FBQ0k3Z0Isd0JBQVF5aEIsb0JBQVIsQ0FBNkJ2Z0IsSUFBN0I7QUFDQTtBQUNKO0FBQ0l4Qix3QkFBUVosS0FBUixDQUFjLDJCQUFkLEVBQTJDOUIsUUFBUWtFLElBQW5EO0FBQ0E7QUFaSjtBQWNILEtBcEJEOztBQXNCQSxTQUFLd2dCLGFBQUwsR0FBcUIsRUFBckI7O0FBRUEsU0FBS0MsTUFBTCxHQUFjLENBQWQ7O0FBRUEsU0FBS25SLElBQUwsR0FBWSxVQUFTdFAsSUFBVCxFQUFlMGdCLFFBQWYsRUFBeUI7O0FBRWpDLFlBQUksQ0FBQ0EsUUFBTCxFQUFlO0FBQ1g7QUFDQTVoQixvQkFBUXFoQixJQUFSLENBQWFuZ0IsSUFBYjtBQUNBO0FBQ0g7QUFDRCxZQUFJbEIsUUFBUTJoQixNQUFSLEtBQW1CRSxPQUFPQyxTQUE5QixFQUF5QztBQUNyQztBQUNBOWhCLG9CQUFRMmhCLE1BQVIsR0FBaUJFLE9BQU9FLFNBQXhCO0FBQ0g7QUFDRCxZQUFJN2dCLEtBQUtvYSxjQUFMLENBQW9CLElBQXBCLENBQUosRUFBK0I7QUFDM0I1YixvQkFBUVosS0FBUixDQUFjLDJDQUEyQ3VCLEtBQUtDLFNBQUwsQ0FBZVksSUFBZixDQUF6RDtBQUNBO0FBQ0g7QUFDREEsYUFBS2hDLEVBQUwsR0FBVWMsUUFBUTJoQixNQUFSLEVBQVY7QUFDQTNoQixnQkFBUTBoQixhQUFSLENBQXNCeGdCLEtBQUtoQyxFQUEzQixJQUFpQzBpQixRQUFqQztBQUNBNWhCLGdCQUFRcWhCLElBQVIsQ0FBYW5nQixJQUFiO0FBQ0gsS0FsQkQ7O0FBb0JBLFNBQUtoQixPQUFMLEdBQWUsRUFBZjs7QUFFQSxTQUFLcWhCLFlBQUwsR0FBb0IsVUFBU3ZrQixPQUFULEVBQWtCOztBQUVsQyxZQUFNcWIsU0FBU3JZLFFBQVFFLE9BQVIsQ0FBZ0JsRCxRQUFRcWIsTUFBeEIsQ0FBZjtBQUNBLFlBQUlBLE1BQUosRUFBWTtBQUNSQSxtQkFBTzJKLGFBQVAsQ0FBcUJobEIsUUFBUTRqQixNQUE3QixFQUFxQzVqQixRQUFReUMsSUFBN0M7QUFDSCxTQUZELE1BRU87QUFDSEMsb0JBQVF1YixJQUFSLENBQWEsdUJBQXVCamUsUUFBUXFiLE1BQS9CLEdBQXdDLElBQXhDLEdBQStDcmIsUUFBUTRqQixNQUFwRTtBQUNIO0FBQ0osS0FSRDs7QUFVQSxTQUFLWSxjQUFMLEdBQXNCLFVBQVN4a0IsT0FBVCxFQUFrQjs7QUFFcEMsWUFBSSxDQUFDQSxRQUFRc2UsY0FBUixDQUF1QixJQUF2QixDQUFMLEVBQW1DO0FBQy9CNWIsb0JBQVFaLEtBQVIsQ0FBYyxxQ0FBZCxFQUFxRHVCLEtBQUtDLFNBQUwsQ0FBZXRELE9BQWYsQ0FBckQ7QUFDQTtBQUNIO0FBQ0RnRCxnQkFBUTBoQixhQUFSLENBQXNCMWtCLFFBQVFrQyxFQUE5QixFQUFrQ2xDLFFBQVFrRSxJQUExQztBQUNBLGVBQU9sQixRQUFRMGhCLGFBQVIsQ0FBc0Ixa0IsUUFBUWtDLEVBQTlCLENBQVA7QUFDSCxLQVJEOztBQVVBLFNBQUt1aUIsb0JBQUwsR0FBNEIsVUFBU3prQixPQUFULEVBQWtCOztBQUUxQyxhQUFLLElBQUlmLENBQVQsSUFBY2UsUUFBUWtFLElBQXRCLEVBQTRCO0FBQ3hCLGdCQUFJQSxPQUFPbEUsUUFBUWtFLElBQVIsQ0FBYWpGLENBQWIsQ0FBWDtBQUNBLGdCQUFJb2MsU0FBU3JZLFFBQVFFLE9BQVIsQ0FBZ0JnQixLQUFLbVgsTUFBckIsQ0FBYjtBQUNBLGdCQUFJQSxNQUFKLEVBQVk7QUFDUkEsdUJBQU93SSxjQUFQLENBQXNCM2YsS0FBSytnQixPQUEzQixFQUFvQy9nQixLQUFLZ2hCLFVBQXpDO0FBQ0gsYUFGRCxNQUVPO0FBQ0h4aUIsd0JBQVF1YixJQUFSLENBQWEsZ0NBQWdDL1osS0FBS21YLE1BQXJDLEdBQThDLElBQTlDLEdBQXFEblgsS0FBSzBmLE1BQXZFO0FBQ0g7QUFDSjtBQUNENWdCLGdCQUFRd1EsSUFBUixDQUFhLEVBQUNwTyxNQUFNdWUsd0JBQXdCRyxJQUEvQixFQUFiO0FBQ0gsS0FaRDs7QUFjQSxTQUFLNVQsS0FBTCxHQUFhLFVBQVNsUSxPQUFULEVBQWtCO0FBQzNCZ0QsZ0JBQVFxaEIsSUFBUixDQUFhLEVBQUNqZixNQUFNdWUsd0JBQXdCelQsS0FBL0IsRUFBc0NoTSxNQUFNbEUsT0FBNUMsRUFBYjtBQUNILEtBRkQ7O0FBSUFnRCxZQUFRd1EsSUFBUixDQUFhLEVBQUNwTyxNQUFNdWUsd0JBQXdCL2pCLElBQS9CLEVBQWIsRUFBbUQsVUFBU3NFLElBQVQsRUFBZTs7QUFFOUQsYUFBSyxJQUFJaWhCLFVBQVQsSUFBdUJqaEIsSUFBdkIsRUFBNkI7QUFDekIsNkJBQWlCLElBQUlraEIsT0FBSixDQUFZRCxVQUFaLEVBQXdCamhCLEtBQUtpaEIsVUFBTCxDQUF4QixFQUEwQ25pQixPQUExQztBQUNwQjtBQUNEO0FBQ0EsYUFBSyxJQUFJbWlCLFdBQVQsSUFBdUJuaUIsUUFBUUUsT0FBL0IsRUFBd0M7QUFDcENGLG9CQUFRRSxPQUFSLENBQWdCaWlCLFdBQWhCLEVBQTRCRSxnQkFBNUI7QUFDSDtBQUNELFlBQUlqQixZQUFKLEVBQWtCO0FBQ2RBLHlCQUFhcGhCLE9BQWI7QUFDSDtBQUNEQSxnQkFBUXdRLElBQVIsQ0FBYSxFQUFDcE8sTUFBTXVlLHdCQUF3QkcsSUFBL0IsRUFBYjtBQUNILEtBYkQ7QUFjSDs7QUFFRCxTQUFTc0IsT0FBVCxDQUFpQjduQixJQUFqQixFQUF1QjJHLElBQXZCLEVBQTZCb2hCLFVBQTdCLEVBQXlDO0FBQ3JDLFNBQUtDLE1BQUwsR0FBY2hvQixJQUFkO0FBQ0ErbkIsZUFBV3BpQixPQUFYLENBQW1CM0YsSUFBbkIsSUFBMkIsSUFBM0I7O0FBRUE7QUFDQSxTQUFLaW9CLGlCQUFMLEdBQXlCLEVBQXpCOztBQUVBO0FBQ0EsU0FBS0MsaUJBQUwsR0FBeUIsRUFBekI7O0FBRUEsUUFBSXBLLFNBQVMsSUFBYjs7QUFFQTs7QUFFQSxTQUFLcUssYUFBTCxHQUFxQixVQUFTamxCLFFBQVQsRUFBbUI7QUFDcEMsWUFBSUEsb0JBQW9CK04sS0FBeEIsRUFBK0I7QUFDM0I7QUFDQSxnQkFBSW9SLE1BQU0sSUFBSXBSLEtBQUosQ0FBVS9OLFNBQVN2QixNQUFuQixDQUFWO0FBQ0EsaUJBQUssSUFBSUQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJd0IsU0FBU3ZCLE1BQTdCLEVBQXFDLEVBQUVELENBQXZDLEVBQTBDO0FBQ3RDMmdCLG9CQUFJM2dCLENBQUosSUFBU29jLE9BQU9xSyxhQUFQLENBQXFCamxCLFNBQVN4QixDQUFULENBQXJCLENBQVQ7QUFDSDtBQUNELG1CQUFPMmdCLEdBQVA7QUFDSDtBQUNELFlBQUksQ0FBQ25mLFFBQUQsSUFBYSxDQUFDQSxTQUFTLGNBQVQsQ0FBZCxJQUEwQ0EsU0FBU3lCLEVBQVQsS0FBZ0J5akIsU0FBOUQsRUFBeUU7QUFDckUsbUJBQU9sbEIsUUFBUDtBQUNIOztBQUVELFlBQUltbEIsV0FBV25sQixTQUFTeUIsRUFBeEI7QUFDQSxZQUFJb2pCLFdBQVdwaUIsT0FBWCxDQUFtQjBpQixRQUFuQixDQUFKLEVBQWtDO0FBQzlCLG1CQUFPTixXQUFXcGlCLE9BQVgsQ0FBbUIwaUIsUUFBbkIsQ0FBUDtBQUNIOztBQUVELFlBQUksQ0FBQ25sQixTQUFTeUQsSUFBZCxFQUFvQjtBQUNoQnhCLG9CQUFRWixLQUFSLENBQWMsbUNBQW1DOGpCLFFBQW5DLEdBQThDLGdCQUE1RDtBQUNBO0FBQ0g7O0FBRUQsWUFBSUMsVUFBVSxJQUFJVCxPQUFKLENBQWFRLFFBQWIsRUFBdUJubEIsU0FBU3lELElBQWhDLEVBQXNDb2hCLFVBQXRDLENBQWQ7QUFDQU8sZ0JBQVFDLFNBQVIsQ0FBa0JDLE9BQWxCLENBQTBCLFlBQVc7QUFDakMsZ0JBQUlULFdBQVdwaUIsT0FBWCxDQUFtQjBpQixRQUFuQixNQUFpQ0MsT0FBckMsRUFBOEM7QUFDMUMsdUJBQU9QLFdBQVdwaUIsT0FBWCxDQUFtQjBpQixRQUFuQixDQUFQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBSUksZ0JBQWdCLEVBQXBCO0FBQ0EscUJBQUssSUFBSUMsWUFBVCxJQUF5QkosT0FBekIsRUFBa0M7QUFDOUJHLGtDQUFjcG9CLElBQWQsQ0FBbUJxb0IsWUFBbkI7QUFDSDtBQUNELHFCQUFLLElBQUlDLEdBQVQsSUFBZ0JGLGFBQWhCLEVBQStCO0FBQzNCLDJCQUFPSCxRQUFRRyxjQUFjRSxHQUFkLENBQVIsQ0FBUDtBQUNIO0FBQ0o7QUFDSixTQWZEO0FBZ0JBO0FBQ0FMLGdCQUFRUixnQkFBUjtBQUNBLGVBQU9RLE9BQVA7QUFDSCxLQTNDRDs7QUE2Q0EsU0FBS1IsZ0JBQUwsR0FBd0IsWUFBVzs7QUFFL0IsYUFBSyxJQUFJYyxXQUFULElBQXdCOUssT0FBT29LLGlCQUEvQixFQUFrRDtBQUM5Q3BLLG1CQUFPb0ssaUJBQVAsQ0FBeUJVLFdBQXpCLElBQXdDOUssT0FBT3FLLGFBQVAsQ0FBcUJySyxPQUFPb0ssaUJBQVAsQ0FBeUJVLFdBQXpCLENBQXJCLENBQXhDO0FBQ0g7QUFDSixLQUxEOztBQU9BLGFBQVNDLFNBQVQsQ0FBbUJDLFVBQW5CLEVBQStCQyxzQkFBL0IsRUFDQTtBQUNJLFlBQUlDLGFBQWFGLFdBQVcsQ0FBWCxDQUFqQjtBQUNBLFlBQUlHLGNBQWNILFdBQVcsQ0FBWCxDQUFsQjtBQUNBaEwsZUFBT2tMLFVBQVAsSUFBcUI7QUFDakJSLHFCQUFTLGlCQUFTbkIsUUFBVCxFQUFtQjtBQUN4QixvQkFBSSxPQUFPQSxRQUFQLEtBQXFCLFVBQXpCLEVBQXFDO0FBQ2pDbGlCLDRCQUFRWixLQUFSLENBQWMsNkNBQTZDeWtCLFVBQTNEO0FBQ0E7QUFDSDs7QUFFRGxMLHVCQUFPbUssaUJBQVAsQ0FBeUJnQixXQUF6QixJQUF3Q25MLE9BQU9tSyxpQkFBUCxDQUF5QmdCLFdBQXpCLEtBQXlDLEVBQWpGO0FBQ0FuTCx1QkFBT21LLGlCQUFQLENBQXlCZ0IsV0FBekIsRUFBc0M1b0IsSUFBdEMsQ0FBMkNnbkIsUUFBM0M7O0FBRUEsb0JBQUksQ0FBQzBCLHNCQUFELElBQTJCQyxlQUFlLFdBQTlDLEVBQTJEO0FBQ3ZEO0FBQ0E7QUFDQWpCLCtCQUFXOVIsSUFBWCxDQUFnQjtBQUNacE8sOEJBQU11ZSx3QkFBd0JLLGVBRGxCO0FBRVozSSxnQ0FBUUEsT0FBT2tLLE1BRkg7QUFHWjNCLGdDQUFRNEM7QUFISSxxQkFBaEI7QUFLSDtBQUNKLGFBbkJnQjtBQW9CakJDLHdCQUFZLG9CQUFTN0IsUUFBVCxFQUFtQjtBQUMzQixvQkFBSSxPQUFPQSxRQUFQLEtBQXFCLFVBQXpCLEVBQXFDO0FBQ2pDbGlCLDRCQUFRWixLQUFSLENBQWMsa0RBQWtEeWtCLFVBQWhFO0FBQ0E7QUFDSDtBQUNEbEwsdUJBQU9tSyxpQkFBUCxDQUF5QmdCLFdBQXpCLElBQXdDbkwsT0FBT21LLGlCQUFQLENBQXlCZ0IsV0FBekIsS0FBeUMsRUFBakY7QUFDQSxvQkFBSU4sTUFBTTdLLE9BQU9tSyxpQkFBUCxDQUF5QmdCLFdBQXpCLEVBQXNDcm9CLE9BQXRDLENBQThDeW1CLFFBQTlDLENBQVY7QUFDQSxvQkFBSXNCLFFBQVEsQ0FBQyxDQUFiLEVBQWdCO0FBQ1p4akIsNEJBQVFaLEtBQVIsQ0FBYyxzQ0FBc0N5a0IsVUFBdEMsR0FBbUQsTUFBbkQsR0FBNEQzQixTQUFTcm5CLElBQW5GO0FBQ0E7QUFDSDtBQUNEOGQsdUJBQU9tSyxpQkFBUCxDQUF5QmdCLFdBQXpCLEVBQXNDcFQsTUFBdEMsQ0FBNkM4UyxHQUE3QyxFQUFrRCxDQUFsRDtBQUNBLG9CQUFJLENBQUNJLHNCQUFELElBQTJCakwsT0FBT21LLGlCQUFQLENBQXlCZ0IsV0FBekIsRUFBc0N0bkIsTUFBdEMsS0FBaUQsQ0FBaEYsRUFBbUY7QUFDL0U7QUFDQW9tQiwrQkFBVzlSLElBQVgsQ0FBZ0I7QUFDWnBPLDhCQUFNdWUsd0JBQXdCTSxvQkFEbEI7QUFFWjVJLGdDQUFRQSxPQUFPa0ssTUFGSDtBQUdaM0IsZ0NBQVE0QztBQUhJLHFCQUFoQjtBQUtIO0FBQ0o7QUF4Q2dCLFNBQXJCO0FBMENIOztBQUVEOzs7QUFHQSxhQUFTRSxxQkFBVCxDQUErQkgsVUFBL0IsRUFBMkNJLFVBQTNDLEVBQXVEOztBQUVuRCxZQUFJQyxjQUFjdkwsT0FBT21LLGlCQUFQLENBQXlCZSxVQUF6QixDQUFsQjs7QUFFQSxZQUFJSyxXQUFKLEVBQWlCO0FBQ2JBLHdCQUFZM0ssT0FBWixDQUFvQixVQUFTMkksUUFBVCxFQUFtQjtBQUNuQ0EseUJBQVM5VSxLQUFULENBQWU4VSxRQUFmLEVBQXlCK0IsVUFBekI7QUFDSCxhQUZEO0FBR0g7QUFDSjs7QUFFRCxTQUFLOUMsY0FBTCxHQUFzQixVQUFTb0IsT0FBVCxFQUFrQjRCLFdBQWxCLEVBQStCO0FBQ2pEO0FBQ0EsYUFBSyxJQUFJQyxhQUFULElBQTBCRCxXQUExQixFQUF1QztBQUNuQyxnQkFBSUUsZ0JBQWdCRixZQUFZQyxhQUFaLENBQXBCO0FBQ0F6TCxtQkFBT29LLGlCQUFQLENBQXlCcUIsYUFBekIsSUFBMENDLGFBQTFDO0FBQ0g7O0FBRUQsYUFBSyxJQUFJUixVQUFULElBQXVCdEIsT0FBdkIsRUFBZ0M7QUFDNUI7QUFDQTtBQUNBeUIsa0NBQXNCSCxVQUF0QixFQUFrQ3RCLFFBQVFzQixVQUFSLENBQWxDO0FBQ0g7QUFDSixLQVpEOztBQWNBLFNBQUt2QixhQUFMLEdBQXFCLFVBQVN1QixVQUFULEVBQXFCSSxVQUFyQixFQUFpQztBQUNsREQsOEJBQXNCSCxVQUF0QixFQUFrQyxLQUFLYixhQUFMLENBQW1CaUIsVUFBbkIsQ0FBbEM7QUFDSCxLQUZEOztBQUlBLGFBQVNLLFNBQVQsQ0FBbUJDLFVBQW5CLEVBQStCO0FBQzNCLFlBQUlDLGFBQWFELFdBQVcsQ0FBWCxDQUFqQjtBQUNBLFlBQUlFLFlBQVlGLFdBQVcsQ0FBWCxDQUFoQjtBQUNBNUwsZUFBTzZMLFVBQVAsSUFBcUIsWUFBVztBQUM1QixnQkFBSXprQixPQUFPLEVBQVg7QUFDQSxnQkFBSW1pQixpQkFBSjtBQUNBLGlCQUFLLElBQUkzbEIsSUFBSSxDQUFiLEVBQWdCQSxJQUFJbW9CLFVBQVVsb0IsTUFBOUIsRUFBc0MsRUFBRUQsQ0FBeEMsRUFBMkM7QUFDdkMsb0JBQUlvb0IsV0FBV0QsVUFBVW5vQixDQUFWLENBQWY7QUFDQSxvQkFBSSxPQUFPb29CLFFBQVAsS0FBb0IsVUFBeEIsRUFBb0M7QUFDaEN6QywrQkFBV3lDLFFBQVg7QUFDSCxpQkFGRCxNQUVPLElBQUlBLG9CQUFvQmpDLE9BQXBCLElBQStCRSxXQUFXcGlCLE9BQVgsQ0FBbUJta0IsU0FBUzlCLE1BQTVCLE1BQXdDSSxTQUEzRSxFQUFzRjtBQUN6RmxqQix5QkFBSzdFLElBQUwsQ0FBVTtBQUNOLDhCQUFNeXBCLFNBQVM5QjtBQURULHFCQUFWO0FBR0gsaUJBSk0sTUFJQTtBQUNIOWlCLHlCQUFLN0UsSUFBTCxDQUFVeXBCLFFBQVY7QUFDSDtBQUNKOztBQUVEL0IsdUJBQVc5UixJQUFYLENBQWdCO0FBQ1pwTyxzQkFBTXVlLHdCQUF3QkksWUFEbEI7QUFFWjFJLHdCQUFRQSxPQUFPa0ssTUFGSDtBQUdaOVcsd0JBQVEwWSxTQUhJO0FBSVoxa0Isc0JBQU1BO0FBSk0sYUFBaEIsRUFLRyxVQUFTaEMsUUFBVCxFQUFtQjtBQUNsQixvQkFBSUEsYUFBYWtsQixTQUFqQixFQUE0QjtBQUN4Qix3QkFBSWpsQixTQUFTMmEsT0FBT3FLLGFBQVAsQ0FBcUJqbEIsUUFBckIsQ0FBYjtBQUNBLHdCQUFJbWtCLFFBQUosRUFBYztBQUNUQSxnQ0FBRCxDQUFXbGtCLE1BQVg7QUFDSDtBQUNKO0FBQ0osYUFaRDtBQWFILFNBN0JEO0FBOEJIOztBQUVELGFBQVM0bUIsZ0JBQVQsQ0FBMEJDLFlBQTFCLEVBQ0E7QUFDSSxZQUFJVCxnQkFBZ0JTLGFBQWEsQ0FBYixDQUFwQjtBQUNBLFlBQUl0QixlQUFlc0IsYUFBYSxDQUFiLENBQW5CO0FBQ0EsWUFBSUMsbUJBQW1CRCxhQUFhLENBQWIsQ0FBdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQWxNLGVBQU9vSyxpQkFBUCxDQUF5QnFCLGFBQXpCLElBQTBDUyxhQUFhLENBQWIsQ0FBMUM7O0FBRUEsWUFBSUMsZ0JBQUosRUFBc0I7QUFDbEIsZ0JBQUlBLGlCQUFpQixDQUFqQixNQUF3QixDQUE1QixFQUErQjtBQUMzQjtBQUNBQSxpQ0FBaUIsQ0FBakIsSUFBc0J2QixlQUFlLFNBQXJDO0FBQ0g7QUFDREcsc0JBQVVvQixnQkFBVixFQUE0QixJQUE1QjtBQUNIOztBQUVEQyxlQUFPQyxjQUFQLENBQXNCck0sTUFBdEIsRUFBOEI0SyxZQUE5QixFQUE0QztBQUN4QzBCLDBCQUFjLElBRDBCO0FBRXhDOW9CLGlCQUFLLGVBQVk7QUFDYixvQkFBSWtvQixnQkFBZ0IxTCxPQUFPb0ssaUJBQVAsQ0FBeUJxQixhQUF6QixDQUFwQjtBQUNBLG9CQUFJQyxrQkFBa0JwQixTQUF0QixFQUFpQztBQUM3QjtBQUNBampCLDRCQUFRdWIsSUFBUixDQUFhLHNEQUFzRGdJLFlBQXRELEdBQXFFLGVBQXJFLEdBQXVGNUssT0FBT2tLLE1BQTNHO0FBQ0g7O0FBRUQsdUJBQU93QixhQUFQO0FBQ0gsYUFWdUM7QUFXeEM1a0IsaUJBQUssYUFBU2dDLEtBQVQsRUFBZ0I7QUFDakIsb0JBQUlBLFVBQVV3aEIsU0FBZCxFQUF5QjtBQUNyQmpqQiw0QkFBUXViLElBQVIsQ0FBYSx5QkFBeUJnSSxZQUF6QixHQUF3QywrQkFBckQ7QUFDQTtBQUNIO0FBQ0Q1Syx1QkFBT29LLGlCQUFQLENBQXlCcUIsYUFBekIsSUFBMEMzaUIsS0FBMUM7QUFDQSxvQkFBSXlqQixjQUFjempCLEtBQWxCO0FBQ0Esb0JBQUl5akIsdUJBQXVCeEMsT0FBdkIsSUFBa0NFLFdBQVdwaUIsT0FBWCxDQUFtQjBrQixZQUFZckMsTUFBL0IsTUFBMkNJLFNBQWpGLEVBQTRGO0FBQ3hGaUMsa0NBQWMsRUFBRSxNQUFNQSxZQUFZckMsTUFBcEIsRUFBZDtBQUNIO0FBQ0RELDJCQUFXOVIsSUFBWCxDQUFnQjtBQUNacE8sMEJBQU11ZSx3QkFBd0JPLFdBRGxCO0FBRVo3SSw0QkFBUUEsT0FBT2tLLE1BRkg7QUFHWnNDLDhCQUFVZixhQUhFO0FBSVozaUIsMkJBQU95akI7QUFKSyxpQkFBaEI7QUFNSDtBQTNCdUMsU0FBNUM7QUE4Qkg7O0FBRUQ7O0FBRUExakIsU0FBSzRqQixPQUFMLENBQWE3TCxPQUFiLENBQXFCK0ssU0FBckI7O0FBRUE5aUIsU0FBS2doQixVQUFMLENBQWdCakosT0FBaEIsQ0FBd0JxTCxnQkFBeEI7O0FBRUFwakIsU0FBSytnQixPQUFMLENBQWFoSixPQUFiLENBQXFCLFVBQVMySCxNQUFULEVBQWlCO0FBQUV3QyxrQkFBVXhDLE1BQVYsRUFBa0IsS0FBbEI7QUFBMkIsS0FBbkU7O0FBRUEsU0FBSyxJQUFJcm1CLEtBQVQsSUFBaUIyRyxLQUFLNmpCLEtBQXRCLEVBQTZCO0FBQ3pCMU0sZUFBTzlkLEtBQVAsSUFBZTJHLEtBQUs2akIsS0FBTCxDQUFXeHFCLEtBQVgsQ0FBZjtBQUNIO0FBQ0o7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEkiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL3J1bnRpbWUvXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgYTcyOTAwZTg1MjExNzMxNWI0ZmYiLCJleHBvcnQgeyBKZXRBcHAgfSBmcm9tIFwiLi9KZXRBcHBcIjtcbmV4cG9ydCB7IEpldFZpZXcgfSBmcm9tIFwiLi9KZXRWaWV3XCI7XG5leHBvcnQgeyBIYXNoUm91dGVyIH0gZnJvbSBcIi4vcm91dGVycy9IYXNoUm91dGVyXCI7XG5leHBvcnQgeyBTdG9yZVJvdXRlciB9IGZyb20gXCIuL3JvdXRlcnMvU3RvcmVSb3V0ZXJcIjtcbmV4cG9ydCB7IFVybFJvdXRlciB9IGZyb20gXCIuL3JvdXRlcnMvVXJsUm91dGVyXCI7XG5leHBvcnQgeyBFbXB0eVJvdXRlciB9IGZyb20gXCIuL3JvdXRlcnMvRW1wdHlSb3V0ZXJcIjtcbmltcG9ydCB7IFVubG9hZEd1YXJkIH0gZnJvbSBcIi4vcGx1Z2lucy9HdWFyZFwiO1xuaW1wb3J0IHsgTG9jYWxlIH0gZnJvbSBcIi4vcGx1Z2lucy9Mb2NhbGVcIjtcbmltcG9ydCB7IE1lbnUgfSBmcm9tIFwiLi9wbHVnaW5zL01lbnVcIjtcbmltcG9ydCB7IFN0YXR1cyB9IGZyb20gXCIuL3BsdWdpbnMvU3RhdHVzXCI7XG5pbXBvcnQgeyBUaGVtZSB9IGZyb20gXCIuL3BsdWdpbnMvVGhlbWVcIjtcbmltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi9wbHVnaW5zL1VzZXJcIjtcbmV4cG9ydCBjb25zdCBwbHVnaW5zID0ge1xuICAgIFVubG9hZEd1YXJkLCBMb2NhbGUsIE1lbnUsIFRoZW1lLCBVc2VyLCBTdGF0dXNcbn07XG5pZiAoIXdpbmRvdy5Qcm9taXNlKSB7XG4gICAgd2luZG93LlByb21pc2UgPSB3ZWJpeC5wcm9taXNlO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9pbmRleC5qcyIsImltcG9ydCB7IEpldEJhc2UgfSBmcm9tIFwiLi9KZXRCYXNlXCI7XG5pbXBvcnQgeyBwYXJzZSwgdXJsMnN0ciB9IGZyb20gXCIuL2hlbHBlcnNcIjtcbmV4cG9ydCBjbGFzcyBKZXRWaWV3IGV4dGVuZHMgSmV0QmFzZSB7XG4gICAgY29uc3RydWN0b3IoYXBwLCBuYW1lKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgICAgIHRoaXMuYXBwID0gYXBwO1xuICAgICAgICB0aGlzLl9uYW1lID0gbmFtZTtcbiAgICAgICAgdGhpcy5fY2hpbGRyZW4gPSBbXTtcbiAgICB9XG4gICAgdWkodWksIG5hbWUpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB1aSA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICBjb25zdCBqZXR2aWV3ID0gbmV3IHVpKHRoaXMuYXBwLCBuYW1lKTtcbiAgICAgICAgICAgIHRoaXMuX2NoaWxkcmVuLnB1c2goamV0dmlldyk7XG4gICAgICAgICAgICBqZXR2aWV3LnJlbmRlcigpO1xuICAgICAgICAgICAgcmV0dXJuIGpldHZpZXc7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBjb25zdCB2aWV3ID0gdGhpcy5hcHAud2ViaXgudWkodWkpO1xuICAgICAgICAgICAgdGhpcy5fY2hpbGRyZW4ucHVzaCh2aWV3KTtcbiAgICAgICAgICAgIHJldHVybiB2aWV3O1xuICAgICAgICB9XG4gICAgfVxuICAgIHNob3cocGF0aCwgY29uZmlnKSB7XG4gICAgICAgIC8vIHZhciB0YXJnZXQgPSB0aGlzLnN1YnNbMF07XG4gICAgICAgIC8vIGlmIChjb25maWcgJiYgY29uZmlnLnRhcmdldClcbiAgICAgICAgLy8gXHR0YXJnZXQgPSB0aGlzLl9zdWJzW3BhdGhdO1xuICAgICAgICBpZiAodHlwZW9mIHBhdGggPT09IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgIC8vIHJvb3QgcGF0aFxuICAgICAgICAgICAgaWYgKHBhdGguc3Vic3RyKDAsIDEpID09PSBcIi9cIikge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmFwcC5zaG93KHBhdGgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gbG9jYWwgcGF0aCwgZG8gbm90aGluZ1xuICAgICAgICAgICAgaWYgKHBhdGguaW5kZXhPZihcIi4vXCIpID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcGF0aCA9IHBhdGguc3Vic3RyKDIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gcGFyZW50IHBhdGgsIGNhbGwgcGFyZW50IHZpZXdcbiAgICAgICAgICAgIGlmIChwYXRoLmluZGV4T2YoXCIuLi9cIikgPT09IDApIHtcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJlbnQgPSB0aGlzLmdldFBhcmVudFZpZXcoKTtcbiAgICAgICAgICAgICAgICBpZiAocGFyZW50KSB7XG4gICAgICAgICAgICAgICAgICAgIHBhcmVudC5zaG93KFwiLi9cIiArIHBhdGguc3Vic3RyKDMpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwLnNob3coXCIvXCIgKyBwYXRoLnN1YnN0cigzKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IHN1YiA9IHRoaXMuZ2V0U3ViVmlldygpO1xuICAgICAgICAgICAgaWYgKCFzdWIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5hcHAuc2hvdyhcIi9cIiArIHBhdGgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHN1Yi5wYXJlbnQgIT09IHRoaXMpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gc3ViLnBhcmVudC5zaG93KHBhdGgsIGNvbmZpZyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBuZXdDaHVuayA9IHBhcnNlKHBhdGgpO1xuICAgICAgICAgICAgbGV0IHVybCA9IG51bGw7XG4gICAgICAgICAgICBjb25zdCBjdXJyZW50VXJsID0gdGhpcy5hcHAuZ2V0Um91dGVyKCkuZ2V0KCk7XG4gICAgICAgICAgICBpZiAodGhpcy5faW5kZXgpIHtcbiAgICAgICAgICAgICAgICB1cmwgPSBwYXJzZShjdXJyZW50VXJsKS5zbGljZSgwLCB0aGlzLl9pbmRleCkuY29uY2F0KG5ld0NodW5rKTtcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHVybC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICB1cmxbaV0uaW5kZXggPSBpICsgMTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY29uc3QgdXJsc3RyID0gdXJsMnN0cih1cmwpO1xuICAgICAgICAgICAgICAgIHRoaXMuYXBwLmNhbk5hdmlnYXRlKHVybHN0ciwgdGhpcykudGhlbihyZWRpcmVjdCA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh1cmxzdHIgIT09IHJlZGlyZWN0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1cmwgd2FzIGJsb2NrZWQgYW5kIHJlZGlyZWN0ZWRcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwLnNob3cocmVkaXJlY3QpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fZmluaXNoU2hvdyhzdWIuc3VidmlldywgdXJsLCByZWRpcmVjdCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KS5jYXRjaCgoKSA9PiBmYWxzZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9maW5pc2hTaG93KHN1Yi5zdWJ2aWV3LCBuZXdDaHVuaywgXCJcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyByb3V0ZSB0byBwYWdlXG4gICAgICAgICAgICAvLyB2YXIgcGFyc2VkID0gcGFyc2UocGF0aCk7XG4gICAgICAgICAgICAvLyB2YXIgcGF0aCB0aGlzLnBhdGguc2xpY2UoMCwgaW5kZXgpLmNvbmNhdChwYXJzZWQpO1xuICAgICAgICAgICAgLy8gaWYgKGNvbmZpZylcbiAgICAgICAgICAgIC8vIFx0d2ViaXguZXh0ZW5kKHNjb3BlLnBhdGhbaW5kZXhdLnBhcmFtcywgY29uZmlnLCB0cnVlKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIC8vIHNldCBwYXJhbWV0ZXJzXG4gICAgICAgICAgICAvLyB3ZWJpeC5leHRlbmQoc2NvcGUucGF0aFtpbmRleF0ucGFyYW1zLCBwYXRoLCB0cnVlKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBzY29wZS5zaG93KHVybF9zdHJpbmcoc2NvcGUucGF0aCksIC0xKTtcbiAgICB9XG4gICAgaW5pdChfJHZpZXcsIF8kdXJsKSB7XG4gICAgICAgIC8vIHN0dWJcbiAgICB9XG4gICAgcmVhZHkoXyR2aWV3LCBfJHVybCkge1xuICAgICAgICAvLyBzdHViXG4gICAgfVxuICAgIGNvbmZpZygpIHtcbiAgICAgICAgdGhpcy5hcHAud2ViaXgubWVzc2FnZShcIlZpZXc6Q29uZmlnIGlzIG5vdCBpbXBsZW1lbnRlZFwiKTtcbiAgICB9XG4gICAgdXJsQ2hhbmdlKF8kdmlldywgXyR1cmwpIHtcbiAgICAgICAgLy8gc3R1YlxuICAgIH1cbiAgICBkZXN0cm95KCkge1xuICAgICAgICAvLyBzdHViXG4gICAgfVxuICAgIGRlc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMuZGVzdHJveSgpO1xuICAgICAgICAvLyBkZXN0cm95IGNoaWxkIHZpZXdzXG4gICAgICAgIGNvbnN0IHVpcyA9IHRoaXMuX2NoaWxkcmVuO1xuICAgICAgICBmb3IgKGxldCBpID0gdWlzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgICAgICBpZiAodWlzW2ldICYmIHVpc1tpXS5kZXN0cnVjdG9yKSB7XG4gICAgICAgICAgICAgICAgdWlzW2ldLmRlc3RydWN0b3IoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAvLyByZXNldCB2YXJzIGZvciBiZXR0ZXIgR0MgcHJvY2Vzc2luZ1xuICAgICAgICB0aGlzLmFwcCA9IHRoaXMuX2NoaWxkcmVuID0gbnVsbDtcbiAgICAgICAgLy8gZGVzdHJveSBhY3R1YWwgVUlcbiAgICAgICAgdGhpcy5fcm9vdC5kZXN0cnVjdG9yKCk7XG4gICAgICAgIHN1cGVyLmRlc3RydWN0b3IoKTtcbiAgICB9XG4gICAgdXNlKHBsdWdpbiwgY29uZmlnKSB7XG4gICAgICAgIHBsdWdpbih0aGlzLmFwcCwgdGhpcywgY29uZmlnKTtcbiAgICB9XG4gICAgX3JlbmRlcih1cmwpIHtcbiAgICAgICAgbGV0IHJlc3BvbnNlO1xuICAgICAgICAvLyB1c2luZyB3cmFwcGVyIG9iamVjdCwgc28gdWkgY2FuIGJlIGNoYW5nZWQgZnJvbSBhcHA6cmVuZGVyIGV2ZW50XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IHsgdWk6IHt9IH07XG4gICAgICAgIHRoaXMuYXBwLmNvcHlDb25maWcodGhpcy5jb25maWcoKSwgcmVzdWx0LnVpLCB0aGlzLl9zdWJzKTtcbiAgICAgICAgdGhpcy5hcHAuY2FsbEV2ZW50KFwiYXBwOnJlbmRlclwiLCBbdGhpcywgdXJsLCByZXN1bHRdKTtcbiAgICAgICAgcmVzdWx0LnVpLiRzY29wZSA9IHRoaXM7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLl9yb290ID0gdGhpcy5hcHAud2ViaXgudWkocmVzdWx0LnVpLCB0aGlzLl9jb250YWluZXIpO1xuICAgICAgICAgICAgaWYgKHRoaXMuX3Jvb3QuZ2V0UGFyZW50VmlldygpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fY29udGFpbmVyID0gdGhpcy5fcm9vdDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuX2luaXQodGhpcy5fcm9vdCwgdXJsKTtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gdGhpcy5fdXJsQ2hhbmdlKHVybCkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucmVhZHkodGhpcy5fcm9vdCwgdXJsKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChlKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IFByb21pc2UucmVqZWN0KGUpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jYXRjaChlcnIgPT4gdGhpcy5faW5pdEVycm9yKHRoaXMsIGVycikpO1xuICAgIH1cbiAgICBfaW5pdCh2aWV3LCB1cmwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5pdCh2aWV3LCB1cmwpO1xuICAgIH1cbiAgICBfdXJsQ2hhbmdlKHVybCkge1xuICAgICAgICBjb25zdCB3YWl0cyA9IFtdO1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiB0aGlzLl9zdWJzKSB7XG4gICAgICAgICAgICBjb25zdCBmcmFtZSA9IHRoaXMuX3N1YnNba2V5XTtcbiAgICAgICAgICAgIGlmIChmcmFtZS51cmwpIHtcbiAgICAgICAgICAgICAgICAvLyB3ZSBoYXZlIGZpeGVkIHN1YnZpZXcgdXJsXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBmcmFtZS51cmwgPT09IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcGFyc2VkID0gcGFyc2UoZnJhbWUudXJsKTtcbiAgICAgICAgICAgICAgICAgICAgd2FpdHMucHVzaCh0aGlzLl9jcmVhdGVTdWJWaWV3KGZyYW1lLCBwYXJzZWQpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCB2aWV3ID0gZnJhbWUudmlldztcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBmcmFtZS51cmwgPT09IFwiZnVuY3Rpb25cIiAmJiAhKHZpZXcgaW5zdGFuY2VvZiBmcmFtZS51cmwpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2aWV3ID0gbmV3IGZyYW1lLnVybCh0aGlzLmFwcCwgXCJcIik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKCF2aWV3KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2aWV3ID0gZnJhbWUudXJsO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHdhaXRzLnB1c2godGhpcy5fcmVuZGVyU3ViVmlldyhmcmFtZSwgdmlldywgdXJsKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAoa2V5ID09PSBcImRlZmF1bHRcIiAmJiB1cmwgJiYgdXJsLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgICAgICAvLyB3ZSBoYXZlIGFuIHVybCBhbmQgc3VidmlldyBmb3IgaXRcbiAgICAgICAgICAgICAgICBjb25zdCBzdWJ1cmwgPSB1cmwuc2xpY2UoMSk7XG4gICAgICAgICAgICAgICAgd2FpdHMucHVzaCh0aGlzLl9jcmVhdGVTdWJWaWV3KGZyYW1lLCBzdWJ1cmwpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gUHJvbWlzZS5hbGwod2FpdHMpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy51cmxDaGFuZ2UodGhpcy5fcm9vdCwgdXJsKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIF9pbml0RXJyb3IodmlldywgZXJyKSB7XG4gICAgICAgIHRoaXMuYXBwLmVycm9yKFwiYXBwOmVycm9yOmluaXR2aWV3XCIsIFtlcnIsIHZpZXddKTtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIF9jcmVhdGVTdWJWaWV3KHN1Yiwgc3VidXJsKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFwcC5jcmVhdGVGcm9tVVJMKHN1YnVybCwgc3ViLnZpZXcpLnRoZW4odmlldyA9PiB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fcmVuZGVyU3ViVmlldyhzdWIsIHZpZXcsIHN1YnVybCk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBfcmVuZGVyU3ViVmlldyhzdWIsIHZpZXcsIHN1YnVybCkge1xuICAgICAgICBjb25zdCBjZWxsID0gdGhpcy5hcHAud2ViaXguJCQoc3ViLmlkKTtcbiAgICAgICAgcmV0dXJuIHZpZXcucmVuZGVyKGNlbGwsIHN1YnVybCwgdGhpcykudGhlbih1aSA9PiB7XG4gICAgICAgICAgICAvLyBkZXN0cm95IG9sZCB2aWV3XG4gICAgICAgICAgICBpZiAoc3ViLnZpZXcgJiYgc3ViLnZpZXcgIT09IHZpZXcpIHtcbiAgICAgICAgICAgICAgICBzdWIudmlldy5kZXN0cnVjdG9yKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBzYXZlIGluZm8gYWJvdXQgYSBuZXcgdmlld1xuICAgICAgICAgICAgc3ViLnZpZXcgPSB2aWV3O1xuICAgICAgICAgICAgc3ViLmlkID0gdWkuY29uZmlnLmlkO1xuICAgICAgICAgICAgcmV0dXJuIHVpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgX2ZpbmlzaFNob3coc3ViLCB1cmwsIHBhdGgpIHtcbiAgICAgICAgaWYgKHRoaXMuX2luZGV4KSB7XG4gICAgICAgICAgICB0aGlzLl91cmxDaGFuZ2UodXJsLnNsaWNlKHRoaXMuX2luZGV4IC0gMSkpO1xuICAgICAgICAgICAgdGhpcy5hcHAuZ2V0Um91dGVyKCkuc2V0KHBhdGgsIHsgc2lsZW50OiB0cnVlIH0pO1xuICAgICAgICAgICAgdGhpcy5hcHAuY2FsbEV2ZW50KFwiYXBwOnJvdXRlXCIsIFt1cmxdKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuX2NyZWF0ZVN1YlZpZXcoc3ViLCB1cmwpO1xuICAgICAgICB9XG4gICAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9KZXRWaWV3LmpzIiwiLyogTWFpbiBFbnRyeSBBcHAgSmF2YVNjcmlwdCAqL1xyXG5cclxuaW1wb3J0IFwiLi9zdHlsZXMvYXBwLmNzc1wiO1xyXG5pbXBvcnQge0pldEFwcH0gZnJvbSBcIndlYml4LWpldFwiO1xyXG5pbXBvcnQgUVdlYkNoYW5uZWwgZnJvbSAndXRpbHMvcXdlYmNoYW5uZWwnO1xyXG5cclxud2ViaXgucmVhZHkoKCkgPT4ge1xyXG4gICAgY29uc3QgYXBwID0gbmV3IEpldEFwcCh7XHJcbiAgICAgICAgaWQ6ICAgICAgICAgQVBQTkFNRSxcclxuICAgICAgICB2ZXJzaW9uOiAgICBWRVJTSU9OLFxyXG4gICAgICAgIHN0YXJ0OiAgICAgIFwiL2xheW91dFwiXHJcbiAgICB9KTtcclxuXHJcbiAgICBhcHAucmVuZGVyKCkudGhlbigoKSA9PiBhdHRhY2hFdmVudHNUb1VJKCkpO1xyXG4gICAgXHJcbiAgICBhcHAuYXR0YWNoRXZlbnQoXCJhcHA6ZXJyb3I6cmVzb2x2ZVwiLCBmdW5jdGlvbiguLi5hcmdzKXtcclxuICAgICAgICB3aW5kb3cuY29uc29sZS5lcnJvcihhcmdzKTtcclxuICAgIH0pO1xyXG59KTtcclxuXHJcbmxldCBuYXRpdmUgPSB7fTtcclxuXHJcbmxldCBuYXRpdmVQcm9taXNlID0gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgIG5ldyBRV2ViQ2hhbm5lbChxdC53ZWJDaGFubmVsVHJhbnNwb3J0LCBmdW5jdGlvbihjaGFubmVsKSB7XHJcbiAgICAgICAgbGV0IGluZm8gPSBcIlwiO1xyXG4gICAgICAgIG5hdGl2ZSA9IGNoYW5uZWwub2JqZWN0cztcclxuXHJcbiAgICAgICAgbmF0aXZlLlV0aWxzLnZlcnNpb24oZnVuY3Rpb24odmVyc2lvbikge1xyXG4gICAgICAgICAgICBpbmZvICs9IHZlcnNpb247XHJcbiAgICAgICAgICAgIG5hdGl2ZS5VdGlscy5zY3JlZW5JbmZvKGZ1bmN0aW9uKHNjcmVlbkluZm8pIHtcclxuICAgICAgICAgICAgICAgIGluZm8gKz0gXCJcXG5cXG5EZXNrdG9wIGluZm8gXCIgKyBKU09OLnN0cmluZ2lmeShzY3JlZW5JbmZvLCBudWxsLCA0KTtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoaW5mbyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbn0pO1xyXG5cclxuXHJcbm5hdGl2ZVByb21pc2UudGhlbihmdW5jdGlvbihpbmZvKSB7XHJcbiAgICBuYXRpdmUuVXRpbHMuc2hvd01lc3NhZ2UoXCJEZXNpZ25lclwiLCBcIkxvYWRlZCBXZWJjaGFubmVsXCIgKyBpbmZvLCBmYWxzZSk7XHJcbiAgICBuYXRpdmUuVXRpbHMubG9nTWVzc2FnZShcIkxvYWRlZCBXZWJDaGFubmVsXCIgKyBpbmZvKTtcclxufSwgZnVuY3Rpb24oaW5mbykge1xyXG4gICAgbmF0aXZlLlV0aWxzLnNob3dNZXNzYWdlKFwiRGVzaWduZXJcIiwgXCJGYWlsZWQgdG8gbG9hZCBXZWJjaGFubmVsXCIsIGZhbHNlKTtcclxuICAgIG5hdGl2ZS5VdGlscy5sb2dNZXNzYWdlKFwiRmFpbCB0byBsb2FkIFdlYkNoYW5uZWxcIik7XHJcbn0pO1xyXG5cclxuZnVuY3Rpb24gYXR0YWNoRXZlbnRzVG9VSSgpIHtcclxuICAgIFxyXG59XHJcblxyXG5leHBvcnQgeyBuYXRpdmUgfTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc291cmNlcy9hcHAuanMiLCIvKiBCYXNlIE1lbnUgSmF2YVNjcmlwdCAqL1xyXG5pbXBvcnQge0pldFZpZXcsIHBsdWdpbnN9IGZyb20gXCJ3ZWJpeC1qZXRcIjtcclxuaW1wb3J0IGRyb3BEb3duTWVudXMgZnJvbSBcInZpZXdzL21lbnUvZHJvcERvd25NZW51c1wiO1xyXG5pbXBvcnQgaWNvbk1lbnUgZnJvbSBcInZpZXdzL21lbnUvaWNvbk1lbnVcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1lbnVWaWV3IGV4dGVuZHMgSmV0VmlldyB7XHJcbiAgICBjb25maWcoKSB7XHJcbiAgICAgICAgbGV0IG1lbnUgPSB7XHJcbiAgICAgICAgICAgIGlkOlwibWVudVwiLFxyXG4gICAgICAgICAgICByb3dzOlsgZHJvcERvd25NZW51cywgaWNvbk1lbnUgXSAgXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG1lbnU7XHJcbiAgICB9O1xyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc291cmNlcy92aWV3cy9tZW51L21lbnUuanMiLCIvKiBEcm9wIERvd24gTWVudXMgSmF2YVNjcmlwdCAqL1xyXG5pbXBvcnQge0pldFZpZXcsIHBsdWdpbnN9IGZyb20gXCJ3ZWJpeC1qZXRcIjtcclxuaW1wb3J0IHsgZmlsZU1lbnUsIGVkaXRNZW51LCB2aWV3TWVudSwgZHJhd01lbnUsIG1vZGlmeU1lbnUsIGhlbHBNZW51IH0gZnJvbSBcIm1vZGVscy9tZW51RGF0YVwiO1xyXG5pbXBvcnQge25hdGl2ZX0gZnJvbSBcImFwcFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRHJvcERvd25NZW51VmlldyBleHRlbmRzIEpldFZpZXcge1xyXG4gICAgY29uZmlnKCkge1xyXG4gICAgICAgIGxldCBkcm9wRG93bk1lbnUgPSB7XHJcbiAgICAgICAgICAgIGlkOlwiZHJvcERvd25NZW51Q29udGFpbmVyXCIsXHJcbiAgICAgICAgICAgIGNvbHM6W1xyXG4gICAgICAgICAgICAgICAgeyB3aWR0aDogMTAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDpcImRyb3BEb3duTWVudVwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpZXc6XCJtZW51XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVybGVzczp0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Ym1lbnVDb25maWc6e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDoyMDBcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6W1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJmaWxlTWVudVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJGaWxlXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VibWVudTpmaWxlTWVudVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJlZGl0TWVudVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJFZGl0XCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VibWVudTplZGl0TWVudVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJ2aWV3TWVudVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJWaWV3XCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VibWVudTp2aWV3TWVudVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJkcmF3TWVudVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJEcmF3XCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VibWVudTpkcmF3TWVudVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJtb2RpZnlNZW51XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTpcIk1vZGlmeVwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1Ym1lbnU6bW9kaWZ5TWVudVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJoZWxwTWVudVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJIZWxwXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VibWVudTpoZWxwTWVudVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICAgICBvbjp7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uTWVudUl0ZW1DbGljazpmdW5jdGlvbihpZCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3ZWJpeC5tZXNzYWdlKFwiQ2xpY2s6IFwiICsgaWQgKyB0aGlzLmdldE1lbnVJdGVtKGlkKS52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhpZCArIHRoaXMuZ2V0TWVudUl0ZW0oaWQpLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpZCA9PT0gXCJGaWxlLT5FeGl0XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYXRpdmUuVXRpbHMuc2hvd01lc3NhZ2UoXCJEZXNpZ25lclwiLCBcIlRoaXMgd2lsbCBleGl0IGFwcGxpY2F0aW9uXCIsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hdGl2ZS5VdGlscy5leGl0QXBwKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGlkID09IFwiRmlsZS0+T3BlblwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmF0aXZlLlV0aWxzLnNob3dNZXNzYWdlKFwiRGVzaWduZXJcIiwgXCJTdGFydCBUZXN0IFN1bVwiLCBmYWxzZSk7XHJcbi8qIFRlc3QgdG8gY2FsbCBwcm9taXNlIGZvciBuYXRpdmUgZnVuY3Rpb24gY2FsbC5cclxuICBTdGlsbCBub3Qgd29ya2luZyAqL1xyXG5sZXQgdGVzdENhbGxQcm9taXNlID0gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgICAgbmF0aXZlLlV0aWxzLnRlc3RDYWxsMygxMDAwMCwgZnVuY3Rpb24oc3VtKXtcclxuICAgICAgICAgIHJlc29sdmUoc3VtKTtcclxuICAgICAgfSk7XHJcbn0pO1xyXG5cclxuXHJcbnRlc3RDYWxsUHJvbWlzZS50aGVuKGZ1bmN0aW9uKHN1bSkge1xyXG4gICAgbmF0aXZlLlV0aWxzLnNob3dNZXNzYWdlKFwiRGVzaWduZXJcIiwgXCJUZXN0IFN1bTogXCIgKyBzdW0sIGZhbHNlKTtcclxufSwgZnVuY3Rpb24odmVyKSB7XHJcbiAgICBuYXRpdmUuVXRpbHMuc2hvd01lc3NhZ2UoXCJEZXNpZ25lclwiLCBcIkZhaWxlZCB0byBUZXN0IFN1bVwiLCBmYWxzZSk7XHJcbn0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qIERpcmVjdCBjYWxsYmFjayBmdW5jdGlvbiAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qIE5vdGUgdGhlIGNhbGxiYWNrIGZ1bmN0aW9uIGNhbGwgaXMgYmxvY2tpbmcuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVGh1cyBpZiB0aGUgZnVuY3Rpb24gdGFrZSBsb25nIHRpbWUsIHRoZW4gdGhlIHdob2xlIFVJIHdpbGwgYmxvY2sgZm9yIGxvbmcgdGltZSAqL1xyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmF0aXZlLlV0aWxzLnRlc3RDYWxsMygxMDAwMCwgZnVuY3Rpb24oc3VtKXtcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYXRpdmUuVXRpbHMuc2hvd01lc3NhZ2UoXCJEZXNpZ25lclwiLCBcIlRlc3QgU3VtIFJlc3VsdDpcIiArIHN1bSwgZmFsc2UpO1xyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWJzaWduOnRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgfSAgIFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gZHJvcERvd25NZW51O1xyXG4gICAgfTtcclxufVxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zb3VyY2VzL3ZpZXdzL21lbnUvZHJvcERvd25NZW51cy5qcyIsIi8qIEljb24gTWVudSBKYXZhU2NyaXB0ICovXHJcbmltcG9ydCB7SmV0VmlldywgcGx1Z2luc30gZnJvbSBcIndlYml4LWpldFwiO1xyXG5pbXBvcnQge25hdGl2ZX0gZnJvbSBcImFwcFwiO1xyXG4vKiBOT1RFOiBCdXR0b25zIGluIGhlcmUgYXJlIGZpeGVkIGFuZCBhcmUgbm90IGRlcGVuZGFudCBvbiB0aGUgYWN0aXZlIG1vZHVsZSAqL1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSWNvbk1lbnVWaWV3IGV4dGVuZHMgSmV0VmlldyB7XHJcbiAgICBjb25maWcoKSB7XHJcbiAgICAgICAgbGV0IGljb25NZW51ID0ge1xyXG4gICAgICAgICAgICBpZDpcImljb25NZW51Q29udGFpbmVyXCIsXHJcbiAgICAgICAgICAgIG1pbkhlaWdodDo0MCxcclxuICAgICAgICAgICAgbWF4SGVpZ2h0OjQwLFxyXG4gICAgICAgICAgICB0eXBlOlwiY2xlYW5cIixcclxuICAgICAgICAgICAgcm93czpbe1xyXG4gICAgICAgICAgICAgICAgaWQ6XCJpY29uTWVudVwiLFxyXG4gICAgICAgICAgICAgICAgdmlldzpcInRvb2xiYXJcIixcclxuICAgICAgICAgICAgICAgIGhlaWdodDo0MCxcclxuICAgICAgICAgICAgICAgIHR5cGU6XCJjbGVhblwiLFxyXG4gICAgICAgICAgICAgICAgZWxlbWVudHM6IFtcclxuICAgICAgICAgICAgICAgICAgICB7IHdpZHRoOiAxMCB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJvcGVuQnV0dG9uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpZXc6IFwiYnV0dG9uXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcImljb25cIiwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb246IFwiZm9sZGVyLW9wZW5cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDMyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjc3M6IFwiYXBwX2J1dHRvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGljazooaWQpID0+IHsgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYXRpdmUuVXRpbHMudGVzdGNhbGwyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYXRpdmUuVXRpbHMuc2hvd01lc3NhZ2UoXCJDYWxsIE5hdGl2ZVwiLCBcIk9wZW4gY2xpY2tlZDogXCIgKyBpZCwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJzYXZlQnV0dG9uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpZXc6IFwiYnV0dG9uXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcImljb25cIiwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb246IFwiZmxvcHB5XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3NzOiBcImFwcF9idXR0b25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6KGlkKSA9PiB7IHdlYml4Lm1lc3NhZ2UoaWQpOyB9IFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDpcImltcG9ydEJ1dHRvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2aWV3OiBcImJ1dHRvblwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJpY29uXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uOiBcImZpbGUtaW1wb3J0XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3NzOiBcImFwcF9idXR0b25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6KGlkKSA9PiB7IHdlYml4Lm1lc3NhZ2UoaWQpOyB9IFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDpcImV4cG9ydEJ1dHRvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2aWV3OiBcImJ1dHRvblwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJpY29uXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uOiBcImZpbGUtZXhwb3J0XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3NzOiBcImFwcF9idXR0b25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6KGlkKSA9PiB7IHdlYml4Lm1lc3NhZ2UoaWQpOyB9IFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgeyB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHsgd2lkdGg6IDEwIH1cclxuICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgfV1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gaWNvbk1lbnU7XHJcbiAgICB9O1xyXG59XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NvdXJjZXMvdmlld3MvbWVudS9pY29uTWVudS5qcyIsIi8qIEJhc2UgU2lkZWJhciBKYXZhU2NyaXB0ICovXHJcbmltcG9ydCB7SmV0VmlldywgcGx1Z2luc30gZnJvbSBcIndlYml4LWpldFwiO1xyXG5pbXBvcnQgcHJvamVjdEV4cGxvcmVyIGZyb20gXCJ2aWV3cy9zaWRlYmFyL3Byb2plY3RFeHBsb3JlclwiO1xyXG5pbXBvcnQgcHJvcGVydHlMaXN0IGZyb20gXCJ2aWV3cy9zaWRlYmFyL3Byb3BlcnR5TGlzdFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2lkZWJhclZpZXcgZXh0ZW5kcyBKZXRWaWV3IHtcclxuICAgIGNvbmZpZygpIHtcclxuICAgICAgICBsZXQgc2lkZWJhciA9IHtcclxuICAgICAgICAgICAgaWQ6IFwic2lkZWJhckNvbnRhaW5lclwiLFxyXG4gICAgICAgICAgICBtaW5XaWR0aDo1MCxcclxuICAgICAgICAgICAgd2lkdGg6MzAwLFxyXG4gICAgICAgICAgICByb3dzOlsgXHJcbiAgICAgICAgICAgICAgICBwcm9qZWN0RXhwbG9yZXIsIFxyXG4gICAgICAgICAgICAgICAgeyBcclxuICAgICAgICAgICAgICAgICAgICBpZDpcInNpZGViYXJIb3JSZXNpemVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgdmlldzpcInJlc2l6ZXJcIlxyXG4gICAgICAgICAgICAgICAgfSwgXHJcbiAgICAgICAgICAgICAgICBwcm9wZXJ0eUxpc3QgXHJcbiAgICAgICAgICAgIF0gIFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBzaWRlYmFyO1xyXG4gICAgfTtcclxufVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NvdXJjZXMvdmlld3Mvc2lkZWJhci9zaWRlYmFyLmpzIiwiLyogUHJvamVjdCBFeHBsb3JlciBKYXZhU2NyaXB0ICovXHJcbmltcG9ydCB7SmV0VmlldywgcGx1Z2luc30gZnJvbSBcIndlYml4LWpldFwiO1xyXG5pbXBvcnQge3Byb2plY3RTdG9yZX0gZnJvbSBcIm1vZGVscy9wcm9qZWN0U3RvcmVcIlxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUHJvamVjdEV4cGxvcmVyVmlldyBleHRlbmRzIEpldFZpZXcge1xyXG4gICAgY29uZmlnKCkge1xyXG4gICAgICAgIGxldCBwcm9qZWN0RXhwbG9yZXIgPSB7XHJcbiAgICAgICAgICAgIGlkOlwicHJvamVjdEV4cGxvcmVyQ29udGFpbmVyXCIsXHJcbiAgICAgICAgICAgIHJvd3M6W1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHZpZXc6XCJ0ZW1wbGF0ZVwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlOlwiUHJvamVjdCBFeHBsb3JlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6XCJoZWFkZXJcIixcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6NDAsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVybGVzczp0cnVlIFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBjb2xzOltcclxuICAgICAgICAgICAgICAgICAgICAgICAgeyB3aWR0aDo1IH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDpcInByb2plY3RFeHBsb3JlclNlYXJjaFwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpZXc6XCJ0ZXh0XCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJTZWFyY2hcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6XCJjbGVhblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OjMwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb246e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uRm9jdXM6KGlkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKGlkLmdldFZhbHVlKCkgPT0gXCJTZWFyY2hcIikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQuc2V0VmFsdWUoXCJcIik7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25CbHVyOihpZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihpZC5nZXRWYWx1ZSgpID09IFwiXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkLnNldFZhbHVlKFwiU2VhcmNoXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHdpZHRoOjUgfVxyXG4gICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7IGhlaWdodDogMTAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICB2aWV3Olwic2Nyb2xsdmlld1wiLCBcclxuICAgICAgICAgICAgICAgICAgICBpZDpcInByb2plY3RFeHBsb3JlclNjcm9sbHZpZXdcIiwgXHJcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsOlwieVwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGJvZHk6e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByb3dzOltcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDpcInByb2plY3RFeHBsb3JlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpZXc6XCJ0cmVlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTpcImxpbmVUcmVlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OjIwMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZHJhZzpmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOltdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlYWR5OmZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJChcInByb2plY3RFeHBsb3JlclwiKS5kYXRhLnN5bmMocHJvamVjdFN0b3JlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOntcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25JdGVtRGJsQ2xpY2s6KC4uLmFyZ3MpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKCQkKFwicHJvamVjdEV4cGxvcmVyXCIpLmdldEl0ZW0oYXJnc1swXSkuaXNGb2xkZXIgIT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzdGF0ZSA9ICQkKFwicHJvamVjdFN0b3JlXCIpLmdldEl0ZW0oYXJnc1swXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoc3RhdGUub3BlbmVkID09IGZhbHNlKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCQoXCJwcm9qZWN0U3RvcmVcIikudXBkYXRlSXRlbShhcmdzWzBdLHtvcGVuZWQ6dHJ1ZSwgaGlkZGVuOmZhbHNlfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHByb2plY3RFeHBsb3JlcjtcclxuICAgIH07XHJcbn1cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zb3VyY2VzL3ZpZXdzL3NpZGViYXIvcHJvamVjdEV4cGxvcmVyLmpzIiwiLyogUHJvcGVydHkgTGlzdCBKYXZhU2NyaXB0ICovXHJcbmltcG9ydCB7SmV0VmlldywgcGx1Z2luc30gZnJvbSBcIndlYml4LWpldFwiO1xyXG5pbXBvcnQge3Byb3BlcnR5U3RvcmV9IGZyb20gXCJtb2RlbHMvcHJvcGVydHlTdG9yZVwiXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQcm9wZXJ0eUxpc3RWaWV3IGV4dGVuZHMgSmV0VmlldyB7XHJcbiAgICBjb25maWcoKSB7XHJcbiAgICAgICAgbGV0IHByb3BlcnR5TGlzdCA9IHtcclxuICAgICAgICAgICAgaWQ6XCJwcm9wZXJ0eUxpc3RDb250YWludGVyXCIsXHJcbiAgICAgICAgICAgIHJvd3M6W1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHZpZXc6XCJ0ZW1wbGF0ZVwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlOlwiUHJvcGVydHkgTGlzdFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6XCJoZWFkZXJcIixcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6NDAsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVybGVzczp0cnVlIFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDpcInByb3BlcnR5TGlzdEZ1bmN0aW9uc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpZXc6XCJ0b29sYmFyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTpcImNsZWFuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OjMwLFxyXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgd2lkdGg6IDUgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJzb3J0QnV0dG9uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2aWV3OiBcImJ1dHRvblwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiaWNvblwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGljb246IFwic29ydC1hbW91bnQtYXNjXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMzIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjc3M6IFwiYXBwX2J1dHRvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6KCkgPT4geyB3ZWJpeC5tZXNzYWdlKFwiZGV0YWlsXCIpOyB9IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDpcInZpZXdGaWx0ZXJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpZXc6IFwidG9nZ2xlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcImljb25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6XCJ2aWV3RmlsdGVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvZmZJY29uOiBcInNvcnQtYW1vdW50LWFzY1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25JY29uOiBcInNvcnQtYW1vdW50LWRlc2NcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOntcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZToobmV3diwgb2xkdikgPT4ge3Byb3BlcnR5U3RvcmUuc2hvd0FsbChuZXd2KTt9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBcInByb3BlcnR5RmlsdGVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2aWV3OlwidGV4dFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb246e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uVGltZWRLZXlQcmVzczpmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wZXJ0eVN0b3JlLmZpbHRlckxhYmVsKHRoaXMuZ2V0VmFsdWUoKS50b0xvd2VyQ2FzZSgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgd2lkdGg6IDUgfVxyXG4gICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7IGhlaWdodDoxMCB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHZpZXc6XCJzY3JvbGx2aWV3XCIsIFxyXG4gICAgICAgICAgICAgICAgICAgIGlkOlwicHJvcGVydHlMaXN0U2Nyb2xsdmlld1wiLCBcclxuICAgICAgICAgICAgICAgICAgICBzY3JvbGw6XCJ5XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9keTp7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvd3M6W1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpZXc6XCJwcm9wZXJ0eVwiLCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJzZXRzXCIsICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDIwMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudHM6W10sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb246e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnb25BZnRlckVkaXRTdG9wJzogZnVuY3Rpb24oc3RhdGUsIGVkaXRvciwgaWdub3JlVXBkYXRlKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKCFpZ25vcmVVcGRhdGUgJiYgcHJvcGVydHlTdG9yZS5nZXRJbmRleEJ5SWQoZWRpdG9yLmlkKSAhPSAtMSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvcGVydHlTdG9yZS51cGRhdGVJdGVtKGVkaXRvci5pZCx7W2VkaXRvci5pZF06c3RhdGUudmFsdWV9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHByb3BlcnR5U3RvcmUuZGF0YS5hdHRhY2hFdmVudChcIm9uU3RvcmVVcGRhdGVkXCIsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICQkKCdzZXRzJykuZGVmaW5lKFwiZWxlbWVudHNcIiwgcHJvcGVydHlTdG9yZS5zZXJpYWxpemUoKSk7XHJcbiAgICAgICAgICAgICQkKCdzZXRzJykucmVmcmVzaCgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBwcm9wZXJ0eUxpc3Q7XHJcbiAgICB9O1xyXG59XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NvdXJjZXMvdmlld3Mvc2lkZWJhci9wcm9wZXJ0eUxpc3QuanMiLCIvKiBNb2R1bGUgSmF2YVNjcmlwdCAqL1xyXG4vKiBCYXNlIE1lbnUgSmF2YVNjcmlwdCAqL1xyXG5pbXBvcnQge0pldFZpZXcsIHBsdWdpbnN9IGZyb20gXCJ3ZWJpeC1qZXRcIjtcclxuaW1wb3J0IHRvb2xzIGZyb20gXCJ2aWV3cy9tb2R1bGUvbW9kdWxlVG9vbHNcIjtcclxuaW1wb3J0IHRhYmJhckNlbGwgZnJvbSBcInZpZXdzL21vZHVsZS9tb2R1bGVUYWJiYXJDZWxsXCI7XHJcbmltcG9ydCBjb21wb25lbnRzIGZyb20gXCJ2aWV3cy9tb2R1bGUvbW9kdWxlQ29tcG9uZW50c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTW9kdWxlVmlldyBleHRlbmRzIEpldFZpZXcge1xyXG4gICAgY29uZmlnKCkge1xyXG4gICAgICAgIGxldCBtb2R1bGUgPSB7XHJcbiAgICAgICAgICAgIGlkOlwibW9kdWxlXCIsXHJcbiAgICAgICAgICAgIHJvd3M6W1xyXG4gICAgICAgICAgICAgICAgdG9vbHMsIFxyXG4gICAgICAgICAgICAgICAgeyBcclxuICAgICAgICAgICAgICAgICAgICBjb2xzOlsgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYmJhckNlbGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd3M6W1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudHMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBoZWlnaHQ6NDAgfSBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIF0gXHJcbiAgICAgICAgICAgICAgICB9ICAgXHJcbiAgICAgICAgICAgIF0gIFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBtb2R1bGU7XHJcbiAgICB9O1xyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc291cmNlcy92aWV3cy9tb2R1bGUvbW9kdWxlLmpzIiwiLyogTW9kdWxlIFRvb2xzIEphdmFTY3JpcHQgKi9cclxuaW1wb3J0IHtKZXRWaWV3LCBwbHVnaW5zfSBmcm9tIFwid2ViaXgtamV0XCI7XHJcblxyXG4vKiBOT1RFOiBCdXR0b25zIGluIGhlcmUgYXJlIHNwZWNpZmljIHRvIHRoZSBtb2R1bGUgbG9hZGVkIGFuZCBzaG91bGQgY2hhbmdlIHVwb24gbW9kdWxlIGxvYWQgKi9cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1vZHVsZVRvb2xzVmlldyBleHRlbmRzIEpldFZpZXcge1xyXG4gICAgY29uZmlnKCkge1xyXG4gICAgICAgIGxldCBtb2R1bGVUb29scyA9IHtcclxuICAgICAgICAgICAgaWQ6XCJtb2R1bGVUb29sc0NvbnRhaW5lclwiLFxyXG4gICAgICAgICAgICBtaW5IZWlnaHQ6NDAsXHJcbiAgICAgICAgICAgIG1heEhlaWdodDo0MCxcclxuICAgICAgICAgICAgdHlwZTpcImNsZWFuXCIsXHJcbiAgICAgICAgICAgIHJvd3M6W3tcclxuICAgICAgICAgICAgICAgIGlkOlwibW9kdWxlVG9vbHNcIixcclxuICAgICAgICAgICAgICAgIHZpZXc6XCJ0b29sYmFyXCIsXHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6NDAsXHJcbiAgICAgICAgICAgICAgICB0eXBlOlwiY2xlYW5cIixcclxuICAgICAgICAgICAgICAgIGVsZW1lbnRzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgeyB3aWR0aDogMTAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOlwic2VsZWN0QnV0dG9uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpZXc6IFwiYnV0dG9uXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcImljb25cIiwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb246IFwiY3Vyc29yLWRlZmF1bHQtb3V0bGluZVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMzIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNzczogXCJhcHBfYnV0dG9uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOihpZCkgPT4geyB3ZWJpeC5tZXNzYWdlKGlkKTsgfSBcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJwYW5CdXR0b25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlldzogXCJidXR0b25cIiwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiaWNvblwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogXCJwYW5cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDMyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjc3M6IFwiYXBwX2J1dHRvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGljazooaWQpID0+IHsgd2ViaXgubWVzc2FnZShpZCk7IH0gXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOlwicm90YXRlQnV0dG9uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpZXc6IFwiYnV0dG9uXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcImljb25cIiwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb246IFwicm90YXRlLXdvcmxkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3NzOiBcImFwcF9idXR0b25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6KGlkKSA9PiB7IHdlYml4Lm1lc3NhZ2UoaWQpOyB9IFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDpcInpvb21JbkJ1dHRvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2aWV3OiBcImJ1dHRvblwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJpY29uXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uOiBcIm1hZ25pZnktcGx1cy1vdXRsaW5lXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3NzOiBcImFwcF9idXR0b25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6KGlkKSA9PiB7IHdlYml4Lm1lc3NhZ2UoaWQpOyB9IFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDpcInpvb21PdXRCdXR0b25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlldzogXCJidXR0b25cIiwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiaWNvblwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogXCJtYWduaWZ5LW1pbnVzLW91dGxpbmVcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDMyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjc3M6IFwiYXBwX2J1dHRvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGljazooaWQpID0+IHsgd2ViaXgubWVzc2FnZShpZCk7IH0gXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7IH0sXHJcbiAgICAgICAgICAgICAgICAgICAgeyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlldzogXCJidXR0b25cIiwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiaWNvblwiLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogXCJtZW51XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiA1MCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3NzOiBcImFwcF9idXR0b25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6KCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCQkKFwiY29tcG9uZW50c0hlYWRlclwiKS5pc1Zpc2libGUoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkKFwiY29tcG9uZW50c0hlYWRlclwiKS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkKFwiY29tcG9uZW50c0hlYWRlclwiKS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJChcImNvbXBvbmVudHNCYXJcIikudG9nZ2xlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJChcImNvbXBvbmVudHNTY3JvbGx2aWV3XCIpLnJlc2l6ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7IHdpZHRoOiAxMCB9XHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH1dXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG1vZHVsZVRvb2xzO1xyXG4gICAgfTtcclxufVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NvdXJjZXMvdmlld3MvbW9kdWxlL21vZHVsZVRvb2xzLmpzIiwiLyogTW9kdWxlIFRhYnZpZXcgSmF2YVNjcmlwdCAqL1xyXG5pbXBvcnQge0pldFZpZXcsIHBsdWdpbnN9IGZyb20gXCJ3ZWJpeC1qZXRcIjtcclxuaW1wb3J0IHNjaGVtYXRpYyBmcm9tIFwibW9kdWxlcy9zY2hlbWF0aWMvdmlld3Mvc2NoZW1hdGljXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNb2R1bGVUYWJiYXJDZWxsVmlldyBleHRlbmRzIEpldFZpZXcge1xyXG4gICAgY29uZmlnKCkge1xyXG4gICAgICAgIGxldCB0YWJ2aWV3ID0ge1xyXG4gICAgICAgICAgICBpZDpcIm1vZHVsZUNvbnRhaW5lclwiLFxyXG4gICAgICAgICAgICByb3dzOltcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDpcIm1vZHVsZUNlbGxzXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZTpmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBjZWxsczpbIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IGlkOlwic3RhcnRzY3JlZW5cIiwgdmlldzpcInRlbXBsYXRlXCIsIHRlbXBsYXRlOlwiU3RhcnQgU2NyZWVuXCIgfVxyXG4gICAgICAgICAgICAgICAgICAgIF0sXHJcblxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICB2aWV3OlwidGFiYmFyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6XCJtb2R1bGVUYWJiYXJcIixcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbEFsaWduOlwicmlnaHRcIixcclxuICAgICAgICAgICAgICAgICAgICB0YWJPZmZzZXQ6NSxcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6NDAsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xpY2s6KC4uLmFyZ3MpID0+IHsgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICggYXJnc1sxXS50YXJnZXQuY2xhc3NOYW1lID09PSBcIndlYml4X3RhYl9jbG9zZSB3ZWJpeF9pY29uIGZhLXRpbWVzXCIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvKiBUaGlzIGlzIHRvIGNoZWNrIGlmIHRoZSBjbG9zZSB0YWIgYnV0dG9uIGhhcyBiZWVuIGNsaWNrZWQgKi9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hvd01vZHVsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgb246e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkJlZm9yZVRhYkNsb3NlOihpZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCQoXCJtb2R1bGVDZWxsc1wiKS5yZW1vdmVWaWV3KCQkKFwibW9kdWxlVGFiYmFyXCIpLmdldFZhbHVlKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCQoXCJwcm9qZWN0U3RvcmVcIikudXBkYXRlSXRlbShpZCx7b3BlbmVkOmZhbHNlLCBoaWRkZW46dHJ1ZX0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBvcHRpb25zOlsgeyBpZDpcInN0YXJ0c2NyZWVuXCIsIHZhbHVlOlwiU3RhcnRcIiwgd2lkdGg6MTUwIH0gXSwgXHJcbiAgICAgICAgICAgICAgICAgICAgbXVsdGl2aWV3OlwidHJ1ZVwiLCBcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOlwiYm90dG9tXCJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgJCQoXCJwcm9qZWN0U3RvcmVcIikuYXR0YWNoRXZlbnQoXCJvbkRhdGFVcGRhdGVcIiwgZnVuY3Rpb24oaWQsIGRhdGEpe1xyXG4gICAgICAgICAgICBpZihkYXRhLm9wZW5lZCAmJiBkYXRhLm1vZHVsZSl7XHJcbiAgICAgICAgICAgICAgICBsZXQgbWF4TGVuZ3RoID0gODtcclxuICAgICAgICAgICAgICAgIGlmKGRhdGEubW9kdWxlKSBtYXhMZW5ndGggPSA0O1xyXG4gICAgICAgICAgICAgICAgaWYoJCQoXCJtb2R1bGVUYWJiYXJcIikuY29uZmlnLm9wdGlvbnMubGVuZ3RoID4gbWF4TGVuZ3RoKXtcclxuICAgICAgICAgICAgICAgICAgICB3ZWJpeC5tZXNzYWdlKFwiTWF4IG9wZW4gdGFiIHJlYWNoZWRcIik7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAkJChcIm1vZHVsZUNlbGxzXCIpLmFkZFZpZXcoe2lkOmlkLCB2aWV3OmRhdGEubW9kdWxlfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgJCQoXCJtb2R1bGVUYWJiYXJcIikuYWRkT3B0aW9uKHtpZDppZCwgdmFsdWU6ZGF0YS52YWx1ZSwgY2xvc2U6dHJ1ZSwgaWNvbjpcImZpbGVcIiwgd2lkdGg6IDIwMH0sIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHRhYnZpZXc7IFxyXG4gICAgfTtcclxuXHJcbiAgICAvKiBNZXRob2Qgc3BhY2UgZm9yIGxvYWRpbmcgU2NoZW1hdGljIEVkaXRvciBvciBEYXNoYm9hcmQgZGVzaWduZXIgb3IgYW55IG90aGVyIG1vZHVsZSAqL1xyXG4gICAgLyogbmVlZGVkIGZvciB3aGVuIHN3aXRjaGluZyBiZXR3ZWVuIHRhYnMsIHNob3VsZCB1bmxvYWQgY3VycmVudCBtb2R1bGUgYW5kIGxvYWQgbmV3IG1vZHVsZSAqL1xyXG4gICAgc2hvd01vZHVsZSgpIHtcclxuICAgICAgICBsZXQgdGFiSUQgPSAkJChcIm1vZHVsZVRhYmJhclwiKS5nZXRWYWx1ZSgpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRhYklEICsgXCIgfiBzaG93TW9kdWxlIGNvZGUgZ29lcyBoZXJlXCIpO1xyXG4gICAgfTtcclxufVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NvdXJjZXMvdmlld3MvbW9kdWxlL21vZHVsZVRhYmJhckNlbGwuanMiLCIvKiBNb2R1bGUgQ29tcG9uZW50cyBKYXZhU2NyaXB0ICovXHJcbmltcG9ydCB7SmV0VmlldywgcGx1Z2luc30gZnJvbSBcIndlYml4LWpldFwiO1xyXG5pbXBvcnQgZGF0YSBmcm9tIFwibW9kZWxzL2NvbXBvbmVudERhdGFcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1vZHVsZUNvbXBvbmVudHNWaWV3IGV4dGVuZHMgSmV0VmlldyB7XHJcbiAgICBjb25maWcoKSB7XHJcbiAgICAgICAgbGV0IGNvbXBvbmVudHMgPSB7XHJcbiAgICAgICAgICAgIHZpZXc6XCJzY3JvbGx2aWV3XCIsIFxyXG4gICAgICAgICAgICBpZDpcImNvbXBvbmVudHNTY3JvbGx2aWV3XCIsIFxyXG4gICAgICAgICAgICBzY3JvbGw6XCJ5XCIsXHJcbiAgICAgICAgICAgIGJvZHk6e1xyXG4gICAgICAgICAgICAgICAgcm93czpbXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDpcImNvbXBvbmVudHNIZWFkZXJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlldzpcInRlbXBsYXRlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlOlwiQ29tcG9uZW50c1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOlwiaGVhZGVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDo0MCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVybGVzczp0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoaWRkZW46dHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDpcImNvbXBvbmVudHNCYXJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlldzogXCJzaWRlYmFyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOlwicmlnaHRcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAyMDAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xsYXBzZWQ6dHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb246e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25BZnRlclNlbGVjdDogZnVuY3Rpb24oaWQpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qIEhlcmUgaXMgd2hlcmUgeW91IGltcGxlbWVudCB0aGUgZ2FkZ2V0IHNlbGVjdGlvbiBjYWxsIGluIHRoZSBUaHJlZS5qcyAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdlYml4Lm1lc3NhZ2UoXCJTZWxlY3RlZDogXCIgKyB0aGlzLmdldEl0ZW0oaWQpLnZhbHVlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGNvbXBvbmVudHM7XHJcbiAgICB9O1xyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc291cmNlcy92aWV3cy9tb2R1bGUvbW9kdWxlQ29tcG9uZW50cy5qcyIsImZ1bmN0aW9uIGxvY2F0ZSh1aSwgaWQpIHtcbiAgICBjb25zdCBjZWxscyA9IHVpLmdldENoaWxkVmlld3MoKTtcbiAgICBmb3IgKGNvbnN0IGNlbGwgb2YgY2VsbHMpIHtcbiAgICAgICAgbGV0IGtpZCA9IGNlbGw7XG4gICAgICAgIGlmIChraWQuY29uZmlnLmxvY2FsSWQgPT09IGlkKSB7XG4gICAgICAgICAgICByZXR1cm4ga2lkO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAga2lkID0gbG9jYXRlKGtpZCwgaWQpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChraWQpIHtcbiAgICAgICAgICAgIHJldHVybiBraWQ7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG59XG5leHBvcnQgY2xhc3MgSmV0QmFzZSB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMuX2lkID0gd2ViaXgudWlkKCk7XG4gICAgICAgIHRoaXMuX2V2ZW50cyA9IFtdO1xuICAgICAgICB0aGlzLl9zdWJzID0ge307XG4gICAgfVxuICAgIGdldFJvb3QoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9yb290O1xuICAgIH1cbiAgICBkZXN0cnVjdG9yKCkge1xuICAgICAgICBjb25zdCBldmVudHMgPSB0aGlzLl9ldmVudHM7XG4gICAgICAgIGZvciAobGV0IGkgPSBldmVudHMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgICAgIGV2ZW50c1tpXS5vYmouZGV0YWNoRXZlbnQoZXZlbnRzW2ldLmlkKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBkZXN0cm95IHN1YiB2aWV3c1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiB0aGlzLl9zdWJzKSB7XG4gICAgICAgICAgICBjb25zdCBzdWJWaWV3ID0gdGhpcy5fc3Vic1trZXldLnZpZXc7XG4gICAgICAgICAgICAvLyBpdCBwb3NzaWJsZSB0aGF0IHN1YnZpZXcgd2FzIG5vdCBsb2FkZWQgd2l0aCBhbnkgY29udGVudCB5ZXRcbiAgICAgICAgICAgIC8vIHNvIGNoZWNrIG9uIG51bGxcbiAgICAgICAgICAgIGlmIChzdWJWaWV3KSB7XG4gICAgICAgICAgICAgICAgc3ViVmlldy5kZXN0cnVjdG9yKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5fZXZlbnRzID0gdGhpcy5fY29udGFpbmVyID0gdGhpcy5fYXBwID0gdGhpcy5fcGFyZW50ID0gbnVsbDtcbiAgICB9XG4gICAgcmVuZGVyKHJvb3QsIHVybCwgcGFyZW50KSB7XG4gICAgICAgIHRoaXMuX3BhcmVudCA9IHBhcmVudDtcbiAgICAgICAgaWYgKHVybCkge1xuICAgICAgICAgICAgdGhpcy5faW5kZXggPSB1cmxbMF0uaW5kZXg7XG4gICAgICAgIH1cbiAgICAgICAgcm9vdCA9IHJvb3QgfHwgZG9jdW1lbnQuYm9keTtcbiAgICAgICAgY29uc3QgX2NvbnRhaW5lciA9ICh0eXBlb2Ygcm9vdCA9PT0gXCJzdHJpbmdcIikgPyB3ZWJpeC50b05vZGUocm9vdCkgOiByb290O1xuICAgICAgICBpZiAodGhpcy5fY29udGFpbmVyICE9PSBfY29udGFpbmVyKSB7XG4gICAgICAgICAgICB0aGlzLl9jb250YWluZXIgPSBfY29udGFpbmVyO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX3JlbmRlcih1cmwpLnRoZW4oKCkgPT4gdGhpcy5nZXRSb290KCkpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX3VybENoYW5nZSh1cmwpLnRoZW4oKCkgPT4gdGhpcy5nZXRSb290KCkpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGdldEluZGV4KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faW5kZXg7XG4gICAgfVxuICAgIGdldElkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faWQ7XG4gICAgfVxuICAgIGdldFBhcmVudFZpZXcoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9wYXJlbnQ7XG4gICAgfVxuICAgICQkKGlkKSB7XG4gICAgICAgIGxldCB2aWV3ID0gd2ViaXguJCQoaWQpO1xuICAgICAgICBpZiAoIXZpZXcpIHtcbiAgICAgICAgICAgIHZpZXcgPSBsb2NhdGUodGhpcy5nZXRSb290KCksIGlkKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdmlldztcbiAgICB9XG4gICAgb24ob2JqLCBuYW1lLCBjb2RlKSB7XG4gICAgICAgIGNvbnN0IGlkID0gb2JqLmF0dGFjaEV2ZW50KG5hbWUsIGNvZGUpO1xuICAgICAgICB0aGlzLl9ldmVudHMucHVzaCh7IG9iaiwgaWQgfSk7XG4gICAgICAgIHJldHVybiBpZDtcbiAgICB9XG4gICAgY29udGFpbnModmlldykge1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiB0aGlzLl9zdWJzKSB7XG4gICAgICAgICAgICBjb25zdCBraWQgPSB0aGlzLl9zdWJzW2tleV0udmlldztcbiAgICAgICAgICAgIGlmIChraWQgPT09IHZpZXcgfHwga2lkLmNvbnRhaW5zKHZpZXcpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBnZXRTdWJWaWV3KG5hbWUpIHtcbiAgICAgICAgY29uc3Qgc3ViID0gdGhpcy5fc3Vic1tuYW1lIHx8IFwiZGVmYXVsdFwiXTtcbiAgICAgICAgaWYgKHN1Yikge1xuICAgICAgICAgICAgcmV0dXJuIHsgc3Vidmlldzogc3ViLCBwYXJlbnQ6IHRoaXMgfTtcbiAgICAgICAgfVxuICAgICAgICAvLyB3aGVuIGNhbGxlZCBmcm9tIGEgY2hpbGQgdmlldywgc2VhcmNoZXMgZm9yIG5lYXJlc3QgcGFyZW50IHdpdGggc3Vidmlld1xuICAgICAgICBpZiAodGhpcy5fcGFyZW50KSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fcGFyZW50LmdldFN1YlZpZXcobmFtZSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIGdldE5hbWUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9uYW1lO1xuICAgIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL25vZGVfbW9kdWxlcy93ZWJpeC1qZXQvSmV0QmFzZS5qcyIsImV4cG9ydCBmdW5jdGlvbiBkaWZmKG9VcmwsIG5VcmwpIHtcbiAgICBsZXQgaSA9IDA7XG4gICAgZm9yIChpOyBpIDwgblVybC5sZW5ndGg7IGkrKykge1xuICAgICAgICBjb25zdCBsZWZ0ID0gb1VybFtpXTtcbiAgICAgICAgY29uc3QgcmlnaHQgPSBuVXJsW2ldO1xuICAgICAgICBpZiAoIWxlZnQpIHtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGlmIChsZWZ0LnBhZ2UgIT09IHJpZ2h0LnBhZ2UpIHtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGZvciAoY29uc3Qga2V5IGluIGxlZnQucGFyYW1zKSB7XG4gICAgICAgICAgICBpZiAobGVmdC5wYXJhbXNba2V5XSAhPT0gcmlnaHQucGFyYW1zW2tleV0pIHtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gaTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBwYXJzZSh1cmwpIHtcbiAgICAvLyByZW1vdmUgc3RhcnRpbmcgL1xuICAgIGlmICh1cmxbMF0gPT09IFwiL1wiKSB7XG4gICAgICAgIHVybCA9IHVybC5zdWJzdHIoMSk7XG4gICAgfVxuICAgIC8vIHNwbGl0IHVybCBieSBcIi9cIlxuICAgIGNvbnN0IHBhcnRzID0gdXJsLnNwbGl0KFwiL1wiKTtcbiAgICBjb25zdCBjaHVua3MgPSBbXTtcbiAgICAvLyBmb3IgZWFjaCBwYWdlIGluIHVybFxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcGFydHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29uc3QgdGVzdCA9IHBhcnRzW2ldO1xuICAgICAgICBjb25zdCByZXN1bHQgPSB7fTtcbiAgICAgICAgLy8gZGV0ZWN0IHBhcmFtc1xuICAgICAgICAvLyBzdXBwb3J0IG9sZCBcdFx0XHRzb21lOmE9YjpjPWRcbiAgICAgICAgLy8gYW5kIG5ldyBub3RhdGlvblx0XHRzb21lP2E9YiZjPWRcbiAgICAgICAgbGV0IHBvcyA9IHRlc3QuaW5kZXhPZihcIjpcIik7XG4gICAgICAgIGlmIChwb3MgPT09IC0xKSB7XG4gICAgICAgICAgICBwb3MgPSB0ZXN0LmluZGV4T2YoXCI/XCIpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChwb3MgIT09IC0xKSB7XG4gICAgICAgICAgICBjb25zdCBwYXJhbXMgPSB0ZXN0LnN1YnN0cihwb3MgKyAxKS5zcGxpdCgvW1xcOlxcP1xcJl0vZyk7XG4gICAgICAgICAgICAvLyBjcmVhdGUgaGFzaCBvZiBuYW1lZCBwYXJhbXNcbiAgICAgICAgICAgIGZvciAoY29uc3QgcGFyYW0gb2YgcGFyYW1zKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgZGNodW5rID0gcGFyYW0uc3BsaXQoXCI9XCIpO1xuICAgICAgICAgICAgICAgIHJlc3VsdFtkY2h1bmtbMF1dID0gZGNodW5rWzFdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC8vIHN0b3JlIHBhcnNlZCB2YWx1ZXNcbiAgICAgICAgY2h1bmtzW2ldID0ge1xuICAgICAgICAgICAgcGFnZTogKHBvcyA+IC0xID8gdGVzdC5zdWJzdHIoMCwgcG9zKSA6IHRlc3QpLFxuICAgICAgICAgICAgcGFyYW1zOiByZXN1bHQsIGluZGV4OiBpICsgMVxuICAgICAgICB9O1xuICAgIH1cbiAgICAvLyByZXR1cm4gYXJyYXkgb2YgcGFnZSBvYmplY3RzXG4gICAgcmV0dXJuIGNodW5rcztcbn1cbmV4cG9ydCBmdW5jdGlvbiB1cmwyc3RyKHN0YWNrKSB7XG4gICAgY29uc3QgdXJsID0gW107XG4gICAgZm9yIChjb25zdCBjaHVuayBvZiBzdGFjaykge1xuICAgICAgICB1cmwucHVzaChcIi9cIiArIGNodW5rLnBhZ2UpO1xuICAgICAgICBjb25zdCBwYXJhbXMgPSBvYmoyc3RyKGNodW5rLnBhcmFtcyk7XG4gICAgICAgIGlmIChwYXJhbXMpIHtcbiAgICAgICAgICAgIHVybC5wdXNoKFwiOlwiICsgcGFyYW1zKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdXJsLmpvaW4oXCJcIik7XG59XG5mdW5jdGlvbiBvYmoyc3RyKG9iaikge1xuICAgIGNvbnN0IHN0ciA9IFtdO1xuICAgIGZvciAoY29uc3Qga2V5IGluIG9iaikge1xuICAgICAgICBpZiAoc3RyLmxlbmd0aCkge1xuICAgICAgICAgICAgc3RyLnB1c2goXCI6XCIpO1xuICAgICAgICB9XG4gICAgICAgIHN0ci5wdXNoKGtleSArIFwiPVwiICsgb2JqW2tleV0pO1xuICAgIH1cbiAgICByZXR1cm4gc3RyLmpvaW4oXCJcIik7XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L2hlbHBlcnMuanMiLCJpbXBvcnQgcm91dGllIGZyb20gXCJ3ZWJpeC1yb3V0aWUvbGliL3JvdXRpZVwiO1xuZXhwb3J0IGNsYXNzIEhhc2hSb3V0ZXIge1xuICAgIGNvbnN0cnVjdG9yKGNiLCBjb25maWcpIHtcbiAgICAgICAgdGhpcy5jb25maWcgPSBjb25maWcgfHwge307XG4gICAgICAgIGxldCByY2IgPSBmdW5jdGlvbiAoXyRhKSB7IH07XG4gICAgICAgIHJvdXRpZShcIiEqXCIsIHVybCA9PiB7XG4gICAgICAgICAgICB0aGlzLl9sYXN0VXJsID0gXCJcIjtcbiAgICAgICAgICAgIHJldHVybiByY2IodGhpcy5nZXQoKSk7XG4gICAgICAgIH0pO1xuICAgICAgICByY2IgPSBjYjtcbiAgICB9XG4gICAgc2V0KHBhdGgsIGNvbmZpZykge1xuICAgICAgICBpZiAodGhpcy5jb25maWcucm91dGVzKSB7XG4gICAgICAgICAgICBjb25zdCBjb21wYXJlID0gcGF0aC5zcGxpdChcIj9cIiwgMik7XG4gICAgICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiB0aGlzLmNvbmZpZy5yb3V0ZXMpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5jb25maWcucm91dGVzW2tleV0gPT09IGNvbXBhcmVbMF0pIHtcbiAgICAgICAgICAgICAgICAgICAgcGF0aCA9IGtleSArIChjb21wYXJlLmxlbmd0aCA+IDEgPyBcIj9cIiArIGNvbXBhcmVbMV0gOiBcIlwiKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2xhc3RVcmwgPSBwYXRoO1xuICAgICAgICByb3V0aWUubmF2aWdhdGUoXCIhXCIgKyBwYXRoLCBjb25maWcpO1xuICAgIH1cbiAgICBnZXQoKSB7XG4gICAgICAgIGxldCBwYXRoID0gdGhpcy5fbGFzdFVybCB8fCAod2luZG93LmxvY2F0aW9uLmhhc2ggfHwgXCJcIikucmVwbGFjZShcIiMhXCIsIFwiXCIpO1xuICAgICAgICBpZiAodGhpcy5jb25maWcucm91dGVzKSB7XG4gICAgICAgICAgICBjb25zdCBjb21wYXJlID0gcGF0aC5zcGxpdChcIj9cIiwgMik7XG4gICAgICAgICAgICBjb25zdCBrZXkgPSB0aGlzLmNvbmZpZy5yb3V0ZXNbY29tcGFyZVswXV07XG4gICAgICAgICAgICBpZiAoa2V5KSB7XG4gICAgICAgICAgICAgICAgcGF0aCA9IGtleSArIChjb21wYXJlLmxlbmd0aCA+IDEgPyBcIj9cIiArIGNvbXBhcmVbMV0gOiBcIlwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcGF0aDtcbiAgICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3JvdXRlcnMvSGFzaFJvdXRlci5qcyIsIi8qIExheW91dCBKYXZhU2NyaXB0ICovXHJcbmltcG9ydCB7SmV0VmlldywgcGx1Z2luc30gZnJvbSBcIndlYml4LWpldFwiO1xyXG5pbXBvcnQgbWVudSBmcm9tIFwidmlld3MvbWVudS9tZW51XCI7XHJcbmltcG9ydCBzaWRlYmFyIGZyb20gXCJ2aWV3cy9zaWRlYmFyL3NpZGViYXJcIjtcclxuaW1wb3J0IG1vZHVsZSBmcm9tIFwidmlld3MvbW9kdWxlL21vZHVsZVwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgV2lsbG93THlueERlc2lnbmVyIGV4dGVuZHMgSmV0VmlldyB7XHJcbiAgICBjb25maWcoKSB7XHJcbiAgICAgICAgbGV0IHVpID0ge1xyXG4gICAgICAgICAgICBpZDpcImxheW91dFwiLFxyXG4gICAgICAgICAgICByb3dzOlsgXHJcbiAgICAgICAgICAgICAgICBtZW51LCBcclxuICAgICAgICAgICAgICAgIHsgXHJcbiAgICAgICAgICAgICAgICAgICAgY29sczpbIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaWRlYmFyLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgeyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOlwic2lkZXJiYXJWZXJSZXNpemVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2aWV3OiBcInJlc2l6ZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kdWxlIFxyXG4gICAgICAgICAgICAgICAgICAgIF0gXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB1aTtcclxuICAgIH1cclxufVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NvdXJjZXMvdmlld3MvbGF5b3V0LmpzIiwiLyogRmlsZSBNZW51IEphdmFTY3JpcHQgKi9cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zb3VyY2VzL3ZpZXdzL21lbnUvZmlsZU1lbnUuanMiLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc291cmNlcy9zdHlsZXMvYXBwLmNzc1xuLy8gbW9kdWxlIGlkID0gMTlcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiaW1wb3J0IHsgSmV0QmFzZSB9IGZyb20gXCIuL0pldEJhc2VcIjtcbmltcG9ydCB7IEpldFZpZXcgfSBmcm9tIFwiLi9KZXRWaWV3XCI7XG5pbXBvcnQgeyBKZXRWaWV3TGVnYWN5IH0gZnJvbSBcIi4vSmV0Vmlld0xlZ2FjeVwiO1xuaW1wb3J0IHsgSmV0Vmlld1JhdyB9IGZyb20gXCIuL0pldFZpZXdSYXdcIjtcbmltcG9ydCB7IEhhc2hSb3V0ZXIgfSBmcm9tIFwiLi9yb3V0ZXJzL0hhc2hSb3V0ZXJcIjtcbmltcG9ydCB7IHBhcnNlLCB1cmwyc3RyIH0gZnJvbSBcIi4vaGVscGVyc1wiO1xuaW1wb3J0IFwiLi9wYXRjaFwiO1xuZXhwb3J0IGNsYXNzIEpldEFwcCBleHRlbmRzIEpldEJhc2Uge1xuICAgIGNvbnN0cnVjdG9yKGNvbmZpZykge1xuICAgICAgICBzdXBlcigpO1xuICAgICAgICB0aGlzLndlYml4ID0gY29uZmlnLndlYml4IHx8IHdlYml4O1xuICAgICAgICAvLyBpbml0IGNvbmZpZ1xuICAgICAgICB0aGlzLmNvbmZpZyA9IHRoaXMud2ViaXguZXh0ZW5kKHtcbiAgICAgICAgICAgIG5hbWU6IFwiQXBwXCIsXG4gICAgICAgICAgICB2ZXJzaW9uOiBcIjEuMFwiLFxuICAgICAgICAgICAgc3RhcnQ6IFwiL2hvbWVcIlxuICAgICAgICB9LCBjb25maWcsIHRydWUpO1xuICAgICAgICB0aGlzLl9uYW1lID0gdGhpcy5jb25maWcubmFtZTtcbiAgICAgICAgdGhpcy5fc2VydmljZXMgPSB7fTtcbiAgICAgICAgd2ViaXguZXh0ZW5kKHRoaXMsIHdlYml4LkV2ZW50U3lzdGVtKTtcbiAgICAgICAgd2ViaXguYXR0YWNoRXZlbnQoXCJvbkNsaWNrXCIsIGUgPT4gdGhpcy5jbGlja0hhbmRsZXIoZSkpO1xuICAgIH1cbiAgICBnZXRTZXJ2aWNlKG5hbWUpIHtcbiAgICAgICAgbGV0IG9iaiA9IHRoaXMuX3NlcnZpY2VzW25hbWVdO1xuICAgICAgICBpZiAodHlwZW9mIG9iaiA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICBvYmogPSB0aGlzLl9zZXJ2aWNlc1tuYW1lXSA9IG9iaih0aGlzKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gb2JqO1xuICAgIH1cbiAgICBzZXRTZXJ2aWNlKG5hbWUsIGhhbmRsZXIpIHtcbiAgICAgICAgdGhpcy5fc2VydmljZXNbbmFtZV0gPSBoYW5kbGVyO1xuICAgIH1cbiAgICAvLyBjb3B5IG9iamVjdCBhbmQgY29sbGVjdCBleHRyYSBoYW5kbGVyc1xuICAgIGNvcHlDb25maWcob2JqLCB0YXJnZXQsIGNvbmZpZykge1xuICAgICAgICAvLyByYXcgdWkgY29uZmlnXG4gICAgICAgIGlmIChvYmouJHVpKSB7XG4gICAgICAgICAgICBvYmogPSB7ICRzdWJ2aWV3OiBuZXcgSmV0Vmlld0xlZ2FjeSh0aGlzLCBcIlwiLCBvYmopIH07XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAob2JqIGluc3RhbmNlb2YgSmV0QXBwKSB7XG4gICAgICAgICAgICBvYmogPSB7ICRzdWJ2aWV3OiBvYmogfTtcbiAgICAgICAgfVxuICAgICAgICAvLyBzdWJ2aWV3IHBsYWNlaG9sZGVyXG4gICAgICAgIGlmIChvYmouJHN1YnZpZXcpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFkZFN1YlZpZXcob2JqLCB0YXJnZXQsIGNvbmZpZyk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gcHJvY2VzcyBzdWItcHJvcGVydGllc1xuICAgICAgICB0YXJnZXQgPSB0YXJnZXQgfHwgKG9iaiBpbnN0YW5jZW9mIEFycmF5ID8gW10gOiB7fSk7XG4gICAgICAgIGZvciAoY29uc3QgbWV0aG9kIGluIG9iaikge1xuICAgICAgICAgICAgbGV0IHBvaW50ID0gb2JqW21ldGhvZF07XG4gICAgICAgICAgICAvLyB2aWV3IGNsYXNzXG4gICAgICAgICAgICBpZiAodHlwZW9mIHBvaW50ID09PSBcImZ1bmN0aW9uXCIgJiZcbiAgICAgICAgICAgICAgICBwb2ludC5wcm90b3R5cGUgJiYgcG9pbnQucHJvdG90eXBlLmNvbmZpZykge1xuICAgICAgICAgICAgICAgIHBvaW50ID0geyAkc3VidmlldzogcG9pbnQgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChwb2ludCAmJiB0eXBlb2YgcG9pbnQgPT09IFwib2JqZWN0XCIgJiZcbiAgICAgICAgICAgICAgICAhKHBvaW50IGluc3RhbmNlb2Ygd2ViaXguRGF0YUNvbGxlY3Rpb24pKSB7XG4gICAgICAgICAgICAgICAgaWYgKHBvaW50IGluc3RhbmNlb2YgRGF0ZSkge1xuICAgICAgICAgICAgICAgICAgICB0YXJnZXRbbWV0aG9kXSA9IG5ldyBEYXRlKHBvaW50KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRhcmdldFttZXRob2RdID0gdGhpcy5jb3B5Q29uZmlnKHBvaW50LCAocG9pbnQgaW5zdGFuY2VvZiBBcnJheSA/IFtdIDoge30pLCBjb25maWcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHRhcmdldFttZXRob2RdID0gcG9pbnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRhcmdldDtcbiAgICB9XG4gICAgZ2V0Um91dGVyKCkge1xuICAgICAgICByZXR1cm4gdGhpcy4kcm91dGVyO1xuICAgIH1cbiAgICBjbGlja0hhbmRsZXIoZSkge1xuICAgICAgICBpZiAoZSkge1xuICAgICAgICAgICAgY29uc3QgdGFyZ2V0ID0gKGUudGFyZ2V0IHx8IGUuc3JjRWxlbWVudCk7XG4gICAgICAgICAgICBpZiAodGFyZ2V0ICYmIHRhcmdldC5nZXRBdHRyaWJ1dGUpIHtcbiAgICAgICAgICAgICAgICBjb25zdCB0cmlnZ2VyID0gdGFyZ2V0LmdldEF0dHJpYnV0ZShcInRyaWdnZXJcIik7XG4gICAgICAgICAgICAgICAgaWYgKHRyaWdnZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlnZ2VyKHRyaWdnZXIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjb25zdCByb3V0ZSA9IHRhcmdldC5nZXRBdHRyaWJ1dGUoXCJyb3V0ZVwiKTtcbiAgICAgICAgICAgICAgICBpZiAocm91dGUpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaG93KHJvdXRlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmVmcmVzaCgpIHtcbiAgICAgICAgY29uc3QgdGVtcCA9IHRoaXMuX2NvbnRhaW5lcjtcbiAgICAgICAgdGhpcy5fdmlldy5kZXN0cnVjdG9yKCk7XG4gICAgICAgIHRoaXMuX3ZpZXcgPSB0aGlzLl9jb250YWluZXIgPSBudWxsO1xuICAgICAgICB0aGlzLnJlbmRlcih0ZW1wLCBwYXJzZSh0aGlzLmdldFJvdXRlcigpLmdldCgpKSwgdGhpcy5fcGFyZW50KTtcbiAgICB9XG4gICAgbG9hZFZpZXcodXJsKSB7XG4gICAgICAgIGNvbnN0IHZpZXdzID0gdGhpcy5jb25maWcudmlld3M7XG4gICAgICAgIGxldCByZXN1bHQgPSBudWxsO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgaWYgKHZpZXdzKSB7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB2aWV3cyA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGN1c3RvbSBsb2FkaW5nIHN0cmF0ZWd5XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHZpZXdzKHVybCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAvLyBwcmVkZWZpbmVkIGhhc2hcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gdmlld3NbdXJsXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQgPT09IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgdXJsID0gcmVzdWx0O1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBudWxsO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICghcmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgdXJsID0gdXJsLnJlcGxhY2UoL1xcLi9nLCBcIi9cIik7XG4gICAgICAgICAgICAgICAgbGV0IHZpZXcgPSByZXF1aXJlKFwiamV0LXZpZXdzL1wiICsgdXJsKTtcbiAgICAgICAgICAgICAgICBpZiAodmlldy5fX2VzTW9kdWxlKSB7XG4gICAgICAgICAgICAgICAgICAgIHZpZXcgPSB2aWV3LmRlZmF1bHQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHZpZXc7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMuX2xvYWRFcnJvcih1cmwsIGUpO1xuICAgICAgICB9XG4gICAgICAgIC8vIGN1c3RvbSBoYW5kbGVyIGNhbiByZXR1cm4gdmlldyBvciBpdHMgcHJvbWlzZVxuICAgICAgICBpZiAoIXJlc3VsdC50aGVuKSB7XG4gICAgICAgICAgICByZXN1bHQgPSBQcm9taXNlLnJlc29sdmUocmVzdWx0KTtcbiAgICAgICAgfVxuICAgICAgICAvLyBzZXQgZXJyb3IgaGFuZGxlclxuICAgICAgICByZXN1bHQgPSByZXN1bHQuY2F0Y2goZXJyID0+IHRoaXMuX2xvYWRFcnJvcih1cmwsIGVycikpO1xuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH1cbiAgICBjcmVhdGVGcm9tVVJMKHVybCwgbm93KSB7XG4gICAgICAgIGNvbnN0IGNodW5rID0gdXJsWzBdO1xuICAgICAgICBjb25zdCBuYW1lID0gY2h1bmsucGFnZTtcbiAgICAgICAgbGV0IHZpZXc7XG4gICAgICAgIGlmIChub3cgJiYgbm93LmdldE5hbWUoKSA9PT0gbmFtZSkge1xuICAgICAgICAgICAgdmlldyA9IFByb21pc2UucmVzb2x2ZShub3cpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdmlldyA9IHRoaXMubG9hZFZpZXcoY2h1bmsucGFnZSlcbiAgICAgICAgICAgICAgICAudGhlbih1aSA9PiB7XG4gICAgICAgICAgICAgICAgbGV0IG9iajtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHVpID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHVpLnByb3RvdHlwZSAmJiB1aS5wcm90b3R5cGUuc2hvdykge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gVUkgY2xhc3NcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgdWkodGhpcywgbmFtZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBVSSBmYWN0b3J5IGZ1bmN0aW9uc1xuICAgICAgICAgICAgICAgICAgICAgICAgdWkgPSB1aSgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmICh1aSBpbnN0YW5jZW9mIEpldEFwcCB8fCB1aSBpbnN0YW5jZW9mIEpldFZpZXcpIHtcbiAgICAgICAgICAgICAgICAgICAgb2JqID0gdWk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAvLyBVSSBvYmplY3RcbiAgICAgICAgICAgICAgICAgICAgaWYgKHVpLiR1aSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgb2JqID0gbmV3IEpldFZpZXdMZWdhY3kodGhpcywgbmFtZSwgdWkpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgb2JqID0gbmV3IEpldFZpZXdSYXcodGhpcywgbmFtZSwgdWkpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiBvYmo7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdmlldztcbiAgICB9XG4gICAgLy8gc2hvdyB2aWV3IHBhdGhcbiAgICBzaG93KG5hbWUpIHtcbiAgICAgICAgaWYgKHRoaXMuJHJvdXRlci5nZXQoKSAhPT0gbmFtZSkge1xuICAgICAgICAgICAgdGhpcy5jYW5OYXZpZ2F0ZShuYW1lKS50aGVuKHVybCA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy4kcm91dGVyLnNldCh1cmwsIHsgc2lsZW50OiB0cnVlIH0pO1xuICAgICAgICAgICAgICAgIHRoaXMuX3JlbmRlcih1cmwpO1xuICAgICAgICAgICAgfSkuY2F0Y2goKCkgPT4gZmFsc2UpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGNhbk5hdmlnYXRlKHVybCwgdmlldykge1xuICAgICAgICBjb25zdCBvYmogPSB7XG4gICAgICAgICAgICB1cmw6IHBhcnNlKHVybCksXG4gICAgICAgICAgICByZWRpcmVjdDogdXJsLFxuICAgICAgICAgICAgY29uZmlybTogUHJvbWlzZS5yZXNvbHZlKHRydWUpXG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IHJlcyA9IHRoaXMuY2FsbEV2ZW50KFwiYXBwOmd1YXJkXCIsIFt1cmwsICh2aWV3IHx8IHRoaXMuX3ZpZXcpLCBvYmpdKTtcbiAgICAgICAgaWYgKCFyZXMpIHtcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChcIlwiKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gb2JqLmNvbmZpcm0udGhlbigoKSA9PiBvYmoucmVkaXJlY3QpO1xuICAgIH1cbiAgICBkZXN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLl92aWV3LmRlc3RydWN0b3IoKTtcbiAgICB9XG4gICAgLy8gZXZlbnQgaGVscGVyc1xuICAgIHRyaWdnZXIobmFtZSwgLi4ucmVzdCkge1xuICAgICAgICB0aGlzLmFwcGx5KG5hbWUsIHJlc3QpO1xuICAgIH1cbiAgICBhcHBseShuYW1lLCBkYXRhKSB7XG4gICAgICAgIHRoaXMuY2FsbEV2ZW50KG5hbWUsIGRhdGEpO1xuICAgIH1cbiAgICBhY3Rpb24obmFtZSkge1xuICAgICAgICByZXR1cm4gdGhpcy53ZWJpeC5iaW5kKGZ1bmN0aW9uICguLi5yZXN0KSB7XG4gICAgICAgICAgICB0aGlzLmFwcGx5KG5hbWUsIHJlc3QpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICB9XG4gICAgb24obmFtZSwgaGFuZGxlcikge1xuICAgICAgICB0aGlzLmF0dGFjaEV2ZW50KG5hbWUsIGhhbmRsZXIpO1xuICAgIH1cbiAgICB1c2UocGx1Z2luLCBjb25maWcpIHtcbiAgICAgICAgcGx1Z2luKHRoaXMsIG51bGwsIGNvbmZpZyk7XG4gICAgfVxuICAgIGVycm9yKG5hbWUsIGVyKSB7XG4gICAgICAgIHRoaXMuY2FsbEV2ZW50KG5hbWUsIGVyKTtcbiAgICAgICAgdGhpcy5jYWxsRXZlbnQoXCJhcHA6ZXJyb3JcIiwgZXIpO1xuICAgICAgICAvKiB0c2xpbnQ6ZGlzYWJsZSAqL1xuICAgICAgICBpZiAodGhpcy5jb25maWcuZGVidWcpIHtcbiAgICAgICAgICAgIGlmIChlclswXSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJbMF0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZGVidWdnZXI7XG4gICAgICAgIH1cbiAgICAgICAgLyogdHNsaW50OmVuYWJsZSAqL1xuICAgIH1cbiAgICAvLyByZW5kZXJzIHRvcCB2aWV3XG4gICAgX3JlbmRlcih1cmwpIHtcbiAgICAgICAgY29uc3QgZmlyc3RJbml0ID0gIXRoaXMuJHJvdXRlcjtcbiAgICAgICAgaWYgKGZpcnN0SW5pdCkge1xuICAgICAgICAgICAgdXJsID0gdGhpcy5fZmlyc3Rfc3RhcnQodXJsKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBzdHJVcmwgPSB0eXBlb2YgdXJsID09PSBcInN0cmluZ1wiID8gdXJsIDogdXJsMnN0cih1cmwpO1xuICAgICAgICByZXR1cm4gdGhpcy5jYW5OYXZpZ2F0ZShzdHJVcmwpLnRoZW4obmV3dXJsID0+IHtcbiAgICAgICAgICAgIHRoaXMuJHJvdXRlci5zZXQobmV3dXJsLCB7IHNpbGVudDogdHJ1ZSB9KTtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9yZW5kZXJfc3RhZ2UobmV3dXJsKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIF9yZW5kZXJfc3RhZ2UodXJsKSB7XG4gICAgICAgIGNvbnN0IHBhcnNlZCA9ICh0eXBlb2YgdXJsID09PSBcInN0cmluZ1wiKSA/IHBhcnNlKHVybCkgOiB1cmw7XG4gICAgICAgIC8vIGJsb2NrIHJlc2l6aW5nIHdoaWxlIHJlbmRlcmluZyBwYXJ0cyBvZiBVSVxuICAgICAgICByZXR1cm4gd2ViaXgudWkuZnJlZXplKCgpID0+IHRoaXMuY3JlYXRlRnJvbVVSTChwYXJzZWQsIHRoaXMuX3ZpZXcpLnRoZW4odmlldyA9PiB7XG4gICAgICAgICAgICAvLyBzYXZlIHJlZmVyZW5jZSBmb3Igb2xkIGFuZCBuZXcgdmlld3NcbiAgICAgICAgICAgIGNvbnN0IG9sZHZpZXcgPSB0aGlzLl92aWV3O1xuICAgICAgICAgICAgdGhpcy5fdmlldyA9IHZpZXc7XG4gICAgICAgICAgICAvLyByZW5kZXIgdXJsIHN0YXRlIGZvciB0aGUgcm9vdFxuICAgICAgICAgICAgcmV0dXJuIHZpZXcucmVuZGVyKHRoaXMuX2NvbnRhaW5lciwgcGFyc2VkLCB0aGlzLl9wYXJlbnQpLnRoZW4ocm9vdCA9PiB7XG4gICAgICAgICAgICAgICAgLy8gZGVzdHJveSBhbmQgZGV0YWNrIG9sZCB2aWV3XG4gICAgICAgICAgICAgICAgaWYgKG9sZHZpZXcgJiYgb2xkdmlldyAhPT0gdGhpcy5fdmlldykge1xuICAgICAgICAgICAgICAgICAgICBvbGR2aWV3LmRlc3RydWN0b3IoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuX3ZpZXcuZ2V0Um9vdCgpLmdldFBhcmVudFZpZXcoKSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9jb250YWluZXIgPSByb290O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLl9yb290ID0gcm9vdDtcbiAgICAgICAgICAgICAgICB0aGlzLmNhbGxFdmVudChcImFwcDpyb3V0ZVwiLCBbcGFyc2VkXSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHZpZXc7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSkuY2F0Y2goZXIgPT4ge1xuICAgICAgICAgICAgdGhpcy5lcnJvcihcImFwcDplcnJvcjpyZW5kZXJcIiwgW2VyXSk7XG4gICAgICAgIH0pKTtcbiAgICB9XG4gICAgX3VybENoYW5nZShfJHVybCkge1xuICAgICAgICBhbGVydChcIk5vdCBpbXBsZW1lbnRlZFwiKTtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSh0cnVlKTtcbiAgICB9XG4gICAgX2ZpcnN0X3N0YXJ0KHVybCkge1xuICAgICAgICBjb25zdCBjYiA9IGEgPT4gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLl9yZW5kZXIoYSk7XG4gICAgICAgIH0sIDEpO1xuICAgICAgICB0aGlzLiRyb3V0ZXIgPSBuZXcgKHRoaXMuY29uZmlnLnJvdXRlciB8fCBIYXNoUm91dGVyKShjYiwgdGhpcy5jb25maWcpO1xuICAgICAgICAvLyBzdGFydCBhbmltYXRpb24gZm9yIHRvcC1sZXZlbCBhcHBcbiAgICAgICAgaWYgKHRoaXMuX2NvbnRhaW5lciA9PT0gZG9jdW1lbnQuYm9keSAmJiB0aGlzLmNvbmZpZy5hbmltYXRpb24gIT09IGZhbHNlKSB7XG4gICAgICAgICAgICBjb25zdCBub2RlID0gdGhpcy5fY29udGFpbmVyO1xuICAgICAgICAgICAgd2ViaXguaHRtbC5hZGRDc3Mobm9kZSwgXCJ3ZWJpeGFwcHN0YXJ0XCIpO1xuICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgd2ViaXguaHRtbC5yZW1vdmVDc3Mobm9kZSwgXCJ3ZWJpeGFwcHN0YXJ0XCIpO1xuICAgICAgICAgICAgICAgIHdlYml4Lmh0bWwuYWRkQ3NzKG5vZGUsIFwid2ViaXhhcHBcIik7XG4gICAgICAgICAgICB9LCAxMCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCF1cmwgfHwgdXJsLmxlbmd0aCA9PT0gMSkge1xuICAgICAgICAgICAgdXJsID0gdGhpcy4kcm91dGVyLmdldCgpIHx8IHRoaXMuY29uZmlnLnN0YXJ0O1xuICAgICAgICAgICAgdGhpcy4kcm91dGVyLnNldCh1cmwsIHsgc2lsZW50OiB0cnVlIH0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB1cmw7XG4gICAgfVxuICAgIC8vIGVycm9yIGR1cmluZyB2aWV3IHJlc29sdmluZ1xuICAgIF9sb2FkRXJyb3IodXJsLCBlcnIpIHtcbiAgICAgICAgdGhpcy5lcnJvcihcImFwcDplcnJvcjpyZXNvbHZlXCIsIFtlcnIsIHVybF0pO1xuICAgICAgICByZXR1cm4geyB0ZW1wbGF0ZTogXCIgXCIgfTtcbiAgICB9XG4gICAgYWRkU3ViVmlldyhvYmosIHRhcmdldCwgY29uZmlnKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IG9iai4kc3VidmlldyAhPT0gdHJ1ZSA/IG9iai4kc3VidmlldyA6IG51bGw7XG4gICAgICAgIGNvbnN0IG5hbWUgPSBvYmoubmFtZSB8fCAodXJsID8gdGhpcy53ZWJpeC51aWQoKSA6IFwiZGVmYXVsdFwiKTtcbiAgICAgICAgdGFyZ2V0LmlkID0gb2JqLmlkIHx8IFwic1wiICsgdGhpcy53ZWJpeC51aWQoKTtcbiAgICAgICAgY29uc3QgdmlldyA9IGNvbmZpZ1tuYW1lXSA9IHsgaWQ6IHRhcmdldC5pZCwgdXJsIH07XG4gICAgICAgIGlmICh2aWV3LnVybCBpbnN0YW5jZW9mIEpldFZpZXcpIHtcbiAgICAgICAgICAgIHZpZXcudmlldyA9IHZpZXcudXJsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0YXJnZXQ7XG4gICAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9KZXRBcHAuanMiLCJpbXBvcnQgeyBKZXRWaWV3IH0gZnJvbSBcIi4vSmV0Vmlld1wiO1xuLy8gd3JhcHBlciBmb3IgcmF3IG9iamVjdHMgYW5kIEpldCAxLnggc3RydWN0c1xuZXhwb3J0IGNsYXNzIEpldFZpZXdMZWdhY3kgZXh0ZW5kcyBKZXRWaWV3IHtcbiAgICBjb25zdHJ1Y3RvcihhcHAsIG5hbWUsIHVpKSB7XG4gICAgICAgIHN1cGVyKGFwcCwgbmFtZSk7XG4gICAgICAgIHRoaXMuX3VpID0gdWk7XG4gICAgICAgIHRoaXMuX3dpbmRvd3MgPSBbXTtcbiAgICB9XG4gICAgZ2V0Um9vdCgpIHtcbiAgICAgICAgaWYgKHRoaXMuYXBwLmNvbmZpZy5qZXQxeE1vZGUpIHtcbiAgICAgICAgICAgIGNvbnN0IHBhcmVudCA9IHRoaXMuZ2V0UGFyZW50VmlldygpO1xuICAgICAgICAgICAgaWYgKHBhcmVudCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBwYXJlbnQuZ2V0Um9vdCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9yb290O1xuICAgIH1cbiAgICBjb25maWcoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl91aS4kdWkgfHwgdGhpcy5fdWk7XG4gICAgfVxuICAgIGRlc3RydWN0b3IoKSB7XG4gICAgICAgIGNvbnN0IGRlc3Ryb3kgPSB0aGlzLl91aS4kb25kZXN0cm95O1xuICAgICAgICBpZiAoZGVzdHJveSkge1xuICAgICAgICAgICAgZGVzdHJveSgpO1xuICAgICAgICB9XG4gICAgICAgIGZvciAoY29uc3Qgd2luZG93IG9mIHRoaXMuX3dpbmRvd3MpIHtcbiAgICAgICAgICAgIHdpbmRvdy5kZXN0cnVjdG9yKCk7XG4gICAgICAgIH1cbiAgICAgICAgc3VwZXIuZGVzdHJ1Y3RvcigpO1xuICAgIH1cbiAgICBzaG93KHBhdGgsIGNvbmZpZykge1xuICAgICAgICBpZiAocGF0aC5pbmRleE9mKFwiL1wiKSA9PT0gMCB8fCBwYXRoLmluZGV4T2YoXCIuL1wiKSA9PT0gMCkge1xuICAgICAgICAgICAgcmV0dXJuIHN1cGVyLnNob3cocGF0aCwgY29uZmlnKTtcbiAgICAgICAgfVxuICAgICAgICBzdXBlci5zaG93KFwiLi4vXCIgKyBwYXRoLCBjb25maWcpO1xuICAgIH1cbiAgICBpbml0KGEsIGIpIHtcbiAgICAgICAgaWYgKHRoaXMuYXBwLmNvbmZpZy5sZWdhY3lFYXJseUluaXQpIHtcbiAgICAgICAgICAgIHRoaXMuX3JlYWxJbml0SGFuZGxlcihhLCBiKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZWFkeShhLCBiKSB7XG4gICAgICAgIGlmICghdGhpcy5hcHAuY29uZmlnLmxlZ2FjeUVhcmx5SW5pdCkge1xuICAgICAgICAgICAgdGhpcy5fcmVhbEluaXRIYW5kbGVyKGEsIGIpO1xuICAgICAgICB9XG4gICAgfVxuICAgIF9yZWFsSW5pdEhhbmRsZXIoYSwgYikge1xuICAgICAgICBjb25zdCBpbml0ID0gdGhpcy5fdWkuJG9uaW5pdDtcbiAgICAgICAgaWYgKGluaXQpIHtcbiAgICAgICAgICAgIGNvbnN0IHJvb3QgPSB0aGlzLmdldFJvb3QoKTtcbiAgICAgICAgICAgIGluaXQocm9vdCwgcm9vdC4kc2NvcGUpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGV2ZW50cyA9IHRoaXMuX3VpLiRvbmV2ZW50O1xuICAgICAgICBpZiAoZXZlbnRzKSB7XG4gICAgICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBldmVudHMpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm9uKHRoaXMuYXBwLCBrZXksIGV2ZW50c1trZXldKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBjb25zdCB3aW5kb3dzID0gdGhpcy5fdWkuJHdpbmRvd3M7XG4gICAgICAgIGlmICh3aW5kb3dzKSB7XG4gICAgICAgICAgICBmb3IgKGNvbnN0IGNvbmYgb2Ygd2luZG93cykge1xuICAgICAgICAgICAgICAgIGlmIChjb25mLiR1aSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCB2aWV3ID0gbmV3IEpldFZpZXdMZWdhY3kodGhpcy5hcHAsIHRoaXMuZ2V0TmFtZSgpLCBjb25mKTtcbiAgICAgICAgICAgICAgICAgICAgdmlldy5yZW5kZXIoZG9jdW1lbnQuYm9keSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3dpbmRvd3MucHVzaCh2aWV3KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudWkoY29uZik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIF91cmxDaGFuZ2UodXJsKSB7XG4gICAgICAgIHJldHVybiBzdXBlci5fdXJsQ2hhbmdlKHVybCkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBvbnVybGNoYW5nZSA9IHRoaXMuX3VpLiRvbnVybGNoYW5nZTtcbiAgICAgICAgICAgIGlmIChvbnVybGNoYW5nZSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IHJvb3QgPSB0aGlzLmdldFJvb3QoKTtcbiAgICAgICAgICAgICAgICBvbnVybGNoYW5nZSh1cmxbMF0ucGFyYW1zLCB1cmwuc2xpY2UoMSksIHJvb3QuJHNjb3BlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9KZXRWaWV3TGVnYWN5LmpzIiwiaW1wb3J0IHsgSmV0VmlldyB9IGZyb20gXCIuL0pldFZpZXdcIjtcbi8vIHdyYXBwZXIgZm9yIHJhdyBvYmplY3RzIGFuZCBKZXQgMS54IHN0cnVjdHNcbmV4cG9ydCBjbGFzcyBKZXRWaWV3UmF3IGV4dGVuZHMgSmV0VmlldyB7XG4gICAgY29uc3RydWN0b3IoYXBwLCBuYW1lLCB1aSkge1xuICAgICAgICBzdXBlcihhcHAsIG5hbWUpO1xuICAgICAgICB0aGlzLl91aSA9IHVpO1xuICAgIH1cbiAgICBjb25maWcoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl91aTtcbiAgICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L0pldFZpZXdSYXcuanMiLCIvKiFcclxuICogd2ViaXgtcm91dGllIC0gcm91dGVyIGZvciBXZWJpeC1KZXRcclxuICogdjAuNC4wXHJcbiAqIE1JVCBMaWNlbnNlXHJcbiAqXHJcbiAqIGJhc2VkIG9uIHJvdXRpZSAtIGEgdGlueSBoYXNoIHJvdXRlclxyXG4gKiBodHRwOi8vcHJvamVjdHMuamdhLm1lL3JvdXRpZVxyXG4gKiBjb3B5cmlnaHQgR3JlZyBBbGxlbiAyMDE2XHJcbiAqIE1JVCBMaWNlbnNlXHJcbiovXHJcblxyXG52YXIgUm91dGllID0gZnVuY3Rpb24odywgaXNNb2R1bGUpIHtcclxuXHJcbiAgdmFyIHJvdXRlcyA9IFtdO1xyXG4gIHZhciBtYXAgPSB7fTtcclxuICB2YXIgcmVmZXJlbmNlID0gJ3JvdXRpZSc7XHJcbiAgdmFyIG9sZFJlZmVyZW5jZSA9IHdbcmVmZXJlbmNlXTtcclxuICB2YXIgb2xkVXJsO1xyXG5cclxuICB2YXIgUm91dGUgPSBmdW5jdGlvbihwYXRoLCBuYW1lKSB7XHJcbiAgICB0aGlzLm5hbWUgPSBuYW1lO1xyXG4gICAgdGhpcy5wYXRoID0gcGF0aDtcclxuICAgIHRoaXMua2V5cyA9IFtdO1xyXG4gICAgdGhpcy5mbnMgPSBbXTtcclxuICAgIHRoaXMucGFyYW1zID0ge307XHJcbiAgICB0aGlzLnJlZ2V4ID0gcGF0aFRvUmVnZXhwKHRoaXMucGF0aCwgdGhpcy5rZXlzLCBmYWxzZSwgZmFsc2UpO1xyXG5cclxuICB9O1xyXG5cclxuICBSb3V0ZS5wcm90b3R5cGUuYWRkSGFuZGxlciA9IGZ1bmN0aW9uKGZuKSB7XHJcbiAgICB0aGlzLmZucy5wdXNoKGZuKTtcclxuICB9O1xyXG5cclxuICBSb3V0ZS5wcm90b3R5cGUucmVtb3ZlSGFuZGxlciA9IGZ1bmN0aW9uKGZuKSB7XHJcbiAgICBmb3IgKHZhciBpID0gMCwgYyA9IHRoaXMuZm5zLmxlbmd0aDsgaSA8IGM7IGkrKykge1xyXG4gICAgICB2YXIgZiA9IHRoaXMuZm5zW2ldO1xyXG4gICAgICBpZiAoZm4gPT0gZikge1xyXG4gICAgICAgIHRoaXMuZm5zLnNwbGljZShpLCAxKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9O1xyXG5cclxuICBSb3V0ZS5wcm90b3R5cGUucnVuID0gZnVuY3Rpb24ocGFyYW1zKSB7XHJcbiAgICBmb3IgKHZhciBpID0gMCwgYyA9IHRoaXMuZm5zLmxlbmd0aDsgaSA8IGM7IGkrKykge1xyXG4gICAgICBpZiAodGhpcy5mbnNbaV0uYXBwbHkodGhpcywgcGFyYW1zKSA9PT0gZmFsc2UpXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfTtcclxuXHJcbiAgUm91dGUucHJvdG90eXBlLm1hdGNoID0gZnVuY3Rpb24ocGF0aCwgcGFyYW1zKXtcclxuICAgIHZhciBtID0gdGhpcy5yZWdleC5leGVjKHBhdGgpO1xyXG5cclxuICAgIGlmICghbSkgcmV0dXJuIGZhbHNlO1xyXG5cclxuXHJcbiAgICBmb3IgKHZhciBpID0gMSwgbGVuID0gbS5sZW5ndGg7IGkgPCBsZW47ICsraSkge1xyXG4gICAgICB2YXIga2V5ID0gdGhpcy5rZXlzW2kgLSAxXTtcclxuXHJcbiAgICAgIHZhciB2YWwgPSAoJ3N0cmluZycgPT0gdHlwZW9mIG1baV0pID8gZGVjb2RlVVJJQ29tcG9uZW50KG1baV0pIDogbVtpXTtcclxuXHJcbiAgICAgIGlmIChrZXkpIHtcclxuICAgICAgICB0aGlzLnBhcmFtc1trZXkubmFtZV0gPSB2YWw7XHJcbiAgICAgIH1cclxuICAgICAgcGFyYW1zLnB1c2godmFsKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICBSb3V0ZS5wcm90b3R5cGUudG9VUkwgPSBmdW5jdGlvbihwYXJhbXMpIHtcclxuICAgIHZhciBwYXRoID0gdGhpcy5wYXRoO1xyXG4gICAgZm9yICh2YXIgcGFyYW0gaW4gcGFyYW1zKSB7XHJcbiAgICAgIHBhdGggPSBwYXRoLnJlcGxhY2UoJy86JytwYXJhbSwgJy8nK3BhcmFtc1twYXJhbV0pO1xyXG4gICAgfVxyXG4gICAgcGF0aCA9IHBhdGgucmVwbGFjZSgvXFwvOi4qXFw/L2csICcvJykucmVwbGFjZSgvXFw/L2csICcnKTtcclxuICAgIGlmIChwYXRoLmluZGV4T2YoJzonKSAhPSAtMSkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ21pc3NpbmcgcGFyYW1ldGVycyBmb3IgdXJsOiAnK3BhdGgpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHBhdGg7XHJcbiAgfTtcclxuXHJcbiAgdmFyIHBhdGhUb1JlZ2V4cCA9IGZ1bmN0aW9uKHBhdGgsIGtleXMsIHNlbnNpdGl2ZSwgc3RyaWN0KSB7XHJcbiAgICBpZiAocGF0aCBpbnN0YW5jZW9mIFJlZ0V4cCkgcmV0dXJuIHBhdGg7XHJcbiAgICBpZiAocGF0aCBpbnN0YW5jZW9mIEFycmF5KSBwYXRoID0gJygnICsgcGF0aC5qb2luKCd8JykgKyAnKSc7XHJcbiAgICBwYXRoID0gcGF0aFxyXG4gICAgICAuY29uY2F0KHN0cmljdCA/ICcnIDogJy8/JylcclxuICAgICAgLnJlcGxhY2UoL1xcL1xcKC9nLCAnKD86LycpXHJcbiAgICAgIC5yZXBsYWNlKC9cXCsvZywgJ19fcGx1c19fJylcclxuICAgICAgLnJlcGxhY2UoLyhcXC8pPyhcXC4pPzooXFx3KykoPzooXFwoLio/XFwpKSk/KFxcPyk/L2csIGZ1bmN0aW9uKF8sIHNsYXNoLCBmb3JtYXQsIGtleSwgY2FwdHVyZSwgb3B0aW9uYWwpe1xyXG4gICAgICAgIGtleXMucHVzaCh7IG5hbWU6IGtleSwgb3B0aW9uYWw6ICEhIG9wdGlvbmFsIH0pO1xyXG4gICAgICAgIHNsYXNoID0gc2xhc2ggfHwgJyc7XHJcbiAgICAgICAgcmV0dXJuICcnICsgKG9wdGlvbmFsID8gJycgOiBzbGFzaCkgKyAnKD86JyArIChvcHRpb25hbCA/IHNsYXNoIDogJycpICsgKGZvcm1hdCB8fCAnJykgKyAoY2FwdHVyZSB8fCAoZm9ybWF0ICYmICcoW14vLl0rPyknIHx8ICcoW14vXSs/KScpKSArICcpJyArIChvcHRpb25hbCB8fCAnJyk7XHJcbiAgICAgIH0pXHJcbiAgICAgIC5yZXBsYWNlKC8oWy8uXSkvZywgJ1xcXFwkMScpXHJcbiAgICAgIC5yZXBsYWNlKC9fX3BsdXNfXy9nLCAnKC4rKScpXHJcbiAgICAgIC5yZXBsYWNlKC9cXCovZywgJyguKiknKTtcclxuICAgIHJldHVybiBuZXcgUmVnRXhwKCdeJyArIHBhdGggKyAnJCcsIHNlbnNpdGl2ZSA/ICcnIDogJ2knKTtcclxuICB9O1xyXG5cclxuICB2YXIgYWRkSGFuZGxlciA9IGZ1bmN0aW9uKHBhdGgsIGZuKSB7XHJcbiAgICB2YXIgcyA9IHBhdGguc3BsaXQoJyAnKTtcclxuICAgIHZhciBuYW1lID0gKHMubGVuZ3RoID09IDIpID8gc1swXSA6IG51bGw7XHJcbiAgICBwYXRoID0gKHMubGVuZ3RoID09IDIpID8gc1sxXSA6IHNbMF07XHJcblxyXG4gICAgaWYgKCFtYXBbcGF0aF0pIHtcclxuICAgICAgbWFwW3BhdGhdID0gbmV3IFJvdXRlKHBhdGgsIG5hbWUpO1xyXG4gICAgICByb3V0ZXMucHVzaChtYXBbcGF0aF0pO1xyXG4gICAgfVxyXG4gICAgbWFwW3BhdGhdLmFkZEhhbmRsZXIoZm4pO1xyXG4gIH07XHJcblxyXG4gIHZhciByb3V0aWUgPSBmdW5jdGlvbihwYXRoLCBmbikge1xyXG4gICAgaWYgKHR5cGVvZiBmbiA9PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgIGFkZEhhbmRsZXIocGF0aCwgZm4pO1xyXG4gICAgICByb3V0aWUucmVsb2FkKCk7XHJcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBwYXRoID09ICdvYmplY3QnKSB7XHJcbiAgICAgIGZvciAodmFyIHAgaW4gcGF0aCkge1xyXG4gICAgICAgIGFkZEhhbmRsZXIocCwgcGF0aFtwXSk7XHJcbiAgICAgIH1cclxuICAgICAgcm91dGllLnJlbG9hZCgpO1xyXG4gICAgfSBlbHNlIGlmICh0eXBlb2YgZm4gPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgIHJvdXRpZS5uYXZpZ2F0ZShwYXRoKTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICByb3V0aWUubG9va3VwID0gZnVuY3Rpb24obmFtZSwgb2JqKSB7XHJcbiAgICBmb3IgKHZhciBpID0gMCwgYyA9IHJvdXRlcy5sZW5ndGg7IGkgPCBjOyBpKyspIHtcclxuICAgICAgdmFyIHJvdXRlID0gcm91dGVzW2ldO1xyXG4gICAgICBpZiAocm91dGUubmFtZSA9PSBuYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuIHJvdXRlLnRvVVJMKG9iaik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9O1xyXG5cclxuICByb3V0aWUucmVtb3ZlID0gZnVuY3Rpb24ocGF0aCwgZm4pIHtcclxuICAgIHZhciByb3V0ZSA9IG1hcFtwYXRoXTtcclxuICAgIGlmICghcm91dGUpXHJcbiAgICAgIHJldHVybjtcclxuICAgIHJvdXRlLnJlbW92ZUhhbmRsZXIoZm4pO1xyXG4gIH07XHJcblxyXG4gIHJvdXRpZS5yZW1vdmVBbGwgPSBmdW5jdGlvbigpIHtcclxuICAgIG1hcCA9IHt9O1xyXG4gICAgcm91dGVzID0gW107XHJcbiAgICBvbGRVcmwgPSAnJztcclxuICB9O1xyXG5cclxuICByb3V0aWUubmF2aWdhdGUgPSBmdW5jdGlvbihwYXRoLCBvcHRpb25zKSB7XHJcbiAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcclxuICAgIHZhciBzaWxlbnQgPSBvcHRpb25zLnNpbGVudCB8fCBmYWxzZTtcclxuXHJcbiAgICBpZiAoc2lsZW50KSB7XHJcbiAgICAgIHJlbW92ZUxpc3RlbmVyKCk7XHJcbiAgICB9XHJcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICB3aW5kb3cubG9jYXRpb24uaGFzaCA9IHBhdGg7XHJcblxyXG4gICAgICBpZiAoc2lsZW50KSB7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHsgXHJcbiAgICAgICAgICBhZGRMaXN0ZW5lcigpO1xyXG4gICAgICAgIH0sIDEpO1xyXG4gICAgICB9XHJcblxyXG4gICAgfSwgMSk7XHJcbiAgfTtcclxuXHJcbiAgcm91dGllLm5vQ29uZmxpY3QgPSBmdW5jdGlvbigpIHtcclxuICAgIHdbcmVmZXJlbmNlXSA9IG9sZFJlZmVyZW5jZTtcclxuICAgIHJldHVybiByb3V0aWU7XHJcbiAgfTtcclxuXHJcbiAgdmFyIGdldEhhc2ggPSBmdW5jdGlvbigpIHtcclxuICAgIHJldHVybiB3aW5kb3cubG9jYXRpb24uaGFzaC5zdWJzdHJpbmcoMSk7XHJcbiAgfTtcclxuXHJcbiAgdmFyIGNoZWNrUm91dGUgPSBmdW5jdGlvbihoYXNoLCByb3V0ZSkge1xyXG4gICAgdmFyIHBhcmFtcyA9IFtdO1xyXG4gICAgaWYgKHJvdXRlLm1hdGNoKGhhc2gsIHBhcmFtcykpIHtcclxuICAgICAgcmV0dXJuIChyb3V0ZS5ydW4ocGFyYW1zKSAhPT0gZmFsc2UgPyAxIDogMCk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gLTE7XHJcbiAgfTtcclxuXHJcbiAgdmFyIGhhc2hDaGFuZ2VkID0gcm91dGllLnJlbG9hZCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIGhhc2ggPSBnZXRIYXNoKCk7XHJcbiAgICBmb3IgKHZhciBpID0gMCwgYyA9IHJvdXRlcy5sZW5ndGg7IGkgPCBjOyBpKyspIHtcclxuICAgICAgdmFyIHJvdXRlID0gcm91dGVzW2ldO1xyXG4gICAgICB2YXIgc3RhdGUgPSBjaGVja1JvdXRlKGhhc2gsIHJvdXRlKTtcclxuICAgICAgaWYgKHN0YXRlID09PSAxKSB7XHJcbiAgICAgICAgLy9yb3V0ZSBwcm9jZXNzZWRcclxuICAgICAgICBvbGRVcmwgPSBoYXNoO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICB9IGVsc2UgaWYgKHN0YXRlID09PSAwKXtcclxuICAgICAgICAvL3JvdXRlIHJlamVjdGVkXHJcbiAgICAgICAgcm91dGllLm5hdmlnYXRlKG9sZFVybCwgeyBzaWxlbnQ6dHJ1ZSB9KTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHZhciBhZGRMaXN0ZW5lciA9IGZ1bmN0aW9uKCkge1xyXG4gICAgaWYgKHcuYWRkRXZlbnRMaXN0ZW5lcikge1xyXG4gICAgICB3LmFkZEV2ZW50TGlzdGVuZXIoJ2hhc2hjaGFuZ2UnLCBoYXNoQ2hhbmdlZCwgZmFsc2UpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdy5hdHRhY2hFdmVudCgnb25oYXNoY2hhbmdlJywgaGFzaENoYW5nZWQpO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHZhciByZW1vdmVMaXN0ZW5lciA9IGZ1bmN0aW9uKCkge1xyXG4gICAgaWYgKHcucmVtb3ZlRXZlbnRMaXN0ZW5lcikge1xyXG4gICAgICB3LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2hhc2hjaGFuZ2UnLCBoYXNoQ2hhbmdlZCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB3LmRldGFjaEV2ZW50KCdvbmhhc2hjaGFuZ2UnLCBoYXNoQ2hhbmdlZCk7XHJcbiAgICB9XHJcbiAgfTtcclxuICBhZGRMaXN0ZW5lcigpO1xyXG4gIG9sZFVybCA9IGdldEhhc2goKTtcclxuXHJcbiAgaWYgKGlzTW9kdWxlKXtcclxuICAgIHJldHVybiByb3V0aWU7XHJcbiAgfSBlbHNlIHtcclxuICAgIHdbcmVmZXJlbmNlXSA9IHJvdXRpZTtcclxuICB9XHJcbiAgIFxyXG59O1xyXG5cclxuaWYgKHR5cGVvZiBtb2R1bGUgPT0gJ3VuZGVmaW5lZCcpe1xyXG4gIFJvdXRpZSh3aW5kb3cpO1xyXG59IGVsc2Uge1xyXG4gIG1vZHVsZS5leHBvcnRzID0gUm91dGllKHdpbmRvdyx0cnVlKTtcclxuICBtb2R1bGUuZXhwb3J0cy5kZWZhdWx0ID0gbW9kdWxlLmV4cG9ydHM7XHJcbn1cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvd2ViaXgtcm91dGllL2xpYi9yb3V0aWUuanMiLCJjb25zdCB3ID0gd2ViaXg7XG4vLyB3aWxsIGJlIGFkZGVkIGluIHdlYml4IDUuMVxuaWYgKCF3LnVpLmZyZWV6ZSkge1xuICAgIHcudWkuZnJlZXplID0gZnVuY3Rpb24gKGhhbmRsZXIpIHtcbiAgICAgICAgLy8gZGlzYWJsZWQgYmVjYXVzZSB3ZWJpeCBqZXQgNS4wIGNhbid0IGhhbmRsZSByZXNpemUgb2Ygc2Nyb2xsdmlldyBjb3JyZWN0bHlcbiAgICAgICAgLy8gdy51aS4kZnJlZXplID0gdHJ1ZTtcbiAgICAgICAgY29uc3QgcmVzID0gaGFuZGxlcigpO1xuICAgICAgICBpZiAocmVzICYmIHJlcy50aGVuKSB7XG4gICAgICAgICAgICByZXMudGhlbihmdW5jdGlvbiAoc29tZSkge1xuICAgICAgICAgICAgICAgIHcudWkuJGZyZWV6ZSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIHcudWkucmVzaXplKCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHNvbWU7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHcudWkuJGZyZWV6ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdy51aS5yZXNpemUoKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzO1xuICAgIH07XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3BhdGNoLmpzIiwidmFyIG1hcCA9IHtcblx0XCIuL2xheW91dFwiOiAxNixcblx0XCIuL2xheW91dC5qc1wiOiAxNixcblx0XCIuL21lbnUvZHJvcERvd25NZW51c1wiOiA0LFxuXHRcIi4vbWVudS9kcm9wRG93bk1lbnVzLmpzXCI6IDQsXG5cdFwiLi9tZW51L2ZpbGVNZW51XCI6IDE3LFxuXHRcIi4vbWVudS9maWxlTWVudS5qc1wiOiAxNyxcblx0XCIuL21lbnUvaWNvbk1lbnVcIjogNSxcblx0XCIuL21lbnUvaWNvbk1lbnUuanNcIjogNSxcblx0XCIuL21lbnUvbWVudVwiOiAzLFxuXHRcIi4vbWVudS9tZW51LmpzXCI6IDMsXG5cdFwiLi9tb2R1bGUvbW9kdWxlXCI6IDksXG5cdFwiLi9tb2R1bGUvbW9kdWxlLmpzXCI6IDksXG5cdFwiLi9tb2R1bGUvbW9kdWxlQ29tcG9uZW50c1wiOiAxMixcblx0XCIuL21vZHVsZS9tb2R1bGVDb21wb25lbnRzLmpzXCI6IDEyLFxuXHRcIi4vbW9kdWxlL21vZHVsZVRhYmJhckNlbGxcIjogMTEsXG5cdFwiLi9tb2R1bGUvbW9kdWxlVGFiYmFyQ2VsbC5qc1wiOiAxMSxcblx0XCIuL21vZHVsZS9tb2R1bGVUb29sc1wiOiAxMCxcblx0XCIuL21vZHVsZS9tb2R1bGVUb29scy5qc1wiOiAxMCxcblx0XCIuL3NpZGViYXIvcHJvamVjdEV4cGxvcmVyXCI6IDcsXG5cdFwiLi9zaWRlYmFyL3Byb2plY3RFeHBsb3Jlci5qc1wiOiA3LFxuXHRcIi4vc2lkZWJhci9wcm9wZXJ0eUxpc3RcIjogOCxcblx0XCIuL3NpZGViYXIvcHJvcGVydHlMaXN0LmpzXCI6IDgsXG5cdFwiLi9zaWRlYmFyL3NpZGViYXJcIjogNixcblx0XCIuL3NpZGViYXIvc2lkZWJhci5qc1wiOiA2XG59O1xuZnVuY3Rpb24gd2VicGFja0NvbnRleHQocmVxKSB7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpKTtcbn07XG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKSB7XG5cdHZhciBpZCA9IG1hcFtyZXFdO1xuXHRpZighKGlkICsgMSkpIC8vIGNoZWNrIGZvciBudW1iZXIgb3Igc3RyaW5nXG5cdFx0dGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJy5cIik7XG5cdHJldHVybiBpZDtcbn07XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gMjU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zb3VyY2VzL3ZpZXdzIF5cXC5cXC8uKiRcbi8vIG1vZHVsZSBpZCA9IDI1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIi8qIERyb3AgRG93biBNZW51IERhdGEgKi9cclxuXHJcbmxldCB2aWV3c1N1Ym1lbnUgPSBbXHJcbiAgICB7IHZhbHVlOlwiQ3Jvc3MgVmlld1wiLCBpY29uOlwiY3Jvc3Mtdmlld1wiIH0sXHJcbiAgICB7IHZhbHVlOlwiMyB4IDEgVmlld1wiLCBpY29uOlwidGhyZWUtb25lLXZpZXdcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIlNpbmdsZVwiLCBpY29uOlwic3F1YXJlXCIgfSxcclxuICAgIHsgdmFsdWU6XCJIb3Jpem9udGFsIFNwbGl0XCIsIGljb246XCJob3Jpem9udGFsLXNwbGl0LXZpZXdcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIlZlcnRpY2FsIFNwbGl0XCIsIGljb246XCJ2ZXJ0aWNhbC1zcGxpdC12aWV3XCIgfVxyXG5dO1xyXG5cclxubGV0IGFjdGl2ZVZpZXdTdWJtZW51ID0gW1xyXG4gICAgeyB2YWx1ZTpcIk9ydGhvZ3JhcGhpY1wiLCBpY29uOlwib3J0aG9ncmFwaGljXCIgfSxcclxuICAgIHsgdmFsdWU6XCJQZXJzcGVjdGl2ZVwiLCBpY29uOlwicGVyc3BlY3RpdmVcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIlRvcFwiLCBpY29uOlwidmlldy10b3BcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIkJvdHRvbVwiLCBpY29uOlwidmlldy1ib3R0b21cIiB9LFxyXG4gICAgeyB2YWx1ZTpcIkZyb250XCIsIGljb246XCJ2aWV3LWZyb250XCIgfSxcclxuICAgIHsgdmFsdWU6XCJCYWNrXCIsIGljb246XCJ2aWV3LWJhY2tcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIkxlZnRcIiwgaWNvbjpcInZpZXctbGVmdFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiUmlnaHRcIiwgaWNvbjpcInZpZXctcmlnaHRcIiB9XHJcbl07XHJcblxyXG5sZXQgdHdvRFN1Ym1lbnUgPSBbXHJcbiAgICB7IHZhbHVlOlwiUmVjdGFuZ2xlXCIsIGljb246XCJyZWN0YW5nbGVcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIlRyaWFuZ2xlXCIsIGljb246XCJ0cmlhbmdsZVwiIH0sXHJcbiAgICB7IHZhbHVlOlwiRWxsaXBzZVwiLCBpY29uOlwiZWxsaXBzZVwiIH0sXHJcbiAgICB7IHZhbHVlOlwiUG9seWdvblwiLCBpY29uOlwiaGV4YWdvblwiIH0sXHJcbiAgICB7IHZhbHVlOlwiRGlza1wiLCBpY29uOlwiZGlza1wiIH0sXHJcbiAgICB7IHZhbHVlOlwiUG9pbnRcIiwgaWNvbjpcInBvaW50XCIgfSxcclxuICAgIHsgdmFsdWU6XCJMaW5lXCIsIGljb246XCJsaW5lXCIgfSxcclxuICAgIHsgdmFsdWU6XCJJbWFnZVwiLCBpY29uOlwiaW1hZ2VcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIkxhYmVsXCIsIGljb246XCJsYWJlbFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiU3RhdHVzIEZsYWdcIiwgaWNvbjpcImZsYWctdmFyaWFudFwiIH1cclxuXTtcclxuXHJcbmxldCB0aHJlZURTdWJtZW51ID0gW1xyXG4gICAgeyB2YWx1ZTpcIkJveFwiLCBpY29uOlwiYm94LTNkXCIgfSxcclxuICAgIHsgdmFsdWU6XCJTcGhlcmVcIiwgaWNvbjpcInNwaGVyZVwiIH0sXHJcbiAgICB7IHZhbHVlOlwiQ3lsaW5kZXJcIiwgaWNvbjpcImN5bGluZGVyXCIgfSxcclxuICAgIHsgdmFsdWU6XCJDb25lXCIsIGljb246XCJjb25lXCIgfSxcclxuICAgIHsgdmFsdWU6XCJUb3J1c1wiLCBpY29uOlwidG9ydXNcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIlR1YmVcIiwgaWNvbjpcInR1YmVcIiB9XHJcbl07XHJcblxyXG5sZXQgYWxpZ25tZW50U3VibWVudSA9IFtcclxuICAgIHsgdmFsdWU6XCJFZGdlIEFsaWduXCIsIGljb246XCJjb2RlXCIgfSxcclxuICAgIHsgdmFsdWU6XCJDZW50ZXIgQWxpZ25cIiwgaWNvbjpcImNvZGVcIiB9LFxyXG5dO1xyXG5cclxubGV0IGZpbGVNZW51ID0gW1xyXG4gICAgeyBpZDogXCJGaWxlLT5PcGVuXCIsIHZhbHVlOlwiT3BlblwiLCBpY29uOlwiZm9sZGVyLW9wZW5cIiB9LFxyXG4gICAgeyBpZDogXCJGaWxlLT5TYXZlXCIsIHZhbHVlOlwiU2F2ZVwiLCBpY29uOlwiZmxvcHB5XCIgfSxcclxuICAgIHsgaWQ6IFwiRmlsZS0+SW1wb3J0XCIsIHZhbHVlOlwiSW1wb3J0XCIsIGljb246XCJmaWxlLWltcG9ydFwiIH0sXHJcbiAgICB7IGlkOiBcIkZpbGUtPkV4cG9ydFwiLCB2YWx1ZTpcIkV4cG9ydFwiLCBpY29uOlwiZmlsZS1leHBvcnRcIiB9LFxyXG4gICAgeyBpZDogXCJGaWxlLT5QYWdlIFByb3BlcnRpZXNcIiwgdmFsdWU6XCJQYWdlIFByb3BlcnRpZXNcIiwgaWNvbjpcIi1cIiB9LFxyXG4gICAgeyBpZDogXCJGaWxlLT5QcmludFwiLCB2YWx1ZTpcIlByaW50XCIsIGljb246XCJwcmludGVyXCIgfSxcclxuICAgIHsgaWQ6IFwiRmlsZS0+T3B0aW9uc1wiLCB2YWx1ZTpcIk9wdGlvbnNcIiwgaWNvbjpcIi1cIiB9LFxyXG4gICAgeyBpZDogXCJGaWxlLT5FeGl0XCIsIHZhbHVlOlwiRXhpdFwiLCBpY29uOlwiZXhpdC10by1hcHBcIiB9XHJcbl07XHJcblxyXG5sZXQgZWRpdE1lbnUgPSBbXHJcbiAgICB7IHZhbHVlOlwiQ3V0XCIsIGljb246XCJjb250ZW50LWN1dFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiQ29weVwiLCBpY29uOlwiY29udGVudC1jb3B5XCIgfSxcclxuICAgIHsgdmFsdWU6XCJQYXN0ZVwiLCBpY29uOlwiY29udGVudC1wYXN0ZVwiIH0sXHJcbiAgICB7IHZhbHVlOlwiVW5kb1wiLCBpY29uOlwidW5kb1wiIH0sXHJcbiAgICB7IHZhbHVlOlwiUmVkb1wiLCBpY29uOlwicmVkb1wiIH0sXHJcbiAgICB7IHZhbHVlOlwiU2VsZWN0IEFsbFwiLCBpY29uOlwic2VsZWN0LWFsbFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiSW52ZXJ0IFNlbGVjdGlvblwiLCBpY29uOlwic2VsZWN0LWludmVyc2VcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIkZpbmRcIiwgaWNvbjpcImZpbGUtZmluZFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiRGVsZXRlXCIsIGljb246XCJkZWxldGVcIiB9XHJcbl07XHJcblxyXG5sZXQgdmlld01lbnUgPSBbXHJcbiAgICB7IHZhbHVlOlwiU2VsZWN0XCIsIGljb246XCJjdXJzb3ItZGVmYXVsdC1vdXRsaW5lXCIgfSxcclxuICAgIHsgdmFsdWU6XCJQYW5cIiwgaWNvbjpcInBhblwiIH0sXHJcbiAgICB7IHZhbHVlOlwiUm90YXRlIFZpZXdcIiwgaWNvbjpcInJvdGF0ZS13b3JsZFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiWm9vbSBJblwiLCBpY29uOlwibWFnbmlmeS1wbHVzLW91dGxpbmVcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIlpvb20gT3V0XCIsIGljb246XCJtYWduaWZ5LW1pbnVzLW91dGxpbmVcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIkZpdCB0byBQYWdlXCIsIGljb246XCJmaXQtdG8tc2NyZWVuXCIgfSxcclxuICAgIHsgdmFsdWU6XCJWaWV3c1wiLCBpY29uOlwidmlldy1xdWlsdFwiLCBzdWJtZW51OnZpZXdzU3VibWVudSB9LFxyXG4gICAgeyB2YWx1ZTpcIkFjdGl2ZSBWaWV3XCIsIGljb246XCItXCIsIHN1Ym1lbnU6YWN0aXZlVmlld1N1Ym1lbnUgfSxcclxuICAgIHsgdmFsdWU6XCJHcmlkXCIsIGljb246XCJncmlkXCIgfSxcclxuICAgIHsgdmFsdWU6XCJUZXN0IE1vZGVcIiwgaWNvbjpcInBsYXktYm94LW91dGxpbmVcIiB9XHJcbl07XHJcblxyXG5sZXQgZHJhd01lbnUgPSBbXHJcbiAgICB7IHZhbHVlOlwiMkQgR2FkZ2V0c1wiLCBpY29uOlwicmVjdGFuZ2xlXCIsIHN1Ym1lbnU6dHdvRFN1Ym1lbnUgfSxcclxuICAgIHsgdmFsdWU6XCIzRCBHYWRnZXRzXCIsIGljb246XCJib3gtM2RcIiwgc3VibWVudTp0aHJlZURTdWJtZW51IH0sXHJcbiAgICB7IHZhbHVlOlwiU3RhdGUgR2FkZ2V0XCIsIGljb246XCJ0cmFmZmljLWxpZ2h0XCIgfSxcclxuICAgIHsgdmFsdWU6XCJUYWJsZSBHYWRnZXRcIiwgaWNvbjpcInRhYmxlLWxhcmdlXCIgfSxcclxuICAgIHsgdmFsdWU6XCJDYW1lcmFcIiwgaWNvbjpcImZyZWUtY2FtZXJhXCIgfSxcclxuICAgIHsgdmFsdWU6XCJTcG90IExpZ2h0XCIsIGljb246XCJzcG90bGlnaHRcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIlBvaW50IExpZ2h0XCIsIGljb246XCJsaWdodGJ1bGJcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIlN5bWJvbCBMaWJyYXJ5XCIsIGljb246XCJoZXhhZ29uLW11bHRpcGxlXCIgfVxyXG5dO1xyXG5cclxubGV0IG1vZGlmeU1lbnUgPSBbXHJcbiAgICB7IHZhbHVlOlwiU2NhbGVcIiwgaWNvbjpcInNjYWxlLW9iamVjdFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiUm90YXRlXCIsIGljb246XCJyb3RhdGUtM2RcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIkdyb3VwXCIsIGljb246XCJncm91cFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiVW5ncm91cFwiLCBpY29uOlwidW5ncm91cFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiRGF0YSBBc3NpZ25cIiwgaWNvbjpcImRhdGFiYXNlXCIgfSxcclxuICAgIHsgdmFsdWU6XCJDb2xvclwiLCBpY29uOlwicGFsZXR0ZVwiIH0sXHJcbiAgICB7IHZhbHVlOlwiVGV4dFwiLCBpY29uOlwiZm9ybWF0LXRleHRcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIkR1cGxpY2F0ZVwiLCBpY29uOlwiY29udGVudC1kdXBsaWNhdGVcIiB9LFxyXG4gICAgeyB2YWx1ZTpcIkFsaWdubWVudFwiLCBpY29uOlwiZm9ybWF0LWFsaWduLW1pZGRsZVwiLCBzdWJtZW51OmFsaWdubWVudFN1Ym1lbnUgfSxcclxuICAgIHsgdmFsdWU6XCJXaXJlZnJhbWVcIiwgaWNvbjpcIndpcmVmcmFtZVwiIH0sXHJcbiAgICB7IHZhbHVlOlwiU2NyaXB0aW5nXCIsIGljb246XCJjb2RlLWJyYWNlc1wiIH0sXHJcbiAgICB7IHZhbHVlOlwiUHJvcGVydHkgUG9wdXBcIiwgaWNvbjpcIndyZW5jaFwiIH1cclxuXTtcclxuXHJcbmxldCBoZWxwTWVudSA9IFtcclxuICAgIHsgdmFsdWU6XCJDb250ZW50c1wiLCBpY29uOlwiYm9vay1vcGVuLXBhZ2UtdmFyaWFudFwiIH0sXHJcbiAgICB7IHZhbHVlOlwiQWJvdXRcIiwgaWNvbjpcImhlbHAtY2lyY2xlLW91dGxpbmVcIiB9XHJcbl07XHJcblxyXG5leHBvcnQgeyBmaWxlTWVudSwgZWRpdE1lbnUsIHZpZXdNZW51LCBkcmF3TWVudSwgbW9kaWZ5TWVudSwgaGVscE1lbnUgfTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zb3VyY2VzL21vZGVscy9tZW51RGF0YS5qcyIsIi8qIFByb2plY3Qgc3RvcmUgY29udGFpbiBwcm9qZWN0IGRhdGEgd2hpY2ggYXJlXHJcbiAgIHVzZWQgYnkgcHJvamVjdCBleHBsb3JlciB0byBwb3B1bGF0aW5nIHRoZSBkYXRhIGRpc3BsYXlcclxuICAgYW5kIGNvbnRyb2xsaW5nIHRoZSBzdGF0ZSBvZiB0aGUgb3BlbmVkIG1vZHVsZSAqL1xyXG5cclxubGV0IHByb2plY3RTdG9yZUNsYXNzID0gd2ViaXgucHJvdG8oe1xyXG4gICRpbml0OiBmdW5jdGlvbihjb25maWcpe1xyXG4gICAgLy9mdW5jdGlvbnMgZXhlY3V0ZWQgb24gY29tcG9uZW50IGluaXRpYWxpemF0aW9uXHJcbiAgfSxcclxuICBsb2FkU3RvcmU6IGZ1bmN0aW9uKCl7XHJcbiAgICAvL3BvcHVsYXRlIHRoZSBzdG9yZSBieSBwYXJzaW5nIHRoZSBjb25maWcgZmlsZVxyXG4gIH1cclxufSwgd2ViaXguVHJlZUNvbGxlY3Rpb24pO1xyXG5cclxuZXhwb3J0IGNvbnN0IHByb2plY3RTdG9yZSA9IG5ldyBwcm9qZWN0U3RvcmVDbGFzcyh7XHJcbiAgICBpZDpcInByb2plY3RTdG9yZVwiLFxyXG4gICAgZGF0YTpbIC8vdGVzdCBkYXRhXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZDpcInByb2plY3RcIiwgXHJcbiAgICAgICAgICAgIHZhbHVlOlwiV2lsbG93bHlueCBQcm9qZWN0XCIsIFxyXG4gICAgICAgICAgICBvcGVuOnRydWUsIFxyXG4gICAgICAgICAgICBpc0ZvbGRlcjp0cnVlLFxyXG4gICAgICAgICAgICBkYXRhOltcclxuICAgICAgICAgICAgICAgIHsgXHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6XCJkYXNoYm9hcmRcIiwgXHJcbiAgICAgICAgICAgICAgICAgICAgb3Blbjp0cnVlLCBcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTpcIkRhc2hib2FyZHNcIixcclxuICAgICAgICAgICAgICAgICAgICBpc0ZvbGRlcjp0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6W1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJkYXNoYm9hcmQxXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJNYWluIFBhZ2VcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJkYXNoYm9hcmQyXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJSZXBvcnRzXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJkYXNoYm9hcmQzXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJUcmVuZFwiIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHsgXHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6XCJzY2hlbWF0aWNzXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgIG9wZW46dHJ1ZSwgXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJTY2hlbWF0aWNzXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgIGlzRm9sZGVyOnRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YTpbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDpcInNjaGVtYXRpYzFcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOlwiUDAwLmpzb25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhdGg6XCJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZHVsZTpcInNjaGVtYXRpY1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BlbmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpZGRlbjogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6XCJzY2hlbWF0aWMyXCIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XCJQMDFfTWFwX092ZXJ2aWV3Lmpzb25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhdGg6XCJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZHVsZTpcInNjaGVtYXRpY1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BlbmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpZGRlbjogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfVxyXG4gICAgXVxyXG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zb3VyY2VzL21vZGVscy9wcm9qZWN0U3RvcmUuanMiLCIvKiBUaGUgcHJvcGVydHkgc3RvcmUgbmVlZCB0byBiZSBkeW5hbWljYWxseSB1cGRhdGVkXHJcbiAgIGFjY29yZGluZyB0byB0aGUgY3VycmVudGx5IHNlbGVjdGVkIG9iamVjdC4gVGhpc1xyXG4gICBjYW4gYmUgZG9uZSBieSBwYXNzaW5nIGRhdGEgdG8gdXBkYXRlRWxlbWVudHMoKSAqL1xyXG5cclxubGV0IHByb3BlcnR5U3RvcmVDbGFzcyA9IHdlYml4LnByb3RvKHtcclxuICAkaW5pdDogZnVuY3Rpb24oY29uZmlnKXtcclxuICAgIC8vZnVuY3Rpb25zIGV4ZWN1dGVkIG9uIGNvbXBvbmVudCBpbml0aWFsaXphdGlvblxyXG4gICAgdGhpcy5zaG93QWxsUHJvcHMgPSBmYWxzZTtcclxuICAgIHRoaXMuZmlsdGVySW5wdXQgPSAnJztcclxuICB9LFxyXG4gIGRlZmF1bHRzOntcclxuICAgIG9uOnsnb25CaW5kUmVxdWVzdCcgOiBmdW5jdGlvbigpe3RoaXMuc2hvd0FsbCgpfX1cclxuICB9LFxyXG4gIC8qIERhdGEgZm9ybWF0XHJcbiAgICAgW3sgaWQ6IHVuaXF1ZSBpZCB0byBiZSByZWZlcnJlZCBmb3IgZGF0YSB1cGRhdGUsXHJcbiAgICAgICAgbGFiZWw6IHRoZSBwcm9wZXJ0eSBuYW1lLFxyXG4gICAgICAgIHZhbHVlOiB0aGUgcHJvcGVydGllJ3MgdmFsdWUsXHJcbiAgICAgICAgdHlwZTogV2ViaXgncyBpbnB1dCBlZGl0b3IgdHlwZSxcclxuICAgICAgICBzaW1wbGU6IHNldCB0byB0cnVlIGlmIHRoaXMgcHJvcGVydHkgaXMgZm9yXHJcbiAgICAgICAgICAgICAgICBzaW1wbGUgY29uZmlndXJhdGlvblxyXG4gICAgIH1dICovXHJcbiAgdXBkYXRlRWxlbWVudHM6IGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgICBpZihkYXRhLmxlbmd0aCl7XHJcbiAgICAgICAgdGhpcy5wYXJzZShkYXRhKTtcclxuICAgICAgICB0aGlzLnNob3dBbGwoKTtcclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgdGhpcy5jbGVhckFsbCgpO1xyXG4gICAgICB9XHJcbiAgfSxcclxuICAvKiB1cGRhdGUgdmlzaWJsZSBkYXRhIGJhc2VkIG9uIGZpbHRlciAqL1xyXG4gIHNob3dBbGw6IGZ1bmN0aW9uKHN0YXR1cyA9IHRoaXMuc2hvd0FsbFByb3BzKXtcclxuICAgICAgdGhpcy5zaG93QWxsUHJvcHMgPSBzdGF0dXM7XHJcbiAgICAgIGxldCB2YWx1ZSA9IHRoaXMuZmlsdGVySW5wdXQ7XHJcbiAgICAgIHRoaXMuZmlsdGVyKGZ1bmN0aW9uKG9iail7XHJcbiAgICAgICAgICByZXR1cm4gb2JqLmxhYmVsLnRvTG93ZXJDYXNlKCkuaW5kZXhPZih2YWx1ZSkgIT09IC0xXHJcbiAgICAgICAgICAgICAgICAgICYmIChzdGF0dXMgfHwgb2JqLnNpbXBsZSAhPSBzdGF0dXMpO1xyXG4gICAgICB9KTtcclxuICB9LFxyXG4gIGZpbHRlckxhYmVsOiBmdW5jdGlvbih2YWx1ZSl7XHJcbiAgICAgIHRoaXMuZmlsdGVySW5wdXQgPSB2YWx1ZTtcclxuICAgICAgbGV0IHN0YXR1cyA9IHRoaXMuc2hvd0FsbFByb3BzO1xyXG4gICAgICB0aGlzLmZpbHRlcihmdW5jdGlvbihvYmope1xyXG4gICAgICAgICAgcmV0dXJuIG9iai5sYWJlbC50b0xvd2VyQ2FzZSgpLmluZGV4T2YodmFsdWUpICE9PSAtMVxyXG4gICAgICAgICAgICAgICAgICAmJiAoc3RhdHVzIHx8IG9iai5zaW1wbGUgIT0gc3RhdHVzKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG59LCB3ZWJpeC5EYXRhQ29sbGVjdGlvbik7XHJcblxyXG5leHBvcnQgY29uc3QgcHJvcGVydHlTdG9yZSA9IG5ldyBwcm9wZXJ0eVN0b3JlQ2xhc3Moe1xyXG4gICAgaWQ6XCJwcm9wZXJ0eVN0b3JlXCIsXHJcbiAgICBkYXRhOiBbXSxcclxufSk7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NvdXJjZXMvbW9kZWxzL3Byb3BlcnR5U3RvcmUuanMiLCJpbXBvcnQge0pldFZpZXd9IGZyb20gXCJ3ZWJpeC1qZXRcIjtcclxuaW1wb3J0IHtzY2hlbWF0aWNTdG9yZX0gZnJvbSBcIi4uL21vZGVscy9zY2hlbWF0aWNTdG9yZVwiXHJcblxyXG53ZWJpeC5wcm90b1VJKHtcclxuICAgIG5hbWU6XCJzY2hlbWF0aWNcIixcclxuICAgICRpbml0OmZ1bmN0aW9uKGNvbmZpZyl7XHJcbiAgICAgICAgLy9jcmVhdGluZyBwcm9taXNlXHJcbiAgICAgICAgdGhpcy5wcm9taXNlID0gd2ViaXgucHJvbWlzZS5kZWZlcigpLnRoZW4od2ViaXguYmluZChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAvL2NyZWF0ZSB0aGUgbW9kdWxlIHN0b3JlXHJcbiAgICAgICAgICAgIGxldCBzdG9yZWlkID0gY29uZmlnLmlkICsgXCJTdG9yZVwiO1xyXG4gICAgICAgICAgICBsZXQgc3RvcmUgPSBuZXcgc2NoZW1hdGljU3RvcmUoe1xyXG4gICAgICAgICAgICAgICAgaWQ6c3RvcmVpZCxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvL2luaXQgbGliIHdoZW4gaXQgaXMgcmVhZHlcclxuICAgICAgICAgICAgbGV0IHNjZW5lID0gbmV3IFRIUkVFLlNjZW5lKCk7XHJcbiAgICAgICAgICAgIGxldCBjYW1lcmEgPSBuZXcgVEhSRUUuUGVyc3BlY3RpdmVDYW1lcmEoIDc1LCB3aW5kb3cuaW5uZXJXaWR0aC93aW5kb3cuaW5uZXJIZWlnaHQsIDAuMSwgMTAwMCApO1xyXG5cclxuICAgICAgICAgICAgbGV0IHJheWNhc3RlciA9IG5ldyBUSFJFRS5SYXljYXN0ZXIoKTtcclxuICAgICAgICAgICAgbGV0IHJlbmRlcmVyID0gbmV3IFRIUkVFLldlYkdMUmVuZGVyZXIoKTtcclxuICAgICAgICAgICAgcmVuZGVyZXIuc2V0U2l6ZSggdGhpcy4kd2lkdGgsIHRoaXMuJGhlaWdodCApO1xyXG4gICAgICAgICAgICB0aGlzLiR2aWV3LmFwcGVuZENoaWxkKCByZW5kZXJlci5kb21FbGVtZW50ICk7XHJcblxyXG4gICAgICAgICAgICAvL0V4YW1wbGUgb24gMSBvYmplY3QgKGN1YmUpIGhhbmRsaW5nXHJcbiAgICAgICAgICAgIGxldCBjdWJlMSA9IHtcclxuICAgICAgICAgICAgICAgICAgICB1aWQ6IFwiY3ViZTFcIixcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiBcImN1YmUxXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lyZWZyYW1lOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMSxcclxuICAgICAgICAgICAgICAgICAgICBkZXB0aDogMSxcclxuICAgICAgICAgICAgICAgICAgICBmcHM6IDMwLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnIzAwZmYwMCdcclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBsZXQgZ2VvbWV0cnkgPSBuZXcgVEhSRUUuQm94R2VvbWV0cnkoIGN1YmUxLndpZHRoLCBjdWJlMS5oZWlnaHQsIGN1YmUxLmRlcHRoICk7XHJcbiAgICAgICAgICAgIGxldCBtYXRlcmlhbCA9IG5ldyBUSFJFRS5NZXNoQmFzaWNNYXRlcmlhbCgpO1xyXG4gICAgICAgICAgICBtYXRlcmlhbC5jb2xvci5zZXRIZXgoY3ViZTEuY29sb3IucmVwbGFjZSgvIy9nICwgXCIweFwiKSk7XHJcbiAgICAgICAgICAgIGxldCBjdWJlID0gbmV3IFRIUkVFLk1lc2goIGdlb21ldHJ5LCBtYXRlcmlhbCApO1xyXG4gICAgICAgICAgICBzY2VuZS5hZGQoIGN1YmUgKTtcclxuICAgICAgICAgICAgY3ViZS5uYW1lID0gY3ViZTEubmFtZTtcclxuICAgICAgICAgICAgc3RvcmUuYWRkQ3ViZVByb3BzKGN1YmUxKTtcclxuXHJcbiAgICAgICAgICAgIGxldCBvdXRsaW5lTWF0ZXJpYWwgPSBuZXcgVEhSRUUuTWVzaEJhc2ljTWF0ZXJpYWwoIHsgY29sb3I6IDB4ZmYwMDAwLCB3aXJlZnJhbWU6IGN1YmUxLndpcmVmcmFtZSB9ICk7XHJcbiAgICAgICAgICAgIGxldCBvdXRsaW5lID0gbmV3IFRIUkVFLk1lc2goIGdlb21ldHJ5LCBvdXRsaW5lTWF0ZXJpYWwgKTtcclxuICAgICAgICAgICAgb3V0bGluZS5uYW1lID0gJ3dpcmVmcmFtZSc7XHJcbiAgICAgICAgICAgIHNjZW5lLmFkZCggb3V0bGluZSApO1xyXG5cclxuICAgICAgICAgICAgY2FtZXJhLnBvc2l0aW9uLnogPSA1O1xyXG5cclxuICAgICAgICAgICAgbGV0IHN0b3AgPSBmYWxzZTtcclxuICAgICAgICAgICAgbGV0IGZwc0ludGVydmFsLCBsYXN0RHJhd1RpbWU7XHJcblxyXG4gICAgICAgICAgICAvL2hhbmRsZSBzdG9yZSB1cGRhdGVcclxuICAgICAgICAgICAgc3RvcmUuZGF0YS5hdHRhY2hFdmVudChcIm9uU3RvcmVVcGRhdGVkXCIsIGZ1bmN0aW9uKGlkLCBvYmope1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoKGlkKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIHN0b3JlLmdldERhdGFJZChcImZwc1wiKTpcclxuICAgICAgICAgICAgICAgICAgICBsZXQgbmV3X2ZwcyA9IG9iai52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICBmcHNJbnRlcnZhbCA9IDEwMDAgLyBuZXdfZnBzO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIHN0b3JlLmdldERhdGFJZChcImNvbG9yXCIpOlxyXG4gICAgICAgICAgICAgICAgICAgIGN1YmUubWF0ZXJpYWwuY29sb3Iuc2V0SGV4KG9iai52YWx1ZS5yZXBsYWNlKC8jL2cgLCBcIjB4XCIpKTsgXHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2Ugc3RvcmUuZ2V0RGF0YUlkKFwid2lyZWZyYW1lXCIpOlxyXG4gICAgICAgICAgICAgICAgICAgIGlmKCFvYmoudmFsdWUpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2VuZS5yZW1vdmUoIG91dGxpbmUgKTtcclxuICAgICAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2NlbmUuYWRkKCBvdXRsaW5lICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIHN0b3JlLmdldERhdGFJZChcIndpZHRoXCIpOlxyXG4gICAgICAgICAgICAgICAgICAgIGN1YmUuc2NhbGUuc2V0WChvYmoudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIHN0b3JlLmdldERhdGFJZChcImhlaWdodFwiKTpcclxuICAgICAgICAgICAgICAgICAgICBjdWJlLnNjYWxlLnNldFkob2JqLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBzdG9yZS5nZXREYXRhSWQoXCJkZXB0aFwiKTpcclxuICAgICAgICAgICAgICAgICAgICBjdWJlLnNjYWxlLnNldFoob2JqLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBsZXQgbW91c2UgPSBuZXcgVEhSRUUuVmVjdG9yMigpLCBJTlRFUlNFQ1RFRDtcclxuXHJcbiAgICAgICAgICAgIC8vaW5pdGlhbGl6ZSB0aGUgdGltZXIgdmFyaWFibGVzIGFuZCBzdGFydCB0aGUgYW5pbWF0aW9uXHJcbiAgICAgICAgICAgIGxldCBzdGFydEFuaW1hdGluZyA9IGZwcyA9PiB7XHJcbiAgICAgICAgICAgICAgICBmcHNJbnRlcnZhbCA9IDEwMDAgLyBmcHM7XHJcbiAgICAgICAgICAgICAgICBsYXN0RHJhd1RpbWUgPSBwZXJmb3JtYW5jZS5ub3coKTtcclxuICAgICAgICAgICAgICAgIGFuaW1hdGUoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy9kcmF3IGxvb3BcclxuICAgICAgICAgICAgbGV0IGFuaW1hdGUgPSBub3cgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKCBhbmltYXRlICk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy9jYWxjIGVsYXBzZWQgdGltZSBzaW5jZSBsYXN0IGxvb3BcclxuICAgICAgICAgICAgICAgIGxldCBlbGFwc2VkID0gbm93IC0gbGFzdERyYXdUaW1lO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vaWYgZW5vdWdoIHRpbWUgaGFzIGVsYXBzZWQsIGRyYXcgdGhlIG5leHQgZnJhbWVcclxuICAgICAgICAgICAgICAgIGlmIChlbGFwc2VkID4gZnBzSW50ZXJ2YWwpIHtcclxuICAgICAgICAgICAgICAgICAgICBsYXN0RHJhd1RpbWUgPSBub3cgLSAoZWxhcHNlZCAlIGZwc0ludGVydmFsKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgY3ViZS5yb3RhdGlvbi54ICs9IDAuMTtcclxuICAgICAgICAgICAgICAgICAgICBjdWJlLnJvdGF0aW9uLnkgKz0gMC4xO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHNjZW5lLmdldE9iamVjdEJ5TmFtZSgnd2lyZWZyYW1lJykpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdXRsaW5lLnJvdGF0aW9uLmNvcHkoY3ViZS5yb3RhdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG91dGxpbmUuc2NhbGUuY29weShjdWJlLnNjYWxlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlcmVyLnJlbmRlcihzY2VuZSwgY2FtZXJhKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGxldCBvbldpbmRvd1Jlc2l6ZSA9ICgpID0+IHtcclxuICAgICAgICAgICAgICAgIGNhbWVyYS5hc3BlY3QgPSB0aGlzLiR3aWR0aCAvIHRoaXMuJGhlaWdodDtcclxuICAgICAgICAgICAgICAgIGNhbWVyYS51cGRhdGVQcm9qZWN0aW9uTWF0cml4KCk7XHJcbiAgICAgICAgICAgICAgICByZW5kZXJlci5zZXRTaXplKCB0aGlzLiR3aWR0aCwgdGhpcy4kaGVpZ2h0ICk7XHJcbiAgICAgICAgICAgIH0gXHJcbiAgICAgICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAncmVzaXplJywgb25XaW5kb3dSZXNpemUsIGZhbHNlICk7XHJcblxyXG4gICAgICAgICAgICBsZXQgb25Nb3VzZURvd24gPSBldmVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgbW91c2UueCA9ICggZXZlbnQuY2xpZW50WCAvIHdpbmRvdy5pbm5lcldpZHRoICkgKiAyIC0gMTtcclxuICAgICAgICAgICAgICAgIG1vdXNlLnkgPSAtICggZXZlbnQuY2xpZW50WSAvIHdpbmRvdy5pbm5lckhlaWdodCApICogMiArIDE7XHJcbiAgICAgICAgICAgICAgICAvLyBmaW5kIGludGVyc2VjdGlvbnNcclxuICAgICAgICAgICAgICAgIHJheWNhc3Rlci5zZXRGcm9tQ2FtZXJhKCBtb3VzZSwgY2FtZXJhICk7XHJcbiAgICAgICAgICAgICAgICBsZXQgaW50ZXJzZWN0cyA9IHJheWNhc3Rlci5pbnRlcnNlY3RPYmplY3RzKCBzY2VuZS5jaGlsZHJlbiApO1xyXG4gICAgICAgICAgICAgICAgaWYgKCBpbnRlcnNlY3RzLmxlbmd0aCA+IDAgKSB7XHJcbiAgICAgICAgICAgICAgICAgIGlmICggSU5URVJTRUNURUQgIT0gaW50ZXJzZWN0c1sgMCBdLm9iamVjdCApIHtcclxuICAgICAgICAgICAgICAgICAgICBJTlRFUlNFQ1RFRCA9IGludGVyc2VjdHNbIDAgXS5vYmplY3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgc3RvcmUuc2VsZWN0T2JqKElOVEVSU0VDVEVELm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgc3RvcmUuZGVzZWxlY3RPYmooKTtcclxuICAgICAgICAgICAgICAgICAgICAgIElOVEVSU0VDVEVEID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lciggJ21vdXNlZG93bicsIG9uTW91c2VEb3duLCBmYWxzZSApO1xyXG5cclxuICAgICAgICAgICAgc3RhcnRBbmltYXRpbmcoMzApO1xyXG5cclxuICAgICAgICAgICAgLy9jYWxsIGFuIG9wdGlvbmFsIGNhbGxiYWNrIGNvZGVcclxuICAgICAgICAgICAgaWYgKHRoaXMuY29uZmlnLnJlYWR5KVxyXG4gICAgICAgICAgICAgICAgdGhpcy5jb25maWcucmVhZHkuY2FsbCh0aGlzLCBzY2VuZSk7XHJcblxyXG4gICAgICAgIH0sIHRoaXMpKTtcclxuXHJcbiAgICAgICAgLy9pZiBmaWxlIGlzIGFscmVhZHkgbG9hZGVkLCByZXNvbHZlIHByb21pc2VcclxuICAgICAgICBpZiAod2luZG93LlRIUkVFKVxyXG4gICAgICAgICAgICB0aGlzLnByb21pc2UucmVzb2x2ZSgpO1xyXG4gICAgICAgIGVsc2VcclxuICAgICAgICAgICAgLy93YWl0IGZvciBkYXRhIGxvYWRpbmcgYW5kIHJlc29sdmUgcHJvbWlzZSBhZnRlciBpdFxyXG4gICAgICAgICAgICB3ZWJpeC5yZXF1aXJlKFwidGhyZWUubWluLmpzXCIsIGZ1bmN0aW9uKCl7IHRoaXMucHJvbWlzZS5yZXNvbHZlKCk7IH0sIHRoaXMpO1xyXG4gICAgfVxyXG59LCB3ZWJpeC51aS52aWV3KTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNjaGVtYXRpY1ZpZXcgZXh0ZW5kcyBKZXRWaWV3e1xyXG4gICAgY29uZmlnKCl7XHJcbiAgICAgICAgcmV0dXJuIHsgaWQ6XCJzY2hlbWF0aWNcIiwgdmlldzpcInNjaGVtYXRpY1wiIH07XHJcbiAgICB9XHJcbn07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NvdXJjZXMvbW9kdWxlcy9zY2hlbWF0aWMvdmlld3Mvc2NoZW1hdGljLmpzIiwiLyogU2NoZW1hdGljIHN0b3JlIGlzIGFuIGV4YW1wbGUgb2YgbW9kdWxlIGJhc2VkIHN0b3JlLlxyXG4gICBDdXJyZW50bHkgaXQncyBkZXNpZ25lZCB0byBzdG9yZSBkYXRhIG9mIHRoZSBzZWxlY3RlZCBjb21wb25lbnQqL1xyXG5cclxuLy90ZW1wb3JhcnkgZm9yIHRlc3RpbmcuIHRoZSBqc29uIGZpbGUgd2lsbCBiZSBsb2FkZWQgYnkgbmF0aXZlIGZ1bmN0aW9uXHJcbmltcG9ydCB0ZXN0RmlsZSBmcm9tIFwiLi4vdGVzdEpTT04vUDAwX0hvbWVQYWdlLmpzb25cIlxyXG5cclxuZXhwb3J0IGNvbnN0IHNjaGVtYXRpY1N0b3JlID0gd2ViaXgucHJvdG8oe1xyXG4gICRpbml0OiBmdW5jdGlvbihjb25maWcpe1xyXG4gICAgLy9mdW5jdGlvbnMgZXhlY3V0ZWQgb24gY29tcG9uZW50IGluaXRpYWxpemF0aW9uXHJcbiAgICB0aGlzLmdhZGdldHMgPSBuZXcgTWFwKCk7XHJcbiAgfSxcclxuICBnZXREYXRhSWQ6IGZ1bmN0aW9uKGlkKXtcclxuICAgIHJldHVybiB0aGlzLmNvbmZpZy5pZCtcIi1cIitpZDtcclxuICB9LFxyXG4gIGdldERhdGFWYWx1ZTogZnVuY3Rpb24oaWQpe1xyXG4gICAgbGV0IHVpZCA9IHRoaXMuY29uZmlnLmlkK1wiLVwiK2lkO1xyXG4gICAgcmV0dXJuIHRoaXMuZ2V0SXRlbSh1aWQpLnZhbHVlO1xyXG4gIH0sXHJcbiAgbG9hZFN0b3JlOiBmdW5jdGlvbigpe1xyXG4gICAgLy8gcG9wdWxhdGUgdGhlIHN0b3JlLiBlZzpcclxuICAgIHRlc3RGaWxlLkxheWVyLmZvckVhY2goZnVuY3Rpb24oaXRlbSwgaW5kZXgsIGFycmF5KSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKGl0ZW0sIGluZGV4KTtcclxuICAgIH0pO1xyXG4gIH0sXHJcbiAgYWRkQ3ViZVByb3BzOiBmdW5jdGlvbih7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVpZCA9ICd1bmtub3duJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZSA9ICd1bmtub3duJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lyZWZyYW1lID0gdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGggPSAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQgPSAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXB0aCA9IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZwcyA9IDMwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvciA9ICcjMDBmZjAwJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgfSl7XHJcbiAgICBsZXQgcHJvcHMgPSBbXHJcbiAgICAgICAgICAgIHsgaWQ6dGhpcy5nZXREYXRhSWQoXCJuYW1lXCIpLCBsYWJlbDpcIk5hbWVcIiwgdmFsdWU6bmFtZSwgdHlwZTpcInRleHRcIiwgc2ltcGxlOnRydWUgfSxcclxuICAgICAgICAgICAgeyBpZDp0aGlzLmdldERhdGFJZChcIndpZHRoXCIpLCBsYWJlbDpcIldpZHRoXCIsIHZhbHVlOndpZHRoLCB0eXBlOlwidGV4dFwiLCBzaW1wbGU6dHJ1ZSB9LFxyXG4gICAgICAgICAgICB7IGlkOnRoaXMuZ2V0RGF0YUlkKFwiaGVpZ2h0XCIpLCBsYWJlbDpcIkhlaWdodFwiLCB2YWx1ZTpoZWlnaHQsIHR5cGU6XCJ0ZXh0XCIsIHNpbXBsZTp0cnVlIH0sXHJcbiAgICAgICAgICAgIHsgaWQ6dGhpcy5nZXREYXRhSWQoXCJkZXB0aFwiKSwgbGFiZWw6XCJEZXB0aFwiLCB2YWx1ZTpkZXB0aCwgdHlwZTpcInRleHRcIiwgc2ltcGxlOnRydWUgfSxcclxuICAgICAgICAgICAgeyBpZDp0aGlzLmdldERhdGFJZChcIndpcmVmcmFtZVwiKSwgbGFiZWw6XCJXaXJlZnJhbWVcIiwgdmFsdWU6d2lyZWZyYW1lLCB0eXBlOlwiY2hlY2tib3hcIiwgc2ltcGxlOmZhbHNlIH0sXHJcbiAgICAgICAgICAgIHsgaWQ6dGhpcy5nZXREYXRhSWQoXCJmcHNcIiksIGxhYmVsOlwiRlBTXCIsIHZhbHVlOmZwcywgdHlwZTpcInRleHRcIiwgc2ltcGxlOmZhbHNlIH0sXHJcbiAgICAgICAgICAgIHsgaWQ6dGhpcy5nZXREYXRhSWQoXCJjb2xvclwiKSwgbGFiZWw6XCJDb2xvclwiLCB2YWx1ZTpjb2xvciwgdHlwZTpcImNvbG9yXCIsIHNpbXBsZTp0cnVlIH1cclxuICAgIF07XHJcbiAgICB0aGlzLmdhZGdldHMuc2V0KHVpZCwgcHJvcHMpO1xyXG4gIH0sXHJcbiAgc2VsZWN0T2JqOiBmdW5jdGlvbih1aWQpe1xyXG4gICAgaWYodGhpcy5nYWRnZXRzLmhhcyh1aWQpKXtcclxuICAgICAgbGV0IGRhdGEgPSB0aGlzLmdhZGdldHMuZ2V0KHVpZCk7XHJcbiAgICAgIHRoaXMucGFyc2UoZGF0YSk7XHJcbiAgICAgICQkKCdwcm9wZXJ0eVN0b3JlJykudXBkYXRlRWxlbWVudHMoZGF0YSk7XHJcbiAgICAgICQkKCdwcm9wZXJ0eVN0b3JlJykuZGF0YS5zeW5jKHRoaXMuZGF0YSk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgY29uc29sZS5sb2codWlkLCdub3QgZm91bmQhJyk7XHJcbiAgICB9XHJcbiAgfSxcclxuICBkZXNlbGVjdE9iajogZnVuY3Rpb24oKXtcclxuICAgICQkKCdwcm9wZXJ0eVN0b3JlJykudXBkYXRlRWxlbWVudHMoW10pO1xyXG4gICAgdGhpcy5kYXRhLnVuc3luYygpO1xyXG4gIH1cclxufSwgd2ViaXguRGF0YUNvbGxlY3Rpb24pO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zb3VyY2VzL21vZHVsZXMvc2NoZW1hdGljL21vZGVscy9zY2hlbWF0aWNTdG9yZS5qcyIsIm1vZHVsZS5leHBvcnRzID0ge1wiZGVjb2RlclwiOntcInZlcnNpb25cIjoxfSxcIlNjaGVtYXRpY0hlYWRlclwiOntcInZlcnNpb25cIjoxLFwidHlwZVwiOlwiQlNGXCIsXCJzaXplXCI6WzI3ODQsMTMyMCw1MDBdLFwiYmFja2NvbG9yXCI6WzAsMCwwXX0sXCJMYXllclwiOlt7XCJ2ZXJzaW9uXCI6MSxcIm5hbWVcIjpcIlwiLFwidmFsdWVcIjowLFwiZ2FkZ2V0XCI6W3tcInZlcnNpb25cIjoxLFwidHlwZVwiOlwiUkVDVEFOR0xFXCIsXCJuYW1lXCI6XCJSRUNUQU5HTEUgMFwiLFwiZ2FkZ2V0TnVtXCI6MjAwMyxcInNjYWxlXCI6WzEsMSwxXSxcInJvdGF0aW9uXCI6WzAsMCwwXSxcInBvc2l0aW9uXCI6Wy03MjUsMjI1LDBdLFwiZ2VvbWV0cnlcIjp7XCJ3aWR0aFwiOjU1LFwiaGVpZ2h0XCI6MzV9LFwiYW5pbWF0aW9uXCI6e1wiY29sb3JhdGlvblwiOjAsXCJibGlua2luZ1wiOjB9LFwiY29sb3JcIjpbMTk2LDE5NiwxOTZdLFwiZGFya25lc3NcIjoxMDAsXCJvcGFjaXR5XCI6MjU1LFwid2lyZWZyYW1lXCI6MCxcImRhdGFcIjp7XCJyb3dpZFwiOlswLDMwMTk5MTM1OV19fSx7XCJ2ZXJzaW9uXCI6MSxcInR5cGVcIjpcIlJFQ1RBTkdMRVwiLFwibmFtZVwiOlwiUkVDVEFOR0xFIDBcIixcImdhZGdldE51bVwiOjIwMDQsXCJzY2FsZVwiOlsxLDEsMV0sXCJyb3RhdGlvblwiOlswLDAsMF0sXCJwb3NpdGlvblwiOlstNzI1LDE3MCwwXSxcImdlb21ldHJ5XCI6e1wid2lkdGhcIjo1NSxcImhlaWdodFwiOjM1fSxcImFuaW1hdGlvblwiOntcImNvbG9yYXRpb25cIjowLFwiYmxpbmtpbmdcIjowfSxcImNvbG9yXCI6WzE5NiwxOTYsMTk2XSxcImRhcmtuZXNzXCI6MTAwLFwib3BhY2l0eVwiOjI1NSxcIndpcmVmcmFtZVwiOjAsXCJkYXRhXCI6e1wicm93aWRcIjpbMCwzMDE5OTEzNjBdfX0se1widmVyc2lvblwiOjEsXCJ0eXBlXCI6XCJSRUNUQU5HTEVcIixcIm5hbWVcIjpcIlJFQ1RBTkdMRSAwXCIsXCJnYWRnZXROdW1cIjoyMDA1LFwic2NhbGVcIjpbMSwxLDFdLFwicm90YXRpb25cIjpbMCwwLDBdLFwicG9zaXRpb25cIjpbLTcyNSwxMTAsMF0sXCJnZW9tZXRyeVwiOntcIndpZHRoXCI6NTUsXCJoZWlnaHRcIjozNX0sXCJhbmltYXRpb25cIjp7XCJjb2xvcmF0aW9uXCI6MCxcImJsaW5raW5nXCI6MH0sXCJjb2xvclwiOlsxOTYsMTk2LDE5Nl0sXCJkYXJrbmVzc1wiOjEwMCxcIm9wYWNpdHlcIjoyNTUsXCJ3aXJlZnJhbWVcIjowLFwiZGF0YVwiOntcInJvd2lkXCI6WzAsMzAxOTkxMzYxXX19LHtcInZlcnNpb25cIjoxLFwidHlwZVwiOlwiUkVDVEFOR0xFXCIsXCJuYW1lXCI6XCJSRUNUQU5HTEUgMFwiLFwiZ2FkZ2V0TnVtXCI6MjAwNixcInNjYWxlXCI6WzEsMSwxXSxcInJvdGF0aW9uXCI6WzAsMCwwXSxcInBvc2l0aW9uXCI6Wy03MjUsNTAsMF0sXCJnZW9tZXRyeVwiOntcIndpZHRoXCI6NTUsXCJoZWlnaHRcIjozNX0sXCJhbmltYXRpb25cIjp7XCJjb2xvcmF0aW9uXCI6MCxcImJsaW5raW5nXCI6MH0sXCJjb2xvclwiOlsxOTYsMTk2LDE5Nl0sXCJkYXJrbmVzc1wiOjEwMCxcIm9wYWNpdHlcIjoyNTUsXCJ3aXJlZnJhbWVcIjowLFwiZGF0YVwiOntcInJvd2lkXCI6WzAsMzAxOTkxMzYyXX19LHtcInZlcnNpb25cIjoxLFwidHlwZVwiOlwiUkVDVEFOR0xFXCIsXCJuYW1lXCI6XCJSRUNUQU5HTEUgMFwiLFwiZ2FkZ2V0TnVtXCI6MjAwNyxcInNjYWxlXCI6WzEsMSwxXSxcInJvdGF0aW9uXCI6WzAsMCwwXSxcInBvc2l0aW9uXCI6Wy03MjUsLTUsMF0sXCJnZW9tZXRyeVwiOntcIndpZHRoXCI6NTUsXCJoZWlnaHRcIjozNX0sXCJhbmltYXRpb25cIjp7XCJjb2xvcmF0aW9uXCI6MCxcImJsaW5raW5nXCI6MH0sXCJjb2xvclwiOlsxOTYsMTk2LDE5Nl0sXCJkYXJrbmVzc1wiOjEwMCxcIm9wYWNpdHlcIjoyNTUsXCJ3aXJlZnJhbWVcIjowLFwiZGF0YVwiOntcInJvd2lkXCI6WzAsMzAxOTkxMzYzXX19LHtcInZlcnNpb25cIjoxLFwidHlwZVwiOlwiUkVDVEFOR0xFXCIsXCJuYW1lXCI6XCJSRUNUQU5HTEUgMFwiLFwiZ2FkZ2V0TnVtXCI6MjAwOCxcInNjYWxlXCI6WzEsMSwxXSxcInJvdGF0aW9uXCI6WzAsMCwwXSxcInBvc2l0aW9uXCI6Wy03MjUsLTYwLDBdLFwiZ2VvbWV0cnlcIjp7XCJ3aWR0aFwiOjU1LFwiaGVpZ2h0XCI6MzV9LFwiYW5pbWF0aW9uXCI6e1wiY29sb3JhdGlvblwiOjAsXCJibGlua2luZ1wiOjB9LFwiY29sb3JcIjpbMTk2LDE5NiwxOTZdLFwiZGFya25lc3NcIjoxMDAsXCJvcGFjaXR5XCI6MjU1LFwid2lyZWZyYW1lXCI6MCxcImRhdGFcIjp7XCJyb3dpZFwiOlswLDMwMTk5MTM2NF19fSx7XCJ2ZXJzaW9uXCI6MSxcInR5cGVcIjpcIlJFQ1RBTkdMRVwiLFwibmFtZVwiOlwiUkVDVEFOR0xFIDBcIixcImdhZGdldE51bVwiOjIwMDksXCJzY2FsZVwiOlsxLDEsMV0sXCJyb3RhdGlvblwiOlswLDAsMF0sXCJwb3NpdGlvblwiOlstNzI1LC0xMTUsMF0sXCJnZW9tZXRyeVwiOntcIndpZHRoXCI6NTUsXCJoZWlnaHRcIjozNX0sXCJhbmltYXRpb25cIjp7XCJjb2xvcmF0aW9uXCI6MCxcImJsaW5raW5nXCI6MH0sXCJjb2xvclwiOlsxOTYsMTk2LDE5Nl0sXCJkYXJrbmVzc1wiOjEwMCxcIm9wYWNpdHlcIjoyNTUsXCJ3aXJlZnJhbWVcIjowLFwiZGF0YVwiOntcInJvd2lkXCI6WzAsMzAxOTkxMzg2XX19LHtcInZlcnNpb25cIjoxLFwidHlwZVwiOlwiUkVDVEFOR0xFXCIsXCJuYW1lXCI6XCJSRUNUQU5HTEUgMFwiLFwiZ2FkZ2V0TnVtXCI6MjAxMCxcInNjYWxlXCI6WzEsMSwxXSxcInJvdGF0aW9uXCI6WzAsMCwwXSxcInBvc2l0aW9uXCI6Wy03MjUsLTE3NSwwXSxcImdlb21ldHJ5XCI6e1wid2lkdGhcIjo1NSxcImhlaWdodFwiOjM1fSxcImFuaW1hdGlvblwiOntcImNvbG9yYXRpb25cIjowLFwiYmxpbmtpbmdcIjowfSxcImNvbG9yXCI6WzE5NiwxOTYsMTk2XSxcImRhcmtuZXNzXCI6MTAwLFwib3BhY2l0eVwiOjI1NSxcIndpcmVmcmFtZVwiOjAsXCJkYXRhXCI6e1wicm93aWRcIjpbMCwzMDE5OTEzODldfX0se1widmVyc2lvblwiOjEsXCJ0eXBlXCI6XCJMQUJFTFwiLFwibmFtZVwiOlwiR2FkZ2V0IDBcIixcImdhZGdldE51bVwiOjIwMTEsXCJzY2FsZVwiOlsxLDEsMV0sXCJyb3RhdGlvblwiOlswLDAsMF0sXCJwb3NpdGlvblwiOlstNjc1LDI1NSwwXSxcImFuaW1hdGlvblwiOntcImNvbG9yYXRpb25cIjowLFwiYmxpbmtpbmdcIjowfSxcImNvbG9yXCI6WzI1NSwyNTUsMjU1XSxcImNvbG9yYmtcIjpbMCwwLDBdLFwidHJhbnNwYXJlbnRia1wiOjAsXCJkYXJrbmVzc1wiOjEwMCxcInRleHRcIjpcIlBhZ2UgMSBNYXAgT3ZlcnZpZXdcIixcImZvbnRcIjp7XCJmYW1pbHlcIjpcIkFyaWFsXCIsXCJzaXplXCI6MTgsXCJib2xkXCI6MCxcIml0YWxpY1wiOjAsXCJ1bmRlcmxpbmVcIjowfSxcImZyYW1lXCI6e1wiZml4ZWRzaXplXCI6MCxcIndpZHRoXCI6MCxcImhlaWdodFwiOjAsXCJhbGlnbm1lbnRoXCI6XCJDRU5URVJcIixcImFsaWdubWVudHZcIjpcIkNFTlRFUlwifSxcImRhdGFcIjp7XCJyb3dpZFwiOlswLDBdfX0se1widmVyc2lvblwiOjEsXCJ0eXBlXCI6XCJMQUJFTFwiLFwibmFtZVwiOlwiR2FkZ2V0IDBcIixcImdhZGdldE51bVwiOjIwMTIsXCJzY2FsZVwiOlsxLDEsMV0sXCJyb3RhdGlvblwiOlswLDAsMF0sXCJwb3NpdGlvblwiOlstNjc1LDIwMCwwXSxcImFuaW1hdGlvblwiOntcImNvbG9yYXRpb25cIjowLFwiYmxpbmtpbmdcIjowfSxcImNvbG9yXCI6WzI1NSwyNTUsMjU1XSxcImNvbG9yYmtcIjpbMCwwLDBdLFwidHJhbnNwYXJlbnRia1wiOjAsXCJkYXJrbmVzc1wiOjEwMCxcInRleHRcIjpcIlBhZ2UgMiBXYXRlcndvcmtzXCIsXCJmb250XCI6e1wiZmFtaWx5XCI6XCJBcmlhbFwiLFwic2l6ZVwiOjE4LFwiYm9sZFwiOjAsXCJpdGFsaWNcIjowLFwidW5kZXJsaW5lXCI6MH0sXCJmcmFtZVwiOntcImZpeGVkc2l6ZVwiOjAsXCJ3aWR0aFwiOjAsXCJoZWlnaHRcIjowLFwiYWxpZ25tZW50aFwiOlwiQ0VOVEVSXCIsXCJhbGlnbm1lbnR2XCI6XCJDRU5URVJcIn0sXCJkYXRhXCI6e1wicm93aWRcIjpbMCwwXX19LHtcInZlcnNpb25cIjoxLFwidHlwZVwiOlwiTEFCRUxcIixcIm5hbWVcIjpcIkdhZGdldCAwXCIsXCJnYWRnZXROdW1cIjoyMDEzLFwic2NhbGVcIjpbMSwxLDFdLFwicm90YXRpb25cIjpbMCwwLDBdLFwicG9zaXRpb25cIjpbLTY3NSwxNDAsMF0sXCJhbmltYXRpb25cIjp7XCJjb2xvcmF0aW9uXCI6MCxcImJsaW5raW5nXCI6MH0sXCJjb2xvclwiOlsyNTUsMjU1LDI1NV0sXCJjb2xvcmJrXCI6WzAsMCwwXSxcInRyYW5zcGFyZW50YmtcIjowLFwiZGFya25lc3NcIjoxMDAsXCJ0ZXh0XCI6XCJQYWdlIDMgUHJvY2VzcyBleGFtcGxlIDFcIixcImZvbnRcIjp7XCJmYW1pbHlcIjpcIkFyaWFsXCIsXCJzaXplXCI6MTgsXCJib2xkXCI6MCxcIml0YWxpY1wiOjAsXCJ1bmRlcmxpbmVcIjowfSxcImZyYW1lXCI6e1wiZml4ZWRzaXplXCI6MCxcIndpZHRoXCI6MCxcImhlaWdodFwiOjAsXCJhbGlnbm1lbnRoXCI6XCJDRU5URVJcIixcImFsaWdubWVudHZcIjpcIkNFTlRFUlwifSxcImRhdGFcIjp7XCJyb3dpZFwiOlswLDBdfX0se1widmVyc2lvblwiOjEsXCJ0eXBlXCI6XCJMQUJFTFwiLFwibmFtZVwiOlwiR2FkZ2V0IDBcIixcImdhZGdldE51bVwiOjIwMTQsXCJzY2FsZVwiOlsxLDEsMV0sXCJyb3RhdGlvblwiOlswLDAsMF0sXCJwb3NpdGlvblwiOlstNjc1LDgwLDBdLFwiYW5pbWF0aW9uXCI6e1wiY29sb3JhdGlvblwiOjAsXCJibGlua2luZ1wiOjB9LFwiY29sb3JcIjpbMjU1LDI1NSwyNTVdLFwiY29sb3Jia1wiOlswLDAsMF0sXCJ0cmFuc3BhcmVudGJrXCI6MCxcImRhcmtuZXNzXCI6MTAwLFwidGV4dFwiOlwiUGFnZSA0IFByb2Nlc3MgZXhhbXBsZSAyXCIsXCJmb250XCI6e1wiZmFtaWx5XCI6XCJBcmlhbFwiLFwic2l6ZVwiOjE4LFwiYm9sZFwiOjAsXCJpdGFsaWNcIjowLFwidW5kZXJsaW5lXCI6MH0sXCJmcmFtZVwiOntcImZpeGVkc2l6ZVwiOjAsXCJ3aWR0aFwiOjAsXCJoZWlnaHRcIjowLFwiYWxpZ25tZW50aFwiOlwiQ0VOVEVSXCIsXCJhbGlnbm1lbnR2XCI6XCJDRU5URVJcIn0sXCJkYXRhXCI6e1wicm93aWRcIjpbMCwwXX19LHtcInZlcnNpb25cIjoxLFwidHlwZVwiOlwiTEFCRUxcIixcIm5hbWVcIjpcIkdhZGdldCAwXCIsXCJnYWRnZXROdW1cIjoyMDE1LFwic2NhbGVcIjpbMSwxLDFdLFwicm90YXRpb25cIjpbMCwwLDBdLFwicG9zaXRpb25cIjpbLTY3NSwyNSwwXSxcImFuaW1hdGlvblwiOntcImNvbG9yYXRpb25cIjowLFwiYmxpbmtpbmdcIjowfSxcImNvbG9yXCI6WzI1NSwyNTUsMjU1XSxcImNvbG9yYmtcIjpbMCwwLDBdLFwidHJhbnNwYXJlbnRia1wiOjAsXCJkYXJrbmVzc1wiOjEwMCxcInRleHRcIjpcIlBhZ2UgNSBJTyBTdGF0dXNcIixcImZvbnRcIjp7XCJmYW1pbHlcIjpcIkFyaWFsXCIsXCJzaXplXCI6MTgsXCJib2xkXCI6MCxcIml0YWxpY1wiOjAsXCJ1bmRlcmxpbmVcIjowfSxcImZyYW1lXCI6e1wiZml4ZWRzaXplXCI6MCxcIndpZHRoXCI6MCxcImhlaWdodFwiOjAsXCJhbGlnbm1lbnRoXCI6XCJDRU5URVJcIixcImFsaWdubWVudHZcIjpcIkNFTlRFUlwifSxcImRhdGFcIjp7XCJyb3dpZFwiOlswLDBdfX0se1widmVyc2lvblwiOjEsXCJ0eXBlXCI6XCJMQUJFTFwiLFwibmFtZVwiOlwiR2FkZ2V0IDBcIixcImdhZGdldE51bVwiOjIwMTYsXCJzY2FsZVwiOlsxLDEsMV0sXCJyb3RhdGlvblwiOlswLDAsMF0sXCJwb3NpdGlvblwiOlstNjc1LC0zMCwwXSxcImFuaW1hdGlvblwiOntcImNvbG9yYXRpb25cIjowLFwiYmxpbmtpbmdcIjowfSxcImNvbG9yXCI6WzI1NSwyNTUsMjU1XSxcImNvbG9yYmtcIjpbMCwwLDBdLFwidHJhbnNwYXJlbnRia1wiOjAsXCJkYXJrbmVzc1wiOjEwMCxcInRleHRcIjpcIlBhZ2UgNiBTUiBOZXV0cmFsaXNpbmdcIixcImZvbnRcIjp7XCJmYW1pbHlcIjpcIkFyaWFsXCIsXCJzaXplXCI6MTgsXCJib2xkXCI6MCxcIml0YWxpY1wiOjAsXCJ1bmRlcmxpbmVcIjowfSxcImZyYW1lXCI6e1wiZml4ZWRzaXplXCI6MCxcIndpZHRoXCI6MCxcImhlaWdodFwiOjAsXCJhbGlnbm1lbnRoXCI6XCJDRU5URVJcIixcImFsaWdubWVudHZcIjpcIkNFTlRFUlwifSxcImRhdGFcIjp7XCJyb3dpZFwiOlswLDBdfX0se1widmVyc2lvblwiOjEsXCJ0eXBlXCI6XCJMQUJFTFwiLFwibmFtZVwiOlwiR2FkZ2V0IDBcIixcImdhZGdldE51bVwiOjIwMTcsXCJzY2FsZVwiOlsxLDEsMV0sXCJyb3RhdGlvblwiOlswLDAsMF0sXCJwb3NpdGlvblwiOlstNjc1LC04NSwwXSxcImFuaW1hdGlvblwiOntcImNvbG9yYXRpb25cIjowLFwiYmxpbmtpbmdcIjowfSxcImNvbG9yXCI6WzI1NSwyNTUsMjU1XSxcImNvbG9yYmtcIjpbMCwwLDBdLFwidHJhbnNwYXJlbnRia1wiOjAsXCJkYXJrbmVzc1wiOjEwMCxcInRleHRcIjpcIlBhZ2UgNyBQb3B1cFwiLFwiZm9udFwiOntcImZhbWlseVwiOlwiQXJpYWxcIixcInNpemVcIjoxOCxcImJvbGRcIjowLFwiaXRhbGljXCI6MCxcInVuZGVybGluZVwiOjB9LFwiZnJhbWVcIjp7XCJmaXhlZHNpemVcIjowLFwid2lkdGhcIjowLFwiaGVpZ2h0XCI6MCxcImFsaWdubWVudGhcIjpcIkNFTlRFUlwiLFwiYWxpZ25tZW50dlwiOlwiQ0VOVEVSXCJ9LFwiZGF0YVwiOntcInJvd2lkXCI6WzAsMF19fSx7XCJ2ZXJzaW9uXCI6MSxcInR5cGVcIjpcIkxBQkVMXCIsXCJuYW1lXCI6XCJHYWRnZXQgMFwiLFwiZ2FkZ2V0TnVtXCI6MjAxOCxcInNjYWxlXCI6WzEsMSwxXSxcInJvdGF0aW9uXCI6WzAsMCwwXSxcInBvc2l0aW9uXCI6Wy02NzUsLTE0NSwwXSxcImFuaW1hdGlvblwiOntcImNvbG9yYXRpb25cIjowLFwiYmxpbmtpbmdcIjowfSxcImNvbG9yXCI6WzI1NSwyNTUsMjU1XSxcImNvbG9yYmtcIjpbMCwwLDBdLFwidHJhbnNwYXJlbnRia1wiOjAsXCJkYXJrbmVzc1wiOjEwMCxcInRleHRcIjpcIlBhZ2UgOCBab29tIEdyb3VwXCIsXCJmb250XCI6e1wiZmFtaWx5XCI6XCJBcmlhbFwiLFwic2l6ZVwiOjE4LFwiYm9sZFwiOjAsXCJpdGFsaWNcIjowLFwidW5kZXJsaW5lXCI6MH0sXCJmcmFtZVwiOntcImZpeGVkc2l6ZVwiOjAsXCJ3aWR0aFwiOjAsXCJoZWlnaHRcIjowLFwiYWxpZ25tZW50aFwiOlwiQ0VOVEVSXCIsXCJhbGlnbm1lbnR2XCI6XCJDRU5URVJcIn0sXCJkYXRhXCI6e1wicm93aWRcIjpbMCwwXX19XX1dfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc291cmNlcy9tb2R1bGVzL3NjaGVtYXRpYy90ZXN0SlNPTi9QMDBfSG9tZVBhZ2UuanNvblxuLy8gbW9kdWxlIGlkID0gMzFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLyogTW9kdWxlIENvbXBvbmVudHMgRGF0YSAqL1xyXG5cclxuLyogTk9URTogVEhJUyBJUyBURVNUIERBVEEgKi9cclxuXHJcbmxldCBkYXRhID0gW1xyXG4gICAgeyBpZDpcIkJveFwiLCB2YWx1ZTpcIkJveFwiLCBpY29uOlwiYm94LTNkXCIgfSxcclxuICAgIHsgaWQ6XCJTcGhlcmVcIiwgdmFsdWU6XCJTcGhlcmVcIiwgaWNvbjpcInNwaGVyZVwiIH0sXHJcbiAgICB7IGlkOlwiQ3lsaW5kZXJcIiwgdmFsdWU6XCJDeWxpbmRlclwiLCBpY29uOlwiY3lsaW5kZXJcIiB9LFxyXG4gICAgeyBpZDpcIkNvbmVcIiwgdmFsdWU6XCJDb25lXCIsIGljb246XCJjb25lXCIgfSxcclxuICAgIHsgaWQ6XCJUb3J1c1wiLCB2YWx1ZTpcIlRvcnVzXCIsIGljb246XCJ0b3J1c1wiIH0sXHJcbiAgICB7IGlkOlwiVHViZVwiLCB2YWx1ZTpcIlR1YmVcIiwgaWNvbjpcInR1YmVcIiB9LFxyXG4gICAgeyBpZDpcIlJlY3RhbmdsZVwiLCB2YWx1ZTpcIlJlY3RhbmdsZVwiLCBpY29uOlwicmVjdGFuZ2xlXCIgfSxcclxuICAgIHsgaWQ6XCJUcmlhbmdsZVwiLCB2YWx1ZTpcIlRyaWFuZ2xlXCIsIGljb246XCJ0cmlhbmdsZVwiIH0sXHJcbiAgICB7IGlkOlwiRWxsaXBzZVwiLCB2YWx1ZTpcIkVsbGlwc2VcIiwgaWNvbjpcImVsbGlwc2VcIiB9LFxyXG4gICAgeyBpZDpcIlBvbHlnb25cIiwgdmFsdWU6XCJQb2x5Z29uXCIsIGljb246XCJoZXhhZ29uXCIgfSxcclxuICAgIHsgaWQ6XCJEaXNrXCIsIHZhbHVlOlwiRGlza1wiLCBpY29uOlwiZGlza1wiIH0sXHJcbiAgICB7IGlkOlwiUG9pbnRcIiwgdmFsdWU6XCJQb2ludFwiLCBpY29uOlwicG9pbnRcIiB9LFxyXG4gICAgeyBpZDpcIkxpbmVcIiwgdmFsdWU6XCJMaW5lXCIsIGljb246XCJsaW5lXCIgfSxcclxuICAgIHsgaWQ6XCJJbWFnZVwiLCB2YWx1ZTpcIkltYWdlXCIsIGljb246XCJpbWFnZVwiIH0sXHJcbiAgICB7IGlkOlwiTGFiZWxcIiwgdmFsdWU6XCJMYWJlbFwiLCBpY29uOlwibGFiZWxcIiB9LFxyXG4gICAgeyBpZDpcIlN0YXR1cyBGbGFnXCIsIHZhbHVlOlwiU3RhdHVzIEZsYWdcIiwgaWNvbjpcImZsYWctdmFyaWFudFwiIH0sXHJcbiAgICB7IGlkOlwiU3RhdGUgR2FkZ2V0XCIsIHZhbHVlOlwiU3RhdGUgR2FkZ2V0XCIsIGljb246XCJ0cmFmZmljLWxpZ2h0XCIgfSxcclxuICAgIHsgaWQ6XCJDYW1lcmFcIiwgdmFsdWU6XCJDYW1lcmFcIiwgaWNvbjpcImZyZWUtY2FtZXJhXCIgfSxcclxuICAgIHsgaWQ6XCJTcG90IExpZ2h0XCIsIHZhbHVlOlwiU3BvdCBMaWdodFwiLCBpY29uOlwic3BvdGxpZ2h0XCIgfSxcclxuICAgIHsgaWQ6XCJQb2ludCBMaWdodFwiLCB2YWx1ZTpcIlBvaW50IExpZ2h0XCIsIGljb246XCJsaWdodGJ1bGJcIiB9XHJcbl07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBkYXRhO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NvdXJjZXMvbW9kZWxzL2NvbXBvbmVudERhdGEuanMiLCJleHBvcnQgY2xhc3MgU3RvcmVSb3V0ZXIge1xuICAgIGNvbnN0cnVjdG9yKGNiLCBjb25maWcpIHtcbiAgICAgICAgdGhpcy5uYW1lID0gKGNvbmZpZy5zdG9yZU5hbWUgfHwgY29uZmlnLmlkICsgXCI6cm91dGVcIik7XG4gICAgICAgIHRoaXMuY2IgPSBjYjtcbiAgICB9XG4gICAgc2V0KHBhdGgsIGNvbmZpZykge1xuICAgICAgICB3ZWJpeC5zdG9yYWdlLnNlc3Npb24ucHV0KHRoaXMubmFtZSwgcGF0aCk7XG4gICAgICAgIGlmICghY29uZmlnIHx8ICFjb25maWcuc2lsZW50KSB7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMuY2IocGF0aCksIDEpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHdlYml4LnN0b3JhZ2Uuc2Vzc2lvbi5nZXQodGhpcy5uYW1lKTtcbiAgICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3JvdXRlcnMvU3RvcmVSb3V0ZXIuanMiLCJleHBvcnQgY2xhc3MgVXJsUm91dGVyIHtcbiAgICBjb25zdHJ1Y3RvcihjYiwgY29uZmlnKSB7XG4gICAgICAgIHRoaXMuY2IgPSBjYjtcbiAgICAgICAgdGhpcy5wcmVmaXggPSBjb25maWcucm91dGVyUHJlZml4IHx8IFwiXCI7XG4gICAgfVxuICAgIHNldChwYXRoLCBjb25maWcpIHtcbiAgICAgICAgd2luZG93Lmhpc3RvcnkucHVzaFN0YXRlKG51bGwsIG51bGwsIHRoaXMucHJlZml4ICsgcGF0aCk7XG4gICAgICAgIGlmICghY29uZmlnIHx8ICFjb25maWcuc2lsZW50KSB7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMuY2IocGF0aCksIDEpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGdldCgpIHtcbiAgICAgICAgY29uc3QgcGF0aCA9IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZS5yZXBsYWNlKHRoaXMucHJlZml4LCBcIlwiKTtcbiAgICAgICAgcmV0dXJuIHBhdGggIT09IFwiL1wiID8gcGF0aCA6IFwiXCI7XG4gICAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9yb3V0ZXJzL1VybFJvdXRlci5qcyIsImV4cG9ydCBjbGFzcyBFbXB0eVJvdXRlciB7XG4gICAgY29uc3RydWN0b3IoY2IsIF8kY29uZmlnKSB7XG4gICAgICAgIHRoaXMucGF0aCA9IFwiXCI7XG4gICAgICAgIHRoaXMuY2IgPSBjYjtcbiAgICB9XG4gICAgc2V0KHBhdGgsIGNvbmZpZykge1xuICAgICAgICB0aGlzLnBhdGggPSBwYXRoO1xuICAgICAgICBpZiAoIWNvbmZpZyB8fCAhY29uZmlnLnNpbGVudCkge1xuICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLmNiKHBhdGgpLCAxKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnBhdGg7XG4gICAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9yb3V0ZXJzL0VtcHR5Um91dGVyLmpzIiwiZXhwb3J0IGZ1bmN0aW9uIFVubG9hZEd1YXJkKGFwcCwgdmlldywgY29uZmlnKSB7XG4gICAgdmlldy5vbihhcHAsIGBhcHA6Z3VhcmRgLCBmdW5jdGlvbiAoXyR1cmwsIHBvaW50LCBwcm9taXNlKSB7XG4gICAgICAgIGlmIChwb2ludCA9PT0gdmlldyB8fCBwb2ludC5jb250YWlucyh2aWV3KSkge1xuICAgICAgICAgICAgY29uc3QgcmVzID0gY29uZmlnKCk7XG4gICAgICAgICAgICBpZiAocmVzID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHByb21pc2UuY29uZmlybSA9IFByb21pc2UucmVqZWN0KHJlcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBwcm9taXNlLmNvbmZpcm0gPSBwcm9taXNlLmNvbmZpcm0udGhlbigoKSA9PiByZXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSk7XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3BsdWdpbnMvR3VhcmQuanMiLCJpbXBvcnQgUG9seWdsb3QgZnJvbSBcIm5vZGUtcG9seWdsb3QvYnVpbGQvcG9seWdsb3RcIjtcbmV4cG9ydCBmdW5jdGlvbiBMb2NhbGUoYXBwLCB2aWV3LCBjb25maWcpIHtcbiAgICBjb25maWcgPSBjb25maWcgfHwge307XG4gICAgY29uc3Qgc3RvcmFnZSA9IGNvbmZpZy5zdG9yYWdlO1xuICAgIGxldCBsYW5nID0gc3RvcmFnZSA/IHN0b3JhZ2UuZ2V0KFwibGFuZ1wiKSA6IChjb25maWcubGFuZyB8fCBcImVuXCIpO1xuICAgIGNvbnN0IHNlcnZpY2UgPSB7XG4gICAgICAgIF86IG51bGwsXG4gICAgICAgIHBvbHlnbG90OiBudWxsLFxuICAgICAgICBnZXRMYW5nKCkgeyByZXR1cm4gbGFuZzsgfSxcbiAgICAgICAgc2V0TGFuZyhuYW1lLCBzaWxlbnQpIHtcbiAgICAgICAgICAgIGxldCBkYXRhID0gcmVxdWlyZShcImpldC1sb2NhbGVzL1wiICsgbmFtZSk7XG4gICAgICAgICAgICBpZiAoZGF0YS5fX2VzTW9kdWxlKSB7XG4gICAgICAgICAgICAgICAgZGF0YSA9IGRhdGEuZGVmYXVsdDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IHBvbHkgPSBzZXJ2aWNlLnBvbHlnbG90ID0gbmV3IFBvbHlnbG90KHsgcGhyYXNlczogZGF0YSB9KTtcbiAgICAgICAgICAgIHBvbHkubG9jYWxlKG5hbWUpO1xuICAgICAgICAgICAgc2VydmljZS5fID0gd2ViaXguYmluZChwb2x5LnQsIHBvbHkpO1xuICAgICAgICAgICAgbGFuZyA9IG5hbWU7XG4gICAgICAgICAgICBpZiAoc3RvcmFnZSkge1xuICAgICAgICAgICAgICAgIHN0b3JhZ2UucHV0KFwibGFuZ1wiLCBsYW5nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICghc2lsZW50KSB7XG4gICAgICAgICAgICAgICAgYXBwLnJlZnJlc2goKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG4gICAgYXBwLnNldFNlcnZpY2UoXCJsb2NhbGVcIiwgc2VydmljZSk7XG4gICAgc2VydmljZS5zZXRMYW5nKGxhbmcsIHRydWUpO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9wbHVnaW5zL0xvY2FsZS5qcyIsIi8vICAgICAoYykgMjAxMiBBaXJibmIsIEluYy5cbi8vXG4vLyAgICAgcG9seWdsb3QuanMgbWF5IGJlIGZyZWVseSBkaXN0cmlidXRlZCB1bmRlciB0aGUgdGVybXMgb2YgdGhlIEJTRFxuLy8gICAgIGxpY2Vuc2UuIEZvciBhbGwgbGljZW5zaW5nIGluZm9ybWF0aW9uLCBkZXRhaWxzLCBhbmQgZG9jdW1lbnRpb246XG4vLyAgICAgaHR0cDovL2FpcmJuYi5naXRodWIuY29tL3BvbHlnbG90LmpzXG4vL1xuLy9cbi8vIFBvbHlnbG90LmpzIGlzIGFuIEkxOG4gaGVscGVyIGxpYnJhcnkgd3JpdHRlbiBpbiBKYXZhU2NyaXB0LCBtYWRlIHRvXG4vLyB3b3JrIGJvdGggaW4gdGhlIGJyb3dzZXIgYW5kIGluIE5vZGUuIEl0IHByb3ZpZGVzIGEgc2ltcGxlIHNvbHV0aW9uIGZvclxuLy8gaW50ZXJwb2xhdGlvbiBhbmQgcGx1cmFsaXphdGlvbiwgYmFzZWQgb2ZmIG9mIEFpcmJuYidzXG4vLyBleHBlcmllbmNlIGFkZGluZyBJMThuIGZ1bmN0aW9uYWxpdHkgdG8gaXRzIEJhY2tib25lLmpzIGFuZCBOb2RlIGFwcHMuXG4vL1xuLy8gUG9seWxnbG90IGlzIGFnbm9zdGljIHRvIHlvdXIgdHJhbnNsYXRpb24gYmFja2VuZC4gSXQgZG9lc24ndCBwZXJmb3JtIGFueVxuLy8gdHJhbnNsYXRpb247IGl0IHNpbXBseSBnaXZlcyB5b3UgYSB3YXkgdG8gbWFuYWdlIHRyYW5zbGF0ZWQgcGhyYXNlcyBmcm9tXG4vLyB5b3VyIGNsaWVudC0gb3Igc2VydmVyLXNpZGUgSmF2YVNjcmlwdCBhcHBsaWNhdGlvbi5cbi8vXG5cblxuKGZ1bmN0aW9uKHJvb3QsIGZhY3RvcnkpIHtcbiAgaWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xuICAgIGRlZmluZShbXSwgZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gZmFjdG9yeShyb290KTtcbiAgICB9KTtcbiAgfSBlbHNlIGlmICh0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpIHtcbiAgICBtb2R1bGUuZXhwb3J0cyA9IGZhY3Rvcnkocm9vdCk7XG4gIH0gZWxzZSB7XG4gICAgcm9vdC5Qb2x5Z2xvdCA9IGZhY3Rvcnkocm9vdCk7XG4gIH1cbn0odGhpcywgZnVuY3Rpb24ocm9vdCkge1xuICAndXNlIHN0cmljdCc7XG5cbiAgLy8gIyMjIFBvbHlnbG90IGNsYXNzIGNvbnN0cnVjdG9yXG4gIGZ1bmN0aW9uIFBvbHlnbG90KG9wdGlvbnMpIHtcbiAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgICB0aGlzLnBocmFzZXMgPSB7fTtcbiAgICB0aGlzLmV4dGVuZChvcHRpb25zLnBocmFzZXMgfHwge30pO1xuICAgIHRoaXMuY3VycmVudExvY2FsZSA9IG9wdGlvbnMubG9jYWxlIHx8ICdlbic7XG4gICAgdGhpcy5hbGxvd01pc3NpbmcgPSAhIW9wdGlvbnMuYWxsb3dNaXNzaW5nO1xuICAgIHRoaXMud2FybiA9IG9wdGlvbnMud2FybiB8fCB3YXJuO1xuICB9XG5cbiAgLy8gIyMjIFZlcnNpb25cbiAgUG9seWdsb3QuVkVSU0lPTiA9ICcwLjQuMyc7XG5cbiAgLy8gIyMjIHBvbHlnbG90LmxvY2FsZShbbG9jYWxlXSlcbiAgLy9cbiAgLy8gR2V0IG9yIHNldCBsb2NhbGUuIEludGVybmFsbHksIFBvbHlnbG90IG9ubHkgdXNlcyBsb2NhbGUgZm9yIHBsdXJhbGl6YXRpb24uXG4gIFBvbHlnbG90LnByb3RvdHlwZS5sb2NhbGUgPSBmdW5jdGlvbihuZXdMb2NhbGUpIHtcbiAgICBpZiAobmV3TG9jYWxlKSB0aGlzLmN1cnJlbnRMb2NhbGUgPSBuZXdMb2NhbGU7XG4gICAgcmV0dXJuIHRoaXMuY3VycmVudExvY2FsZTtcbiAgfTtcblxuICAvLyAjIyMgcG9seWdsb3QuZXh0ZW5kKHBocmFzZXMpXG4gIC8vXG4gIC8vIFVzZSBgZXh0ZW5kYCB0byB0ZWxsIFBvbHlnbG90IGhvdyB0byB0cmFuc2xhdGUgYSBnaXZlbiBrZXkuXG4gIC8vXG4gIC8vICAgICBwb2x5Z2xvdC5leHRlbmQoe1xuICAvLyAgICAgICBcImhlbGxvXCI6IFwiSGVsbG9cIixcbiAgLy8gICAgICAgXCJoZWxsb19uYW1lXCI6IFwiSGVsbG8sICV7bmFtZX1cIlxuICAvLyAgICAgfSk7XG4gIC8vXG4gIC8vIFRoZSBrZXkgY2FuIGJlIGFueSBzdHJpbmcuICBGZWVsIGZyZWUgdG8gY2FsbCBgZXh0ZW5kYCBtdWx0aXBsZSB0aW1lcztcbiAgLy8gaXQgd2lsbCBvdmVycmlkZSBhbnkgcGhyYXNlcyB3aXRoIHRoZSBzYW1lIGtleSwgYnV0IGxlYXZlIGV4aXN0aW5nIHBocmFzZXNcbiAgLy8gdW50b3VjaGVkLlxuICAvL1xuICAvLyBJdCBpcyBhbHNvIHBvc3NpYmxlIHRvIHBhc3MgbmVzdGVkIHBocmFzZSBvYmplY3RzLCB3aGljaCBnZXQgZmxhdHRlbmVkXG4gIC8vIGludG8gYW4gb2JqZWN0IHdpdGggdGhlIG5lc3RlZCBrZXlzIGNvbmNhdGVuYXRlZCB1c2luZyBkb3Qgbm90YXRpb24uXG4gIC8vXG4gIC8vICAgICBwb2x5Z2xvdC5leHRlbmQoe1xuICAvLyAgICAgICBcIm5hdlwiOiB7XG4gIC8vICAgICAgICAgXCJoZWxsb1wiOiBcIkhlbGxvXCIsXG4gIC8vICAgICAgICAgXCJoZWxsb19uYW1lXCI6IFwiSGVsbG8sICV7bmFtZX1cIixcbiAgLy8gICAgICAgICBcInNpZGViYXJcIjoge1xuICAvLyAgICAgICAgICAgXCJ3ZWxjb21lXCI6IFwiV2VsY29tZVwiXG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgICB9XG4gIC8vICAgICB9KTtcbiAgLy9cbiAgLy8gICAgIGNvbnNvbGUubG9nKHBvbHlnbG90LnBocmFzZXMpO1xuICAvLyAgICAgLy8ge1xuICAvLyAgICAgLy8gICAnbmF2LmhlbGxvJzogJ0hlbGxvJyxcbiAgLy8gICAgIC8vICAgJ25hdi5oZWxsb19uYW1lJzogJ0hlbGxvLCAle25hbWV9JyxcbiAgLy8gICAgIC8vICAgJ25hdi5zaWRlYmFyLndlbGNvbWUnOiAnV2VsY29tZSdcbiAgLy8gICAgIC8vIH1cbiAgLy9cbiAgLy8gYGV4dGVuZGAgYWNjZXB0cyBhbiBvcHRpb25hbCBzZWNvbmQgYXJndW1lbnQsIGBwcmVmaXhgLCB3aGljaCBjYW4gYmUgdXNlZFxuICAvLyB0byBwcmVmaXggZXZlcnkga2V5IGluIHRoZSBwaHJhc2VzIG9iamVjdCB3aXRoIHNvbWUgc3RyaW5nLCB1c2luZyBkb3RcbiAgLy8gbm90YXRpb24uXG4gIC8vXG4gIC8vICAgICBwb2x5Z2xvdC5leHRlbmQoe1xuICAvLyAgICAgICBcImhlbGxvXCI6IFwiSGVsbG9cIixcbiAgLy8gICAgICAgXCJoZWxsb19uYW1lXCI6IFwiSGVsbG8sICV7bmFtZX1cIlxuICAvLyAgICAgfSwgXCJuYXZcIik7XG4gIC8vXG4gIC8vICAgICBjb25zb2xlLmxvZyhwb2x5Z2xvdC5waHJhc2VzKTtcbiAgLy8gICAgIC8vIHtcbiAgLy8gICAgIC8vICAgJ25hdi5oZWxsbyc6ICdIZWxsbycsXG4gIC8vICAgICAvLyAgICduYXYuaGVsbG9fbmFtZSc6ICdIZWxsbywgJXtuYW1lfSdcbiAgLy8gICAgIC8vIH1cbiAgLy9cbiAgLy8gVGhpcyBmZWF0dXJlIGlzIHVzZWQgaW50ZXJuYWxseSB0byBzdXBwb3J0IG5lc3RlZCBwaHJhc2Ugb2JqZWN0cy5cbiAgUG9seWdsb3QucHJvdG90eXBlLmV4dGVuZCA9IGZ1bmN0aW9uKG1vcmVQaHJhc2VzLCBwcmVmaXgpIHtcbiAgICB2YXIgcGhyYXNlO1xuXG4gICAgZm9yICh2YXIga2V5IGluIG1vcmVQaHJhc2VzKSB7XG4gICAgICBpZiAobW9yZVBocmFzZXMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICBwaHJhc2UgPSBtb3JlUGhyYXNlc1trZXldO1xuICAgICAgICBpZiAocHJlZml4KSBrZXkgPSBwcmVmaXggKyAnLicgKyBrZXk7XG4gICAgICAgIGlmICh0eXBlb2YgcGhyYXNlID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgIHRoaXMuZXh0ZW5kKHBocmFzZSwga2V5KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnBocmFzZXNba2V5XSA9IHBocmFzZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICAvLyAjIyMgcG9seWdsb3QuY2xlYXIoKVxuICAvL1xuICAvLyBDbGVhcnMgYWxsIHBocmFzZXMuIFVzZWZ1bCBmb3Igc3BlY2lhbCBjYXNlcywgc3VjaCBhcyBmcmVlaW5nXG4gIC8vIHVwIG1lbW9yeSBpZiB5b3UgaGF2ZSBsb3RzIG9mIHBocmFzZXMgYnV0IG5vIGxvbmdlciBuZWVkIHRvXG4gIC8vIHBlcmZvcm0gYW55IHRyYW5zbGF0aW9uLiBBbHNvIHVzZWQgaW50ZXJuYWxseSBieSBgcmVwbGFjZWAuXG4gIFBvbHlnbG90LnByb3RvdHlwZS5jbGVhciA9IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMucGhyYXNlcyA9IHt9O1xuICB9O1xuXG4gIC8vICMjIyBwb2x5Z2xvdC5yZXBsYWNlKHBocmFzZXMpXG4gIC8vXG4gIC8vIENvbXBsZXRlbHkgcmVwbGFjZSB0aGUgZXhpc3RpbmcgcGhyYXNlcyB3aXRoIGEgbmV3IHNldCBvZiBwaHJhc2VzLlxuICAvLyBOb3JtYWxseSwganVzdCB1c2UgYGV4dGVuZGAgdG8gYWRkIG1vcmUgcGhyYXNlcywgYnV0IHVuZGVyIGNlcnRhaW5cbiAgLy8gY2lyY3Vtc3RhbmNlcywgeW91IG1heSB3YW50IHRvIG1ha2Ugc3VyZSBubyBvbGQgcGhyYXNlcyBhcmUgbHlpbmcgYXJvdW5kLlxuICBQb2x5Z2xvdC5wcm90b3R5cGUucmVwbGFjZSA9IGZ1bmN0aW9uKG5ld1BocmFzZXMpIHtcbiAgICB0aGlzLmNsZWFyKCk7XG4gICAgdGhpcy5leHRlbmQobmV3UGhyYXNlcyk7XG4gIH07XG5cblxuICAvLyAjIyMgcG9seWdsb3QudChrZXksIG9wdGlvbnMpXG4gIC8vXG4gIC8vIFRoZSBtb3N0LXVzZWQgbWV0aG9kLiBQcm92aWRlIGEga2V5LCBhbmQgYHRgIHdpbGwgcmV0dXJuIHRoZVxuICAvLyBwaHJhc2UuXG4gIC8vXG4gIC8vICAgICBwb2x5Z2xvdC50KFwiaGVsbG9cIik7XG4gIC8vICAgICA9PiBcIkhlbGxvXCJcbiAgLy9cbiAgLy8gVGhlIHBocmFzZSB2YWx1ZSBpcyBwcm92aWRlZCBmaXJzdCBieSBhIGNhbGwgdG8gYHBvbHlnbG90LmV4dGVuZCgpYCBvclxuICAvLyBgcG9seWdsb3QucmVwbGFjZSgpYC5cbiAgLy9cbiAgLy8gUGFzcyBpbiBhbiBvYmplY3QgYXMgdGhlIHNlY29uZCBhcmd1bWVudCB0byBwZXJmb3JtIGludGVycG9sYXRpb24uXG4gIC8vXG4gIC8vICAgICBwb2x5Z2xvdC50KFwiaGVsbG9fbmFtZVwiLCB7bmFtZTogXCJTcGlrZVwifSk7XG4gIC8vICAgICA9PiBcIkhlbGxvLCBTcGlrZVwiXG4gIC8vXG4gIC8vIElmIHlvdSBsaWtlLCB5b3UgY2FuIHByb3ZpZGUgYSBkZWZhdWx0IHZhbHVlIGluIGNhc2UgdGhlIHBocmFzZSBpcyBtaXNzaW5nLlxuICAvLyBVc2UgdGhlIHNwZWNpYWwgb3B0aW9uIGtleSBcIl9cIiB0byBzcGVjaWZ5IGEgZGVmYXVsdC5cbiAgLy9cbiAgLy8gICAgIHBvbHlnbG90LnQoXCJpX2xpa2VfdG9fd3JpdGVfaW5fbGFuZ3VhZ2VcIiwge1xuICAvLyAgICAgICBfOiBcIkkgbGlrZSB0byB3cml0ZSBpbiAle2xhbmd1YWdlfS5cIixcbiAgLy8gICAgICAgbGFuZ3VhZ2U6IFwiSmF2YVNjcmlwdFwiXG4gIC8vICAgICB9KTtcbiAgLy8gICAgID0+IFwiSSBsaWtlIHRvIHdyaXRlIGluIEphdmFTY3JpcHQuXCJcbiAgLy9cbiAgUG9seWdsb3QucHJvdG90eXBlLnQgPSBmdW5jdGlvbihrZXksIG9wdGlvbnMpIHtcbiAgICB2YXIgcGhyYXNlLCByZXN1bHQ7XG4gICAgb3B0aW9ucyA9IG9wdGlvbnMgPT0gbnVsbCA/IHt9IDogb3B0aW9ucztcbiAgICAvLyBhbGxvdyBudW1iZXIgYXMgYSBwbHVyYWxpemF0aW9uIHNob3J0Y3V0XG4gICAgaWYgKHR5cGVvZiBvcHRpb25zID09PSAnbnVtYmVyJykge1xuICAgICAgb3B0aW9ucyA9IHtzbWFydF9jb3VudDogb3B0aW9uc307XG4gICAgfVxuICAgIGlmICh0eXBlb2YgdGhpcy5waHJhc2VzW2tleV0gPT09ICdzdHJpbmcnKSB7XG4gICAgICBwaHJhc2UgPSB0aGlzLnBocmFzZXNba2V5XTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBvcHRpb25zLl8gPT09ICdzdHJpbmcnKSB7XG4gICAgICBwaHJhc2UgPSBvcHRpb25zLl87XG4gICAgfSBlbHNlIGlmICh0aGlzLmFsbG93TWlzc2luZykge1xuICAgICAgcGhyYXNlID0ga2V5O1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLndhcm4oJ01pc3NpbmcgdHJhbnNsYXRpb24gZm9yIGtleTogXCInK2tleSsnXCInKTtcbiAgICAgIHJlc3VsdCA9IGtleTtcbiAgICB9XG4gICAgaWYgKHR5cGVvZiBwaHJhc2UgPT09ICdzdHJpbmcnKSB7XG4gICAgICBvcHRpb25zID0gY2xvbmUob3B0aW9ucyk7XG4gICAgICByZXN1bHQgPSBjaG9vc2VQbHVyYWxGb3JtKHBocmFzZSwgdGhpcy5jdXJyZW50TG9jYWxlLCBvcHRpb25zLnNtYXJ0X2NvdW50KTtcbiAgICAgIHJlc3VsdCA9IGludGVycG9sYXRlKHJlc3VsdCwgb3B0aW9ucyk7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG4gIH07XG5cblxuICAvLyAjIyMgcG9seWdsb3QuaGFzKGtleSlcbiAgLy9cbiAgLy8gQ2hlY2sgaWYgcG9seWdsb3QgaGFzIGEgdHJhbnNsYXRpb24gZm9yIGdpdmVuIGtleVxuICBQb2x5Z2xvdC5wcm90b3R5cGUuaGFzID0gZnVuY3Rpb24oa2V5KSB7XG4gICAgcmV0dXJuIGtleSBpbiB0aGlzLnBocmFzZXM7XG4gIH07XG5cblxuICAvLyAjIyMjIFBsdXJhbGl6YXRpb24gbWV0aG9kc1xuICAvLyBUaGUgc3RyaW5nIHRoYXQgc2VwYXJhdGVzIHRoZSBkaWZmZXJlbnQgcGhyYXNlIHBvc3NpYmlsaXRpZXMuXG4gIHZhciBkZWxpbWV0ZXIgPSAnfHx8fCc7XG5cbiAgLy8gTWFwcGluZyBmcm9tIHBsdXJhbGl6YXRpb24gZ3JvdXAgcGx1cmFsIGxvZ2ljLlxuICB2YXIgcGx1cmFsVHlwZXMgPSB7XG4gICAgY2hpbmVzZTogICBmdW5jdGlvbihuKSB7IHJldHVybiAwOyB9LFxuICAgIGdlcm1hbjogICAgZnVuY3Rpb24obikgeyByZXR1cm4gbiAhPT0gMSA/IDEgOiAwOyB9LFxuICAgIGZyZW5jaDogICAgZnVuY3Rpb24obikgeyByZXR1cm4gbiA+IDEgPyAxIDogMDsgfSxcbiAgICBydXNzaWFuOiAgIGZ1bmN0aW9uKG4pIHsgcmV0dXJuIG4gJSAxMCA9PT0gMSAmJiBuICUgMTAwICE9PSAxMSA/IDAgOiBuICUgMTAgPj0gMiAmJiBuICUgMTAgPD0gNCAmJiAobiAlIDEwMCA8IDEwIHx8IG4gJSAxMDAgPj0gMjApID8gMSA6IDI7IH0sXG4gICAgY3plY2g6ICAgICBmdW5jdGlvbihuKSB7IHJldHVybiAobiA9PT0gMSkgPyAwIDogKG4gPj0gMiAmJiBuIDw9IDQpID8gMSA6IDI7IH0sXG4gICAgcG9saXNoOiAgICBmdW5jdGlvbihuKSB7IHJldHVybiAobiA9PT0gMSA/IDAgOiBuICUgMTAgPj0gMiAmJiBuICUgMTAgPD0gNCAmJiAobiAlIDEwMCA8IDEwIHx8IG4gJSAxMDAgPj0gMjApID8gMSA6IDIpOyB9LFxuICAgIGljZWxhbmRpYzogZnVuY3Rpb24obikgeyByZXR1cm4gKG4gJSAxMCAhPT0gMSB8fCBuICUgMTAwID09PSAxMSkgPyAxIDogMDsgfVxuICB9O1xuXG4gIC8vIE1hcHBpbmcgZnJvbSBwbHVyYWxpemF0aW9uIGdyb3VwIHRvIGluZGl2aWR1YWwgbG9jYWxlcy5cbiAgdmFyIHBsdXJhbFR5cGVUb0xhbmd1YWdlcyA9IHtcbiAgICBjaGluZXNlOiAgIFsnZmEnLCAnaWQnLCAnamEnLCAna28nLCAnbG8nLCAnbXMnLCAndGgnLCAndHInLCAnemgnXSxcbiAgICBnZXJtYW46ICAgIFsnZGEnLCAnZGUnLCAnZW4nLCAnZXMnLCAnZmknLCAnZWwnLCAnaGUnLCAnaHUnLCAnaXQnLCAnbmwnLCAnbm8nLCAncHQnLCAnc3YnXSxcbiAgICBmcmVuY2g6ICAgIFsnZnInLCAndGwnLCAncHQtYnInXSxcbiAgICBydXNzaWFuOiAgIFsnaHInLCAncnUnXSxcbiAgICBjemVjaDogICAgIFsnY3MnXSxcbiAgICBwb2xpc2g6ICAgIFsncGwnXSxcbiAgICBpY2VsYW5kaWM6IFsnaXMnXVxuICB9O1xuXG4gIGZ1bmN0aW9uIGxhbmdUb1R5cGVNYXAobWFwcGluZykge1xuICAgIHZhciB0eXBlLCBsYW5ncywgbCwgcmV0ID0ge307XG4gICAgZm9yICh0eXBlIGluIG1hcHBpbmcpIHtcbiAgICAgIGlmIChtYXBwaW5nLmhhc093blByb3BlcnR5KHR5cGUpKSB7XG4gICAgICAgIGxhbmdzID0gbWFwcGluZ1t0eXBlXTtcbiAgICAgICAgZm9yIChsIGluIGxhbmdzKSB7XG4gICAgICAgICAgcmV0W2xhbmdzW2xdXSA9IHR5cGU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJldDtcbiAgfVxuXG4gIC8vIFRyaW0gYSBzdHJpbmcuXG4gIGZ1bmN0aW9uIHRyaW0oc3RyKXtcbiAgICB2YXIgdHJpbVJlID0gL15cXHMrfFxccyskL2c7XG4gICAgcmV0dXJuIHN0ci5yZXBsYWNlKHRyaW1SZSwgJycpO1xuICB9XG5cbiAgLy8gQmFzZWQgb24gYSBwaHJhc2UgdGV4dCB0aGF0IGNvbnRhaW5zIGBuYCBwbHVyYWwgZm9ybXMgc2VwYXJhdGVkXG4gIC8vIGJ5IGBkZWxpbWV0ZXJgLCBhIGBsb2NhbGVgLCBhbmQgYSBgY291bnRgLCBjaG9vc2UgdGhlIGNvcnJlY3RcbiAgLy8gcGx1cmFsIGZvcm0sIG9yIG5vbmUgaWYgYGNvdW50YCBpcyBgbnVsbGAuXG4gIGZ1bmN0aW9uIGNob29zZVBsdXJhbEZvcm0odGV4dCwgbG9jYWxlLCBjb3VudCl7XG4gICAgdmFyIHJldCwgdGV4dHMsIGNob3NlblRleHQ7XG4gICAgaWYgKGNvdW50ICE9IG51bGwgJiYgdGV4dCkge1xuICAgICAgdGV4dHMgPSB0ZXh0LnNwbGl0KGRlbGltZXRlcik7XG4gICAgICBjaG9zZW5UZXh0ID0gdGV4dHNbcGx1cmFsVHlwZUluZGV4KGxvY2FsZSwgY291bnQpXSB8fCB0ZXh0c1swXTtcbiAgICAgIHJldCA9IHRyaW0oY2hvc2VuVGV4dCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldCA9IHRleHQ7XG4gICAgfVxuICAgIHJldHVybiByZXQ7XG4gIH1cblxuICBmdW5jdGlvbiBwbHVyYWxUeXBlTmFtZShsb2NhbGUpIHtcbiAgICB2YXIgbGFuZ1RvUGx1cmFsVHlwZSA9IGxhbmdUb1R5cGVNYXAocGx1cmFsVHlwZVRvTGFuZ3VhZ2VzKTtcbiAgICByZXR1cm4gbGFuZ1RvUGx1cmFsVHlwZVtsb2NhbGVdIHx8IGxhbmdUb1BsdXJhbFR5cGUuZW47XG4gIH1cblxuICBmdW5jdGlvbiBwbHVyYWxUeXBlSW5kZXgobG9jYWxlLCBjb3VudCkge1xuICAgIHJldHVybiBwbHVyYWxUeXBlc1twbHVyYWxUeXBlTmFtZShsb2NhbGUpXShjb3VudCk7XG4gIH1cblxuICAvLyAjIyMgaW50ZXJwb2xhdGVcbiAgLy9cbiAgLy8gRG9lcyB0aGUgZGlydHkgd29yay4gQ3JlYXRlcyBhIGBSZWdFeHBgIG9iamVjdCBmb3IgZWFjaFxuICAvLyBpbnRlcnBvbGF0aW9uIHBsYWNlaG9sZGVyLlxuICBmdW5jdGlvbiBpbnRlcnBvbGF0ZShwaHJhc2UsIG9wdGlvbnMpIHtcbiAgICBmb3IgKHZhciBhcmcgaW4gb3B0aW9ucykge1xuICAgICAgaWYgKGFyZyAhPT0gJ18nICYmIG9wdGlvbnMuaGFzT3duUHJvcGVydHkoYXJnKSkge1xuICAgICAgICAvLyBXZSBjcmVhdGUgYSBuZXcgYFJlZ0V4cGAgZWFjaCB0aW1lIGluc3RlYWQgb2YgdXNpbmcgYSBtb3JlLWVmZmljaWVudFxuICAgICAgICAvLyBzdHJpbmcgcmVwbGFjZSBzbyB0aGF0IHRoZSBzYW1lIGFyZ3VtZW50IGNhbiBiZSByZXBsYWNlZCBtdWx0aXBsZSB0aW1lc1xuICAgICAgICAvLyBpbiB0aGUgc2FtZSBwaHJhc2UuXG4gICAgICAgIHBocmFzZSA9IHBocmFzZS5yZXBsYWNlKG5ldyBSZWdFeHAoJyVcXFxceycrYXJnKydcXFxcfScsICdnJyksIG9wdGlvbnNbYXJnXSk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBwaHJhc2U7XG4gIH1cblxuICAvLyAjIyMgd2FyblxuICAvL1xuICAvLyBQcm92aWRlcyBhIHdhcm5pbmcgaW4gdGhlIGNvbnNvbGUgaWYgYSBwaHJhc2Uga2V5IGlzIG1pc3NpbmcuXG4gIGZ1bmN0aW9uIHdhcm4obWVzc2FnZSkge1xuICAgIHJvb3QuY29uc29sZSAmJiByb290LmNvbnNvbGUud2FybiAmJiByb290LmNvbnNvbGUud2FybignV0FSTklORzogJyArIG1lc3NhZ2UpO1xuICB9XG5cbiAgLy8gIyMjIGNsb25lXG4gIC8vXG4gIC8vIENsb25lIGFuIG9iamVjdC5cbiAgZnVuY3Rpb24gY2xvbmUoc291cmNlKSB7XG4gICAgdmFyIHJldCA9IHt9O1xuICAgIGZvciAodmFyIHByb3AgaW4gc291cmNlKSB7XG4gICAgICByZXRbcHJvcF0gPSBzb3VyY2VbcHJvcF07XG4gICAgfVxuICAgIHJldHVybiByZXQ7XG4gIH1cblxuICByZXR1cm4gUG9seWdsb3Q7XG59KSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvbm9kZS1wb2x5Z2xvdC9idWlsZC9wb2x5Z2xvdC5qcyIsInZhciBtYXAgPSB7XG5cdFwiLi9lblwiOiAxOCxcblx0XCIuL2VuLmpzXCI6IDE4XG59O1xuZnVuY3Rpb24gd2VicGFja0NvbnRleHQocmVxKSB7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpKTtcbn07XG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKSB7XG5cdHZhciBpZCA9IG1hcFtyZXFdO1xuXHRpZighKGlkICsgMSkpIC8vIGNoZWNrIGZvciBudW1iZXIgb3Igc3RyaW5nXG5cdFx0dGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJy5cIik7XG5cdHJldHVybiBpZDtcbn07XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gMzk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zb3VyY2VzL2xvY2FsZXMgXlxcLlxcLy4qJFxuLy8gbW9kdWxlIGlkID0gMzlcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiZnVuY3Rpb24gc2hvdyh2aWV3LCBjb25maWcsIHZhbHVlKSB7XG4gICAgaWYgKGNvbmZpZy51cmxzKSB7XG4gICAgICAgIHZhbHVlID0gY29uZmlnLnVybHNbdmFsdWVdIHx8IHZhbHVlO1xuICAgIH1cbiAgICB2aWV3LnNob3coXCIuL1wiICsgdmFsdWUpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIE1lbnUoYXBwLCB2aWV3LCBjb25maWcpIHtcbiAgICBjb25zdCB1aSA9IHZpZXcuJCQoY29uZmlnLmlkIHx8IGNvbmZpZyk7XG4gICAgbGV0IHNpbGVudCA9IGZhbHNlO1xuICAgIHVpLmF0dGFjaEV2ZW50KFwib25jaGFuZ2VcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoIXNpbGVudCkge1xuICAgICAgICAgICAgc2hvdyh2aWV3LCBjb25maWcsIHRoaXMuZ2V0VmFsdWUoKSk7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICB1aS5hdHRhY2hFdmVudChcIm9uYWZ0ZXJzZWxlY3RcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoIXNpbGVudCkge1xuICAgICAgICAgICAgbGV0IGlkID0gbnVsbDtcbiAgICAgICAgICAgIGlmICh1aS5zZXRWYWx1ZSkge1xuICAgICAgICAgICAgICAgIGlkID0gdGhpcy5nZXRWYWx1ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAodWkuZ2V0U2VsZWN0ZWRJZCkge1xuICAgICAgICAgICAgICAgIGlkID0gdWkuZ2V0U2VsZWN0ZWRJZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2hvdyh2aWV3LCBjb25maWcsIGlkKTtcbiAgICAgICAgfVxuICAgIH0pO1xuICAgIHZpZXcub24oYXBwLCBgYXBwOnJvdXRlYCwgZnVuY3Rpb24gKHVybCkge1xuICAgICAgICBjb25zdCBzZWdtZW50ID0gdXJsW3ZpZXcuZ2V0SW5kZXgoKV07XG4gICAgICAgIGlmIChzZWdtZW50KSB7XG4gICAgICAgICAgICBzaWxlbnQgPSB0cnVlO1xuICAgICAgICAgICAgY29uc3QgcGFnZSA9IHNlZ21lbnQucGFnZTtcbiAgICAgICAgICAgIGlmICh1aS5zZXRWYWx1ZSAmJiB1aS5nZXRWYWx1ZSgpICE9PSBwYWdlKSB7XG4gICAgICAgICAgICAgICAgdWkuc2V0VmFsdWUocGFnZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICh1aS5zZWxlY3QgJiYgdWkuZXhpc3RzKHBhZ2UpICYmIHVpLmdldFNlbGVjdGVkSWQoKSAhPT0gcGFnZSkge1xuICAgICAgICAgICAgICAgIHVpLnNlbGVjdChwYWdlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNpbGVudCA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgfSk7XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3BsdWdpbnMvTWVudS5qcyIsImNvbnN0IGJhc2VpY29ucyA9IHtcbiAgICBcImdvb2RcIjogXCJjaGVja1wiLFxuICAgIFwiZXJyb3JcIjogXCJ3YXJuaW5nXCIsXG4gICAgXCJzYXZpbmdcIjogXCJyZWZyZXNoIGZhLXNwaW5cIlxufTtcbmNvbnN0IGJhc2V0ZXh0ID0ge1xuICAgIFwiZ29vZFwiOiBcIk9rXCIsXG4gICAgXCJlcnJvclwiOiBcIkVycm9yXCIsXG4gICAgXCJzYXZpbmdcIjogXCJDb25uZWN0aW5nLi4uXCJcbn07XG5leHBvcnQgZnVuY3Rpb24gU3RhdHVzKGFwcCwgdmlldywgY29uZmlnKSB7XG4gICAgbGV0IHN0YXR1cyA9IFwiZ29vZFwiO1xuICAgIGxldCBjb3VudCA9IDA7XG4gICAgbGV0IGlzZXJyb3IgPSBmYWxzZTtcbiAgICBsZXQgZXhwaXJlRGVsYXkgPSBjb25maWcuZXhwaXJlO1xuICAgIGlmICghZXhwaXJlRGVsYXkgJiYgZXhwaXJlRGVsYXkgIT09IGZhbHNlKVxuICAgICAgICBleHBpcmVEZWxheSA9IDIwMDA7XG4gICAgbGV0IHRleHRzID0gY29uZmlnLnRleHRzIHx8IGJhc2V0ZXh0O1xuICAgIGxldCBpY29ucyA9IGNvbmZpZy5pY29ucyB8fCBiYXNlaWNvbnM7XG4gICAgaWYgKHR5cGVvZiBjb25maWcgPT09IFwic3RyaW5nXCIpXG4gICAgICAgIGNvbmZpZyA9IHsgdGFyZ2V0OiBjb25maWcgfTtcbiAgICBmdW5jdGlvbiByZWZyZXNoKGNvbnRlbnQpIHtcbiAgICAgICAgY29uc3QgYXJlYSA9IHZpZXcuJCQoY29uZmlnLnRhcmdldCk7XG4gICAgICAgIGlmIChhcmVhKSB7XG4gICAgICAgICAgICBpZiAoIWNvbnRlbnQpXG4gICAgICAgICAgICAgICAgY29udGVudCA9IFwiPGRpdiBjbGFzcz0nc3RhdHVzX1wiICsgc3RhdHVzICsgXCInPjxzcGFuIGNsYXNzPSd3ZWJpeF9pY29uIGZhLVwiICsgaWNvbnNbc3RhdHVzXSArIFwiJz48L3NwYW4+IFwiICsgdGV4dHNbc3RhdHVzXSArIFwiPC9kaXY+XCI7XG4gICAgICAgICAgICBhcmVhLnNldEhUTUwoY29udGVudCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgZnVuY3Rpb24gc3VjY2VzcygpIHtcbiAgICAgICAgY291bnQtLTtcbiAgICAgICAgc2V0U3RhdHVzKFwiZ29vZFwiKTtcbiAgICB9XG4gICAgZnVuY3Rpb24gZmFpbChlcnIpIHtcbiAgICAgICAgY291bnQtLTtcbiAgICAgICAgc2V0U3RhdHVzKFwiZXJyb3JcIiwgZXJyKTtcbiAgICB9XG4gICAgZnVuY3Rpb24gc3RhcnQocHJvbWlzZSkge1xuICAgICAgICBjb3VudCsrO1xuICAgICAgICBzZXRTdGF0dXMoXCJzYXZpbmdcIik7XG4gICAgICAgIGlmIChwcm9taXNlICYmIHByb21pc2UudGhlbikge1xuICAgICAgICAgICAgcHJvbWlzZS50aGVuKHN1Y2Nlc3MsIGZhaWwpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGZ1bmN0aW9uIGdldFN0YXR1cygpIHtcbiAgICAgICAgcmV0dXJuIHN0YXR1cztcbiAgICB9XG4gICAgZnVuY3Rpb24gaGlkZVN0YXR1cygpIHtcbiAgICAgICAgaWYgKGNvdW50ID09IDApIHtcbiAgICAgICAgICAgIHJlZnJlc2goXCIgXCIpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGZ1bmN0aW9uIHNldFN0YXR1cyhtb2RlLCBlcnIpIHtcbiAgICAgICAgaWYgKGNvdW50IDwgMCkge1xuICAgICAgICAgICAgY291bnQgPSAwO1xuICAgICAgICB9XG4gICAgICAgIGlmIChtb2RlID09PSBcInNhdmluZ1wiKSB7XG4gICAgICAgICAgICBzdGF0dXMgPSBcInNhdmluZ1wiO1xuICAgICAgICAgICAgcmVmcmVzaCgpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgaXNlcnJvciA9IChtb2RlID09PSBcImVycm9yXCIpO1xuICAgICAgICAgICAgaWYgKGNvdW50ID09PSAwKSB7XG4gICAgICAgICAgICAgICAgc3RhdHVzID0gaXNlcnJvciA/IFwiZXJyb3JcIiA6IFwiZ29vZFwiO1xuICAgICAgICAgICAgICAgIGlmIChpc2Vycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGFwcC5lcnJvcihcImFwcDplcnJvcjpzZXJ2ZXJcIiwgW2Vyci5yZXNwb25zZVRleHQgfHwgZXJyXSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAoZXhwaXJlRGVsYXkpXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGhpZGVTdGF0dXMsIGV4cGlyZURlbGF5KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmVmcmVzaCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIGZ1bmN0aW9uIHRyYWNrKGRhdGEpIHtcbiAgICAgICAgY29uc3QgZHAgPSB3ZWJpeC5kcChkYXRhKTtcbiAgICAgICAgaWYgKGRwKSB7XG4gICAgICAgICAgICB2aWV3Lm9uKGRwLCBcIm9uQWZ0ZXJEYXRhU2VuZFwiLCBzdGFydCk7XG4gICAgICAgICAgICB2aWV3Lm9uKGRwLCBcIm9uQWZ0ZXJTYXZlRXJyb3JcIiwgKGlkLCBvYmosIHJlc3BvbnNlKSA9PiBmYWlsKHJlc3BvbnNlKSk7XG4gICAgICAgICAgICB2aWV3Lm9uKGRwLCBcIm9uQWZ0ZXJTYXZlXCIsIHN1Y2Nlc3MpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGFwcC5zZXRTZXJ2aWNlKFwic3RhdHVzXCIsIHtcbiAgICAgICAgZ2V0U3RhdHVzLFxuICAgICAgICBzZXRTdGF0dXMsXG4gICAgICAgIHRyYWNrXG4gICAgfSk7XG4gICAgaWYgKGNvbmZpZy5yZW1vdGUpIHtcbiAgICAgICAgdmlldy5vbih3ZWJpeCwgXCJvblJlbW90ZUNhbGxcIiwgc3RhcnQpO1xuICAgIH1cbiAgICBpZiAoY29uZmlnLmFqYXgpIHtcbiAgICAgICAgdmlldy5vbih3ZWJpeCwgXCJvbkJlZm9yZUFqYXhcIiwgKG1vZGUsIHVybCwgZGF0YSwgcmVxdWVzdCwgaGVhZGVycywgZmlsZXMsIHByb21pc2UpID0+IHtcbiAgICAgICAgICAgIHN0YXJ0KHByb21pc2UpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgaWYgKGNvbmZpZy5kYXRhKSB7XG4gICAgICAgIHRyYWNrKGNvbmZpZy5kYXRhKTtcbiAgICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9ub2RlX21vZHVsZXMvd2ViaXgtamV0L3BsdWdpbnMvU3RhdHVzLmpzIiwiZXhwb3J0IGZ1bmN0aW9uIFRoZW1lKGFwcCwgdmlldywgY29uZmlnKSB7XG4gICAgY29uZmlnID0gY29uZmlnIHx8IHt9O1xuICAgIGNvbnN0IHN0b3JhZ2UgPSBjb25maWcuc3RvcmFnZTtcbiAgICBsZXQgdGhlbWUgPSBzdG9yYWdlID8gc3RvcmFnZS5nZXQoXCJ0aGVtZVwiKSA6IChjb25maWcudGhlbWUgfHwgXCJmbGF0LWRlZmF1bHRcIik7XG4gICAgY29uc3Qgc2VydmljZSA9IHtcbiAgICAgICAgZ2V0VGhlbWUoKSB7IHJldHVybiB0aGVtZTsgfSxcbiAgICAgICAgc2V0VGhlbWUobmFtZSwgc2lsZW50KSB7XG4gICAgICAgICAgICB2YXIgcGFydHMgPSBuYW1lLnNwbGl0KFwiLVwiKTtcbiAgICAgICAgICAgIHZhciBsaW5rcyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwibGlua1wiKTtcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGlua3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICB2YXIgbG5hbWUgPSBsaW5rc1tpXS5nZXRBdHRyaWJ1dGUoXCJ0aXRsZVwiKTtcbiAgICAgICAgICAgICAgICBpZiAobG5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGxuYW1lID09PSBuYW1lIHx8IGxuYW1lID09PSBwYXJ0c1swXSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGlua3NbaV0ucmVsID0gXCJzdHlsZXNoZWV0XCI7XG4gICAgICAgICAgICAgICAgICAgICAgICBsaW5rc1tpXS5kaXNhYmxlZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGlua3NbaV0ucmVsID0gXCJhbHRlcm5hdGUgc3R5bGVzaGVldFwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgbGlua3NbaV0uZGlzYWJsZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHdlYml4LnNraW4uc2V0KHBhcnRzWzBdKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLy9yZW1vdmUgb2xkIGNzc1xuICAgICAgICAgICAgICAgIHdlYml4Lmh0bWwucmVtb3ZlQ3NzKGRvY3VtZW50LmJvZHksIFwidGhlbWUtXCIgKyB0aGVtZSk7XG4gICAgICAgICAgICAgICAgLy9hZGQgbmV3IGNzc1xuICAgICAgICAgICAgICAgIHdlYml4Lmh0bWwuYWRkQ3NzKGRvY3VtZW50LmJvZHksIFwidGhlbWUtXCIgKyBuYW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoZW1lID0gbmFtZTtcbiAgICAgICAgICAgIGlmIChzdG9yYWdlKSB7XG4gICAgICAgICAgICAgICAgc3RvcmFnZS5wdXQoXCJ0aGVtZVwiLCBuYW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICghc2lsZW50KSB7XG4gICAgICAgICAgICAgICAgYXBwLnJlZnJlc2goKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG4gICAgYXBwLnNldFNlcnZpY2UoXCJ0aGVtZVwiLCBzZXJ2aWNlKTtcbiAgICBzZXJ2aWNlLnNldFRoZW1lKHRoZW1lLCB0cnVlKTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL25vZGVfbW9kdWxlcy93ZWJpeC1qZXQvcGx1Z2lucy9UaGVtZS5qcyIsImV4cG9ydCBmdW5jdGlvbiBVc2VyKGFwcCwgdmlldywgY29uZmlnKSB7XG4gICAgY29uZmlnID0gY29uZmlnIHx8IHt9O1xuICAgIGNvbnN0IGxvZ2luID0gY29uZmlnLmxvZ2luIHx8IFwiL2xvZ2luXCI7XG4gICAgY29uc3QgbG9nb3V0ID0gY29uZmlnLmxvZ291dCB8fCBcIi9sb2dvdXRcIjtcbiAgICBjb25zdCBhZnRlckxvZ2luID0gY29uZmlnLmFmdGVyTG9naW4gfHwgYXBwLmNvbmZpZy5zdGFydDtcbiAgICBjb25zdCBhZnRlckxvZ291dCA9IGNvbmZpZy5hZnRlckxvZ291dCB8fCBcIi9sb2dpblwiO1xuICAgIGNvbnN0IHBpbmcgPSBjb25maWcucGluZyB8fCA1ICogNjAgKiAxMDAwO1xuICAgIGNvbnN0IG1vZGVsID0gY29uZmlnLm1vZGVsO1xuICAgIGxldCB1c2VyID0gY29uZmlnLnVzZXI7XG4gICAgY29uc3Qgc2VydmljZSA9IHtcbiAgICAgICAgZ2V0VXNlcigpIHtcbiAgICAgICAgICAgIHJldHVybiB1c2VyO1xuICAgICAgICB9LFxuICAgICAgICBnZXRTdGF0dXMoc2VydmVyKSB7XG4gICAgICAgICAgICBpZiAoIXNlcnZlcikge1xuICAgICAgICAgICAgICAgIHJldHVybiB1c2VyICE9PSBudWxsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG1vZGVsLnN0YXR1cygpLmNhdGNoKCgpID0+IG51bGwpLnRoZW4oZGF0YSA9PiB7XG4gICAgICAgICAgICAgICAgdXNlciA9IGRhdGE7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcbiAgICAgICAgbG9naW4obmFtZSwgcGFzcykge1xuICAgICAgICAgICAgcmV0dXJuIG1vZGVsLmxvZ2luKG5hbWUsIHBhc3MpLnRoZW4oZGF0YSA9PiB7XG4gICAgICAgICAgICAgICAgdXNlciA9IGRhdGE7XG4gICAgICAgICAgICAgICAgaWYgKCFkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIHRocm93IChcIkFjY2VzcyBkZW5pZWRcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGFwcC5zaG93KGFmdGVyTG9naW4pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG4gICAgICAgIGxvZ291dCgpIHtcbiAgICAgICAgICAgIHVzZXIgPSBudWxsO1xuICAgICAgICAgICAgcmV0dXJuIG1vZGVsLmxvZ291dCgpO1xuICAgICAgICB9XG4gICAgfTtcbiAgICBmdW5jdGlvbiBjYW5OYXZpZ2F0ZSh1cmwsIG9iaikge1xuICAgICAgICBpZiAodXJsID09PSBsb2dvdXQpIHtcbiAgICAgICAgICAgIHNlcnZpY2UubG9nb3V0KCk7XG4gICAgICAgICAgICBvYmoucmVkaXJlY3QgPSBhZnRlckxvZ291dDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh1cmwgIT09IGxvZ2luICYmICFzZXJ2aWNlLmdldFN0YXR1cygpKSB7XG4gICAgICAgICAgICBvYmoucmVkaXJlY3QgPSBsb2dpbjtcbiAgICAgICAgfVxuICAgIH1cbiAgICBhcHAuc2V0U2VydmljZShcInVzZXJcIiwgc2VydmljZSk7XG4gICAgYXBwLmF0dGFjaEV2ZW50KGBhcHA6Z3VhcmRgLCBmdW5jdGlvbiAodXJsLCBfJHJvb3QsIG9iaikge1xuICAgICAgICBpZiAodHlwZW9mIHVzZXIgPT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgICAgIG9iai5jb25maXJtID0gc2VydmljZS5nZXRTdGF0dXModHJ1ZSkudGhlbihzb21lID0+IGNhbk5hdmlnYXRlKHVybCwgb2JqKSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGNhbk5hdmlnYXRlKHVybCwgb2JqKTtcbiAgICB9KTtcbiAgICBpZiAocGluZykge1xuICAgICAgICBzZXRJbnRlcnZhbCgoKSA9PiBzZXJ2aWNlLmdldFN0YXR1cyh0cnVlKSwgcGluZyk7XG4gICAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vbm9kZV9tb2R1bGVzL3dlYml4LWpldC9wbHVnaW5zL1VzZXIuanMiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4qKlxyXG4qKiBDb3B5cmlnaHQgKEMpIDIwMTYgVGhlIFF0IENvbXBhbnkgTHRkLlxyXG4qKiBDb3B5cmlnaHQgKEMpIDIwMTYgS2xhcu+/vWx2ZGFsZW5zIERhdGFrb25zdWx0IEFCLCBhIEtEQUIgR3JvdXAgY29tcGFueSwgaW5mb0BrZGFiLmNvbSwgYXV0aG9yIE1pbGlhbiBXb2xmZiA8bWlsaWFuLndvbGZmQGtkYWIuY29tPlxyXG4qKiBDb250YWN0OiBodHRwczovL3d3dy5xdC5pby9saWNlbnNpbmcvXHJcbioqXHJcbioqIFRoaXMgZmlsZSBpcyBwYXJ0IG9mIHRoZSBRdFdlYkNoYW5uZWwgbW9kdWxlIG9mIHRoZSBRdCBUb29sa2l0LlxyXG4qKlxyXG4qKiAkUVRfQkVHSU5fTElDRU5TRTpMR1BMJFxyXG4qKiBDb21tZXJjaWFsIExpY2Vuc2UgVXNhZ2VcclxuKiogTGljZW5zZWVzIGhvbGRpbmcgdmFsaWQgY29tbWVyY2lhbCBRdCBsaWNlbnNlcyBtYXkgdXNlIHRoaXMgZmlsZSBpblxyXG4qKiBhY2NvcmRhbmNlIHdpdGggdGhlIGNvbW1lcmNpYWwgbGljZW5zZSBhZ3JlZW1lbnQgcHJvdmlkZWQgd2l0aCB0aGVcclxuKiogU29mdHdhcmUgb3IsIGFsdGVybmF0aXZlbHksIGluIGFjY29yZGFuY2Ugd2l0aCB0aGUgdGVybXMgY29udGFpbmVkIGluXHJcbioqIGEgd3JpdHRlbiBhZ3JlZW1lbnQgYmV0d2VlbiB5b3UgYW5kIFRoZSBRdCBDb21wYW55LiBGb3IgbGljZW5zaW5nIHRlcm1zXHJcbioqIGFuZCBjb25kaXRpb25zIHNlZSBodHRwczovL3d3dy5xdC5pby90ZXJtcy1jb25kaXRpb25zLiBGb3IgZnVydGhlclxyXG4qKiBpbmZvcm1hdGlvbiB1c2UgdGhlIGNvbnRhY3QgZm9ybSBhdCBodHRwczovL3d3dy5xdC5pby9jb250YWN0LXVzLlxyXG4qKlxyXG4qKiBHTlUgTGVzc2VyIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgVXNhZ2VcclxuKiogQWx0ZXJuYXRpdmVseSwgdGhpcyBmaWxlIG1heSBiZSB1c2VkIHVuZGVyIHRoZSB0ZXJtcyBvZiB0aGUgR05VIExlc3NlclxyXG4qKiBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIHZlcnNpb24gMyBhcyBwdWJsaXNoZWQgYnkgdGhlIEZyZWUgU29mdHdhcmVcclxuKiogRm91bmRhdGlvbiBhbmQgYXBwZWFyaW5nIGluIHRoZSBmaWxlIExJQ0VOU0UuTEdQTDMgaW5jbHVkZWQgaW4gdGhlXHJcbioqIHBhY2thZ2luZyBvZiB0aGlzIGZpbGUuIFBsZWFzZSByZXZpZXcgdGhlIGZvbGxvd2luZyBpbmZvcm1hdGlvbiB0b1xyXG4qKiBlbnN1cmUgdGhlIEdOVSBMZXNzZXIgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSB2ZXJzaW9uIDMgcmVxdWlyZW1lbnRzXHJcbioqIHdpbGwgYmUgbWV0OiBodHRwczovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2xncGwtMy4wLmh0bWwuXHJcbioqXHJcbioqIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIFVzYWdlXHJcbioqIEFsdGVybmF0aXZlbHksIHRoaXMgZmlsZSBtYXkgYmUgdXNlZCB1bmRlciB0aGUgdGVybXMgb2YgdGhlIEdOVVxyXG4qKiBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIHZlcnNpb24gMi4wIG9yIChhdCB5b3VyIG9wdGlvbikgdGhlIEdOVSBHZW5lcmFsXHJcbioqIFB1YmxpYyBsaWNlbnNlIHZlcnNpb24gMyBvciBhbnkgbGF0ZXIgdmVyc2lvbiBhcHByb3ZlZCBieSB0aGUgS0RFIEZyZWVcclxuKiogUXQgRm91bmRhdGlvbi4gVGhlIGxpY2Vuc2VzIGFyZSBhcyBwdWJsaXNoZWQgYnkgdGhlIEZyZWUgU29mdHdhcmVcclxuKiogRm91bmRhdGlvbiBhbmQgYXBwZWFyaW5nIGluIHRoZSBmaWxlIExJQ0VOU0UuR1BMMiBhbmQgTElDRU5TRS5HUEwzXHJcbioqIGluY2x1ZGVkIGluIHRoZSBwYWNrYWdpbmcgb2YgdGhpcyBmaWxlLiBQbGVhc2UgcmV2aWV3IHRoZSBmb2xsb3dpbmdcclxuKiogaW5mb3JtYXRpb24gdG8gZW5zdXJlIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSByZXF1aXJlbWVudHMgd2lsbFxyXG4qKiBiZSBtZXQ6IGh0dHBzOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sIGFuZFxyXG4qKiBodHRwczovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0zLjAuaHRtbC5cclxuKipcclxuKiogJFFUX0VORF9MSUNFTlNFJFxyXG4qKlxyXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuY29uc3QgUVdlYkNoYW5uZWxNZXNzYWdlVHlwZXMgPSB7XHJcbiAgICBzaWduYWw6IDEsXHJcbiAgICBwcm9wZXJ0eVVwZGF0ZTogMixcclxuICAgIGluaXQ6IDMsXHJcbiAgICBpZGxlOiA0LFxyXG4gICAgZGVidWc6IDUsXHJcbiAgICBpbnZva2VNZXRob2Q6IDYsXHJcbiAgICBjb25uZWN0VG9TaWduYWw6IDcsXHJcbiAgICBkaXNjb25uZWN0RnJvbVNpZ25hbDogOCxcclxuICAgIHNldFByb3BlcnR5OiA5LFxyXG4gICAgcmVzcG9uc2U6IDEwLFxyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24odHJhbnNwb3J0LCBpbml0Q2FsbGJhY2spIHtcclxuXHJcbiAgICBpZiAodHlwZW9mIHRyYW5zcG9ydCAhPT0gJ29iamVjdCcgfHwgdHlwZW9mIHRyYW5zcG9ydC5zZW5kICE9PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcignVGhlIFFXZWJDaGFubmVsIGV4cGVjdHMgYSB0cmFuc3BvcnQgb2JqZWN0IHdpdGggYSBzZW5kIGZ1bmN0aW9uIGFuZCBvbm1lc3NhZ2UgY2FsbGJhY2sgcHJvcGVydHkuJyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAnIEdpdmVuIGlzOiB0cmFuc3BvcnQ6ICcgKyB0eXBlb2YodHJhbnNwb3J0KSArICcsIHRyYW5zcG9ydC5zZW5kOiAnICsgdHlwZW9mKHRyYW5zcG9ydC5zZW5kKSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IGNoYW5uZWwgPSB0aGlzO1xyXG4gICAgdGhpcy50cmFuc3BvcnQgPSB0cmFuc3BvcnQ7XHJcblxyXG4gICAgdGhpcy5zZW5kID0gZnVuY3Rpb24oZGF0YSkge1xyXG5cclxuICAgICAgICBpZiAodHlwZW9mKGRhdGEpICE9PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBkYXRhID0gSlNPTi5zdHJpbmdpZnkoZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNoYW5uZWwudHJhbnNwb3J0LnNlbmQoZGF0YSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMudHJhbnNwb3J0Lm9ubWVzc2FnZSA9IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcclxuXHJcbiAgICAgICAgbGV0IGRhdGEgPSBtZXNzYWdlLmRhdGE7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBkYXRhID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZShkYXRhKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgc3dpdGNoIChkYXRhLnR5cGUpIHtcclxuICAgICAgICBjYXNlIFFXZWJDaGFubmVsTWVzc2FnZVR5cGVzLnNpZ25hbDpcclxuICAgICAgICAgICAgY2hhbm5lbC5oYW5kbGVTaWduYWwoZGF0YSk7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGNhc2UgUVdlYkNoYW5uZWxNZXNzYWdlVHlwZXMucmVzcG9uc2U6XHJcbiAgICAgICAgICAgIGNoYW5uZWwuaGFuZGxlUmVzcG9uc2UoZGF0YSk7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGNhc2UgUVdlYkNoYW5uZWxNZXNzYWdlVHlwZXMucHJvcGVydHlVcGRhdGU6XHJcbiAgICAgICAgICAgIGNoYW5uZWwuaGFuZGxlUHJvcGVydHlVcGRhdGUoZGF0YSk7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ2ludmFsaWQgbWVzc2FnZSByZWNlaXZlZDonLCBtZXNzYWdlLmRhdGEpO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuZXhlY0NhbGxiYWNrcyA9IHt9O1xyXG5cclxuICAgIHRoaXMuZXhlY0lkID0gMDtcclxuXHJcbiAgICB0aGlzLmV4ZWMgPSBmdW5jdGlvbihkYXRhLCBjYWxsYmFjaykge1xyXG5cclxuICAgICAgICBpZiAoIWNhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgIC8vIGlmIG5vIGNhbGxiYWNrIGlzIGdpdmVuLCBzZW5kIGRpcmVjdGx5XHJcbiAgICAgICAgICAgIGNoYW5uZWwuc2VuZChkYXRhKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY2hhbm5lbC5leGVjSWQgPT09IE51bWJlci5NQVhfVkFMVUUpIHtcclxuICAgICAgICAgICAgLy8gd3JhcFxyXG4gICAgICAgICAgICBjaGFubmVsLmV4ZWNJZCA9IE51bWJlci5NSU5fVkFMVUU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChkYXRhLmhhc093blByb3BlcnR5KCdpZCcpKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Nhbm5vdCBleGVjIG1lc3NhZ2Ugd2l0aCBwcm9wZXJ0eSBpZDogJyArIEpTT04uc3RyaW5naWZ5KGRhdGEpKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkYXRhLmlkID0gY2hhbm5lbC5leGVjSWQrKztcclxuICAgICAgICBjaGFubmVsLmV4ZWNDYWxsYmFja3NbZGF0YS5pZF0gPSBjYWxsYmFjaztcclxuICAgICAgICBjaGFubmVsLnNlbmQoZGF0YSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMub2JqZWN0cyA9IHt9O1xyXG5cclxuICAgIHRoaXMuaGFuZGxlU2lnbmFsID0gZnVuY3Rpb24obWVzc2FnZSkge1xyXG5cclxuICAgICAgICBjb25zdCBvYmplY3QgPSBjaGFubmVsLm9iamVjdHNbbWVzc2FnZS5vYmplY3RdO1xyXG4gICAgICAgIGlmIChvYmplY3QpIHtcclxuICAgICAgICAgICAgb2JqZWN0LnNpZ25hbEVtaXR0ZWQobWVzc2FnZS5zaWduYWwsIG1lc3NhZ2UuYXJncyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKCdVbmhhbmRsZWQgc2lnbmFsOiAnICsgbWVzc2FnZS5vYmplY3QgKyAnOjonICsgbWVzc2FnZS5zaWduYWwpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5oYW5kbGVSZXNwb25zZSA9IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcclxuXHJcbiAgICAgICAgaWYgKCFtZXNzYWdlLmhhc093blByb3BlcnR5KCdpZCcpKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0ludmFsaWQgcmVzcG9uc2UgbWVzc2FnZSByZWNlaXZlZDogJywgSlNPTi5zdHJpbmdpZnkobWVzc2FnZSkpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNoYW5uZWwuZXhlY0NhbGxiYWNrc1ttZXNzYWdlLmlkXShtZXNzYWdlLmRhdGEpO1xyXG4gICAgICAgIGRlbGV0ZSBjaGFubmVsLmV4ZWNDYWxsYmFja3NbbWVzc2FnZS5pZF07XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuaGFuZGxlUHJvcGVydHlVcGRhdGUgPSBmdW5jdGlvbihtZXNzYWdlKSB7XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgaW4gbWVzc2FnZS5kYXRhKSB7XHJcbiAgICAgICAgICAgIGxldCBkYXRhID0gbWVzc2FnZS5kYXRhW2ldO1xyXG4gICAgICAgICAgICBsZXQgb2JqZWN0ID0gY2hhbm5lbC5vYmplY3RzW2RhdGEub2JqZWN0XTtcclxuICAgICAgICAgICAgaWYgKG9iamVjdCkge1xyXG4gICAgICAgICAgICAgICAgb2JqZWN0LnByb3BlcnR5VXBkYXRlKGRhdGEuc2lnbmFscywgZGF0YS5wcm9wZXJ0aWVzKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybignVW5oYW5kbGVkIHByb3BlcnR5IHVwZGF0ZTogJyArIGRhdGEub2JqZWN0ICsgJzo6JyArIGRhdGEuc2lnbmFsKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBjaGFubmVsLmV4ZWMoe3R5cGU6IFFXZWJDaGFubmVsTWVzc2FnZVR5cGVzLmlkbGV9KTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5kZWJ1ZyA9IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcclxuICAgICAgICBjaGFubmVsLnNlbmQoe3R5cGU6IFFXZWJDaGFubmVsTWVzc2FnZVR5cGVzLmRlYnVnLCBkYXRhOiBtZXNzYWdlfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIGNoYW5uZWwuZXhlYyh7dHlwZTogUVdlYkNoYW5uZWxNZXNzYWdlVHlwZXMuaW5pdH0sIGZ1bmN0aW9uKGRhdGEpIHtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgb2JqZWN0TmFtZSBpbiBkYXRhKSB7XHJcbiAgICAgICAgICAgIC8qbGV0IG9iamVjdCA9ICovbmV3IFFPYmplY3Qob2JqZWN0TmFtZSwgZGF0YVtvYmplY3ROYW1lXSwgY2hhbm5lbCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIG5vdyB1bndyYXAgcHJvcGVydGllcywgd2hpY2ggbWlnaHQgcmVmZXJlbmNlIG90aGVyIHJlZ2lzdGVyZWQgb2JqZWN0c1xyXG4gICAgICAgIGZvciAobGV0IG9iamVjdE5hbWUgaW4gY2hhbm5lbC5vYmplY3RzKSB7XHJcbiAgICAgICAgICAgIGNoYW5uZWwub2JqZWN0c1tvYmplY3ROYW1lXS51bndyYXBQcm9wZXJ0aWVzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChpbml0Q2FsbGJhY2spIHtcclxuICAgICAgICAgICAgaW5pdENhbGxiYWNrKGNoYW5uZWwpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjaGFubmVsLmV4ZWMoe3R5cGU6IFFXZWJDaGFubmVsTWVzc2FnZVR5cGVzLmlkbGV9KTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBRT2JqZWN0KG5hbWUsIGRhdGEsIHdlYkNoYW5uZWwpIHtcclxuICAgIHRoaXMuX19pZF9fID0gbmFtZTtcclxuICAgIHdlYkNoYW5uZWwub2JqZWN0c1tuYW1lXSA9IHRoaXM7XHJcblxyXG4gICAgLy8gTGlzdCBvZiBjYWxsYmFja3MgdGhhdCBnZXQgaW52b2tlZCB1cG9uIHNpZ25hbCBlbWlzc2lvblxyXG4gICAgdGhpcy5fX29iamVjdFNpZ25hbHNfXyA9IHt9O1xyXG5cclxuICAgIC8vIENhY2hlIG9mIGFsbCBwcm9wZXJ0aWVzLCB1cGRhdGVkIHdoZW4gYSBub3RpZnkgc2lnbmFsIGlzIGVtaXR0ZWRcclxuICAgIHRoaXMuX19wcm9wZXJ0eUNhY2hlX18gPSB7fTtcclxuXHJcbiAgICBsZXQgb2JqZWN0ID0gdGhpcztcclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgdGhpcy51bndyYXBRT2JqZWN0ID0gZnVuY3Rpb24ocmVzcG9uc2UpIHtcclxuICAgICAgICBpZiAocmVzcG9uc2UgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICAgICAgICAvLyBzdXBwb3J0IGxpc3Qgb2Ygb2JqZWN0c1xyXG4gICAgICAgICAgICBsZXQgcmV0ID0gbmV3IEFycmF5KHJlc3BvbnNlLmxlbmd0aCk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcmVzcG9uc2UubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgICAgIHJldFtpXSA9IG9iamVjdC51bndyYXBRT2JqZWN0KHJlc3BvbnNlW2ldKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcmV0O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXJlc3BvbnNlIHx8ICFyZXNwb25zZVsnX19RT2JqZWN0Kl9fJ10gfHwgcmVzcG9uc2UuaWQgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgb2JqZWN0SWQgPSByZXNwb25zZS5pZDtcclxuICAgICAgICBpZiAod2ViQ2hhbm5lbC5vYmplY3RzW29iamVjdElkXSkge1xyXG4gICAgICAgICAgICByZXR1cm4gd2ViQ2hhbm5lbC5vYmplY3RzW29iamVjdElkXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghcmVzcG9uc2UuZGF0YSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdDYW5ub3QgdW53cmFwIHVua25vd24gUU9iamVjdCAnICsgb2JqZWN0SWQgKyAnIHdpdGhvdXQgZGF0YS4nKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHFPYmplY3QgPSBuZXcgUU9iamVjdCggb2JqZWN0SWQsIHJlc3BvbnNlLmRhdGEsIHdlYkNoYW5uZWwgKTtcclxuICAgICAgICBxT2JqZWN0LmRlc3Ryb3llZC5jb25uZWN0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBpZiAod2ViQ2hhbm5lbC5vYmplY3RzW29iamVjdElkXSA9PT0gcU9iamVjdCkge1xyXG4gICAgICAgICAgICAgICAgZGVsZXRlIHdlYkNoYW5uZWwub2JqZWN0c1tvYmplY3RJZF07XHJcbiAgICAgICAgICAgICAgICAvLyByZXNldCB0aGUgbm93IGRlbGV0ZWQgUU9iamVjdCB0byBhbiBlbXB0eSB7fSBvYmplY3RcclxuICAgICAgICAgICAgICAgIC8vIGp1c3QgYXNzaWduaW5nIHt9IHRob3VnaCB3b3VsZCBub3QgaGF2ZSB0aGUgZGVzaXJlZCBlZmZlY3QsIGJ1dCB0aGVcclxuICAgICAgICAgICAgICAgIC8vIGJlbG93IGFsc28gZW5zdXJlcyBhbGwgZXh0ZXJuYWwgcmVmZXJlbmNlcyB3aWxsIHNlZSB0aGUgZW1wdHkgbWFwXHJcbiAgICAgICAgICAgICAgICAvLyBOT1RFOiB0aGlzIGRldG91ciBpcyBuZWNlc3NhcnkgdG8gd29ya2Fyb3VuZCBRVEJVRy00MDAyMVxyXG4gICAgICAgICAgICAgICAgbGV0IHByb3BlcnR5TmFtZXMgPSBbXTtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IHByb3BlcnR5TmFtZSBpbiBxT2JqZWN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJvcGVydHlOYW1lcy5wdXNoKHByb3BlcnR5TmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpZHggaW4gcHJvcGVydHlOYW1lcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBxT2JqZWN0W3Byb3BlcnR5TmFtZXNbaWR4XV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICAvLyBoZXJlIHdlIGFyZSBhbHJlYWR5IGluaXRpYWxpemVkLCBhbmQgdGh1cyBtdXN0IGRpcmVjdGx5IHVud3JhcCB0aGUgcHJvcGVydGllc1xyXG4gICAgICAgIHFPYmplY3QudW53cmFwUHJvcGVydGllcygpO1xyXG4gICAgICAgIHJldHVybiBxT2JqZWN0O1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnVud3JhcFByb3BlcnRpZXMgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgcHJvcGVydHlJZHggaW4gb2JqZWN0Ll9fcHJvcGVydHlDYWNoZV9fKSB7XHJcbiAgICAgICAgICAgIG9iamVjdC5fX3Byb3BlcnR5Q2FjaGVfX1twcm9wZXJ0eUlkeF0gPSBvYmplY3QudW53cmFwUU9iamVjdChvYmplY3QuX19wcm9wZXJ0eUNhY2hlX19bcHJvcGVydHlJZHhdKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGZ1bmN0aW9uIGFkZFNpZ25hbChzaWduYWxEYXRhLCBpc1Byb3BlcnR5Tm90aWZ5U2lnbmFsKVxyXG4gICAge1xyXG4gICAgICAgIGxldCBzaWduYWxOYW1lID0gc2lnbmFsRGF0YVswXTtcclxuICAgICAgICBsZXQgc2lnbmFsSW5kZXggPSBzaWduYWxEYXRhWzFdO1xyXG4gICAgICAgIG9iamVjdFtzaWduYWxOYW1lXSA9IHtcclxuICAgICAgICAgICAgY29ubmVjdDogZnVuY3Rpb24oY2FsbGJhY2spIHtcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YoY2FsbGJhY2spICE9PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignQmFkIGNhbGxiYWNrIGdpdmVuIHRvIGNvbm5lY3QgdG8gc2lnbmFsICcgKyBzaWduYWxOYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgb2JqZWN0Ll9fb2JqZWN0U2lnbmFsc19fW3NpZ25hbEluZGV4XSA9IG9iamVjdC5fX29iamVjdFNpZ25hbHNfX1tzaWduYWxJbmRleF0gfHwgW107XHJcbiAgICAgICAgICAgICAgICBvYmplY3QuX19vYmplY3RTaWduYWxzX19bc2lnbmFsSW5kZXhdLnB1c2goY2FsbGJhY2spO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICghaXNQcm9wZXJ0eU5vdGlmeVNpZ25hbCAmJiBzaWduYWxOYW1lICE9PSAnZGVzdHJveWVkJykge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIG9ubHkgcmVxdWlyZWQgZm9yIFwicHVyZVwiIHNpZ25hbHMsIGhhbmRsZWQgc2VwYXJhdGVseSBmb3IgcHJvcGVydGllcyBpbiBwcm9wZXJ0eVVwZGF0ZVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGFsc28gbm90ZSB0aGF0IHdlIGFsd2F5cyBnZXQgbm90aWZpZWQgYWJvdXQgdGhlIGRlc3Ryb3llZCBzaWduYWxcclxuICAgICAgICAgICAgICAgICAgICB3ZWJDaGFubmVsLmV4ZWMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBRV2ViQ2hhbm5lbE1lc3NhZ2VUeXBlcy5jb25uZWN0VG9TaWduYWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9iamVjdDogb2JqZWN0Ll9faWRfXyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2lnbmFsOiBzaWduYWxJbmRleFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBkaXNjb25uZWN0OiBmdW5jdGlvbihjYWxsYmFjaykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZihjYWxsYmFjaykgIT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdCYWQgY2FsbGJhY2sgZ2l2ZW4gdG8gZGlzY29ubmVjdCBmcm9tIHNpZ25hbCAnICsgc2lnbmFsTmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgb2JqZWN0Ll9fb2JqZWN0U2lnbmFsc19fW3NpZ25hbEluZGV4XSA9IG9iamVjdC5fX29iamVjdFNpZ25hbHNfX1tzaWduYWxJbmRleF0gfHwgW107XHJcbiAgICAgICAgICAgICAgICBsZXQgaWR4ID0gb2JqZWN0Ll9fb2JqZWN0U2lnbmFsc19fW3NpZ25hbEluZGV4XS5pbmRleE9mKGNhbGxiYWNrKTtcclxuICAgICAgICAgICAgICAgIGlmIChpZHggPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignQ2Fubm90IGZpbmQgY29ubmVjdGlvbiBvZiBzaWduYWwgJyArIHNpZ25hbE5hbWUgKyAnIHRvICcgKyBjYWxsYmFjay5uYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBvYmplY3QuX19vYmplY3RTaWduYWxzX19bc2lnbmFsSW5kZXhdLnNwbGljZShpZHgsIDEpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFpc1Byb3BlcnR5Tm90aWZ5U2lnbmFsICYmIG9iamVjdC5fX29iamVjdFNpZ25hbHNfX1tzaWduYWxJbmRleF0ubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gb25seSByZXF1aXJlZCBmb3IgXCJwdXJlXCIgc2lnbmFscywgaGFuZGxlZCBzZXBhcmF0ZWx5IGZvciBwcm9wZXJ0aWVzIGluIHByb3BlcnR5VXBkYXRlXHJcbiAgICAgICAgICAgICAgICAgICAgd2ViQ2hhbm5lbC5leGVjKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUVdlYkNoYW5uZWxNZXNzYWdlVHlwZXMuZGlzY29ubmVjdEZyb21TaWduYWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9iamVjdDogb2JqZWN0Ll9faWRfXyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2lnbmFsOiBzaWduYWxJbmRleFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEludm9rZXMgYWxsIGNhbGxiYWNrcyBmb3IgdGhlIGdpdmVuIHNpZ25hbG5hbWUuIEFsc28gd29ya3MgZm9yIHByb3BlcnR5IG5vdGlmeSBjYWxsYmFja3MuXHJcbiAgICAgKi9cclxuICAgIGZ1bmN0aW9uIGludm9rZVNpZ25hbENhbGxiYWNrcyhzaWduYWxOYW1lLCBzaWduYWxBcmdzKSB7XHJcblxyXG4gICAgICAgIGxldCBjb25uZWN0aW9ucyA9IG9iamVjdC5fX29iamVjdFNpZ25hbHNfX1tzaWduYWxOYW1lXTtcclxuXHJcbiAgICAgICAgaWYgKGNvbm5lY3Rpb25zKSB7XHJcbiAgICAgICAgICAgIGNvbm5lY3Rpb25zLmZvckVhY2goZnVuY3Rpb24oY2FsbGJhY2spIHtcclxuICAgICAgICAgICAgICAgIGNhbGxiYWNrLmFwcGx5KGNhbGxiYWNrLCBzaWduYWxBcmdzKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRoaXMucHJvcGVydHlVcGRhdGUgPSBmdW5jdGlvbihzaWduYWxzLCBwcm9wZXJ0eU1hcCkge1xyXG4gICAgICAgIC8vIHVwZGF0ZSBwcm9wZXJ0eSBjYWNoZVxyXG4gICAgICAgIGZvciAobGV0IHByb3BlcnR5SW5kZXggaW4gcHJvcGVydHlNYXApIHtcclxuICAgICAgICAgICAgbGV0IHByb3BlcnR5VmFsdWUgPSBwcm9wZXJ0eU1hcFtwcm9wZXJ0eUluZGV4XTtcclxuICAgICAgICAgICAgb2JqZWN0Ll9fcHJvcGVydHlDYWNoZV9fW3Byb3BlcnR5SW5kZXhdID0gcHJvcGVydHlWYWx1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZvciAobGV0IHNpZ25hbE5hbWUgaW4gc2lnbmFscykge1xyXG4gICAgICAgICAgICAvLyBJbnZva2UgYWxsIGNhbGxiYWNrcywgYXMgc2lnbmFsRW1pdHRlZCgpIGRvZXMgbm90LiBUaGlzIGVuc3VyZXMgdGhlXHJcbiAgICAgICAgICAgIC8vIHByb3BlcnR5IGNhY2hlIGlzIHVwZGF0ZWQgYmVmb3JlIHRoZSBjYWxsYmFja3MgYXJlIGludm9rZWQuXHJcbiAgICAgICAgICAgIGludm9rZVNpZ25hbENhbGxiYWNrcyhzaWduYWxOYW1lLCBzaWduYWxzW3NpZ25hbE5hbWVdKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuc2lnbmFsRW1pdHRlZCA9IGZ1bmN0aW9uKHNpZ25hbE5hbWUsIHNpZ25hbEFyZ3MpIHtcclxuICAgICAgICBpbnZva2VTaWduYWxDYWxsYmFja3Moc2lnbmFsTmFtZSwgdGhpcy51bndyYXBRT2JqZWN0KHNpZ25hbEFyZ3MpKTtcclxuICAgIH07XHJcblxyXG4gICAgZnVuY3Rpb24gYWRkTWV0aG9kKG1ldGhvZERhdGEpIHtcclxuICAgICAgICBsZXQgbWV0aG9kTmFtZSA9IG1ldGhvZERhdGFbMF07XHJcbiAgICAgICAgbGV0IG1ldGhvZElkeCA9IG1ldGhvZERhdGFbMV07XHJcbiAgICAgICAgb2JqZWN0W21ldGhvZE5hbWVdID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGxldCBhcmdzID0gW107XHJcbiAgICAgICAgICAgIGxldCBjYWxsYmFjaztcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgICAgIGxldCBhcmd1bWVudCA9IGFyZ3VtZW50c1tpXTtcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgYXJndW1lbnQgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayA9IGFyZ3VtZW50O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChhcmd1bWVudCBpbnN0YW5jZW9mIFFPYmplY3QgJiYgd2ViQ2hhbm5lbC5vYmplY3RzW2FyZ3VtZW50Ll9faWRfX10gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFyZ3MucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdpZCc6IGFyZ3VtZW50Ll9faWRfX1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBhcmdzLnB1c2goYXJndW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB3ZWJDaGFubmVsLmV4ZWMoe1xyXG4gICAgICAgICAgICAgICAgdHlwZTogUVdlYkNoYW5uZWxNZXNzYWdlVHlwZXMuaW52b2tlTWV0aG9kLFxyXG4gICAgICAgICAgICAgICAgb2JqZWN0OiBvYmplY3QuX19pZF9fLFxyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiBtZXRob2RJZHgsXHJcbiAgICAgICAgICAgICAgICBhcmdzOiBhcmdzXHJcbiAgICAgICAgICAgIH0sIGZ1bmN0aW9uKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCByZXN1bHQgPSBvYmplY3QudW53cmFwUU9iamVjdChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChjYWxsYmFjaykocmVzdWx0KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gYmluZEdldHRlclNldHRlcihwcm9wZXJ0eUluZm8pXHJcbiAgICB7XHJcbiAgICAgICAgbGV0IHByb3BlcnR5SW5kZXggPSBwcm9wZXJ0eUluZm9bMF07XHJcbiAgICAgICAgbGV0IHByb3BlcnR5TmFtZSA9IHByb3BlcnR5SW5mb1sxXTtcclxuICAgICAgICBsZXQgbm90aWZ5U2lnbmFsRGF0YSA9IHByb3BlcnR5SW5mb1syXTtcclxuICAgICAgICAvLyBpbml0aWFsaXplIHByb3BlcnR5IGNhY2hlIHdpdGggY3VycmVudCB2YWx1ZVxyXG4gICAgICAgIC8vIE5PVEU6IGlmIHRoaXMgaXMgYW4gb2JqZWN0LCBpdCBpcyBub3QgZGlyZWN0bHkgdW53cmFwcGVkIGFzIGl0IG1pZ2h0XHJcbiAgICAgICAgLy8gcmVmZXJlbmNlIG90aGVyIFFPYmplY3QgdGhhdCB3ZSBkbyBub3Qga25vdyB5ZXRcclxuICAgICAgICBvYmplY3QuX19wcm9wZXJ0eUNhY2hlX19bcHJvcGVydHlJbmRleF0gPSBwcm9wZXJ0eUluZm9bM107XHJcblxyXG4gICAgICAgIGlmIChub3RpZnlTaWduYWxEYXRhKSB7XHJcbiAgICAgICAgICAgIGlmIChub3RpZnlTaWduYWxEYXRhWzBdID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBzaWduYWwgbmFtZSBpcyBvcHRpbWl6ZWQgYXdheSwgcmVjb25zdHJ1Y3QgdGhlIGFjdHVhbCBuYW1lXHJcbiAgICAgICAgICAgICAgICBub3RpZnlTaWduYWxEYXRhWzBdID0gcHJvcGVydHlOYW1lICsgJ0NoYW5nZWQnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGFkZFNpZ25hbChub3RpZnlTaWduYWxEYXRhLCB0cnVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmplY3QsIHByb3BlcnR5TmFtZSwge1xyXG4gICAgICAgICAgICBjb25maWd1cmFibGU6IHRydWUsXHJcbiAgICAgICAgICAgIGdldDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHByb3BlcnR5VmFsdWUgPSBvYmplY3QuX19wcm9wZXJ0eUNhY2hlX19bcHJvcGVydHlJbmRleF07XHJcbiAgICAgICAgICAgICAgICBpZiAocHJvcGVydHlWYWx1ZSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVGhpcyBzaG91bGRuJ3QgaGFwcGVuXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCdVbmRlZmluZWQgdmFsdWUgaW4gcHJvcGVydHkgY2FjaGUgZm9yIHByb3BlcnR5IFxcJycgKyBwcm9wZXJ0eU5hbWUgKyAnXFwnIGluIG9iamVjdCAnICsgb2JqZWN0Ll9faWRfXyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHByb3BlcnR5VmFsdWU7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHNldDogZnVuY3Rpb24odmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIGlmICh2YWx1ZSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCdQcm9wZXJ0eSBzZXR0ZXIgZm9yICcgKyBwcm9wZXJ0eU5hbWUgKyAnIGNhbGxlZCB3aXRoIHVuZGVmaW5lZCB2YWx1ZSEnKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBvYmplY3QuX19wcm9wZXJ0eUNhY2hlX19bcHJvcGVydHlJbmRleF0gPSB2YWx1ZTtcclxuICAgICAgICAgICAgICAgIGxldCB2YWx1ZVRvU2VuZCA9IHZhbHVlO1xyXG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlVG9TZW5kIGluc3RhbmNlb2YgUU9iamVjdCAmJiB3ZWJDaGFubmVsLm9iamVjdHNbdmFsdWVUb1NlbmQuX19pZF9fXSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVUb1NlbmQgPSB7ICdpZCc6IHZhbHVlVG9TZW5kLl9faWRfXyB9O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgd2ViQ2hhbm5lbC5leGVjKHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBRV2ViQ2hhbm5lbE1lc3NhZ2VUeXBlcy5zZXRQcm9wZXJ0eSxcclxuICAgICAgICAgICAgICAgICAgICBvYmplY3Q6IG9iamVjdC5fX2lkX18sXHJcbiAgICAgICAgICAgICAgICAgICAgcHJvcGVydHk6IHByb3BlcnR5SW5kZXgsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHZhbHVlVG9TZW5kXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgZGF0YS5tZXRob2RzLmZvckVhY2goYWRkTWV0aG9kKTtcclxuXHJcbiAgICBkYXRhLnByb3BlcnRpZXMuZm9yRWFjaChiaW5kR2V0dGVyU2V0dGVyKTtcclxuXHJcbiAgICBkYXRhLnNpZ25hbHMuZm9yRWFjaChmdW5jdGlvbihzaWduYWwpIHsgYWRkU2lnbmFsKHNpZ25hbCwgZmFsc2UpOyB9KTtcclxuXHJcbiAgICBmb3IgKGxldCBuYW1lIGluIGRhdGEuZW51bXMpIHtcclxuICAgICAgICBvYmplY3RbbmFtZV0gPSBkYXRhLmVudW1zW25hbWVdO1xyXG4gICAgfVxyXG59XHJcblxyXG4vL3JlcXVpcmVkIGZvciB1c2Ugd2l0aCBub2RlanNcclxuLy8gaWYgKHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnKSB7XHJcbi8vICAgICBtb2R1bGUuZXhwb3J0cyA9IHtcclxuLy8gICAgICAgICBRV2ViQ2hhbm5lbDogUVdlYkNoYW5uZWxcclxuLy8gICAgIH07XHJcbi8vIH1cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc291cmNlcy91dGlscy9xd2ViY2hhbm5lbC5qcyJdLCJzb3VyY2VSb290IjoiIn0=