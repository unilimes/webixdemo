/* Main Entry App JavaScript */

import "./styles/app.css";
import {JetApp} from "webix-jet";
import QWebChannel from 'utils/qwebchannel';

webix.ready(() => {
    const app = new JetApp({
        id:         APPNAME,
        version:    VERSION,
        start:      "/layout"
    });

    app.render().then(() => attachEventsToUI());
    
    app.attachEvent("app:error:resolve", function(...args){
        window.console.error(args);
    });
});

let native = {};

let nativePromise = new Promise((resolve) => {
    new QWebChannel(qt.webChannelTransport, function(channel) {
        let info = "";
        native = channel.objects;

        native.Utils.version(function(version) {
            info += version;
            native.Utils.screenInfo(function(screenInfo) {
                info += "\n\nDesktop info " + JSON.stringify(screenInfo, null, 4);
                resolve(info);
            });
        });
    });
});


nativePromise.then(function(info) {
    native.Utils.showMessage("Designer", "Loaded Webchannel" + info, false);
    native.Utils.logMessage("Loaded WebChannel" + info);
}, function(info) {
    native.Utils.showMessage("Designer", "Failed to load Webchannel", false);
    native.Utils.logMessage("Fail to load WebChannel");
});

function attachEventsToUI() {
    
}

export { native };
