/* Base Sidebar JavaScript */
import {JetView, plugins} from "webix-jet";
import projectExplorer from "views/sidebar/projectExplorer";
import propertyList from "views/sidebar/propertyList";

export default class SidebarView extends JetView {
    config() {
        let sidebar = {
            id: "sidebarContainer",
            minWidth:50,
            width:300,
            rows:[ 
                projectExplorer, 
                { 
                    id:"sidebarHorResizer",
                    view:"resizer"
                }, 
                propertyList 
            ]  
        };

        return sidebar;
    };
}