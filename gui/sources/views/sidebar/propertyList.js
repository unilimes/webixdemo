/* Property List JavaScript */
import {JetView, plugins} from "webix-jet";
import {propertyStore} from "models/propertyStore"

export default class PropertyListView extends JetView {
    config() {
        let propertyList = {
            id:"propertyListContainter",
            rows:[
                {
                    view:"template",
                    template:"Property List",
                    type:"header",
                    height:40,
                    borderless:true 
                },
                {
                    id:"propertyListFunctions",
                    view:"toolbar",
                    type:"clean",
                    height:30,
                    elements: [
                        { width: 5 },
                        {
                            id:"sortButton",
                            view: "button", 
                            type: "icon", 
                            icon: "sort-amount-asc",
                            width: 32,
                            css: "app_button",
                            click:() => { webix.message("detail"); } 
                        },
                        {
                            id:"viewFilter",
                            view: "toggle",
                            type: "icon",
                            name:"viewFilter",
                            offIcon: "sort-amount-asc",
                            onIcon: "sort-amount-desc",
                            width: 32,
                            on:{
                                onChange:(newv, oldv) => {propertyStore.showAll(newv);}
                            }
                        },
                        {
                            id: "propertyFilter",
                            view:"text",
                            on:{
                                onTimedKeyPress:function(){
                                    propertyStore.filterLabel(this.getValue().toLowerCase());
                                }
                            }
                        },
                        { width: 5 }
                    ]
                },
                { height:10 },
                {
                    view:"scrollview", 
                    id:"propertyListScrollview", 
                    scroll:"y",
                    body:{
                        rows:[
                            {
                                view:"property",  
                                id:"sets",                    
                                height: 2000,
                                elements:[],
                                on:{
                                    'onAfterEditStop': function(state, editor, ignoreUpdate){
                                        if(!ignoreUpdate && propertyStore.getIndexById(editor.id) != -1){
                                            propertyStore.updateItem(editor.id,{[editor.id]:state.value});
                                        }
                                    },
                                }
                            }
                        ]
                    }
                }
            ]
        };
        propertyStore.data.attachEvent("onStoreUpdated", function(){
            $$('sets').define("elements", propertyStore.serialize());
            $$('sets').refresh();
        });
        return propertyList;
    };
}
