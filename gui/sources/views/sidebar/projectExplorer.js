/* Project Explorer JavaScript */
import {JetView, plugins} from "webix-jet";
import {projectStore} from "models/projectStore"

export default class ProjectExplorerView extends JetView {
    config() {
        let projectExplorer = {
            id:"projectExplorerContainer",
            rows:[
                {
                    view:"template",
                    template:"Project Explorer",
                    type:"header",
                    height:40,
                    borderless:true 
                },
                {
                    cols:[
                        { width:5 },
                        { 
                            id:"projectExplorerSearch", 
                            view:"text", 
                            value:"Search",
                            type:"clean",
                            height:30,
                            on:{
                                onFocus:(id) => {
                                    if(id.getValue() == "Search") {
                                        id.setValue("");                                                                                      
                                    }
                                },
                                onBlur:(id) => {
                                    if(id.getValue() == "") {
                                        id.setValue("Search");
                                    }
                                }
                            }
                        },
                        { width:5 }
                    ]
                },
                { height: 10 },
                {
                    view:"scrollview", 
                    id:"projectExplorerScrollview", 
                    scroll:"y",
                    body:{
                        rows:[
                            {
                                id:"projectExplorer",
                                view:"tree",
                                type:"lineTree",
                                height:2000,
                                drag:false,
                                data:[],
                                ready:function() {
                                    $$("projectExplorer").data.sync(projectStore);
                                },
                                on:{
                                    onItemDblClick:(...args) => {
                                        if($$("projectExplorer").getItem(args[0]).isFolder != true) {
                                            var state = $$("projectStore").getItem(args[0]);
                                            if(state.opened == false){
                                                $$("projectStore").updateItem(args[0],{opened:true, hidden:false});
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    }
                }
            ]
        };

        return projectExplorer;
    };
}