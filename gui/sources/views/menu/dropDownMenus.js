/* Drop Down Menus JavaScript */
import {JetView, plugins} from "webix-jet";
import { fileMenu, editMenu, viewMenu, drawMenu, modifyMenu, helpMenu } from "models/menuData";
import {native} from "app";

export default class DropDownMenuView extends JetView {
    config() {
        let dropDownMenu = {
            id:"dropDownMenuContainer",
            cols:[
                { width: 10 },
                {
                    id:"dropDownMenu",
                    view:"menu",
                    borderless:true,
                    submenuConfig:{
                        width:200
                    },
                    data:[
                        { 
                            id:"fileMenu",
                            value:"File", 
                            submenu:fileMenu
                        },
                        { 
                            id:"editMenu",
                            value:"Edit", 
                            submenu:editMenu
                        },
                        { 
                            id:"viewMenu",
                            value:"View", 
                            submenu:viewMenu
                        },
                        { 
                            id:"drawMenu",
                            value:"Draw", 
                            submenu:drawMenu
                        },
                        { 
                            id:"modifyMenu",
                            value:"Modify", 
                            submenu:modifyMenu
                        },
                        { 
                            id:"helpMenu",
                            value:"Help", 
                            submenu:helpMenu
                        }
                    ],
                    on:{
                        onMenuItemClick:function(id){
                            webix.message("Click: " + id + this.getMenuItem(id).value);
                            console.log(id + this.getMenuItem(id).value);
                            if (id === "File->Exit") {
                                native.Utils.showMessage("Designer", "This will exit application", true);
                                native.Utils.exitApp();
                            } else if (id == "File->Open") {
                                native.Utils.showMessage("Designer", "Start Test Sum", false);
/* Test to call promise for native function call.
  Still not working */
let testCallPromise = new Promise((resolve) => {
      native.Utils.testCall3(10000, function(sum){
          resolve(sum);
      });
});


testCallPromise.then(function(sum) {
    native.Utils.showMessage("Designer", "Test Sum: " + sum, false);
}, function(ver) {
    native.Utils.showMessage("Designer", "Failed to Test Sum", false);
});
                                /* Direct callback function */
                                /* Note the callback function call is blocking.
                                   Thus if the function take long time, then the whole UI will block for long time */
//                                native.Utils.testCall3(10000, function(sum){
//                                    native.Utils.showMessage("Designer", "Test Sum Result:" + sum, false);
//                                });
                            }
                        }
                    },
                    type:{
                        subsign:true,
                    }   
                },
                {

                }
            ]
        };

        return dropDownMenu;
    };
}
