/* Base Menu JavaScript */
import {JetView, plugins} from "webix-jet";
import dropDownMenus from "views/menu/dropDownMenus";
import iconMenu from "views/menu/iconMenu";

export default class MenuView extends JetView {
    config() {
        let menu = {
            id:"menu",
            rows:[ dropDownMenus, iconMenu ]  
        };

        return menu;
    };
}