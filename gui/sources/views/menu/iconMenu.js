/* Icon Menu JavaScript */
import {JetView, plugins} from "webix-jet";
import {native} from "app";
/* NOTE: Buttons in here are fixed and are not dependant on the active module */

export default class IconMenuView extends JetView {
    config() {
        let iconMenu = {
            id:"iconMenuContainer",
            minHeight:40,
            maxHeight:40,
            type:"clean",
            rows:[{
                id:"iconMenu",
                view:"toolbar",
                height:40,
                type:"clean",
                elements: [
                    { width: 10 },
                    {
                        id:"openButton",
                        view: "button", 
                        type: "icon", 
                        icon: "folder-open",
                        width: 32,
                        css: "app_button",
                        click:(id) => { 
                            native.Utils.testcall2();
                            native.Utils.showMessage("Call Native", "Open clicked: " + id, false);
                        }  
                    },
                    {
                        id:"saveButton",
                        view: "button", 
                        type: "icon", 
                        icon: "floppy",
                        width: 32,
                        css: "app_button",
                        click:(id) => { webix.message(id); } 
                    },
                    {
                        id:"importButton",
                        view: "button", 
                        type: "icon", 
                        icon: "file-import",
                        width: 32,
                        css: "app_button",
                        click:(id) => { webix.message(id); } 
                    },
                    {
                        id:"exportButton",
                        view: "button", 
                        type: "icon", 
                        icon: "file-export",
                        width: 32,
                        css: "app_button",
                        click:(id) => { webix.message(id); } 
                    },
                    { },
                    { width: 10 }
                ]
            }]
        };

        return iconMenu;
    };
}
