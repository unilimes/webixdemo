/* Layout JavaScript */
import {JetView, plugins} from "webix-jet";
import menu from "views/menu/menu";
import sidebar from "views/sidebar/sidebar";
import module from "views/module/module";

export default class WillowLynxDesigner extends JetView {
    config() {
        let ui = {
            id:"layout",
            rows:[ 
                menu, 
                { 
                    cols:[ 
                        sidebar, 
                        { 
                            id:"siderbarVerResizer",
                            view: "resizer"
                        }, 
                        module 
                    ] 
                }
            ]
        };
        return ui;
    }
}