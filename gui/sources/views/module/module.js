/* Module JavaScript */
/* Base Menu JavaScript */
import {JetView, plugins} from "webix-jet";
import tools from "views/module/moduleTools";
import tabbarCell from "views/module/moduleTabbarCell";
import components from "views/module/moduleComponents";

export default class ModuleView extends JetView {
    config() {
        let module = {
            id:"module",
            rows:[
                tools, 
                { 
                    cols:[ 
                        tabbarCell,
                        {
                            rows:[
                                components,
                                { height:40 } 
                            ]
                        }
                    ] 
                }   
            ]  
        };

        return module;
    };
}