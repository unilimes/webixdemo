/* Module Tabview JavaScript */
import {JetView, plugins} from "webix-jet";
import schematic from "modules/schematic/views/schematic";
import stream from "modules/stream/views/stream";
import threeJS from "modules/threeJS/views/threeJS";



export default class ModuleTabbarCellView extends JetView {
    config() {
        let tabview = {
            id:"moduleContainer",
            rows:[
                {
                    id:"moduleCells",
                    animate:false,
                    cells:[ 
                        { id:"startscreen", view:"template", template:"Start Screen" }
                    ],

                },
                {
                    view:"tabbar",
                    id:"moduleTabbar",
                    labelAlign:"right",
                    tabOffset:5,
                    height:40,
                    click:(...args) => { 
                        if ( args[1].target.className === "webix_tab_close webix_icon fa-times" ) {
                            /* This is to check if the close tab button has been clicked */
                            return;
                        } else {
                            this.showModule();
                        } 
                    },
                    on:{
                        onBeforeTabClose:(id) => {
                            $$("moduleCells").removeView($$("moduleTabbar").getValue());
                            $$("projectStore").updateItem(id,{opened:false, hidden:true});
                        }
                    },
                    options:[ { id:"startscreen", value:"Start", width:150 } ], 
                    multiview:"true", 
                    type:"bottom"
                }
            ]
        };
        $$("projectStore").attachEvent("onDataUpdate", function(id, data){
            if(data.opened && data.module){
                let maxLength = 8;
                if(data.module) maxLength = 4;
                if($$("moduleTabbar").config.options.length > maxLength){
                    webix.message("Max open tab reached");
                }else{
                    $$("moduleCells").addView({id:id, view:data.module});
                    $$("moduleTabbar").addOption({id:id, value:data.value, close:true, icon:"file", width: 200}, true);
                }
            }
        });
        return tabview; 
    };

    /* Method space for loading Schematic Editor or Dashboard designer or any other module */
    /* needed for when switching between tabs, should unload current module and load new module */
    showModule() {
        let tabID = $$("moduleTabbar").getValue();
        console.log(tabID + " ~ showModule code goes here");
    };
}