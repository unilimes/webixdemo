/* Module Components JavaScript */
import {JetView, plugins} from "webix-jet";
import data from "models/componentData";

export default class ModuleComponentsView extends JetView {
    config() {
        let components = {
            view:"scrollview", 
            id:"componentsScrollview", 
            scroll:"y",
            body:{
                rows:[
                    {
                        id:"componentsHeader",
                        view:"template",
                        template:"Components",
                        type:"header",
                        height:40,
                        borderless:true,
                        hidden:true
                    },
                    {
                        id:"componentsBar",
                        view: "sidebar",
                        data: data,
                        position:"right",
                        height: 2000,
                        collapsed:true,
                        on:{
                            onAfterSelect: function(id){
                                /* Here is where you implement the gadget selection call in the Three.js */
                                webix.message("Selected: " + this.getItem(id).value)
                            }
                        }
                    }
                ]
            }
        };

        return components;
    };
}