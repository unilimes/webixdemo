/* Module Tools JavaScript */
import {JetView, plugins} from "webix-jet";

/* NOTE: Buttons in here are specific to the module loaded and should change upon module load */

export default class ModuleToolsView extends JetView {
    config() {
        let moduleTools = {
            id:"moduleToolsContainer",
            minHeight:40,
            maxHeight:40,
            type:"clean",
            rows:[{
                id:"moduleTools",
                view:"toolbar",
                height:40,
                type:"clean",
                elements: [
                    { width: 10 },
                    {
                        id:"selectButton",
                        view: "button", 
                        type: "icon", 
                        icon: "cursor-default-outline",
                        width: 32,
                        css: "app_button",
                        click:(id) => { webix.message(id); } 
                    },
                    {
                        id:"panButton",
                        view: "button", 
                        type: "icon", 
                        icon: "pan",
                        width: 32,
                        css: "app_button",
                        click:(id) => { webix.message(id); } 
                    },
                    {
                        id:"rotateButton",
                        view: "button", 
                        type: "icon", 
                        icon: "rotate-world",
                        width: 32,
                        css: "app_button",
                        click:(id) => { webix.message(id); } 
                    },
                    {
                        id:"zoomInButton",
                        view: "button", 
                        type: "icon", 
                        icon: "magnify-plus-outline",
                        width: 32,
                        css: "app_button",
                        click:(id) => { webix.message(id); } 
                    },
                    {
                        id:"zoomOutButton",
                        view: "button", 
                        type: "icon", 
                        icon: "magnify-minus-outline",
                        width: 32,
                        css: "app_button",
                        click:(id) => { webix.message(id); } 
                    },
                    { },
                    { 
                        view: "button", 
                        type: "icon", 
                        icon: "menu",
                        width: 50,
                        css: "app_button",
                        click:() => {
                            if ($$("componentsHeader").isVisible()) {
                                $$("componentsHeader").hide();
                            } else {
                                $$("componentsHeader").show();
                            }
                            $$("componentsBar").toggle();
                            $$("componentsScrollview").resize();
                        }
                    },
                    { width: 10 }
                ]
            }]
        };

        return moduleTools;
    };
}