/* The property store need to be dynamically updated
   according to the currently selected object. This
   can be done by passing data to updateElements() */

let propertyStoreClass = webix.proto({
  $init: function(config){
    //functions executed on component initialization
    this.showAllProps = false;
    this.filterInput = '';
  },
  defaults:{
    on:{'onBindRequest' : function(){this.showAll()}}
  },
  /* Data format
     [{ id: unique id to be referred for data update,
        label: the property name,
        value: the propertie's value,
        type: Webix's input editor type,
        simple: set to true if this property is for
                simple configuration
     }] */
  updateElements: function(data){
      if(data.length){
        this.parse(data);
        this.showAll();
      }else{
        this.clearAll();
      }
  },
  /* update visible data based on filter */
  showAll: function(status = this.showAllProps){
      this.showAllProps = status;
      let value = this.filterInput;
      this.filter(function(obj){
          return obj.label.toLowerCase().indexOf(value) !== -1
                  && (status || obj.simple != status);
      });
  },
  filterLabel: function(value){
      this.filterInput = value;
      let status = this.showAllProps;
      this.filter(function(obj){
          return obj.label.toLowerCase().indexOf(value) !== -1
                  && (status || obj.simple != status);
      });
  }
}, webix.DataCollection);

export const propertyStore = new propertyStoreClass({
    id:"propertyStore",
    data: [],
});
