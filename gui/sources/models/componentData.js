/* Module Components Data */

/* NOTE: THIS IS TEST DATA */

let data = [
    { id:"Box", value:"Box", icon:"box-3d" },
    { id:"Sphere", value:"Sphere", icon:"sphere" },
    { id:"Cylinder", value:"Cylinder", icon:"cylinder" },
    { id:"Cone", value:"Cone", icon:"cone" },
    { id:"Torus", value:"Torus", icon:"torus" },
    { id:"Tube", value:"Tube", icon:"tube" },
    { id:"Rectangle", value:"Rectangle", icon:"rectangle" },
    { id:"Triangle", value:"Triangle", icon:"triangle" },
    { id:"Ellipse", value:"Ellipse", icon:"ellipse" },
    { id:"Polygon", value:"Polygon", icon:"hexagon" },
    { id:"Disk", value:"Disk", icon:"disk" },
    { id:"Point", value:"Point", icon:"point" },
    { id:"Line", value:"Line", icon:"line" },
    { id:"Image", value:"Image", icon:"image" },
    { id:"Label", value:"Label", icon:"label" },
    { id:"Status Flag", value:"Status Flag", icon:"flag-variant" },
    { id:"State Gadget", value:"State Gadget", icon:"traffic-light" },
    { id:"Camera", value:"Camera", icon:"free-camera" },
    { id:"Spot Light", value:"Spot Light", icon:"spotlight" },
    { id:"Point Light", value:"Point Light", icon:"lightbulb" }
];

export default data;