/* Drop Down Menu Data */

let viewsSubmenu = [
    { value:"Cross View", icon:"cross-view" },
    { value:"3 x 1 View", icon:"three-one-view" },
    { value:"Single", icon:"square" },
    { value:"Horizontal Split", icon:"horizontal-split-view" },
    { value:"Vertical Split", icon:"vertical-split-view" }
];

let activeViewSubmenu = [
    { value:"Orthographic", icon:"orthographic" },
    { value:"Perspective", icon:"perspective" },
    { value:"Top", icon:"view-top" },
    { value:"Bottom", icon:"view-bottom" },
    { value:"Front", icon:"view-front" },
    { value:"Back", icon:"view-back" },
    { value:"Left", icon:"view-left" },
    { value:"Right", icon:"view-right" }
];

let twoDSubmenu = [
    { value:"Rectangle", icon:"rectangle" },
    { value:"Triangle", icon:"triangle" },
    { value:"Ellipse", icon:"ellipse" },
    { value:"Polygon", icon:"hexagon" },
    { value:"Disk", icon:"disk" },
    { value:"Point", icon:"point" },
    { value:"Line", icon:"line" },
    { value:"Image", icon:"image" },
    { value:"Label", icon:"label" },
    { value:"Status Flag", icon:"flag-variant" }
];

let threeDSubmenu = [
    { value:"Box", icon:"box-3d" },
    { value:"Sphere", icon:"sphere" },
    { value:"Cylinder", icon:"cylinder" },
    { value:"Cone", icon:"cone" },
    { value:"Torus", icon:"torus" },
    { value:"Tube", icon:"tube" }
];

let alignmentSubmenu = [
    { value:"Edge Align", icon:"code" },
    { value:"Center Align", icon:"code" },
];

let fileMenu = [
    { id: "File->Open", value:"Open", icon:"folder-open" },
    { id: "File->Save", value:"Save", icon:"floppy" },
    { id: "File->Import", value:"Import", icon:"file-import" },
    { id: "File->Export", value:"Export", icon:"file-export" },
    { id: "File->Page Properties", value:"Page Properties", icon:"-" },
    { id: "File->Print", value:"Print", icon:"printer" },
    { id: "File->Options", value:"Options", icon:"-" },
    { id: "File->Exit", value:"Exit", icon:"exit-to-app" }
];

let editMenu = [
    { value:"Cut", icon:"content-cut" },
    { value:"Copy", icon:"content-copy" },
    { value:"Paste", icon:"content-paste" },
    { value:"Undo", icon:"undo" },
    { value:"Redo", icon:"redo" },
    { value:"Select All", icon:"select-all" },
    { value:"Invert Selection", icon:"select-inverse" },
    { value:"Find", icon:"file-find" },
    { value:"Delete", icon:"delete" }
];

let viewMenu = [
    { value:"Select", icon:"cursor-default-outline" },
    { value:"Pan", icon:"pan" },
    { value:"Rotate View", icon:"rotate-world" },
    { value:"Zoom In", icon:"magnify-plus-outline" },
    { value:"Zoom Out", icon:"magnify-minus-outline" },
    { value:"Fit to Page", icon:"fit-to-screen" },
    { value:"Views", icon:"view-quilt", submenu:viewsSubmenu },
    { value:"Active View", icon:"-", submenu:activeViewSubmenu },
    { value:"Grid", icon:"grid" },
    { value:"Test Mode", icon:"play-box-outline" }
];

let drawMenu = [
    { value:"2D Gadgets", icon:"rectangle", submenu:twoDSubmenu },
    { value:"3D Gadgets", icon:"box-3d", submenu:threeDSubmenu },
    { value:"State Gadget", icon:"traffic-light" },
    { value:"Table Gadget", icon:"table-large" },
    { value:"Camera", icon:"free-camera" },
    { value:"Spot Light", icon:"spotlight" },
    { value:"Point Light", icon:"lightbulb" },
    { value:"Symbol Library", icon:"hexagon-multiple" }
];

let modifyMenu = [
    { value:"Scale", icon:"scale-object" },
    { value:"Rotate", icon:"rotate-3d" },
    { value:"Group", icon:"group" },
    { value:"Ungroup", icon:"ungroup" },
    { value:"Data Assign", icon:"database" },
    { value:"Color", icon:"palette" },
    { value:"Text", icon:"format-text" },
    { value:"Duplicate", icon:"content-duplicate" },
    { value:"Alignment", icon:"format-align-middle", submenu:alignmentSubmenu },
    { value:"Wireframe", icon:"wireframe" },
    { value:"Scripting", icon:"code-braces" },
    { value:"Property Popup", icon:"wrench" }
];

let helpMenu = [
    { value:"Contents", icon:"book-open-page-variant" },
    { value:"About", icon:"help-circle-outline" }
];

export { fileMenu, editMenu, viewMenu, drawMenu, modifyMenu, helpMenu };