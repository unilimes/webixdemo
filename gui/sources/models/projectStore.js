/* Project store contain project data which are
   used by project explorer to populating the data display
   and controlling the state of the opened module */

let projectStoreClass = webix.proto({
  $init: function(config){
    //functions executed on component initialization
  },
  loadStore: function(){
    //populate the store by parsing the config file
  }
}, webix.TreeCollection);

export const projectStore = new projectStoreClass({
    id:"projectStore",
    data:[ //test data
        {
            id:"project", 
            value:"Willowlynx Project", 
            open:true, 
            isFolder:true,
            data:[
                { 
                    id:"dashboard", 
                    open:true, 
                    value:"Dashboards",
                    isFolder:true,
                    data:[
                        { 
                            id:"dashboard1", 
                            value:"Main Page"
                        },
                        { 
                            id:"dashboard2", 
                            value:"Reports", 
                        },
                        { 
                            id:"dashboard3", 
                            value:"Trend" 
                        },
                        {
                            id:"dashboard4",
                            value: "Stream",
                            path:"",
                            module:"stream",
                            opened: false,
                            hidden: true
                        }
                    ]
                },
                { 
                    id:"schematics",
                    open:true, 
                    value:"Schematics",
                    isFolder:true,
                    data:[
                        { 
                            id:"schematic1",
                            value:"P00.json",
                            path:"",
                            module:"schematic",
                            opened: false,
                            hidden: true
                        },
                        { 
                            id:"schematic2", 
                            value:"P01_Map_Overview.json",
                            path:"",
                            module:"schematic",
                            opened: false,
                            hidden: true
                        },
                        {
                            id:"schematic3",
                            value:"threeJS",
                            path:"",
                            module:"threeJS",
                            opened: false,
                            hidden: true
                        }
                    ]
                }
            ]
        }
    ]
});