/* Schematic store is an example of module based store.
   Currently it's designed to store data of the selected component*/

//temporary for testing. the json file will be loaded by native function
import testFile from "../testJSON/P00_HomePage.json"

export const schematicStore = webix.proto({
  $init: function(config){
    //functions executed on component initialization
    this.gadgets = new Map();
  },
  getDataId: function(id){
    return this.config.id+"-"+id;
  },
  getDataValue: function(id){
    let uid = this.config.id+"-"+id;
    return this.getItem(uid).value;
  },
  loadStore: function(){
    // populate the store. eg:
    testFile.Layer.forEach(function(item, index, array) {
      console.log(item, index);
    });
  },
  addCubeProps: function({
                        uid = 'unknown',
                        name = 'unknown',
                        wireframe = true,
                        width = 1,
                        height = 1,
                        depth = 1,
                        fps = 30,
                        color = '#00ff00'
                      }){
    let props = [
            { id:this.getDataId("name"), label:"Name", value:name, type:"text", simple:true },
            { id:this.getDataId("width"), label:"Width", value:width, type:"text", simple:true },
            { id:this.getDataId("height"), label:"Height", value:height, type:"text", simple:true },
            { id:this.getDataId("depth"), label:"Depth", value:depth, type:"text", simple:true },
            { id:this.getDataId("wireframe"), label:"Wireframe", value:wireframe, type:"checkbox", simple:false },
            { id:this.getDataId("fps"), label:"FPS", value:fps, type:"text", simple:false },
            { id:this.getDataId("color"), label:"Color", value:color, type:"color", simple:true }
    ];
    this.gadgets.set(uid, props);
  },
  selectObj: function(uid){
    if(this.gadgets.has(uid)){
      let data = this.gadgets.get(uid);
      this.parse(data);
      $$('propertyStore').updateElements(data);
      $$('propertyStore').data.sync(this.data);
    }else{
      console.log(uid,'not found!');
    }
  },
  deselectObj: function(){
    $$('propertyStore').updateElements([]);
    this.data.unsync();
  }
}, webix.DataCollection);
