import {JetView} from "webix-jet";
import {schematicStore} from "../models/schematicStore"

webix.protoUI({
    name:"schematic",
    $init:function(config){

        //creating promise
        this.promise = webix.promise.defer().then(webix.bind(function(){

            console.log(777, this.$view);

            //create the module store
            let storeid = config.id + "Store";
            let store = new schematicStore({
                id:storeid,
            });

            //init lib when it is ready
            let scene = new THREE.Scene();
            let camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

            let raycaster = new THREE.Raycaster();
            let renderer = new THREE.WebGLRenderer();
            renderer.setSize( this.$width, this.$height );
            this.$view.appendChild( renderer.domElement );

            //Example on 1 object (cube) handling
            let cube1 = {
                    uid: "cube1",
                    name: "cube1",
                    wireframe: true,
                    width: 1,
                    height: 1,
                    depth: 1,
                    fps: 30,
                    color: '#00ff00'
                };

            let geometry = new THREE.BoxGeometry( cube1.width, cube1.height, cube1.depth );
            let material = new THREE.MeshBasicMaterial();
            material.color.setHex(cube1.color.replace(/#/g , "0x"));
            let cube = new THREE.Mesh( geometry, material );
            scene.add( cube );
            cube.name = cube1.name;
            store.addCubeProps(cube1);

            let outlineMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: cube1.wireframe } );
            let outline = new THREE.Mesh( geometry, outlineMaterial );
            outline.name = 'wireframe';
            scene.add( outline );

            camera.position.z = 5;

            let stop = false;
            let fpsInterval, lastDrawTime;

            //handle store update
            store.data.attachEvent("onStoreUpdated", function(id, obj){
                switch(id) {
                case store.getDataId("fps"):
                    let new_fps = obj.value;
                    fpsInterval = 1000 / new_fps;
                break;
                case store.getDataId("color"):
                    cube.material.color.setHex(obj.value.replace(/#/g , "0x"));
                break;
                case store.getDataId("wireframe"):
                    if(!obj.value){
                        scene.remove( outline );
                    }else{
                        scene.add( outline );
                    }
                break;
                case store.getDataId("width"):
                    cube.scale.setX(obj.value);
                break;
                case store.getDataId("height"):
                    cube.scale.setY(obj.value);
                break;
                case store.getDataId("depth"):
                    cube.scale.setZ(obj.value);
                break;
                default:
                }
            });

            let mouse = new THREE.Vector2(), INTERSECTED;

            //initialize the timer variables and start the animation
            let startAnimating = fps => {
                fpsInterval = 1000 / fps;
                lastDrawTime = performance.now();
                animate();
            }

            //draw loop
            let animate = now => {
                requestAnimationFrame( animate );

                //calc elapsed time since last loop
                let elapsed = now - lastDrawTime;

                //if enough time has elapsed, draw the next frame
                if (elapsed > fpsInterval) {
                    lastDrawTime = now - (elapsed % fpsInterval);

                    cube.rotation.x += 0.1;
                    cube.rotation.y += 0.1;
                    if(scene.getObjectByName('wireframe')){
                        outline.rotation.copy(cube.rotation);
                        outline.scale.copy(cube.scale);
                    }

                    renderer.render(scene, camera);
                }
            };

            let onWindowResize = () => {
                camera.aspect = this.$width / this.$height;
                camera.updateProjectionMatrix();
                renderer.setSize( this.$width, this.$height );
            }
            window.addEventListener( 'resize', onWindowResize, false );

            let onMouseDown = event => {
                event.preventDefault();
                mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
                mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
                // find intersections
                raycaster.setFromCamera( mouse, camera );
                let intersects = raycaster.intersectObjects( scene.children );
                if ( intersects.length > 0 ) {
                  if ( INTERSECTED != intersects[ 0 ].object ) {
                    INTERSECTED = intersects[ 0 ].object;
                    store.selectObj(INTERSECTED.name);
                  } else {
                      store.deselectObj();
                      INTERSECTED = null;
                    }
                }
            }
            document.addEventListener( 'mousedown', onMouseDown, false );

            startAnimating(30);

            //call an optional callback code
            if (this.config.ready)
                this.config.ready.call(this, scene);

        }, this));

        //if file is already loaded, resolve promise
        if (window.THREE)
            this.promise.resolve();
        else
            //wait for data loading and resolve promise after it
            webix.require("three.min.js", function(){ this.promise.resolve(); }, this);
    }
}, webix.ui.view);

export default class SchematicView extends JetView{
    config(){
        return { id:"schematic", view:"schematic" };
    }
};
