
var conf = {
"7chanel" : {
    "low": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer0.m3u8",
    "high": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer7.m3u8"
},
"1plus1" : {
    "low": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer0.m3u8",
    "high": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer7.m3u8"
},
"Nemo" : {
    "low": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer0.m3u8",
    "high": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer7.m3u8"
},
"ukraine" : {
    "low": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer0.m3u8",
    "high": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer7.m3u8"
},
"2plus2" : {
    "low": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer0.m3u8",
    "high": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer7.m3u8"
},
"bigudi" : {
    "low": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer0.m3u8",
    "high": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer7.m3u8"
},
"tet" : {
    "low": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer0.m3u8",
    "high": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer7.m3u8"
},
"unian" : {
    "low": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer0.m3u8",
    "high": "https://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer7.m3u8"
}
};
var player;




for(var key in conf){
    TVlow(conf[key].low, conf[key].high, key);
}



function TVhigh(low, high, id) {

    let container = document.createElement('div');
    container.className = 'mainStrem';
    container.innerHTML = "<video id=" + id + " class='video-js vjs-default-skin' controls> <source src=" + high + " type='application/x-mpegURL'> </video>";
    document.body.appendChild( container );

    player = videojs(  id );
    player.play();

    var close = document.createElement('span');
    close.id = 'close';
    close.style.color = 'white';
    close.style.fontSize = '22px';
    close.style.right = '5px';
    close.style.top = '5px';
    close.style.position = 'absolute';
    close.innerHTML = 'X';

    document.getElementById(id).appendChild(close);

    //$("#"+id+' .vjs-control-bar').append("<span id='close' style='color: white; font-size: 20px; margin: 5px'>Close</span>");
    //$("#"+id+' .vjs-fullscreen-control').remove();

    document.getElementById('close').addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
    });

}

function TVlow(low, high, id) {
    let container = document.createElement('div');
    container.className = 'stream';
    container.innerHTML = "<video id=" + id + " class='video-js vjs-default-skin' controls> <source src=" + low + " type='application/x-mpegURL'> </video>";
    document.body.appendChild( container );


    player = videojs(id);
    player.play();



    var full = document.createElement('span');
    full.id = 'full';
    full.style.color = 'white';
    full.style.fontSize = '20px';
    full.style.margin = '5px';
    full.innerHTML = 'Full';

    document.getElementById(id).querySelector('.vjs-control-bar').appendChild(full);

    document.getElementById(id).querySelector('#full').addEventListener("click", function() {
        TVhigh(low ,high, getRandomColor());
    });

}


function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '1';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}




