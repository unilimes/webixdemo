/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_app__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__styles_style_css__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__styles_style_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__styles_style_css__);




$( document ).ready(function() {
    window.app = new __WEBPACK_IMPORTED_MODULE_0__app_app__["a" /* default */]();
    window.app.init();
});



/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__viewer_viewer__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__house_house__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__planes_planes__ = __webpack_require__(4);




class App {
    constructor() {

    }

    init() {
        this.viewerHouse = new __WEBPACK_IMPORTED_MODULE_0__viewer_viewer__["a" /* default */]();
        this.viewerHouse.init('house');
        this.house = new __WEBPACK_IMPORTED_MODULE_1__house_house__["a" /* default */]();
        this.viewerHouse.scene.add(this.house.houseHolder);

        this.viewerPlane = new __WEBPACK_IMPORTED_MODULE_0__viewer_viewer__["a" /* default */]();
        this.viewerPlane.init('planes');






        this.planes = new __WEBPACK_IMPORTED_MODULE_2__planes_planes__["a" /* default */]();

        this.viewerPlane.camera.position.set(0,0,5);

        this.viewerPlane.scene.add(this.planes.planeHolder);
        this.viewerPlane.controls.noRotate = true;
        this.viewerPlane.controls.maxDistance = 10;
        this.viewerPlane.controls.minDistance = 0.7;
        this.viewerPlane.controls.zoomSpeed = 0.7;

        // this._interv = setInterval(()=>{
        //
        //     if(this.viewerPlane.camera.position.z < 3 && !this.highQ){
        //
        //         this.planes.clearScene();
        //
        //
        //
        //     }
        //
        // },200);

        window.addEventListener('resize', () => {
            this.viewerHouse.onWindowResize();
            this.viewerPlane.onWindowResize();
        });


    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = App;


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Viewer {
    constructor() {
        this.renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
        this.camera = new THREE.PerspectiveCamera(125, this.width / this.height, 0.1, 1000000);

        this.scene = new THREE.Scene();

        // this.camera.checkChange = ( ) => {
        //     if( !this.camera.oldOptions ){
        //         this.camera.oldOptions = {
        //             position: this.camera.position.clone(),
        //             zoom: this.camera.zoom
        //         }
        //     } else {
        //
        //         let callChangeEvent = false;
        //         let oldParams = {
        //             position:  this.camera.oldOptions.position.clone(),
        //             zoom:  this.camera.oldOptions.zoom
        //         };
        //
        //         if( !this.camera.position.equals( this.camera.oldOptions.position ) ){
        //             callChangeEvent = true;
        //             this.camera.oldOptions.position.copy( this.camera.position );
        //         }
        //
        //         if( this.camera.zoom != this.camera.oldOptions.zoom ){
        //             callChangeEvent = true;
        //             this.camera.oldOptions.zoom = this.camera.zoom;
        //         }
        //
        //         if( callChangeEvent ){
        //             let eve = new Event('change');
        //
        //             eve.data = {
        //                 oldparams: oldParams,
        //                 nextParams: {
        //                     position:  this.camera.oldOptions.position.clone(),
        //                     zoom:  this.camera.oldOptions.zoom
        //                 }
        //             };
        //             this.camera.dispatchEvent( this, eve );
        //         }
        //     }
        // };
        //
        // this.camera.checkChange();
        //
        // this.camera.addEventListener( 'change', ( event, data ) => {
        //     console.log( event, data );
        // });
    }

    init(html) {

        this.container = document.getElementById(html);
        console.log(this.container);
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
        this.renderer.gammaInput = true;
        this.renderer.gammaOutput = true;
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.renderReverseSided = false;
        this.container.appendChild(this.renderer.domElement);

        this.camera.name = 'Camera';
        this.camera.position.set(6850, 42676, 34600);

        this.scene.add(this.camera);

        this.controls = new THREE.TrackballControls(this.camera, this.renderer.domElement);
        this.controls.name = 'Trackball';

        var light = new THREE.AmbientLight( 0x404040 ); // soft white light
        light.intensity = 5;
        this.scene.add( light );



        this.scene.name = 'Scene';

        this.controls.addEventListener('change', this.render);

        this.onWindowResize();

        this.animate();
    }

    // cameraEvent(){
    //
    //     this.camera.addEventListener( 'change', ( event, data ) => {
    //         console.log( event, data );
    //     });
    //
    // }

    onWindowResize() {
        this.camera.aspect = this.container.offsetWidth / this.container.offsetHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
        this.controls.handleResize();
    }

    animate() {
        this.controls.update();
        this.render();

        requestAnimationFrame(() => {
            this.animate();
        });
    }

    render() {
        if (this.renderer) this.renderer.render(this.scene, this.camera);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Viewer;


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class House {

    constructor(){

        this.houseHolder = new THREE.Object3D();
        this.houseHolder.name = 'house';

        this.pressed = false;

        this.loadOnScene();
        this.houseEvents();

    }

    loadOnScene(){

        var manager = new THREE.LoadingManager();

        manager.onLoad = ( ) => {
            $('.house .loader').hide();
            $('.house .start').show();
        };


        // THREE.Loader.Handlers.add( /\.tga$/i, new THREE.TGALoader() );

        var mtlLoader = new THREE.MTLLoader();
        var loader = new THREE.OBJLoader( manager );

        mtlLoader.setPath('/sources/modules/threeJS/assets/obj/');
        mtlLoader.load( 'house.mtl', ( materials )=> {

            loader.setMaterials( materials );

            loader.load( '/sources/modules/threeJS/assets/obj/house.obj', ( object ) => {

                object.traverse(  ( child ) => {
                    if ( child instanceof THREE.Mesh ) {
                       child.material.needsUpdate = true;
                       child.material.transparent = false;
                       child.material.side =2;
                       console.log(child.material);
                       if(child.material.length > 0){
                           for(var i = 0; i < child.material.length; i++){
                               child.material[i].transparent = false;
                               child.material[i].side = 2;
                               child.material[i].needsUpdate=true;
                           }
                       }
                    }
                });

                this.houseHolder.add( object );

            });

        });





    }

    houseEvents(){
        var haose = document.getElementById("haose");
        haose.addEventListener("click", ()=>{

            if(!this.pressed){
                this._interv = setInterval(()=>{

                 for(var i = 0; i < this.houseHolder.children[0].children.length; i++){
                     this.houseHolder.children[0].children[i].position.x += this.getRandomArbitrary(-50, 50);
                     this.houseHolder.children[0].children[i].position.y += this.getRandomArbitrary(-50, 50);
                     this.houseHolder.children[0].children[i].position.z += this.getRandomArbitrary(-50, 50);

                     this.houseHolder.children[0].children[i].rotation.x += this.getRandomArbitrary(-3, 3);
                     this.houseHolder.children[0].children[i].rotation.y += this.getRandomArbitrary(-3, 3);
                     this.houseHolder.children[0].children[i].rotation.z += this.getRandomArbitrary(-3, 3);

                     this.houseHolder.children[0].children[i].scale.x = Math.random();
                     this.houseHolder.children[0].children[i].scale.y = Math.random();
                     this.houseHolder.children[0].children[i].scale.z = Math.random();

                     if(this.houseHolder.children[0].children[i].material.color) {
                        this.houseHolder.children[0].children[i].material.color.r = Math.random();
                        this.houseHolder.children[0].children[i].material.color.g = Math.random();
                        this.houseHolder.children[0].children[i].material.color.b = Math.random();
                     }

                 }

                },250);
            }else{
                clearInterval(this._interv);
            }

            this.pressed = !this.pressed;
        }, false);



    }


    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }


    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = House;




/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class House {

    constructor(){

        this.planeHolder = new THREE.Object3D();
        this.planeHolder.name = 'plane';
        this.planeHolder.visible = false;
        this.planeHolder.position.set(0,70,0);

        this.pressed = false;
        this.image = {};

        this.fonts = ['cursive', 'fantasy', 'monospace', 'sans-serif', 'serif'];

        this.loadOnScene();
        this.events();

    }

    loadOnScene(){


        setTimeout(()=>{

            $('#planes .loader').hide();
            $('#change').show();
            this.planeHolder.visible = true;

        }, 500);

        for(var i = 0; i < 200; i++) {
            for (var j = 0; j < 50; j++) {

                this.createCanvas(this.getRandomColor(), j * 1.2, (0 - i * 1.2), 0, this.getRandomArbitrary(10, 40), 'middle', this.fonts[this.getRandomInt(0, 5)], 256, 128);

            }
        }

    }

    clearScene(){
        for(var i = this.planeHolder.children.length; 0 < this.planeHolder.children.length; i--){
            this.planeHolder.children.splice(i,1);
        }
    }

    createImage(image){
        this.image.texture = new THREE.TextureLoader().load(image);
        this.image.geometry = new THREE.PlaneGeometry(1, 1);
        this.image.material = new THREE.MeshBasicMaterial({
            map: this.image.texture,
            side: THREE.DoubleSide,
            transparent: true
        });
        this.image.mesh = new THREE.Mesh(this.image.geometry, this.image.material);
        this.image.mesh.rotation.y += Math.PI / 2;
        this.planeHolder.add(this.image.mesh);
        this.image.deleteImage = function () {
            this.texture.dispose();
            this.geometry.dispose();
            this.material.dispose();
            delete this.texture;
            delete this.geometry;
            delete this.material;
            return this.mesh;
        }
    }

    createCanvas (text, x, y, z, size, place, font, canvasX, canvasY) {

        const canvas = document.createElement('canvas');
        const context = canvas.getContext('2d');
        canvas.width = canvasX;
        canvas.height = canvasY;
        canvas.background = text;
        context.font = 'normal ' + size + 'px ' + font;
        context.textAlign = 'center';
        context.textBaseline = place;
        context.beginPath();
        context.rect(0, 0, 256, 128);
        context.fillStyle = text;
        context.fill();
        context.fillStyle = 'black';
        context.fillText(text, 128, 20, 256);

        let object = {};

        object.texture = new THREE.Texture(canvas);
        object.texture.needsUpdate = true;
        object.geometry = new THREE.PlaneGeometry(1, 1, 1, 1);
        object.material = new THREE.MeshBasicMaterial({
            map: object.texture,
            side: 0,
        });

        object.mesh = new THREE.Mesh(object.geometry, object.material);

        object.mesh.position.set(x, y, z);
        this.planeHolder.add(object.mesh);
        object.deleteText = function () {
            this.texture.dispose();
            this.geometry.dispose();
            this.material.dispose();
            delete this.texture;
            delete this.geometry;
            delete this.material;
            return this.mesh;
        }
    };

    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }


    changeOnePlane (text, size, place, font, canvasX, canvasY, item) {

            item.material.map.dispose();
            item.material.dispose();

            const canvas = document.createElement('canvas');
            const context = canvas.getContext('2d');
            canvas.width = canvasX;
            canvas.height = canvasY;
            canvas.background = text;
            context.font = 'normal ' + size + 'px ' + font;
            context.textAlign = 'center';
            context.textBaseline = place;
            context.beginPath();
            context.rect(0, 0, 256, 128);
            context.fillStyle = text;
            context.fill();
            context.fillStyle = 'black';
            context.fillText(text, 128, 20, 256);

            const object = {};
            object.texture = new THREE.Texture(canvas);
            object.texture.needsUpdate = true;
            object.material = new THREE.MeshBasicMaterial({
                map: object.texture,
                side: 0,
            });

            item.material = object.material;
            //item.material.needsUpdate = true;
        };


    events(){
        $('#change').click(()=>{

            if(!this.pressed){
                this._interv = setInterval(()=>{

                    for(var i = 0; i < 25; i++){

                        this.changeOnePlane(this.getRandomColor(), this.getRandomArbitrary(10, 40), 'middle', this.fonts[this.getRandomInt(0, 5)], 256, 128, this.planeHolder.children[this.getRandomInt(0, this.planeHolder.children.length)]);
                    }

                },80);
            }else{
                clearInterval(this._interv);
            }

            this.pressed = !this.pressed;
        });
    }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = House;




/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(6);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {"hmr":true}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(8)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!./style.css", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!./style.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(7)(false);
// imports


// module
exports.push([module.i, "body, html{height:100%}\n.house{\n    width: 50%;\n    height: 100%;\n    float: left;\n    position: relative;\n}\n.start{\n    position: absolute;\n    bottom: 20px;\n    left: 20px;\n    background: darkorange;\n    border-radius: 10px;\n    width: 85px;\n    height: 23px;\n    text-align: center;\n    font-size: 18px;\n    color: white;\n    cursor: pointer;\n    display: none;\n}\n.change{\n    position: absolute;\n    bottom: 20px;\n    right: 20px;\n    background: blueviolet;\n    border-radius: 10px;\n    width: 85px;\n    height: 23px;\n    text-align: center;\n    font-size: 18px;\n    color: white;\n    cursor: pointer;\n    display: none;\n}\n\n.planes{\n    width: 50%;\n    height: 100%;\n    float: right;\n    position: relative;\n}\n\n\n.loader{\n    position: absolute;\n    transform: translate(-50%, -50%);\n    top: 40%;\n    left: 40%;\n    height: 120px;\n    animation-name: rotate;\n    animation-duration: 1.2s;\n    animation-iteration-count: infinite;\n}\n\n@keyframes rotate {\n    0%   {transform: rotateY(0deg);}\n\n    100% {transform: rotateY(360deg);}\n}", ""]);

// exports


/***/ }),
/* 7 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getElement = (function (fn) {
	var memo = {};

	return function(selector) {
		if (typeof memo[selector] === "undefined") {
			var styleTarget = fn.call(this, selector);
			// Special case to return head of iframe instead of iframe itself
			if (styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[selector] = styleTarget;
		}
		return memo[selector]
	};
})(function (target) {
	return document.querySelector(target)
});

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(9);

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
	if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertInto + " " + options.insertAt.before);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	options.attrs.type = "text/css";

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	options.attrs.type = "text/css";
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),
/* 9 */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ })
/******/ ]);