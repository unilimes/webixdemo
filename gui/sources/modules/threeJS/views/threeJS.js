import {JetView} from "webix-jet";


webix.protoUI({
    name:"threeJS",
    $init:function(config){

        let $self = this;

        //creating promise
        this.promise = webix.promise.defer().then(webix.bind(function(){

            // let container = document.createElement("div");
            // container.className = "house";
            // container.id = "house";
            //
            // let start = document.createElement("div");
            // start.className = "start";
            // start.id = "haose";
            // start.innerHTML = "START";
            //
            // let loader = document.createElement("img");
            // loader.className = "loader";
            // loader.src = "sources/modules/threeJS/assets/img/icon.svg";
            //
            // $self.$view.appendChild( container );
            // document.getElementById("house").appendChild(start);
            // document.getElementById("house").appendChild(loader);
            //
            //
            //
            // container = document.createElement("div");
            // container.className = "planes";
            // container.id = "planes";
            //
            // loader = document.createElement("img");
            // loader.className = "loader";
            // loader.src = "sources/modules/threeJS/assets/img/file.svg";
            //
            // let change = document.createElement("div");
            // change.className = "change";
            // change.id = "change";
            // change.innerHTML = "CHANGE";
            //
            //
            //
            // $self.$view.appendChild( container );
            // document.getElementById("planes").appendChild(change);
            // document.getElementById("planes").appendChild(loader);


            let iframe = document.createElement("iframe");

            iframe.src = 'sources/modules/threeJS/index.html';

            iframe.style.height = '100%';
            iframe.style.width = '100%';
            $self.$view.appendChild( iframe );





            // <div class="house" id="house">
            //     <img class="loader" src="assets/img/icon.svg">
            //
            //     <div class="start" id="haose">HAOSE</div>
            // </div>
            //
            // <div class="planes" id="planes">
            //  <img class="loader" src="assets/img/file.svg">
            // </div>

        }, this));

        //if file is already loaded, resolve promise
        if (window.THREE)
            this.promise.resolve();
        else
        //wait for data loading and resolve promise after it
            webix.require("three.min.js", function(){ this.promise.resolve(); }, this);
    }
}, webix.ui.view);

export default class ThreeView extends JetView{
    config(){
        return { id:"threeJS", view:"threeJS" };
    }
};