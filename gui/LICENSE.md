Copyright © 2017-2018 Willowglen MSC Berhad
Proprietary and confidential

WillowLynx7 Designer includes code from the following project, which have their own licenses:
- [three.js](https://github.com/mrdoob/three.js/blob/dev/LICENSE)[MIT]