import QtQuick 2.6
import QtQuick.Window 2.2
import QtWebEngine 1.3
import QtWebChannel 1.0

Window {
    id: mainWindow
    objectName: "mainWindowObject"
    WebChannel.id: "mainWindow"

    visible: true
    width: 1280
    height: 720
    title: qsTr("Designer")

    // Expose a Custom Property to Outside
    property url webEngineURL: webEngine.url

    // QML function to set value of custom property 'webEngineUrl'
    function setWebEngineURL( newURL) {
        webEngineURL = newURL;
        console.log("called setWebEngineURL" , webEngineURL);
     }

    onWebEngineURLChanged: {
        // Write the handler code
        // webEngine.url = webEngineURL;
    }


    // Create WebEngine in QML
    WebEngineView {
        id: webEngine
        objectName: "webEngineObject"
        anchors.fill: parent
        url:"qrc:/gui/index.html"
        webChannel: webChannel
    }

    // create WebChannel
    WebChannel {
        id: webChannel
        objectName: "webChannelObject"
    }

    Component.onCompleted: {
        // Register QML Objects to Publish in Webchannel
        webChannel.registerObject("mainWindow", mainWindow);
        mainWindow.showMaximized();
    }
}
