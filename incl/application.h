#ifndef APPLICATION_H
#define APPLICATION_H

#include <QQmlApplicationEngine>
#include <QQmlWebChannel>
#include <QGuiApplication>
#include <QtWebEngine>

class Application
{
public:
    static Application* getInstance();
    void registerObject(QString objName , QObject *obj);
    static QQmlApplicationEngine *engine;

protected:
    Application();                  // Protect object creation/deletion from public scope. Use getInstance() to get the object
    ~Application();

protected:
    static Application *single;
    static QQmlWebChannel *qmlWebChannel;
};

#endif // APPLICATION_H
