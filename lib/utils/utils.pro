TEMPLATE = lib
CONFIG += staticlib
QT += widgets

# Input
HEADERS += $$files(incl/*.h, true)
SOURCES += $$files(src/*.cpp, true)