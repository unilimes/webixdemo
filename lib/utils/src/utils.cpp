#include <QApplication>
#include <QDateTime>
#include <QDebug>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QtConcurrent/QtConcurrent>

#include "../incl/utils.h"


Utils::Utils()
{

}

Utils::~Utils()
{

}

QVariant Utils::version()
{
    return "1.0";
}

void Utils::testcall()
{
    logMessage("this is my test message");
}

void Utils::testcall2()
{
    logMessage("this is my test message2");
}

QVariant Utils::testCall3(qint64 loop)
{
    logMessage("start testCall3 (even sum)" + QString().sprintf("%d", loop));
    qint64 sum = 0;

    QFuture<QVariant> future = QtConcurrent::run(Utils::testCall3SubRoutine, loop);         /* Concurrent not working properly yet!!! */
    sum = future.result().toInt();

    logMessage("testCall3: " + QString().sprintf("%d", sum));
    return sum;
}

QVariant Utils::testCall3SubRoutine(qint64 loop)
{
    qint64 sum = 0;
    for (qint64 i = 0; i < loop; ++i) {
        for (qint64 j = 0; j < 100000; ++j) {
            /* Do nothing, just to slow the process */
            for (qint64 k = 0; k < 100000; ++k) {

            }
        }
        if (i % 2 > 0) {
            sum += i;
        }
    }

    return sum;
}

QVariant Utils::screenInfo()
{
    QDesktopWidget *desk = QApplication::desktop();
    QRect deskGeo;
    QJsonObject jsonObj, jsonDeskObj;
    QJsonArray jsonDeskArray;

    if (desk) {
        jsonObj.insert("virtualDesktop", desk->isVirtualDesktop());
        jsonObj.insert("screenCount", desk->screenCount());
        jsonObj.insert("primaryScreen", desk->primaryScreen());

        for (qint32 i = 0; i < desk->screenCount(); i++) {
            deskGeo = desk->screenGeometry(i);
            jsonDeskObj.insert("left", deskGeo.left());
//            jsonDeskObj.insert("right", deskGeo.right());
            jsonDeskObj.insert("top", deskGeo.top());
//            jsonDeskObj.insert("bottom", deskGeo.bottom());
            jsonDeskObj.insert("width", deskGeo.width());
            jsonDeskObj.insert("height", deskGeo.height());

            jsonDeskArray.append(jsonDeskObj);
        }
        jsonObj.insert("screens", jsonDeskArray);
    }

    return jsonObj;
}

void Utils::logMessage(const QString &msg)
{
    qDebug("%s    %s", qPrintable(QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss.zzz")), qPrintable(msg));
}

void Utils::showMessage(const QString &title, const QString &msg, bool modal)
{
    if (modal) {
        QMessageBox::information(Q_NULLPTR, title, "Native block API called! Be careful, suppose not to call block API.\n\n" + msg);
    } else {
        QMessageBox *msgbox = new QMessageBox(QMessageBox::Information, title, msg);
        msgbox->show();
        connect(msgbox, SIGNAL(accepted()), msgbox, SLOT(deleteLater()));
        connect(msgbox, SIGNAL(rejected()), msgbox, SLOT(deleteLater()));
    }
}

void Utils::exitApp()
{
    QApplication::exit(0);
}
