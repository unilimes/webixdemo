#ifndef UTILS_H
#define UTILS_H

#include <QtGlobal>
#include <QObject>
#include <QVariant>

class Utils : public QObject
{
    Q_OBJECT
public:
    Utils();
    ~Utils();
    Q_INVOKABLE QVariant version();

    Q_INVOKABLE void testcall();
    Q_INVOKABLE void testcall2();
    Q_INVOKABLE QVariant testCall3(qint64 loop);

    Q_INVOKABLE QVariant screenInfo();

    Q_INVOKABLE void logMessage(const QString &msg);
    Q_INVOKABLE void showMessage(const QString &title, const QString &msg, bool modal);
    Q_INVOKABLE void exitApp();

protected:
    static QVariant testCall3SubRoutine(qint64 loop);
};

#endif // UTILS_H
