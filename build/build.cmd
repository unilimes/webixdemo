CD %~dp0..\gui
CALL npm run build
CD %~dp0
SET "VSCMD_START_DIR=%CD%"
CALL "G:\program\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" amd64
SET _ROOT=G:\program\Qt
SET PATH=%_ROOT%\Tools\QtCreator\bin;%_ROOT%\5.10.0\msvc2017_64\bin;%PATH%
CALL jom.exe distclean
CALL qmake.exe ..\W7designer.pro -spec win32-msvc "CONFIG+=debug" "CONFIG+=qml_debug"
CALL jom.exe all
@rem CALL jom.exe -f .\Makefile.Release
@rem CALL windeployqt.exe --qmldir ..\qml .\debug\W7designer.exe --debug --dir .\debug\deploy
@rem CALL .\debug\W7designer.exe